
/*===================================================================
	File: TiledStaggeredIsometricMap.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifdef BASE_SUPPORT_BOX2D

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "PhysicsBase.h"
#include "ScriptBase.h"

#include "Support/Tiled/TiledStaggeredIsometricMap.h"

using support::tiled::TiledStaggeredIsometricMap;

namespace
{

}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
TiledStaggeredIsometricMap::TiledStaggeredIsometricMap()
{
	m_ShaderProg = renderer::INVALID_OBJECT;

	m_nVertexAttribLocation = -1;
	m_nTexCoordsAttribLocation = -1;
	//m_nColourAttribLocation = -1;
	m_nTexSamplerUniform = -1;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
TiledStaggeredIsometricMap::~TiledStaggeredIsometricMap()
{
	renderer::RemoveShaderProgram( m_ShaderProg );
	m_ShaderProg = renderer::INVALID_OBJECT;
}

/////////////////////////////////////////////////////
/// Method: DrawLayers
/// Params: [in]startLayer, [in]endLayer
///
/////////////////////////////////////////////////////
void TiledStaggeredIsometricMap::DrawLayers( int startLayer, int endLayer )
{
	int i=0;

	int x=0, y=0;
	GLuint currentTexture = renderer::INVALID_OBJECT;

	if( startLayer < 0 )
		startLayer = 0;

	if( (endLayer > m_NumLayers-1) ||
		(endLayer == -1) )
		endLayer = m_NumLayers-1;

	for( i=startLayer; i <= endLayer; ++i )
	{
		if( m_Layers[i].data != 0 )
		{
			for( y=0; y < m_Layers[i].height; y++ )
			{
				for( x=0; x < m_Layers[i].width; x++ )
				{
					int tileId = m_Layers[i].data[x + (y*m_Layers[i].width)].tileId;
					TilesetHandler::AnimationData* pAnimData = m_Layers[i].data[x + (y*m_Layers[i].width)].anim;
					bool enabled = m_Layers[i].data[x + (y*m_Layers[i].width)].enabled;
					glm::ivec2 center = m_Layers[i].data[x + (y*m_Layers[i].width)].tileCenter;

					if( !enabled || tileId == 0 )
						continue;

					if( pAnimData != 0 )
					{
						if( pAnimData->textureId != renderer::INVALID_OBJECT &&
							currentTexture != pAnimData->textureId )
						{
							renderer::OpenGL::GetInstance()->BindTexture( pAnimData->textureId );
							currentTexture = pAnimData->textureId;
						}

						int frameIndex = pAnimData->currentFrame;
						glm::vec2 tileSize = pAnimData->pTileFrames[frameIndex].scaledTileSize;
						glm::vec2 tileUV = pAnimData->uvWidthHeight;

						// so starting from top left in the map/layer
						float posX = static_cast<float>(center.x) - pAnimData->pTileFrames[frameIndex].scaledHalfTileSize.x;
						float posY = -(static_cast<float>(center.y) + pAnimData->pTileFrames[frameIndex].scaledHalfTileSize.y);

						float uvCoordX = pAnimData->pTileFrames[frameIndex].uvCoord.s;
						float uvCoordY = pAnimData->pTileFrames[frameIndex].uvCoord.t;

						float verts[] =
						{
							posX, posY, 0.0f,
							posX+tileSize.x, posY, 0.0f,
							posX, posY+tileSize.y, 0.0f,
							posX+tileSize.x, posY+tileSize.y, 0.0f,
						};

						float uvCoords[] =
						{
							uvCoordX, uvCoordY,
							uvCoordX+tileUV.x, uvCoordY,
							uvCoordX, uvCoordY+tileUV.y,
							uvCoordX+tileUV.x, uvCoordY+tileUV.y,
						};

						glVertexAttribPointer( m_nVertexAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, verts );
						glVertexAttribPointer( m_nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, uvCoords );

						glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
					}
					else
					{
						Tileset* pTileset = GetTilesetForId( tileId );
						if( pTileset != 0 )
						{
							if( pTileset->tilesetTexture != renderer::INVALID_OBJECT &&
								currentTexture != pTileset->tilesetTexture )
							{
								renderer::OpenGL::GetInstance()->BindTexture( pTileset->tilesetTexture );
								currentTexture = pTileset->tilesetTexture;
							}

							// because tilesets have an offset let's remove it to get a lookup id in a single set
							int realId = (tileId - pTileset->firstgid);

							// get the id as an x/y index into the tile grid
							int offsetY = realId / pTileset->totalTilesX;
							int offsetX = pTileset->totalTilesX - ((pTileset->totalTilesX * (offsetY+1)) - realId);

							// get the pixel (from top left)
							int pixelStartX = (pTileset->tileWidth * offsetX) + (pTileset->spacing * (offsetX+1));
							int pixelStartY = (pTileset->tileHeight * offsetY) + (pTileset->spacing * (offsetY+1));

							float uvCoordX = renderer::GetUVCoord( pixelStartX, pTileset->imageWidth );
							float uvCoordY = renderer::GetUVCoord( (pTileset->imageHeight - pixelStartY) - pTileset->tileHeight, pTileset->imageHeight ); // bottom left for OGL

							// so starting from top left in the map/layer
							float posX = static_cast<float>(center.x) - pTileset->scaledHalfTileSize.x;
							float posY = -(static_cast<float>(center.y) + pTileset->scaledHalfTileSize.y);

							float verts[] =
							{
								posX, posY, 0.0f,
								posX+pTileset->scaledTileSize.x, posY, 0.0f,
								posX, posY+pTileset->scaledTileSize.y, 0.0f,
								posX+pTileset->scaledTileSize.x, posY+pTileset->scaledTileSize.y, 0.0f,
							};

							float uvCoords[] =
							{
								uvCoordX, uvCoordY,
								uvCoordX+pTileset->uvTileWidth, uvCoordY,
								uvCoordX, uvCoordY+pTileset->uvTileHeight,
								uvCoordX+pTileset->uvTileWidth, uvCoordY+pTileset->uvTileHeight,
							};

							glVertexAttribPointer( m_nVertexAttribLocation, 3, GL_FLOAT, GL_FALSE, 0, verts );
							glVertexAttribPointer( m_nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, 0, uvCoords );

							glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
						}
					}
				}
			}
		}
	}
}

#endif // BASE_SUPPORT_BOX2D
