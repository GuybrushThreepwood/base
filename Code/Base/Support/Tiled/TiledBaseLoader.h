
/*===================================================================
	File: TiledBaseLoader.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDBASELOADER_H__
#define __TILEDBASELOADER_H__

#ifdef BASE_SUPPORT_BOX2D

#include "PhysicsBase.h"
#include "Support/Tiled/TiledCommon.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		class TiledBaseLoader : public physics::PhysicsIdentifier
		{
		public:
			enum MapOrientation
			{
				MapOrientation_Orthogonal = 0,
				MapOrientation_Isometric,
				MapOrientation_StaggeredIsometric,
			};

		public:
			TiledBaseLoader();
			virtual ~TiledBaseLoader();

			void LoadLevel(const char* szFilename);
			virtual void DrawLayers(int startLayer = -1, int endLayer = -1);
			void Update(float deltaTime);

			MapOrientation GetMapOrientation()						{ return m_MapOrientation; }

			int GetMapTileWidth()									{ return m_MapTileWidth; }
			int GetMapTileHeight()									{ return m_MapTileHeight; }

			int GetMapWidth()										{ return m_MapWidth; }
			int GetMapHeight()										{ return m_MapHeight; }

			glm::vec2& GetScaleFactor()							{ return m_TileScale; }

			void GetTileIndexFromPosition(const glm::vec3& pos, int* tileX, int* tileY);
			TileData* GetTileDataFromPosition(int whichLayer, const glm::vec3& pos);
			TileData** GetTileIdInLayer(int whichLayer, int realId, int* numFound);
			Tileset* GetTilesetForId(int tileId);

		protected:
			PropertyValue* ParseValuesFromString(const char* propertyString, int *numValues);

		private:
			void ParseMapProperties();

			void ParseTilesets();
			void ParseTilesetProperties(int tileSetIndex);
			void ParseTilesetTilePropertiesAndAnimation(int tileSetIndex);

			void ParseLayers();
			void ParseLayerProperties(int layerIndex);
			void ParseLayerData(int layerIndex);
			void ParseLayerObjectData(int layerIndex);
			void ParseLayerObjectDataProperties(int layerIndex, int objectLayerIndex);
			void ParseLayerObjectDataPolygon(int layerIndex, int objectLayerIndex, bool polyLine);

			void ProcessLevel();

			void CreateCollisionData(int numValues, PropertyValue* layerValues);
			void CreateCollisionFromObjectLayer(int layerIndex);

			void CreateCollisionFromObjectLayerOrthogonal(int layerIndex);
			void CreateCollisionFromObjectLayerIsometric(int layerIndex);

			void LoadAnimations(int numValues, PropertyValue* animationValues);

			void CreateB2DLine(float startX, float startY, float endX, float endY);
			void CreateB2DBox(float x, float y, float w, float h, float angle);
			void CreateB2DCircle(float x, float y, float r);

			TilesetHandler* GetTileset(const char* imageName, const glm::ivec2& tileSize, int spacing, int margin);
			TilesetHandler::AnimationData* GetTileAnimation(int animationId);

		protected:
			file::TFile m_FilePath;
			glm::vec2 m_TileScale;

			int m_MapWidth;
			int m_MapHeight;
			int m_MapTileWidth;
			int m_MapTileHeight;

			glm::vec2 m_MapTileSize;
			glm::vec2 m_MapHalfTileSize;

			glm::vec2 m_ScaledMapTileSize;
			glm::vec2 m_ScaledMapHalfTileSize;

			MapOrientation m_MapOrientation;

			int m_NumMapProperties;
			Property* m_MapProperties;

			int m_NumTilesets;
			Tileset* m_Tilesets;

			int m_NumLayers;
			Layer* m_Layers;

			b2Body* m_pBody;
			float m_Box2DScaleFactor;

			std::vector<AnimationFiles*> m_AnimationFiles;
			std::vector<TilesetHandler::AnimationData*> m_TileAnimations;
		};
	}
}
#endif // BASE_SUPPORT_BOX2D
#endif // __TILEDBASELOADER_H__

