
/*===================================================================
	File: BaseScriptSupport.cpp
	Library: Script

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "RenderBase.h"
#include "ScriptBase.h"

#ifdef BASE_SUPPORT_SCRIPTING

/////////////////////////////////////////////////////
/// Function: ScriptLoadScript
/// Params: [in]pState
///
/////////////////////////////////////////////////////
int ScriptLoadScript( lua_State* pState )
{
#ifdef _DEBUG
	CHECK_LUASTATE()
#endif // _DEBUG

	script::LoadScript( lua_tostring( pState, 1 ) );

	return 0;
}

#endif // BASE_SUPPORT_SCRIPTING