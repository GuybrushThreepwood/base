
#ifndef __ASSERTION_H__
#define __ASSERTION_H__

#ifndef __COREDEFINES_H__
	#include "Core/CoreDefines.h"
#endif // __COREDEFINES_H__

#ifdef _DEBUG
	#define ASSERTS_ENABLED
#endif // _DEBUG

namespace dbg
{
	int ReportAssert( const char* condition, const char* file, int line, const char* description, ... );
}

#ifdef BASE_PLATFORM_WINDOWS
	#define DBG_HALT() __debugbreak()
#endif // BASE_PLATFORM_WINDOWS

#if defined(BASE_PLATFORM_iOS) || defined(BASE_PLATFORM_RASPBERRYPI) || defined(BASE_PLATFORM_MAC)
	#define DBG_HALT() kill( getpid(), SIGINT )
#endif // defined(BASE_PLATFORM_iOS) || defined(BASE_PLATFORM_RASPBERRYPI) || defined(BASE_PLATFORM_MAC)

#define ASSERT_UNUSED(x) do { (void)sizeof(x); } while(0)

#ifdef ASSERTS_ENABLED
	#define DBG_ASSERT(cond) \
		do \
		{ \
			if (!(cond)) \
			{ \
				if (dbg::ReportAssert(#cond, __FILE__, __LINE__, 0) ) \
					DBG_HALT(); \
			} \
		} while(0)

	#define DBG_ASSERT_MSG(cond, msg, ...) \
		do \
		{ \
			if (!(cond)) \
			{ \
				if (dbg::ReportAssert(#cond, __FILE__, __LINE__, msg, ##__VA_ARGS__) ) \
					DBG_HALT(); \
			} \
		} while(0)

	//#define DBG_ASSERT_FAIL(msg, ...) \
	//	do \
	//	{ \
	//		if (dbg::ReportAssert(0, __FILE__, __LINE__, (msg), __VA_ARGS__) ) \
	//		DBG_HALT(); \
	//	} while(0)

	//#define DBG_VERIFY(cond) DBG_ASSERT(cond)
	//#define DBG_VERIFY_MSG(cond, msg, ...) DBG_ASSERT_MSG(cond, msg, ##__VA_ARGS__)
#else

	#define DBG_ASSERT(condition) \
		do { ASSERT_UNUSED(condition); } while(0)

	#define DBG_ASSERT_MSG(condition, msg, ...) \
		do { ASSERT_UNUSED(condition); ASSERT_UNUSED(msg); } while(0)

	#define DBG_ASSERT_FAIL(msg, ...) \
		do { ASSERT_UNUSED(msg); } while(0)

	#define DBG_VERIFY(cond) (void)(cond)

	#define DBG_VERIFY_MSG(cond, msg, ...) \
		do { (void)(cond); ASSERT_UNUSED(msg); } while(0)

#endif // ASSERTS_ENABLED

#endif // __ASSERTION_H__

