
#ifndef __PLATFORMBASE_H__
#define __PLATFORMBASE_H__

// ######## MOST IMPORTANT ########

// BASE_PLATFORM_WINDOWS		// - add at preprocessor level - Windows platform
// BASE_PLATFORM_iOS			// - add at preprocessor level - iOS platform
// BASE_PLATFORM_MAC			// - add at preprocessor level - Mac OS X platform
// BASE_NO_PNG					// - add at preprocessor level - disable any png library support

// ################################

// BASE_USE_DEBUG_FONT			// - allow the debug font outside of a debug build

// image format support
// BASE_SUPPORT_PNG				// support PNG image file loading
// BASE_SUPPORT_DDS				// support DDS image file loading

// sound format support
// BASE_NO_OPENAL				// - add at preprocessor level - disable OpenAL library loading
// BASE_SUPPORT_OPENAL          // normal OpenAL library support
// BASE_SUPPORT_OGG				// - add at preprocessor level - support for OGG vorbis container using libvorbis and libogg

// lua script support
// BASE_SUPPORT_SCRIPTING		// - add at preprocessor level - support for LUA scripting

// physis support
// BASE_SUPPORT_BOX2D			// - add at preprocessor level - supports for Box2D library
// BASE_SUPPORT_BULLET			// - add at preprocessor level - support for Bullet library

// freetype library support
// BASE_SUPPORT_FREETYPE		// - add at preprocessor level - support for Freetype library

// utility
// BASE_MEMORY_CHECKS			// - add at preprocessor level - support for memory leak checks

//////////////////////////////////////////////////////////////////////////

#ifdef BASE_PLATFORM_WINDOWS
	#ifndef BASE_NO_OPENAL	
		#define BASE_SUPPORT_OPENAL
	#endif

	#ifndef BASE_NO_PNG
		#define BASE_SUPPORT_PNG
	#endif // 
#endif // BASE_PLATFORM_WINDOWS

#endif // __PLATFORMBASE_H__