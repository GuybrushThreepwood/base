
/*===================================================================
	File: TiledLevelStaggeredIsometric.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifdef BASE_SUPPORT_BOX2D

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "PhysicsBase.h"
#include "ScriptBase.h"

#include "Support/Tiled/TiledLevelStaggeredIsometric.h"

using support::tiled::TiledLevelStaggeredIsometric;

namespace
{

}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
TiledLevelStaggeredIsometric::TiledLevelStaggeredIsometric()
{
	SetBaseId(PHYSICSBASICID_WORLD);
	SetCastingId(PHYSICSCASTID_WORLD);

	m_ShaderProg = renderer::LoadShaderFilesForProgram( "shaders/tile_render.vert", "shaders/tile_render.frag" );
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
TiledLevelStaggeredIsometric::~TiledLevelStaggeredIsometric()
{

}

/////////////////////////////////////////////////////
/// Method: DrawLayers
/// Params:
///
/////////////////////////////////////////////////////
void TiledLevelStaggeredIsometric::DrawLayers( int startLayer, int endLayer )
{
	//if( startLayer == endLayer )
	//	return;

	GLuint currentProg = renderer::OpenGL::GetInstance()->GetCurrentProgram();
	renderer::OpenGL::GetInstance()->UseProgram( m_ShaderProg );

	m_nVertexAttribLocation = glGetAttribLocation( m_ShaderProg, "base_v" );
	m_nTexCoordsAttribLocation = glGetAttribLocation( m_ShaderProg, "base_uv0" );

	m_nTexSamplerUniform = glGetUniformLocation( m_ShaderProg, "texUnit0" ); 
	if( m_nTexSamplerUniform != -1 )
		glUniform1i( m_nTexSamplerUniform, 0 );

	glm::mat4 projMatrix = renderer::OpenGL::GetInstance()->GetProjectionMatrix();
	glm::mat4 modelMatrix = renderer::OpenGL::GetInstance()->GetModelMatrix();
	glm::mat4 viewMatrix = renderer::OpenGL::GetInstance()->GetViewMatrix();
	glm::mat4 modelViewMatrix = viewMatrix*modelMatrix;

	// gl_ModelViewProjectionMatrix
	GLint ogl_ModelViewProjectionMatrix = glGetUniformLocation(m_ShaderProg, "ogl_ModelViewProjectionMatrix");
	if( ogl_ModelViewProjectionMatrix != -1 )
		glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMatrix*modelViewMatrix) );

	glEnableVertexAttribArray( m_nVertexAttribLocation );
	glEnableVertexAttribArray( m_nTexCoordsAttribLocation );

	TiledStaggeredIsometricMap::DrawLayers( startLayer, endLayer );

	glDisableVertexAttribArray( m_nVertexAttribLocation );
	glDisableVertexAttribArray( m_nTexCoordsAttribLocation );

	renderer::OpenGL::GetInstance()->UseProgram( currentProg );
}

#endif // BASE_SUPPORT_BOX2D