
/*===================================================================
	File: Box2dTestState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __BOX2DTESTSTATE_H__
#define __BOX2DTESTSTATE_H__

#include "ScriptAccess/ScriptDataHolder.h"

// forward declare
class IState;
class IBaseGameState;

class Box2DTestState : public IBaseGameState
{
	public:
		Box2DTestState( StateManager& stateManager );
		virtual ~Box2DTestState();

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();

		void SpawnType();

	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;
};

#endif // __BOX2DTESTSTATE_H__

