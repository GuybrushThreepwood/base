
/*===================================================================
	File: Box2DTestState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "RenderBase.h"
#include "PhysicsBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "GameStates/IBaseGameState.h"
#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/UI/UIIds.h"
//#include "GameStates/UI/FrontendTitleScreenUI.h"

#include "GameStates/Examples/Box2DTestState.h"

namespace
{

	const int32 k_maxBodies = 256;

	int32 bodyIndex;
	b2Body* bodies[k_maxBodies];
	b2PolygonShape polygons[4];
	b2CircleShape circle;

	const char* description = "This is an example of a Box2D physics setup\n\n- Press space right click or second touch to add a random physics object to the scene";
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
Box2DTestState::Box2DTestState( StateManager& stateManager )
: IBaseGameState( stateManager, ID_BOX2DEXAMPLESTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
Box2DTestState::~Box2DTestState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void Box2DTestState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

	renderer::OpenGL::GetInstance()->ClearColour( 0.5f, 0.5f, 0.5f, 1.0f );

	glm::vec3 camPos = glm::vec3(0, 0, 20);
	glm::vec3 camTarget = glm::vec3(0, 0, -20);
	m_GameCamera->SetLerp(true);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );

	// load from script

	b2Vec2 gravity(0.0f,-10.0f);
	b2Vec2 areaMin( -1000.0f, -1000.0f );
	b2Vec2 areaMax(1000.0f, 1000.0f);

	physics::PhysicsWorldB2D::Create(areaMin,areaMax,gravity,true);

	// Ground body
	{
		b2BodyDef bd;
		b2Body* ground = physics::PhysicsWorldB2D::GetWorld()->CreateBody(&bd);

		b2EdgeShape shape;
		shape.Set(b2Vec2(-40.0f, 0.0f), b2Vec2(40.0f, 0.0f));
		ground->CreateFixture(&shape, 0.0f);
	}

	{
		b2Vec2 vertices[3];
		vertices[0].Set(-0.5f, 0.0f);
		vertices[1].Set(0.5f, 0.0f);
		vertices[2].Set(0.0f, 1.5f);
		polygons[0].Set(vertices, 3);
	}
		
	{
		b2Vec2 vertices[3];
		vertices[0].Set(-0.1f, 0.0f);
		vertices[1].Set(0.1f, 0.0f);
		vertices[2].Set(0.0f, 1.5f);
		polygons[1].Set(vertices, 3);
	}

	{
		float32 w = 1.0f;
		float32 b = w / (2.0f + b2Sqrt(2.0f));
		float32 s = b2Sqrt(2.0f) * b;

		b2Vec2 vertices[8];
		vertices[0].Set(0.5f * s, 0.0f);
		vertices[1].Set(0.5f * w, b);
		vertices[2].Set(0.5f * w, b + s);
		vertices[3].Set(0.5f * s, w);
		vertices[4].Set(-0.5f * s, w);
		vertices[5].Set(-0.5f * w, b + s);
		vertices[6].Set(-0.5f * w, b);
		vertices[7].Set(-0.5f * s, 0.0f);

		polygons[2].Set(vertices, 8);
	}

	{
		polygons[3].SetAsBox(0.5f, 0.5f);
	}

	{
		circle.m_radius = 0.5f;
	}

	bodyIndex = 0;
	memset(bodies, 0, sizeof(bodies));

	// set the description
	/*IState* pCurrState = m_pStateManager->GetSecondaryStateManager()->GetCurrentState();
	if( pCurrState->GetId() == UI_TITLESCREEN )
	{
		FrontendTitleScreenUI* pState = static_cast<FrontendTitleScreenUI*>( pCurrState );
		if( pState != 0 )
		{
			pState->SetDescriptionString( description );
		}
	}*/
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void Box2DTestState::Exit()
{
	m_GameCamera = 0;
	
	physics::PhysicsWorldB2D::Destroy();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int Box2DTestState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int Box2DTestState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void Box2DTestState::Update( float deltaTime )
{
	const int PHYSICS_VEL_ITERATIONS = 8;
	const int PHYSICS_POS_ITERATIONS = 3;

	m_LastDelta = deltaTime;

	physics::PhysicsWorldB2D::GetWorld()->Step( PHYSICS_TIMESTEP, PHYSICS_VEL_ITERATIONS, PHYSICS_POS_ITERATIONS );

	m_GameCamera->Update( deltaTime );
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void Box2DTestState::Draw()
{
	// default colour
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	if( m_DevData.allowDebugCam && gDebugCamera.IsEnabled() )
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 0.5f, 100000.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView(core::app::GetFrameWidth(), core::app::GetFrameHeight(), true);

		renderer::OpenGL::GetInstance()->SetLookAt( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z, 
												gDebugCamera.GetTarget().x, gDebugCamera.GetTarget().y, gDebugCamera.GetTarget().z );
	
		snd::SoundManager::GetInstance()->SetListenerPosition( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z );
		snd::SoundManager::GetInstance()->SetListenerOrientation((float)std::sin(glm::radians(-gDebugCamera.GetAngle())), 0.0f, (float)std::cos(glm::radians(-gDebugCamera.GetAngle())),
																	0.0f, 1.0f, 0.0f );
	}
	else
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 1.0f, 100.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		m_GameCamera->SetupCamera();

		snd::SoundManager::GetInstance()->SetListenerOrientation(std::sin(glm::radians(180.0f)), 0.0f, std::cos(glm::radians(180.0f)), 0.0f, 1.0f, 0.0f);
	}
	
	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LESS );
	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->DisableTexturing();
	renderer::OpenGL::GetInstance()->DisableLighting();

	physics::PhysicsWorldB2D::DrawDebugData();

	renderer::OpenGL::GetInstance()->EnableTexturing();
	renderer::OpenGL::GetInstance()->SetNearFarClip( NEAR_CLIP_ORTHO, FAR_CLIP_ORTHO );
	renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );
	renderer::OpenGL::GetInstance()->DisableLighting();

	GL_CHECK;
		
	int offset = 10;
	int xPos;
	int yPos;

	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;

	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	DBGPRINT( xPos, yPos-=offset, "DELTA (%.6f)", m_LastDelta );
	DBGPRINT( xPos, yPos-=offset, "CAM POS (%.2f  %.2f  %.2f)", m_GameCamera->GetPosition().x, m_GameCamera->GetPosition().y, m_GameCamera->GetPosition().z );
	DBGPRINT( xPos, yPos-=offset, "CAM TARGET (%.2f  %.2f  %.2f)", m_GameCamera->GetTarget().x, m_GameCamera->GetTarget().y, m_GameCamera->GetTarget().z );
	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

/////////////////////////////////////////////////////
/// Method: SpawnType
/// Params: None
///
/////////////////////////////////////////////////////
void Box2DTestState::SpawnType()
{
	int index = rand()%4;

	if (bodies[bodyIndex] != 0)
	{
		physics::PhysicsWorldB2D::GetWorld()->DestroyBody(bodies[bodyIndex]);
		bodies[bodyIndex] = 0;
	}

	b2BodyDef bd;
	bd.type = b2_dynamicBody;

	float32 x = -2.0f;//glm::linearRand(-2.0f, 2.0f);
	bd.position.Set(x, 10.0f);
	bd.angle = b2_pi;//glm::linearRand(-b2_pi, b2_pi);

	if (index == 4)
	{
		bd.angularDamping = 0.02f;
	}

	bodies[bodyIndex] = physics::PhysicsWorldB2D::GetWorld()->CreateBody(&bd);

	if (index < 4)
	{
		b2FixtureDef fd;
		fd.shape = polygons + index;
		fd.density = 1.0f;
		fd.friction = 0.3f;
		bodies[bodyIndex]->CreateFixture(&fd);
	}
	else
	{
		b2FixtureDef fd;
		fd.shape = &circle;
		fd.density = 1.0f;
		fd.friction = 0.3f;

		bodies[bodyIndex]->CreateFixture(&fd);
	}

	bodyIndex = (bodyIndex + 1) % k_maxBodies;
}
