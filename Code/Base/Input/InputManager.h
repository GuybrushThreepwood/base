
#ifndef __INPUTMANAGER_H__
#define __INPUTMANAGER_H__

#ifndef __DEBUGBASE_H__
	#include "DebugBase.h"
#endif // __DEBUGBASE_H__

#include "Input/InputDeviceController.h"

namespace input
{
	class InputManager
	{
		public:
			class InputEventCallback
			{
				public:
					InputEventCallback() {}
					virtual ~InputEventCallback() {}

					virtual void DeviceAdded(const input::InputDeviceController *pDevice) = 0;
					virtual void DeviceRemoved(const input::InputDeviceController *pDevice) = 0;

					virtual void InputControllerButtonDown(const InputDeviceController *pDevice, int buttonIndex) = 0;
					virtual void InputControllerButtonUp(const InputDeviceController *pDevice, int buttonIndex) = 0;

					virtual void InputControllerAxisMotion(const InputDeviceController *pDevice, int axisIndex, signed short value) = 0;
                
					virtual void InputControllerHatMotion(const InputDeviceController *pDevice, unsigned char value) = 0;
			};

		public:
			InputManager();
			~InputManager();

			/// Initialise - Sets up just the default values
			void Init(void);

			/// cleanup
			/// Release - Clears out all allocated data by the class
			void Release(void);

			static void Initialise(void);
			static void Shutdown(void);

			/// Update -
			void Update( float deltaTime );

			/// AddDevice -
			void AddDevice(int whichId);
			/// RemoveDevice -
			void RemoveDevice(SDL_JoystickID instanceID);

			/// AddDeviceEvent -
			void AddDeviceEvent(int whichId);
			/// RemoveDeviceEvent -
			void RemoveDeviceEvent(SDL_JoystickID instanceID);
			/// ButtonEvent
			void ButtonEvent(int instanceID, int buttonIndex, bool state);
			/// AxisEvent
			void AxisEvent(int instanceID, int axisIndex, signed short value);
            /// HatEvent
            void HatEvent(int instanceID, unsigned char value);

			/// DeviceStillExists -
			bool DeviceStillExists(InputDeviceController *pDevice);

			static InputManager *GetInstance(void)
			{
				DBG_ASSERT((ms_Instance != nullptr));

				return ms_Instance.get();
			}
			static bool IsInitialised(void)
			{
				return(ms_Instance != nullptr);
			}

			/// GetDeviceList - get the current device list
			std::vector<InputDeviceController *> GetDeviceList()						{ return m_DeviceList; }
            /// AddCallback -
			void AddCallback(input::InputManager::InputEventCallback* pCallback)		
			{
				for (auto c : m_EventCallbacks)
				{
					if (c == pCallback)
					{
						return;
					}
				}
				m_EventCallbacks.push_back(pCallback);  
			}
			void RemoveCallback(input::InputManager::InputEventCallback* pCallback)		
			{
				int i = 0;
				for (auto c : m_EventCallbacks)
				{
					if (c == pCallback)
					{
						m_EventCallbacks.erase(m_EventCallbacks.begin()+i);
						return;
					}

					i++;
				}
			}

			bool BindCheck(int btnBind, InputDeviceController *pPreferredDevice=0);

		private:
			/// AddJoystick -
			void AddJoystick(SDL_JoystickID instanceID, SDL_Joystick* pJoy, SDL_Haptic* pHaptic);

			/// AddGamepad -
			void AddGamepad(SDL_JoystickID instanceID, SDL_GameController* pController, SDL_Haptic* pHaptic);

			input::InputDeviceController* FindDevice(int instanceID);

		private:
			//static InputManager* ms_Instance;
			static std::unique_ptr<InputManager> ms_Instance;

			std::vector<InputEventCallback*> m_EventCallbacks;

			std::vector<InputDeviceController *> m_DeviceList;

			bool m_IsBindScanning;
	};

} // namespace input

#endif // __INPUTMANAGER_H__

