
#ifndef __WININCLUDES_H__
#define __WININCLUDES_H__

#define WIN32_LEAN_AND_MEAN // Windows specific includes and defines
#define NOMINMAX // disable macro min/max

#define _CRT_SECURE_NO_DEPRECATE // don't care about secure c functions
#define _CRT_NONSTDC_NO_DEPRECATE // don't complain about non std c
#define _CRT_SECURE_NO_WARNINGS // no warnings

#endif // __WININCLUDES_H__