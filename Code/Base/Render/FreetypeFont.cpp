
/*===================================================================
	File: FreetypeFontGL.cpp
	Library: Render

	(C)Hidden Games
=====================================================================*/

#ifdef BASE_SUPPORT_FREETYPE

#include "CoreBase.h"

#include "Render/OpenGLCommon.h"
#include "Render/glewES.h"
#include "Render/OpenGL.h"
#include "Render/TextureShared.h"
#include "Render/Texture.h"
#include "Render/TextureLoadAndUpload.h"
#include "Render/ShaderShared.h"
#include "Render/Primitives.h"

#include "Render/FreetypeCommon.h"
#include "Render/FreetypeFont.h"

using renderer::FreetypeFont;

namespace
{
#ifdef BASE_PLATFORM_iOS
	const int ITALIC_EXTRA_W = 0;
#else
    const int ITALIC_EXTRA_W = 6;
#endif
    
	const int ATLAS_DEPTH = 1;

	const int ATLAS_STARTWIDTH = 512;
	const int ATLAS_STARTHEIGHT = 512;

	//const int ATLAS_MAXWIDTH = 1024;

	const int NUM_CHARACTERS_PER_BATCH = 150;

	GLuint freeTypeRenderProgram = renderer::INVALID_OBJECT;
	GLuint freeTypeRenderBatchProgram = renderer::INVALID_OBJECT;
	//GLuint freeTypeUnderlineTexture = renderer::INVALID_OBJECT;
	
	const char FreetypeVertexShader[] = 
	"	//_FREETYPE_ precision highp float;\n\
		attribute vec2 base_v;\n\
		attribute vec2 base_uv0;\n\
		varying vec2 tu0;\n\
		varying vec4 colour0;\n\
		uniform mat4 ogl_ModelViewProjectionMatrix;\n\
		uniform vec4 ogl_VertexColour;\n\
		void main()\n\
		{\n\
			tu0 = base_uv0;\n\
			colour0 = ogl_VertexColour;\n\
			vec4 vInVertex = ogl_ModelViewProjectionMatrix * vec4(vec2(base_v.xy), 0.0, 1.0);\n\
			gl_Position = vInVertex;\n\
		}\n\
	";
	const char FreetypeFragmentShader[] = 
	"	//_FREETYPE_ precision highp float;\n\
		uniform sampler2D texUnit0;\n\
		varying vec2 tu0;\n\
		varying vec4 colour0;\n\
		void main()\n\
		{\n\
			vec4 color = colour0 * texture2D(texUnit0, tu0.xy).a;\n\
			gl_FragColor = color;\n\
		}\n\
	";
	
	const char FreetypeBatchVertexShader[] = 
	"	//_FREETYPEBATCH_ precision highp float;\n\
		attribute vec2 base_v;\n\
		attribute vec2 base_uv0;\n\
		attribute vec4 base_col;\n\
		varying vec2 tu0;\n\
		varying vec4 colour0;\n\
		uniform mat4 ogl_ModelViewProjectionMatrix;\n\
		uniform vec4 ogl_VertexColour;\n\
		void main()\n\
		{\n\
			tu0 = base_uv0;\n\
			colour0 = base_col;\n\
			vec4 vInVertex = ogl_ModelViewProjectionMatrix * vec4(vec2(base_v.xy), 0.0, 1.0);\n\
			gl_Position = vInVertex;\n\
		}\n\
	";
	const char FreetypeBatchFragmentShader[] = 
	"	//_FREETYPEBATCH_ precision highp float;\n\
		uniform sampler2D texUnit0;\n\
		varying vec2 tu0;\n\
		varying vec4 colour0;\n\
		void main()\n\
		{\n\
			vec4 color = colour0 * texture2D(texUnit0, tu0.xy).a;\n\
			gl_FragColor = color;\n\
		}\n\
	";
	const int UNDERLINE_TEX_WIDTH = 4;
	const int UNDERLINE_TEX_HEIGHT = 4;
	unsigned char underlineTex[UNDERLINE_TEX_WIDTH * UNDERLINE_TEX_HEIGHT * 1] = {
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
		0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff
	};

	unsigned short FontStyleCombinations[FONT_STYLE_TOTAL] =
	{
		0,
		FONT_FLAG_ITALIC,
		FONT_FLAG_BOLD,
		(FONT_FLAG_ITALIC | FONT_FLAG_BOLD),
	};
}

/////////////////////////////////////////////////////
/// Method: InitialiseFreetype
/// Params: None
///
/////////////////////////////////////////////////////
void renderer::InitialiseFreetype( renderer::OpenGL* openGLContext )
{
	if( openGLContext != 0 )
	{
		ShutdownFreetype();

		freeTypeRenderProgram = renderer::INVALID_OBJECT;//renderer::LoadShaderStringsForProgram( FreetypeVertexShader, FreetypeFragmentShader );
		freeTypeRenderBatchProgram = renderer::INVALID_OBJECT;//renderer::LoadShaderStringsForProgram(FreetypeBatchVertexShader, FreetypeBatchFragmentShader);
	}
}

/////////////////////////////////////////////////////
/// Method: ShutdownFreetype
/// Params: None
///
/////////////////////////////////////////////////////
void renderer::ShutdownFreetype()
{
	if( freeTypeRenderProgram != renderer::INVALID_OBJECT )
	{
		renderer::RemoveShaderProgram( freeTypeRenderProgram );
		freeTypeRenderProgram = renderer::INVALID_OBJECT;
	}

	if (freeTypeRenderBatchProgram != renderer::INVALID_OBJECT)
	{
		renderer::RemoveShaderProgram(freeTypeRenderBatchProgram);
		freeTypeRenderBatchProgram = renderer::INVALID_OBJECT;
	}
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
FreetypeFont::FreetypeFont( renderer::OpenGL* openGLContext )
{
	if( openGLContext == 0 )
		m_OpenGLContext = renderer::OpenGL::GetInstance();
	else
		m_OpenGLContext = openGLContext;

	if(  freeTypeRenderProgram == renderer::INVALID_OBJECT ||
		freeTypeRenderBatchProgram == renderer::INVALID_OBJECT )
		renderer::InitialiseFreetype( m_OpenGLContext );

	Initialise();
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
FreetypeFont::~FreetypeFont()
{
	Release();
}

/////////////////////////////////////////////////////
/// Method: Initialise
/// Params: None
///
/////////////////////////////////////////////////////
void FreetypeFont::Initialise( void )
{
	std::memset( &m_FontFile, 0, sizeof(file::TFile) );
	m_FixedWidth = false;
	m_vDimensions = glm::vec2(10.0f, 10.0f);

	m_UseBlockColour = true;
	m_vBlockColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);

	m_UseBackgroundColour = false;
	m_vBGColour = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
 
	m_vTopColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	m_vBottomColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	m_vScale = glm::vec2(1.0f, 1.0f);

    m_BatchExists = false;
	m_RenderBatches = 0;
	m_TotalRenderBatches = 0;
}

/////////////////////////////////////////////////////
/// Method: Release
/// Params: None
///
/////////////////////////////////////////////////////
void FreetypeFont::Release( void )
{
	int i=0;
	
	if (m_RenderBatches != 0)
	{
		for (i = 0; i < m_TotalRenderBatches; ++i)
		{
            if (m_RenderBatches[i].batchBackground != 0)
            {
                delete[] m_RenderBatches[i].batchBackground;
                m_RenderBatches[i].batchBackground = 0;
            }
            
            if (m_RenderBatches[i].batchForeground != 0)
            {
                delete[] m_RenderBatches[i].batchForeground;
                m_RenderBatches[i].batchForeground = 0;
            }
		}

		delete[] m_RenderBatches;
		m_RenderBatches = 0;
	}
	m_TotalRenderBatches = 0;
    m_BatchExists = false;
    
	m_AtlasTexture.Destroy();
}

/////////////////////////////////////////////////////
/// Method: Load
/// Params: [in]szFilename, [in]pData, [in]dataSize [in]vDims, [in]bDropShadow, [in]vTopColour, [in]vBottomColour
///
/////////////////////////////////////////////////////
int FreetypeFont::Load(const char *szFilename, void *pData, std::size_t dataSize, const glm::vec2 &vDims, const glm::vec4 &vTopColour, const glm::vec4 &vBottomColour, bool fixedWidth, unsigned short fontStyleSupport, int maxAtlasSize)
{
	int i=0;

	m_vDimensions = vDims;
	m_vBlockColour = vTopColour;
	m_FixedWidth = fixedWidth;

	// colour store
	m_vTopColour = vTopColour;
	m_vBottomColour = vBottomColour;

	// Create and initilize a freetype font library.
	FT_Library library;
	if( FT_Init_FreeType( &library ) ) 
	{
		DBGLOG( "FREETYPEFONT: *ERROR* FT_Init_FreeType failed" );
		return 1;
	}

	// The object in which Freetype holds information on a given font is called a "face".
	FT_Face face;

	CreateFileStructure( szFilename, &m_FontFile );

	// This is where we load in the font information from the file. Of all the places where the code might die, this is the most likely,
	// as FT_New_Face will die if the font file does not exist or is somehow broken.
	
	if (szFilename != 0)
	{
		if (FT_New_Face(library, szFilename, 0, &face))
		{
			DBGLOG("FREETYPEFONT: *ERROR* FT_New_Face failed\n");
			FT_Done_FreeType(library);
			return 1;
		}
	}
	else
	{
		if (FT_New_Memory_Face(library, static_cast<const FT_Byte*>(pData), static_cast<FT_Long>(dataSize), 0, &face))
		{
			DBGLOG("FREETYPEFONT: *ERROR* FT_New_Face failed\n");
			FT_Done_FreeType(library);
			return 1;
		}
	}

	if (FT_Select_Charmap(face, FT_ENCODING_UNICODE))
	{
		DBGLOG("FREETYPEFONT: *ERROR* FT_Select_Charmap failed\n");

		FT_Done_Face(face);
		FT_Done_FreeType(library);

		return 1;
	}

	// For some twisted reason, Freetype measures font size in terms of 1/64ths of pixels.  Thus, to make a font
	// nFontHeight pixels high, we need to request a size of nFontHeight*64. (nFontHeight << 6 is just a prettier way of writting nFontHeight*64)
	//FT_Set_Char_Size( face, (int)m_vDimensions.fWidth << 6, (int)m_vDimensions.fHeight << 6, FREETYPEFONT_DPI, FREETYPEFONT_DPI ); // 72dpi
	FT_Set_Pixel_Sizes( face, static_cast<int>(m_vDimensions.x), static_cast<int>(m_vDimensions.y) );

    m_MaxAtlasSize = maxAtlasSize;
    
	int atlasWidth = ATLAS_STARTWIDTH;
	int atlasHeight = ATLAS_STARTHEIGHT;
	int result = FindBestAtlasSize(library, face, fontStyleSupport, &atlasWidth, &atlasHeight);

	if (result == -1)
		DBGLOG("FREETYPEFONT: *WARNING* Atlas is too big some characters may not appear for %s\n", szFilename );

	// now the real atlas size can be created
	m_AtlasTexture.Create( m_OpenGLContext, atlasWidth, atlasHeight, ATLAS_DEPTH);

	glm::ivec4 underLineRegion = m_AtlasTexture.GetRegion(UNDERLINE_TEX_WIDTH + 1, UNDERLINE_TEX_HEIGHT + 1);
	if (underLineRegion.x < 0)
		DBGLOG("Texture atlas is full\n");
	else
		m_AtlasTexture.SetRegion(underLineRegion.x, underLineRegion.y, UNDERLINE_TEX_WIDTH, UNDERLINE_TEX_HEIGHT, underlineTex, UNDERLINE_TEX_WIDTH);
	glm::vec4 underlineData = glm::vec4(static_cast<float>(underLineRegion.x + 1), static_cast<float>(underLineRegion.y + 1), static_cast<float>(UNDERLINE_TEX_WIDTH - 2), static_cast<float>(UNDERLINE_TEX_HEIGHT - 2));

	for( i = 0; i < FONT_STYLE_TOTAL; ++i )
	{
		unsigned short flags = FontStyleCombinations[i];
		
		if( fontStyleSupport < i )
			continue;
		
		// This is where we actually create each of the fonts display lists.
		for(unsigned char j = 0; j < FREETYPEFONT_CHARACTERS; j++ )
		{
			// The first thing we do is get FreeType to render our character
			// into a bitmap.  This actually requires a couple of FreeType commands:

			FT_Int32 filterFlags = 0;
			filterFlags = FT_LOAD_DEFAULT | FT_LOAD_IGNORE_GLOBAL_ADVANCE_WIDTH | FT_LOAD_LINEAR_DESIGN;

			if (ATLAS_DEPTH == 3)
			{
				unsigned char lcd_weights[5];

				// FT_LCD_FILTER_LIGHT   is (0x00, 0x55, 0x56, 0x55, 0x00)
				// FT_LCD_FILTER_DEFAULT is (0x10, 0x40, 0x70, 0x40, 0x10)
				lcd_weights[0] = 0x10;
				lcd_weights[1] = 0x40;
				lcd_weights[2] = 0x70;
				lcd_weights[3] = 0x40;
				lcd_weights[4] = 0x10;

				FT_Library_SetLcdFilter(library, FT_LCD_FILTER_LIGHT);
				filterFlags |= FT_LOAD_TARGET_LCD;
				if (1)
				{
					FT_Library_SetLcdFilterWeights(library, lcd_weights);
				}
			}
			else
				filterFlags |= FT_LOAD_TARGET_MONO;

			// Load the Glyph for our character.
			if (FT_Load_Glyph(face, FT_Get_Char_Index(face, j), filterFlags))
			{
				DBGLOG( "FREETYPEFONT: *ERROR* FT_Load_Glyph failed" );
				return 1;
			}
			
			// Move the face's glyph into a Glyph object.
			FT_Glyph glyph;
			FT_GlyphSlot slot = face->glyph;
			
			if( flags & FONT_FLAG_ITALIC )
				FT_GlyphSlot_Oblique( slot );
			
			if( flags & FONT_FLAG_BOLD )
				FT_GlyphSlot_Embolden( slot );
			
			if( FT_Get_Glyph( face->glyph, &glyph ) )
			{
				DBGLOG( "FREETYPEFONT: *ERROR* FT_Get_Glyph failed\n" );
				return 1;
			}
			
			// Convert the glyph to a bitmap.
			if (ATLAS_DEPTH == 1)
				FT_Glyph_To_Bitmap( &glyph, FT_RENDER_MODE_NORMAL, 0, 1 );
			else
				FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_LCD, 0, 1);

			FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;

			// This reference will make accessing the bitmap easier
			FT_Bitmap &bitmap = bitmap_glyph->bitmap;

			// We want each glyph to be separated by at least one black pixel
			int bmp_w = bitmap.width / ATLAS_DEPTH + 1;
			int bmp_h = bitmap.rows + 1;
			int bmp_x = 0;
			int bmp_y = 0;

			if (bmp_w > 1 &&
				bmp_h > 1)
			{
				glm::ivec4 region = m_AtlasTexture.GetRegion(bmp_w, bmp_h);
				if (region.x < 0)
				{
					//missed++;
					DBGLOG("Texture atlas is full\n");

					// clear the glyph memory
					FT_Done_Glyph(glyph);

					continue;
				}
				bmp_w = bmp_w - 1;
				bmp_h = bmp_h - 1;
				bmp_x = region.x;
				bmp_y = region.y;

				m_AtlasTexture.SetRegion(bmp_x, bmp_y, bmp_w, bmp_h, bitmap.buffer, bitmap.pitch);
			}

			// save the glyph width, to use for space counting
			if( flags & FONT_FLAG_ITALIC )
				m_GlyphWidths[i][j] = (face->glyph->advance.x >> 6) + (ITALIC_EXTRA_W );
			else
				m_GlyphWidths[i][j] = face->glyph->advance.x >> 6;

			// create triangle strip
			// 2 ---- 3
			// |\     |
			// |  \   |
			// |    \ |
			// 0 ---- 1
			if( m_FixedWidth )
				GlyphQuad[i][j].Offsets[0] = glm::vec3(static_cast<float>(bitmap_glyph->left) + m_vDimensions.x*0.25f, 0.0f, 0.0f);
			else
				GlyphQuad[i][j].Offsets[0] = glm::vec3(static_cast<float>(bitmap_glyph->left), 0.0f, 0.0f);
			GlyphQuad[i][j].Offsets[1] = glm::vec3(0.0f, static_cast<float>(bitmap_glyph->top) - static_cast<float>(bitmap.rows), 0.0f);

			GlyphQuad[i][j].ShadowOffset = glm::vec3(m_vDimensions.x / FONT_DROP_SHADOW_SHIFT, -m_vDimensions.y / FONT_DROP_SHADOW_SHIFT, 0.0f);

			float UNDERLINE_HEIGHT = m_vDimensions.y*0.075f;
			float UNDERLINE_INC_W = m_vDimensions.x*0.055f;

			float BG_INC_H = m_vDimensions.y*0.025f;
			float BG_INC_W = m_vDimensions.x*0.015f;

			glm::vec4 charData = glm::vec4(static_cast<float>(bmp_x), static_cast<float>(bmp_y), static_cast<float>(bitmap.width), static_cast<float>(bitmap.rows));
			float glw = static_cast<float>(m_GlyphWidths[i][j]);

			GlyphQuad[i][j].textUV[0] = glm::vec2(charData.x / static_cast<float>(atlasWidth), (charData.y + charData.w) / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textPoint[0] = glm::vec2(0.0f, 0.0f);
			GlyphQuad[i][j].textColour[0] = vBottomColour;
			GlyphQuad[i][j].textShadowColour[0] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			GlyphQuad[i][j].textUnderline[0] = glm::vec2(-UNDERLINE_INC_W, 0.0f);
			GlyphQuad[i][j].textUnderlineUV[0] = glm::vec2(underlineData.x / static_cast<float>(atlasWidth), (underlineData.y + underlineData.w) / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textBGBox[0] = glm::vec2(-(BG_INC_W), -BG_INC_H);
			GlyphQuad[i][j].fillUV[0] = glm::vec2(0.0f, 0.0f);

			GlyphQuad[i][j].textUV[1] = glm::vec2((charData.x + charData.z) / static_cast<float>(atlasWidth), (charData.y + charData.w) / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textPoint[1] = glm::vec2(charData.z, 0.0f);
			GlyphQuad[i][j].textColour[1] = vBottomColour;
			GlyphQuad[i][j].textShadowColour[1] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			GlyphQuad[i][j].textUnderline[1] = glm::vec2(glw + UNDERLINE_INC_W, 0.0f);
			GlyphQuad[i][j].textUnderlineUV[1] = glm::vec2((underlineData.x + underlineData.z) / static_cast<float>(atlasWidth), (underlineData.y + underlineData.w) / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textBGBox[1] = glm::vec2(glw + BG_INC_W, -BG_INC_H);
			GlyphQuad[i][j].fillUV[1] = glm::vec2(1.0f, 0.0f);

			GlyphQuad[i][j].textUV[2] = glm::vec2(charData.x / static_cast<float>(atlasWidth), charData.y / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textPoint[2] = glm::vec2(0.0f, charData.w);
			GlyphQuad[i][j].textColour[2] = vTopColour;
			GlyphQuad[i][j].textShadowColour[2] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			GlyphQuad[i][j].textUnderline[2] = glm::vec2(-UNDERLINE_INC_W, UNDERLINE_HEIGHT);
			GlyphQuad[i][j].textUnderlineUV[2] = glm::vec2(underlineData.x / static_cast<float>(atlasWidth), underlineData.y / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textBGBox[2] = glm::vec2(-(BG_INC_W), (m_vDimensions.y*0.95f));
			GlyphQuad[i][j].fillUV[2] = glm::vec2(0.0f, 1.0f);

			GlyphQuad[i][j].textUV[3] = glm::vec2((charData.x + charData.z) / static_cast<float>(atlasWidth), charData.y / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textPoint[3] = glm::vec2(charData.z, charData.w);
			GlyphQuad[i][j].textColour[3] = vTopColour;
			GlyphQuad[i][j].textShadowColour[3] = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
			GlyphQuad[i][j].textUnderline[3] = glm::vec2(glw + UNDERLINE_INC_W, UNDERLINE_HEIGHT);
			GlyphQuad[i][j].textUnderlineUV[3] = glm::vec2((underlineData.x + underlineData.z) / static_cast<float>(atlasWidth), underlineData.y / static_cast<float>(atlasHeight));
			GlyphQuad[i][j].textBGBox[3] = glm::vec2(glw + BG_INC_W, (m_vDimensions.y*0.95f));
			GlyphQuad[i][j].fillUV[3] = glm::vec2(1.0f, 1.0f);

			if( m_FixedWidth )
				GlyphQuad[i][j].Offsets[2] = glm::vec3(m_vDimensions.x*0.75f, 0.0f, 0.0f);
			else
				GlyphQuad[i][j].Offsets[2] = glm::vec3(glw, 0.0f, 0.0f);

			// clear the glyph memory
			FT_Done_Glyph( glyph );
		}
	}

	//char imageFileCheck[core::MAX_PATH + core::MAX_PATH];
	//file::TFile fileStruct;
	//file::CreateFileStructure(szFilename, &fileStruct);
	//snprintf(imageFileCheck, core::MAX_PATH + core::MAX_PATH, "%s%d.png", fileStruct.szFile, (int)m_vDimensions.Width );
	//m_AtlasTexture.WriteToFile(imageFileCheck);

	// We don't need the face information now that the display lists have been created, so we free the associated resources.
	FT_Done_Face(face);

	// Ditto for the library.
	FT_Done_FreeType(library);

	return 0;
}

/////////////////////////////////////////////////////
/// Method: ClearBatches
/// Params: None
///
/////////////////////////////////////////////////////
void FreetypeFont::ClearBatches()
{
    unsigned int i = 0;
    
    if (m_RenderBatches != 0)
    {
        for (i = 0; i < static_cast<unsigned int>(m_TotalRenderBatches); ++i)
        {
            if (m_RenderBatches[i].batchBackground != 0)
            {
                delete[] m_RenderBatches[i].batchBackground;
                m_RenderBatches[i].batchBackground = 0;
            }
            
            if (m_RenderBatches[i].batchForeground != 0)
            {
                delete[] m_RenderBatches[i].batchForeground;
                m_RenderBatches[i].batchForeground = 0;
            }
        }
        
        delete[] m_RenderBatches;
        m_RenderBatches = 0;
    }
    m_TotalRenderBatches = 0;
    
    m_BatchExists = false;
}

/////////////////////////////////////////////////////
/// Method: BuildBufferCache
/// Params: [in]szFilename, [in]pData, [in]dataSize [in]vDims, [in]bDropShadow, [in]vTopColour, [in]vBottomColour
///
/////////////////////////////////////////////////////
void FreetypeFont::BuildBufferCache(int x, int y, bool dropShadow, std::vector<TFormatChangeBlock>& formatChanges, std::string& fmtStr, ...)
{
	int style = FONT_STYLE_NORMAL;
	unsigned int i = 0, j = 0, k = 0;
	std::string s, ss;
	int n, size = 100;
	bool b = false;
	va_list marker;

	if (freeTypeRenderBatchProgram == renderer::INVALID_OBJECT ||
		freeTypeRenderBatchProgram == 0)
		return;

	if (fmtStr.length() == 0)
		return;

	while (!b)
	{
		ss.resize(size);
		va_start(marker, fmtStr);
		n = vsnprintf((char*)ss.c_str(), size, fmtStr.c_str(), marker);
		va_end(marker);
		if ((n>0) && ((b = (n<size)) == true))
			ss.resize(n);
		else
			size *= 2;
	}
	s += ss;

	glm::mat4 modelMat = m_OpenGLContext->GetModelMatrix();

	float xOffsetTotal = 0.0f;

	// initial block
	TFormatChangeBlock block;
	block.style = FONT_STYLE_NORMAL;
	block.numCharsInBlockText = static_cast<int>(s.length());
	block.textColour = m_vBlockColour;
	block.underline = false;
	block.bgColour = false;
	block.textBGColour = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

	bool hasChanges = false;
	bool doUnderline = false;
	bool doBackground = false;

	int currentChangeIndex = 0;

	if (formatChanges.size() != 0)
	{
		hasChanges = true;

		style = formatChanges[currentChangeIndex].style;
		block.numCharsInBlockText = formatChanges[currentChangeIndex].numCharsInBlockText;
		m_vBlockColour = formatChanges[currentChangeIndex].textColour;
		doUnderline = formatChanges[currentChangeIndex].underline;
		m_vBGColour = formatChanges[currentChangeIndex].textBGColour;
		doBackground = formatChanges[currentChangeIndex].bgColour;
	}

	int currentCharChangeOffset = 0;

	// set the start point
	glm::mat4 offsetMatrix = glm::translate(modelMat, glm::vec3(static_cast<GLfloat>(x), static_cast<GLfloat>(y), 0.0f));

	if (s.length() == 0)
		return;

	if (m_RenderBatches != 0)
	{
		for (i = 0; i < static_cast<unsigned int>(m_TotalRenderBatches); ++i)
		{
            if (m_RenderBatches[i].batchBackground != 0)
            {
                delete[] m_RenderBatches[i].batchBackground;
                m_RenderBatches[i].batchBackground = 0;
            }
            
			if (m_RenderBatches[i].batchForeground != 0)
			{
				delete[] m_RenderBatches[i].batchForeground;
				m_RenderBatches[i].batchForeground = 0;
			}
		}

		delete[] m_RenderBatches;
		m_RenderBatches = 0;
	}
	m_TotalRenderBatches = 0;

	m_TotalRenderBatches = ((int)s.length() / NUM_CHARACTERS_PER_BATCH) + 1;

	m_RenderBatches = new FreetypeRenderBatch[m_TotalRenderBatches];
	DBG_ASSERT(m_RenderBatches != 0);

	// init all data inside
	for (i = 0; i < static_cast<unsigned int>(m_TotalRenderBatches); ++i)
	{
		FreetypeRenderBatch* pCurrentBatch = &m_RenderBatches[i];
		
		pCurrentBatch->numberOfCharacters = 0;
		pCurrentBatch->bufferAABB.Reset();
		pCurrentBatch->bufferAABB.vBoxMax.z = 0.0f;
		pCurrentBatch->bufferAABB.vBoxMin.z = 0.0f;

        pCurrentBatch->batchBackground = new GlyphVert[(2*6)*NUM_CHARACTERS_PER_BATCH]; // 2 triangles with 6 vertices and 2 versions for bg, shadow
        DBG_ASSERT(pCurrentBatch->batchBackground != 0);

		pCurrentBatch->batchForeground = new GlyphVert[(2*6)*NUM_CHARACTERS_PER_BATCH]; // 2 triangles with 6 vertices and 2 versions for glyph and underline
		DBG_ASSERT(pCurrentBatch->batchForeground != 0);

		int triOffset = 0;

		for (j = 0; j < NUM_CHARACTERS_PER_BATCH; ++j)
		{
			for (k = 0; k < 2; ++k) // 2 versions for bg, shadow
			{
				// vert init
				// glyph quad = 2 tris = 6 verts
				pCurrentBatch->batchBackground[triOffset].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 1].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 2].v = glm::vec2(0.0f, 0.0f);

				pCurrentBatch->batchBackground[triOffset + 3].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 4].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 5].v = glm::vec2(0.0f, 0.0f);
				///

				// texture coords
				pCurrentBatch->batchBackground[triOffset].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 1].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 2].uv = glm::vec2(0.0f, 0.0f);

				pCurrentBatch->batchBackground[triOffset + 3].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 4].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 5].uv = glm::vec2(0.0f, 0.0f);

				// colour init
				pCurrentBatch->batchBackground[triOffset].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 1].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 2].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

				pCurrentBatch->batchBackground[triOffset + 3].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 4].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchBackground[triOffset + 5].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
                
                
                // glyph quad = 2 tris = 6 verts
				pCurrentBatch->batchForeground[triOffset].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 1].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 2].v = glm::vec2(0.0f, 0.0f);
                
				pCurrentBatch->batchForeground[triOffset + 3].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 4].v = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 5].v = glm::vec2(0.0f, 0.0f);
                ///
                
                // texture coords
				pCurrentBatch->batchForeground[triOffset].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 1].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 2].uv = glm::vec2(0.0f, 0.0f);
                
				pCurrentBatch->batchForeground[triOffset + 3].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 4].uv = glm::vec2(0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 5].uv = glm::vec2(0.0f, 0.0f);
                
                // colour init
				pCurrentBatch->batchForeground[triOffset].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 1].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 2].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
                
				pCurrentBatch->batchForeground[triOffset + 3].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 4].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
				pCurrentBatch->batchForeground[triOffset + 5].col = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);

				triOffset += 6;
			}
		}

		//DBGLOG("Size = %d\n", triOffset);
	}

	// set to first batch
	int currentBatchIndex = 0;
	int currentBatchCharacter = 0;
	FreetypeRenderBatch* pCurrentBatch = &m_RenderBatches[currentBatchIndex];

	int bgOffset = 0;
    int fgOffset = 0;
    
	for (i = 0; i < s.length(); ++i)
    {
		if (currentBatchCharacter >= NUM_CHARACTERS_PER_BATCH)
		{
            pCurrentBatch->numberOfCharacters = currentBatchCharacter;
            
			currentBatchCharacter = 0;
			currentBatchIndex++;

			bgOffset = 0;
            fgOffset = 0;
			pCurrentBatch = &m_RenderBatches[currentBatchIndex];
		}

		if (hasChanges)
		{
			if (currentCharChangeOffset >= block.numCharsInBlockText)
			{
				currentChangeIndex++;

				if (currentChangeIndex < static_cast<int>(formatChanges.size()))
				{
					style = formatChanges[currentChangeIndex].style;
					block.numCharsInBlockText = formatChanges[currentChangeIndex].numCharsInBlockText;
					m_vBlockColour = formatChanges[currentChangeIndex].textColour;
					doUnderline = formatChanges[currentChangeIndex].underline;
					m_vBGColour = formatChanges[currentChangeIndex].textBGColour;
					doBackground = formatChanges[currentChangeIndex].bgColour;

					currentCharChangeOffset = 0;
				}
			}
		}

		if (s[i] == '\n')
		{
			// need to handle this so the AABB keeps the right size

			offsetMatrix = glm::translate(offsetMatrix, glm::vec3(-xOffsetTotal, -m_vDimensions.y, 0.0f));
			xOffsetTotal = 0.0f;
            
            currentCharChangeOffset++;
            
			continue;
		}

		currentCharChangeOffset++;
		unsigned char index = s[i];

		float alpha = m_vBGColour.a;
		if (!doBackground)
			alpha = 0.0f;
		else
		{
			alpha = m_vBGColour.a;
		}

		///// background

		glm::mat4 textMatrix = glm::translate(offsetMatrix, glm::vec3(0.0f, -m_vDimensions.y*0.15f, 0.0f));
		glm::vec2 m = glm::vec2(textMatrix[3][0], textMatrix[3][1]);

		// verts
		// bottom, left, bottom right, top left
		pCurrentBatch->batchBackground[bgOffset].v = glm::vec2(GlyphQuad[style][index].textBGBox[0].x + m.x, GlyphQuad[style][index].textBGBox[0].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset].v);
		pCurrentBatch->batchBackground[bgOffset + 1].v = glm::vec2(GlyphQuad[style][index].textBGBox[1].x + m.x, GlyphQuad[style][index].textBGBox[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+1].v);
		pCurrentBatch->batchBackground[bgOffset + 2].v = glm::vec2(GlyphQuad[style][index].textBGBox[2].x + m.x, GlyphQuad[style][index].textBGBox[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+2].v);

		// bottom right, top right, top left
		pCurrentBatch->batchBackground[bgOffset + 3].v = glm::vec2(GlyphQuad[style][index].textBGBox[1].x + m.x, GlyphQuad[style][index].textBGBox[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset + 3].v);
		pCurrentBatch->batchBackground[bgOffset + 4].v = glm::vec2(GlyphQuad[style][index].textBGBox[3].x + m.x, GlyphQuad[style][index].textBGBox[3].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset + 4].v);
		pCurrentBatch->batchBackground[bgOffset + 5].v = glm::vec2(GlyphQuad[style][index].textBGBox[2].x + m.x, GlyphQuad[style][index].textBGBox[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset + 5].v);

		// texture coords
		// bottom, left, bottom right, top left
		pCurrentBatch->batchBackground[bgOffset].uv = GlyphQuad[style][index].textUnderlineUV[0];
		pCurrentBatch->batchBackground[bgOffset + 1].uv = GlyphQuad[style][index].textUnderlineUV[1];
		pCurrentBatch->batchBackground[bgOffset + 2].uv = GlyphQuad[style][index].textUnderlineUV[2];

		// bottom right, top right, top left
		pCurrentBatch->batchBackground[bgOffset + 3].uv = GlyphQuad[style][index].textUnderlineUV[1];
		pCurrentBatch->batchBackground[bgOffset + 4].uv = GlyphQuad[style][index].textUnderlineUV[3];
		pCurrentBatch->batchBackground[bgOffset + 5].uv = GlyphQuad[style][index].textUnderlineUV[2];

		// colour init

		// bottom, left, bottom right, top left
		pCurrentBatch->batchBackground[bgOffset].col = glm::vec4(m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, alpha);
		pCurrentBatch->batchBackground[bgOffset + 1].col = glm::vec4(m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, alpha);
		pCurrentBatch->batchBackground[bgOffset + 2].col = glm::vec4(m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, alpha);

		// bottom right, top right, top left
		pCurrentBatch->batchBackground[bgOffset + 3].col = glm::vec4(m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, alpha);
		pCurrentBatch->batchBackground[bgOffset + 4].col = glm::vec4(m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, alpha);
		pCurrentBatch->batchBackground[bgOffset + 5].col = glm::vec4(m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, alpha);

		bgOffset += 6;

		///// drop shadow 

		alpha = m_vBlockColour.a;
		if (!dropShadow)
			alpha = 0;

		glm::mat4 shadowMatrix = glm::mat4(1.0f);
		shadowMatrix = glm::translate(offsetMatrix, glm::vec3(GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z));
		shadowMatrix = glm::translate(shadowMatrix, glm::vec3(GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z));
		shadowMatrix = glm::translate(shadowMatrix, glm::vec3(GlyphQuad[style][index].ShadowOffset.x, GlyphQuad[style][index].ShadowOffset.y, GlyphQuad[style][index].ShadowOffset.z));
		m = glm::vec2(shadowMatrix[3][0], shadowMatrix[3][1]);

		// verts
		// bottom, left, bottom right, top left
		pCurrentBatch->batchBackground[bgOffset].v = glm::vec2(GlyphQuad[style][index].textPoint[0].x + m.x, GlyphQuad[style][index].textPoint[0].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset].v);
		pCurrentBatch->batchBackground[bgOffset + 1].v = glm::vec2(GlyphQuad[style][index].textPoint[1].x + m.x, GlyphQuad[style][index].textPoint[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+1].v);
		pCurrentBatch->batchBackground[bgOffset + 2].v = glm::vec2(GlyphQuad[style][index].textPoint[2].x + m.x, GlyphQuad[style][index].textPoint[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+2].v);

		// bottom right, top right, top left
		pCurrentBatch->batchBackground[bgOffset + 3].v = glm::vec2(GlyphQuad[style][index].textPoint[1].x + m.x, GlyphQuad[style][index].textPoint[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+3].v);
		pCurrentBatch->batchBackground[bgOffset + 4].v = glm::vec2(GlyphQuad[style][index].textPoint[3].x + m.x, GlyphQuad[style][index].textPoint[3].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+4].v);
		pCurrentBatch->batchBackground[bgOffset + 5].v = glm::vec2(GlyphQuad[style][index].textPoint[2].x + m.x, GlyphQuad[style][index].textPoint[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchBackground[bgOffset+5].v);

		// texture coords
		// bottom, left, bottom right, top left
		pCurrentBatch->batchBackground[bgOffset].uv = GlyphQuad[style][index].textUV[0];
		pCurrentBatch->batchBackground[bgOffset + 1].uv = GlyphQuad[style][index].textUV[1];
		pCurrentBatch->batchBackground[bgOffset + 2].uv = GlyphQuad[style][index].textUV[2];

		// bottom right, top right, top left
		pCurrentBatch->batchBackground[bgOffset + 3].uv = GlyphQuad[style][index].textUV[1];
		pCurrentBatch->batchBackground[bgOffset + 4].uv = GlyphQuad[style][index].textUV[3];
		pCurrentBatch->batchBackground[bgOffset + 5].uv = GlyphQuad[style][index].textUV[2];

		// colour is ok
		// bottom, left, bottom right, top left
		pCurrentBatch->batchBackground[bgOffset].col = glm::vec4(0.0f, 0.0f, 0.0f, alpha);
		pCurrentBatch->batchBackground[bgOffset + 1].col = glm::vec4(0.0f, 0.0f, 0.0f, alpha);
		pCurrentBatch->batchBackground[bgOffset + 2].col = glm::vec4(0.0f, 0.0f, 0.0f, alpha);

		// bottom right, top right, top left
		pCurrentBatch->batchBackground[bgOffset + 3].col = glm::vec4(0.0f, 0.0f, 0.0f, alpha);
		pCurrentBatch->batchBackground[bgOffset + 4].col = glm::vec4(0.0f, 0.0f, 0.0f, alpha);
		pCurrentBatch->batchBackground[bgOffset + 5].col = glm::vec4(0.0f, 0.0f, 0.0f, alpha);

		bgOffset += 6;

		///// glyph 
		
		textMatrix = glm::translate(offsetMatrix, glm::vec3(GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z));
		textMatrix = glm::translate(textMatrix, glm::vec3(GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z));
		m = glm::vec2(textMatrix[3][0], textMatrix[3][1]);

		// verts
		// bottom, left, bottom right, top left
		pCurrentBatch->batchForeground[fgOffset].v = glm::vec2(GlyphQuad[style][index].textPoint[0].x + m.x, GlyphQuad[style][index].textPoint[0].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset].v);
		pCurrentBatch->batchForeground[fgOffset + 1].v = glm::vec2(GlyphQuad[style][index].textPoint[1].x + m.x, GlyphQuad[style][index].textPoint[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+1].v);
		pCurrentBatch->batchForeground[fgOffset + 2].v = glm::vec2(GlyphQuad[style][index].textPoint[2].x + m.x, GlyphQuad[style][index].textPoint[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+2].v);

		// bottom right, top right, top left
		pCurrentBatch->batchForeground[fgOffset + 3].v = glm::vec2(GlyphQuad[style][index].textPoint[1].x + m.x, GlyphQuad[style][index].textPoint[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+3].v);
		pCurrentBatch->batchForeground[fgOffset + 4].v = glm::vec2(GlyphQuad[style][index].textPoint[3].x + m.x, GlyphQuad[style][index].textPoint[3].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+4].v);
		pCurrentBatch->batchForeground[fgOffset + 5].v = glm::vec2(GlyphQuad[style][index].textPoint[2].x + m.x, GlyphQuad[style][index].textPoint[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+5].v);

		// texture coords
		// bottom, left, bottom right, top left
		pCurrentBatch->batchForeground[fgOffset].uv = GlyphQuad[style][index].textUV[0];
		pCurrentBatch->batchForeground[fgOffset + 1].uv = GlyphQuad[style][index].textUV[1];
		pCurrentBatch->batchForeground[fgOffset + 2].uv = GlyphQuad[style][index].textUV[2];

		// bottom right, top right, top left
		pCurrentBatch->batchForeground[fgOffset + 3].uv = GlyphQuad[style][index].textUV[1];
		pCurrentBatch->batchForeground[fgOffset + 4].uv = GlyphQuad[style][index].textUV[3];
		pCurrentBatch->batchForeground[fgOffset + 5].uv = GlyphQuad[style][index].textUV[2];

		// colour init

		// bottom, left, bottom right, top left
		pCurrentBatch->batchForeground[fgOffset].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a);
		pCurrentBatch->batchForeground[fgOffset + 1].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a);
		pCurrentBatch->batchForeground[fgOffset + 2].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a);

		// bottom right, top right, top left
		pCurrentBatch->batchForeground[fgOffset + 3].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a);
		pCurrentBatch->batchForeground[fgOffset + 4].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a);
		pCurrentBatch->batchForeground[fgOffset + 5].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a);

		fgOffset += 6;

		///// underline 

		alpha = m_vBlockColour.a;
		if (!doUnderline)
			alpha = 0.0f;

		textMatrix = glm::translate(offsetMatrix, glm::vec3(0.0f, -m_vDimensions.y*0.15f, 0.0f));
		m = glm::vec2(textMatrix[3][0], textMatrix[3][1]);

		// verts
		// bottom, left, bottom right, top left
		pCurrentBatch->batchForeground[fgOffset].v = glm::vec2(GlyphQuad[style][index].textUnderline[0].x + m.x, GlyphQuad[style][index].textUnderline[0].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset].v);
		pCurrentBatch->batchForeground[fgOffset + 1].v = glm::vec2(GlyphQuad[style][index].textUnderline[1].x + m.x, GlyphQuad[style][index].textUnderline[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+1].v);
		pCurrentBatch->batchForeground[fgOffset + 2].v = glm::vec2(GlyphQuad[style][index].textUnderline[2].x + m.x, GlyphQuad[style][index].textUnderline[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+2].v);

		// bottom right, top right, top left
		pCurrentBatch->batchForeground[fgOffset + 3].v = glm::vec2(GlyphQuad[style][index].textUnderline[1].x + m.x, GlyphQuad[style][index].textUnderline[1].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+3].v);
		pCurrentBatch->batchForeground[fgOffset + 4].v = glm::vec2(GlyphQuad[style][index].textUnderline[3].x + m.x, GlyphQuad[style][index].textUnderline[3].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+4].v);
		pCurrentBatch->batchForeground[fgOffset + 5].v = glm::vec2(GlyphQuad[style][index].textUnderline[2].x + m.x, GlyphQuad[style][index].textUnderline[2].y + m.y);
		pCurrentBatch->bufferAABB.ReCalculate(pCurrentBatch->batchForeground[fgOffset+5].v);

		// texture coords
		// bottom, left, bottom right, top left
		pCurrentBatch->batchForeground[fgOffset].uv = GlyphQuad[style][index].textUnderlineUV[0];
		pCurrentBatch->batchForeground[fgOffset + 1].uv = GlyphQuad[style][index].textUnderlineUV[1];
		pCurrentBatch->batchForeground[fgOffset + 2].uv = GlyphQuad[style][index].textUnderlineUV[2];

		// bottom right, top right, top left
		pCurrentBatch->batchForeground[fgOffset + 3].uv = GlyphQuad[style][index].textUnderlineUV[1];
		pCurrentBatch->batchForeground[fgOffset + 4].uv = GlyphQuad[style][index].textUnderlineUV[3];
		pCurrentBatch->batchForeground[fgOffset + 5].uv = GlyphQuad[style][index].textUnderlineUV[2];

		// colour init

		// bottom, left, bottom right, top left
		pCurrentBatch->batchForeground[fgOffset].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, alpha);
		pCurrentBatch->batchForeground[fgOffset + 1].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, alpha);
		pCurrentBatch->batchForeground[fgOffset + 2].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, alpha);

		// bottom right, top right, top left
		pCurrentBatch->batchForeground[fgOffset + 3].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, alpha);
		pCurrentBatch->batchForeground[fgOffset + 4].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, alpha);
		pCurrentBatch->batchForeground[fgOffset + 5].col = glm::vec4(m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, alpha);

		fgOffset += 6;

		offsetMatrix = glm::translate(offsetMatrix, glm::vec3(GlyphQuad[style][index].Offsets[2].x, GlyphQuad[style][index].Offsets[2].y, GlyphQuad[style][index].Offsets[2].z));

		// track how far word has gone
		xOffsetTotal += GlyphQuad[style][index].Offsets[2].x;

		currentBatchCharacter++;
        
        if( i+1 == s.length() )
            pCurrentBatch->numberOfCharacters = currentBatchCharacter;
	}
    
    m_BatchExists = true;
}

/////////////////////////////////////////////////////
/// Method: RenderBatches
/// Params: [in]filter
///
/////////////////////////////////////////////////////
void FreetypeFont::RenderBatches(GLenum filter)
{
    if( !m_BatchExists )
        return;
    
	m_OpenGLContext->DisableVBO();

	GLuint prevProg = m_OpenGLContext->GetCurrentProgram();
	m_OpenGLContext->UseProgram(freeTypeRenderBatchProgram);

	// grab all matrices
	glm::mat4 projMat = m_OpenGLContext->GetProjectionMatrix();
	glm::mat4 modelMat = m_OpenGLContext->GetModelMatrix();

	GLint nVertexAttribLocation = glGetAttribLocation(freeTypeRenderBatchProgram, "base_v");
	GLint nTexCoordsAttribLocation = glGetAttribLocation(freeTypeRenderBatchProgram, "base_uv0");
	GLint nColourAttribLocation = glGetAttribLocation(freeTypeRenderBatchProgram, "base_col");

	GLint ogl_ModelViewProjectionMatrix = glGetUniformLocation(freeTypeRenderBatchProgram, "ogl_ModelViewProjectionMatrix");

	GLint nTexSamplerUniform = glGetUniformLocation(freeTypeRenderBatchProgram, "texUnit0");
	if (nTexSamplerUniform != -1)
		glUniform1i(nTexSamplerUniform, 0);

	glEnableVertexAttribArray(nVertexAttribLocation);
	glEnableVertexAttribArray(nTexCoordsAttribLocation);
	glEnableVertexAttribArray(nColourAttribLocation);

	if (ogl_ModelViewProjectionMatrix != -1)
		glUniformMatrix4fv(ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*modelMat));

	m_AtlasTexture.Bind(filter);

	m_OpenGLContext->ExtractFrustum();

	int i = 0;
	for (i = 0; i < m_TotalRenderBatches; ++i)
	{
		FreetypeRenderBatch* pCurrentBatch = &m_RenderBatches[i];

		if (pCurrentBatch->numberOfCharacters > 0)
		{
			if( m_OpenGLContext->AABBInFrustum(pCurrentBatch->bufferAABB) )
			{
                glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(GlyphVert), &pCurrentBatch->batchBackground[0].v);
                glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(GlyphVert), &pCurrentBatch->batchBackground[0].uv);
				glVertexAttribPointer(nColourAttribLocation, 4, GL_FLOAT, GL_TRUE, sizeof(GlyphVert), &pCurrentBatch->batchBackground[0].col);
                
                glDrawArrays(GL_TRIANGLES, 0, pCurrentBatch->numberOfCharacters * 12);
                
				glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(GlyphVert), &pCurrentBatch->batchForeground[0].v);
				glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(GlyphVert), &pCurrentBatch->batchForeground[0].uv);
				glVertexAttribPointer(nColourAttribLocation, 4, GL_FLOAT, GL_TRUE, sizeof(GlyphVert), &pCurrentBatch->batchForeground[0].col);

				glDrawArrays(GL_TRIANGLES, 0, pCurrentBatch->numberOfCharacters * 12);
			}
		}
	}

	if (nVertexAttribLocation != -1)
		glDisableVertexAttribArray(nVertexAttribLocation);

	if (nTexCoordsAttribLocation != -1)
		glDisableVertexAttribArray(nTexCoordsAttribLocation);

	if (nColourAttribLocation != -1)
		glDisableVertexAttribArray(nColourAttribLocation);

	m_OpenGLContext->UseProgram(prevProg);

	/*for (i = 0; i < m_TotalRenderBatches; ++i)
	{
		FreetypeRenderBatch* pCurrentBatch = &m_RenderBatches[i];

		if (pCurrentBatch->numberOfCharacters > 0)
		{
			m_OpenGLContext->SetColour4f(0, 0, 1.0f, 1.0f);
			renderer::DrawAABB(pCurrentBatch->bufferAABB.vBoxMin, pCurrentBatch->bufferAABB.vBoxMax, false);
		}
	}*/
}

/////////////////////////////////////////////////////
/// Method: Print
/// Params: [in]x, [in]y, [in]dropShadow, [in]szString, [in]optional
///
/////////////////////////////////////////////////////
void FreetypeFont::Print(int x, int y, bool dropShadow, GLenum filter, std::vector<TFormatChangeBlock>& formatChanges, std::string& fmtStr, ...)
{
	int style = FONT_STYLE_NORMAL;
	unsigned int i=0;
	std::string s, ss;
	int n, size=100;
	bool b=false;
	va_list marker;

	if( freeTypeRenderProgram == renderer::INVALID_OBJECT ||
		freeTypeRenderProgram == 0 )
		return;

	if( fmtStr.length() == 0 )
		return;

	while (!b)
	{
		ss.resize(size);
		va_start(marker, fmtStr);
			n = vsnprintf((char*)ss.c_str(), size, fmtStr.c_str(), marker);
		va_end(marker);
		if ((n>0) && ((b=(n<size))==true)) 
			ss.resize(n); 
		else 
			size*=2;
    }
    s += ss;			

	m_OpenGLContext->DisableVBO();

	GLuint prevProg = m_OpenGLContext->GetCurrentProgram();
	m_OpenGLContext->UseProgram(freeTypeRenderProgram);

	// grab all matrices
	glm::mat4 projMat = m_OpenGLContext->GetProjectionMatrix();
	glm::mat4 modelMat = m_OpenGLContext->GetModelMatrix();
	
	GLint nVertexAttribLocation = glGetAttribLocation( freeTypeRenderProgram, "base_v" );
	GLint nTexCoordsAttribLocation = glGetAttribLocation( freeTypeRenderProgram, "base_uv0" );
	GLint ogl_ModelViewProjectionMatrix = glGetUniformLocation(freeTypeRenderProgram, "ogl_ModelViewProjectionMatrix");

	GLint nTexSamplerUniform = glGetUniformLocation( freeTypeRenderProgram, "texUnit0" );
	if( nTexSamplerUniform != -1 )
		glUniform1i( nTexSamplerUniform, 0 );
	
	glEnableVertexAttribArray( nVertexAttribLocation );
	glEnableVertexAttribArray( nTexCoordsAttribLocation );

	float xOffsetTotal = 0.0f;
		
	// initial block
	TFormatChangeBlock block;
	block.style = FONT_STYLE_NORMAL;
	block.numCharsInBlockText = static_cast<int>(s.length());
	block.textColour = m_vBlockColour;
	block.underline = false;
	block.bgColour = false;
	block.textBGColour = glm::vec4(0.0f, 0.0f, 0.0f, 0.0f);
    
	bool hasChanges = false;
	bool doUnderline = false;
    bool doBackground = false;
    
	int currentChangeIndex = 0;
	
	if( formatChanges.size() != 0 )
	{
		hasChanges = true;
		
		style = formatChanges[currentChangeIndex].style;
		block.numCharsInBlockText = formatChanges[currentChangeIndex].numCharsInBlockText;
		m_vBlockColour = formatChanges[currentChangeIndex].textColour;
		doUnderline = formatChanges[currentChangeIndex].underline;
		m_vBGColour = formatChanges[currentChangeIndex].textBGColour;
		doBackground = formatChanges[currentChangeIndex].bgColour;
	}
	
	int currentCharChangeOffset = 0;
	
	glm::mat4 offsetMatrix = glm::translate( modelMat, glm::vec3( static_cast<GLfloat>(x), static_cast<GLfloat>(y), 0.0f ) );

	m_AtlasTexture.Bind(filter);

	for( i=0; i < s.length(); ++i )
	{
		if( hasChanges )
		{
			if( currentCharChangeOffset >= block.numCharsInBlockText )
			{
				currentChangeIndex++;
				
				if( currentChangeIndex < static_cast<int>( formatChanges.size() ) )
				{
					style = formatChanges[currentChangeIndex].style;
					block.numCharsInBlockText = formatChanges[currentChangeIndex].numCharsInBlockText;
					m_vBlockColour = formatChanges[currentChangeIndex].textColour;
					doUnderline = formatChanges[currentChangeIndex].underline;
					m_vBGColour = formatChanges[currentChangeIndex].textBGColour;
					doBackground = formatChanges[currentChangeIndex].bgColour;

					currentCharChangeOffset = 0;
				}
			}
		}
		
		if( s[i] == '\n' )
		{
			offsetMatrix = glm::translate( offsetMatrix, glm::vec3( -xOffsetTotal, -m_vDimensions.y, 0.0f ) );
			xOffsetTotal = 0.0f;
			continue;
		}

		currentCharChangeOffset++;
		unsigned char index = s[i];
		
		if( doBackground )
		{
			//m_OpenGLContext->BindTexture( freeTypeUnderlineTexture );

			m_OpenGLContext->SetColour( m_vBGColour.r, m_vBGColour.g, m_vBGColour.b, m_vBGColour.a );

			glm::mat4 textMatrix = glm::translate( offsetMatrix, glm::vec3( 0.0f, -m_vDimensions.y*0.15f, 0.0f ) );
			
			if( ogl_ModelViewProjectionMatrix != -1 )
				glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*textMatrix) );
			
			glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textBGBox[0]);
			glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUnderlineUV[0]);

			glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
		}

		if( dropShadow )
		{
			glm::mat4 shadowMatrix = glm::mat4(1.0f);

			shadowMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z ) );

			shadowMatrix = glm::translate( shadowMatrix, glm::vec3( GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z ) );

			shadowMatrix = glm::translate( shadowMatrix, glm::vec3( GlyphQuad[style][index].ShadowOffset.x, GlyphQuad[style][index].ShadowOffset.y, GlyphQuad[style][index].ShadowOffset.z ) );

			if( ogl_ModelViewProjectionMatrix != -1 )
				glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*shadowMatrix) );

			glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textPoint[0]);
			glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUV[0]);

			m_OpenGLContext->SetColour( 0, 0, 0, m_vBlockColour.a );

			glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
		}

		glm::mat4 textMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z ) );

		textMatrix = glm::translate( textMatrix, glm::vec3( GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z ) );

		if( ogl_ModelViewProjectionMatrix != -1 )
			glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*textMatrix) );

		glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textPoint[0]);
		glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUV[0]);

		m_OpenGLContext->SetColour( m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a );

		glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
		
		if( doUnderline )
		{
			//m_OpenGLContext->BindTexture( freeTypeUnderlineTexture );
			
			textMatrix = glm::translate( offsetMatrix, glm::vec3( 0.0f, -m_vDimensions.y*0.15f, 0.0f ) );
			
			if( ogl_ModelViewProjectionMatrix != -1 )
				glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*textMatrix) );
			
			glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUnderline[0]);
			glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUnderlineUV[0]);
			
			glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
			
		}
		
		offsetMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[2].x, GlyphQuad[style][index].Offsets[2].y, GlyphQuad[style][index].Offsets[2].z ) );

		// track how far word has gone
		xOffsetTotal += GlyphQuad[style][index].Offsets[2].x;
	}

	if( nVertexAttribLocation != -1 )
		glDisableVertexAttribArray( nVertexAttribLocation );

	if( nTexCoordsAttribLocation != -1 )
		glDisableVertexAttribArray( nTexCoordsAttribLocation );

	m_OpenGLContext->UseProgram(prevProg);
	
}

/////////////////////////////////////////////////////
/// Method: Print
/// Params: [in]x, [in]y, [in]dropShadow, [in]szString, [in]optional
///
/////////////////////////////////////////////////////
void FreetypeFont::Print(int x, int y, bool dropShadow, GLenum filter, const char *szString, ...)
{
	int style = FONT_STYLE_NORMAL;
	char szText[MAX_FREETYPE_STRINGLEN];
	std::va_list ap;				
	unsigned int i=0;

	if( szString == 0 ||
		std::strlen(szString) >= MAX_FREETYPE_STRINGLEN-1 )		
		return;				

	if( freeTypeRenderProgram == renderer::INVALID_OBJECT ||
		freeTypeRenderProgram == 0 )
		return;

	va_start( ap, szString );					
		std::vsprintf( szText, szString, ap );			
	va_end( ap );

	m_OpenGLContext->DisableVBO();

	GLuint prevProg = m_OpenGLContext->GetCurrentProgram();
	m_OpenGLContext->UseProgram(freeTypeRenderProgram);

	// grab all matrices
	glm::mat4 projMat = m_OpenGLContext->GetProjectionMatrix();
	glm::mat4 modelMat = m_OpenGLContext->GetModelMatrix();
	
	GLint nVertexAttribLocation = glGetAttribLocation( freeTypeRenderProgram, "base_v" );
	GLint nTexCoordsAttribLocation = glGetAttribLocation( freeTypeRenderProgram, "base_uv0" );
	GLint ogl_ModelViewProjectionMatrix = glGetUniformLocation(freeTypeRenderProgram, "ogl_ModelViewProjectionMatrix");

	GLint nTexSamplerUniform = glGetUniformLocation( freeTypeRenderProgram, "texUnit0" );
	if( nTexSamplerUniform != -1 )
		glUniform1i( nTexSamplerUniform, 0 );
	
	glEnableVertexAttribArray( nVertexAttribLocation );
	glEnableVertexAttribArray( nTexCoordsAttribLocation );

	float xOffsetTotal = 0.0f;
	
	glm::mat4 offsetMatrix = glm::translate( modelMat, glm::vec3( static_cast<GLfloat>(x), static_cast<GLfloat>(y), 0.0f ) );

	m_AtlasTexture.Bind(filter);

	for( i=0; i < (int)std::strlen(szText); ++i )
	{
		if( szText[i] == '\n' )
		{
			offsetMatrix = glm::translate( offsetMatrix, glm::vec3( -xOffsetTotal, -m_vDimensions.y, 0.0f ) );
			xOffsetTotal = 0.0f;
			continue;
		}

		unsigned char index = szText[i];

		if( dropShadow )
		{
			glm::mat4 shadowMatrix = glm::mat4(1.0f);

			shadowMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z ) );

			shadowMatrix = glm::translate( shadowMatrix, glm::vec3( GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z ) );

			shadowMatrix = glm::translate( shadowMatrix, glm::vec3( GlyphQuad[style][index].ShadowOffset.x, GlyphQuad[style][index].ShadowOffset.y, GlyphQuad[style][index].ShadowOffset.z ) );

			if( ogl_ModelViewProjectionMatrix != -1 )
				glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*shadowMatrix) );

			glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textPoint[0]);
			glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUV[0]);

			m_OpenGLContext->SetColour( 0.0f, 0.0f, 0.0f, m_vBlockColour.a );

			glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
		}

		glm::mat4 textMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z ) );

		textMatrix = glm::translate( textMatrix, glm::vec3( GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z ) );

		if( ogl_ModelViewProjectionMatrix != -1 )
			glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*textMatrix) );

		glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textPoint[0]);
		glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUV[0]);

		m_OpenGLContext->SetColour( m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, m_vBlockColour.a );

		glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
			
		offsetMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[2].x, GlyphQuad[style][index].Offsets[2].y, GlyphQuad[style][index].Offsets[2].z ) );

		// track how far word has gone
		xOffsetTotal += GlyphQuad[style][index].Offsets[2].x;
	}

	if( nVertexAttribLocation != -1 )
		glDisableVertexAttribArray( nVertexAttribLocation );

	if( nTexCoordsAttribLocation != -1 )
		glDisableVertexAttribArray( nTexCoordsAttribLocation );

	m_OpenGLContext->UseProgram(prevProg);
	
}

/////////////////////////////////////////////////////
/// Method: Print
/// Params: [in]x, [in]y, [in]vColour, [in]szString, [in]optional
///
/////////////////////////////////////////////////////
void FreetypeFont::Print(int x, int y, const glm::vec4 &vColour, GLenum filter, const char *szString, ...)
{
	int style = FONT_STYLE_NORMAL;
	char szText[MAX_FREETYPE_STRINGLEN];
	std::va_list ap;				
	unsigned int i=0;

	if( szString == 0 ||
		std::strlen(szString) >= MAX_FREETYPE_STRINGLEN-1)		
		return;				

	va_start( ap, szString );					
		std::vsprintf( szText, szString, ap );			
	va_end( ap );

	m_OpenGLContext->DisableVBO();

	m_OpenGLContext->UseProgram(freeTypeRenderProgram);

	// grab all matrices
	glm::mat4 projMat = m_OpenGLContext->GetProjectionMatrix();
	glm::mat4 modelMat = m_OpenGLContext->GetModelMatrix();
	
	GLint nVertexAttribLocation = glGetAttribLocation( freeTypeRenderProgram, "base_v" );
	GLint nTexCoordsAttribLocation = glGetAttribLocation( freeTypeRenderProgram, "base_uv0" );
	GLint ogl_ModelViewProjectionMatrix = glGetUniformLocation(freeTypeRenderProgram, "ogl_ModelViewProjectionMatrix");

	GLint nTexSamplerUniform = glGetUniformLocation( freeTypeRenderProgram, "texUnit0" );
	if( nTexSamplerUniform != -1 )
		glUniform1i( nTexSamplerUniform, 0 );
	
	glEnableVertexAttribArray( nVertexAttribLocation );
	glEnableVertexAttribArray( nTexCoordsAttribLocation );

	glm::vec4 globalColour = m_OpenGLContext->GetColour();

	glm::mat4 offsetMatrix = glm::translate( modelMat, glm::vec3( static_cast<GLfloat>(x), static_cast<GLfloat>(y), 0.0f ) );

	m_AtlasTexture.Bind(filter);

	for( i=0; i < (int)std::strlen(szText); ++i )
	{
		char index = szText[i];

		glm::mat4 textMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[0].x, GlyphQuad[style][index].Offsets[0].y, GlyphQuad[style][index].Offsets[0].z ) );

		textMatrix = glm::translate( textMatrix, glm::vec3( GlyphQuad[style][index].Offsets[1].x, GlyphQuad[style][index].Offsets[1].y, GlyphQuad[style][index].Offsets[1].z ) );

		if( ogl_ModelViewProjectionMatrix != -1 )
			glUniformMatrix4fv( ogl_ModelViewProjectionMatrix, 1, GL_FALSE, glm::value_ptr(projMat*textMatrix) );

		glVertexAttribPointer(nVertexAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textPoint[0]);
		glVertexAttribPointer(nTexCoordsAttribLocation, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), &GlyphQuad[style][index].textUV[0]);

		m_OpenGLContext->SetColour( m_vBlockColour.r, m_vBlockColour.g, m_vBlockColour.b, globalColour.a );

		glDrawArrays( GL_TRIANGLE_STRIP, 0, 4 );
			
		offsetMatrix = glm::translate( offsetMatrix, glm::vec3( GlyphQuad[style][index].Offsets[2].x, GlyphQuad[style][index].Offsets[2].y, GlyphQuad[style][index].Offsets[2].z ) );

	}

	if( nVertexAttribLocation != -1 )
		glDisableVertexAttribArray( nVertexAttribLocation );

	if( nTexCoordsAttribLocation != -1 )
		glDisableVertexAttribArray( nTexCoordsAttribLocation );
	
}

/////////////////////////////////////////////////////
/// Method: GetFontWidthHeight
/// Params: [in]c, [out]nCharacterWidth
///
/////////////////////////////////////////////////////
void FreetypeFont::GetCharacterWidth( unsigned char c, int *nCharacterWidth, int style )
{
	int nIndex = static_cast<int>(c);

	if( nCharacterWidth )
	{
		if( m_FixedWidth )
			*nCharacterWidth = static_cast<int>(m_vDimensions.x*0.75f);
		else
		{
			*nCharacterWidth = m_GlyphWidths[style][nIndex];
		}
	}
}

/////////////////////////////////////////////////////
/// Method: GetStringLength
/// Params: [in]szString
///
/////////////////////////////////////////////////////
float  FreetypeFont::GetStringLength(const char *szString, ...)
{
    int style = FONT_STYLE_NORMAL;
    char szText[MAX_FREETYPE_STRINGLEN];
    std::va_list ap;
    unsigned int i=0;
    
    if( szString == 0 ||
       std::strlen(szString) >= MAX_FREETYPE_STRINGLEN-1)
        return 0.0f;
    
    va_start( ap, szString );
        std::vsprintf( szText, szString, ap );
    va_end( ap );
    
    float xOffsetTotal = 0.0f;
    
    for( i=0; i < (int)std::strlen(szText); ++i )
    {
        if( szText[i] == '\n' )
        {
            return xOffsetTotal;
        }
        
        unsigned char index = szText[i];
        
        // track how far word has gone
        xOffsetTotal += GlyphQuad[style][index].Offsets[2].x;
    }
    
    return xOffsetTotal;
}

/////////////////////////////////////////////////////
/// Method: FindBestAtlasSize
/// Params: [in]library, [in]face, [in]fontStyleSupport, [in/out]atlasWidth, [in/out]atlasHeight
///
/////////////////////////////////////////////////////
int FreetypeFont::FindBestAtlasSize(FT_Library& library, FT_Face& face, unsigned short fontStyleSupport, int *atlasWidth, int *atlasHeight)
{
	// preprocess all characters to try and get the smallest atlas
	renderer::TextureAtlas tmpAtlas;
	tmpAtlas.Create( m_OpenGLContext, *atlasWidth, *atlasHeight, ATLAS_DEPTH);

	int i = 0;
	for (i = 0; i < FONT_STYLE_TOTAL; ++i)
	{
		unsigned short flags = FontStyleCombinations[i];

		if (fontStyleSupport < i)
			continue;

		// This is where we actually create each of the fonts display lists.
		for (unsigned char j = 0; j < FREETYPEFONT_CHARACTERS; j++)
		{
			// The first thing we do is get FreeType to render our character
			// into a bitmap.  This actually requires a couple of FreeType commands:

			FT_Int32 filterFlags = 0;
			filterFlags = FT_LOAD_DEFAULT | FT_LOAD_IGNORE_GLOBAL_ADVANCE_WIDTH | FT_LOAD_LINEAR_DESIGN;

			if (ATLAS_DEPTH == 3)
			{
				unsigned char lcd_weights[5];

				// FT_LCD_FILTER_LIGHT   is (0x00, 0x55, 0x56, 0x55, 0x00)
				// FT_LCD_FILTER_DEFAULT is (0x10, 0x40, 0x70, 0x40, 0x10)
				lcd_weights[0] = 0x10;
				lcd_weights[1] = 0x40;
				lcd_weights[2] = 0x70;
				lcd_weights[3] = 0x40;
				lcd_weights[4] = 0x10;

				FT_Library_SetLcdFilter(library, FT_LCD_FILTER_LIGHT);
				filterFlags |= FT_LOAD_TARGET_LCD;
				if (1)
				{
					FT_Library_SetLcdFilterWeights(library, lcd_weights);
				}
			}
			else
				filterFlags |= FT_LOAD_TARGET_MONO;

			// Load the Glyph for our character.
			if (FT_Load_Glyph(face, FT_Get_Char_Index(face, j), filterFlags))
			{
				DBGLOG("FREETYPEFONT: *ERROR* FT_Load_Glyph failed");
				return 1;
			}

			// Move the face's glyph into a Glyph object.
			FT_Glyph glyph;
			FT_GlyphSlot slot = face->glyph;

			if (flags & FONT_FLAG_ITALIC)
				FT_GlyphSlot_Oblique(slot);

			if (flags & FONT_FLAG_BOLD)
				FT_GlyphSlot_Embolden(slot);

			if (FT_Get_Glyph(face->glyph, &glyph))
			{
				DBGLOG("FREETYPEFONT: *ERROR* FT_Get_Glyph failed\n");

				FT_Done_Face(face);
				FT_Done_FreeType(library);

				return 1;
			}

			// Convert the glyph to a bitmap.
			if (ATLAS_DEPTH == 1)
				FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_NORMAL, 0, 1);
			else
				FT_Glyph_To_Bitmap(&glyph, FT_RENDER_MODE_LCD, 0, 1);

			FT_BitmapGlyph bitmap_glyph = (FT_BitmapGlyph)glyph;

			// This reference will make accessing the bitmap easier
			FT_Bitmap &bitmap = bitmap_glyph->bitmap;

			// We want each glyph to be separated by at least one black pixel
			int bmp_w = bitmap.width / ATLAS_DEPTH + 1;
			int bmp_h = bitmap.rows + 1;
			int bmp_x = 0;
			int bmp_y = 0;

			if (bmp_w > 2 &&
				bmp_h > 2)
			{
				glm::ivec4 region = tmpAtlas.GetRegion(bmp_w, bmp_h);
				if (region.x < 0)
				{
					DBGLOG("Texture atlas (%d x %d) is full\n", *atlasWidth, *atlasHeight);

					// clear the glyph memory
					FT_Done_Glyph(glyph);

					tmpAtlas.Destroy();
                    
                    int maxSize = m_OpenGLContext->GetMaxTextureSize();

					*atlasWidth = renderer::GetNextPowerOfTwo(*atlasWidth, maxSize);
					*atlasHeight = renderer::GetNextPowerOfTwo(*atlasHeight, maxSize);

					if (*atlasWidth < m_MaxAtlasSize)
						return FindBestAtlasSize(library, face, fontStyleSupport, atlasWidth, atlasHeight);
					else
                    {
						return -1;
                    }
				}
				bmp_w = bmp_w - 1;
				bmp_h = bmp_h - 1;
				bmp_x = region.x;
				bmp_y = region.y;

				tmpAtlas.SetRegion(bmp_x, bmp_y, bmp_w, bmp_h, bitmap.buffer, bitmap.pitch);
			}

			// clear the glyph memory
			FT_Done_Glyph(glyph);
		}
	}

	tmpAtlas.Destroy();

	return 0;
}

#endif // BASE_SUPPORT_FREETYPE
