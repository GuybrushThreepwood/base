
/*===================================================================
	File: GameCamera.h
	Game: 

	(C)Hidden Games
=====================================================================*/

#ifndef __GAMECAMERA_H__
#define __GAMECAMERA_H__

#include "Camera/Camera.h"

#include "HTemplateConsts.h"

class GameCamera : public Camera
{
	public:
		GameCamera();
		virtual ~GameCamera();

		virtual void SetPosition(glm::vec3& posVector);
		virtual void SetTarget(glm::vec3& targetVector);
		virtual void RotateAroundPoint(glm::vec3& vCenter, float x, float y, float z);

		void SetupCamera();
		void Update( float deltaTime );
		void SetZoom( float zoom );

		void SetRotation(const glm::vec3& rot)	{ m_Rot = rot; }
		void SetUpVector(const glm::vec3& up)	{ m_UpVector = up; }

		void SetLerp( bool state );
		void SetLerpSpeed( float lerpSpeed );
		
	private:
		bool m_LerpEnabled;
		float m_LerpSpeed;
		//ScriptDataHolder::CameraSetup m_CameraData;

		float m_CurrentZoom;

		float m_FollowObjectTime;
		float m_FollowTime;

		glm::vec3 m_LerpPos;
		glm::vec3 m_LerpTarget;

		glm::vec3 m_Rot;
		glm::vec3 m_Dir;
		glm::vec3 m_FinalDir;
		glm::vec3 m_UpVector;
};

/////////////////////////////////////////////////////
/// Method: SetPosition
/// 
///
/////////////////////////////////////////////////////
inline void GameCamera::SetPosition(glm::vec3& posVector)
{
	m_LerpPos = posVector;
	if( !m_LerpEnabled )
		m_Pos = posVector;
}

/////////////////////////////////////////////////////
/// Method: SetTarget
/// 
///
/////////////////////////////////////////////////////
inline void GameCamera::SetTarget(glm::vec3& targetVector)
{
	m_LerpTarget = targetVector;
	if( !m_LerpEnabled )
		m_Target = targetVector;

}

/////////////////////////////////////////////////////
/// Method: SetZoom
/// 
///
/////////////////////////////////////////////////////
inline void GameCamera::SetZoom( float zoom )
{
	m_CurrentZoom = zoom;
}

/////////////////////////////////////////////////////
/// Method: SetLerp
/// 
///
/////////////////////////////////////////////////////
inline void GameCamera::SetLerp( bool state )
{
	m_LerpEnabled = state;
}

/////////////////////////////////////////////////////
/// Method: SetLerpSpeed
/// 
///
/////////////////////////////////////////////////////
inline void GameCamera::SetLerpSpeed( float lerpSpeed )
{
	m_LerpSpeed = lerpSpeed;
}

#endif // __GAMECAMERA_H__
