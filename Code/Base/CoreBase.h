
#ifndef __COREBASE_H__
#define __COREBASE_H__

#ifndef __PLATFORMBASE_H__
	#include "PlatformBase.h"
#endif // __PLATFORMBASE_H__

#ifndef __COREDEFINES_H__
	#include "Core/CoreDefines.h"
#endif // __COREDEFINES_H__

#ifndef __CORECONSTS_H__
	#include "Core/CoreConsts.h"
#endif // __CORECONSTS_H__

#ifndef __DEBUGBASE_H__
	#include "DebugBase.h"
#endif // __DEBUGBASE_H__

#ifndef __COREFUNCTIONS_H__
	#include "Core/CoreFunctions.h"
#endif // __COREFUNCTIONS_H__

#ifndef __APP_H__
	#include "Core/App.h"
#endif // __APP_H__

#ifndef __ENDIAN_H__
	#include "Core/Endian.h"
#endif // __ENDIAN_H__

#ifndef __FILEIO_H__
	#include "Core/FileIO.h"
#endif // __FILEIO_H__

#endif // __COREBASE_H__
