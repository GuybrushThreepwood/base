
/*===================================================================
	File: HTemplate.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __HTEMPLATE_H__
#define __HTEMPLATE_H__

#include "CoreBase.h"

#include "StateManage/IState.h"
#include "StateManage/StateManager.h"
#include "GameStates/IBaseGameState.h"

#include "ScriptAccess/ScriptDataHolder.h"
#include "GameSystems.h"

// forward declare
class StateManager;

class HTemplate : public core::app::App
{
	public:
		HTemplate();
		~HTemplate();

		virtual int Initialise( void );
		virtual int Update( void );
		virtual int Render( void );
		virtual int Shutdown( void );

	private:
		StateManager m_MainStateManager;
		StateManager m_UIStateManager;

		char m_ProfileName[core::MAX_PATH+core::MAX_PATH];

		ScriptDataHolder* m_pScriptHolder;
		GameSystems* m_pGameSystems;
};

#endif // __HTEMPLATE_H__

