
/*===================================================================
	File: DebugPrint.h
	Library: Debug

	(C)Hidden Games
=====================================================================*/

#ifndef __DEBUGPRINT_H__
#define __DEBUGPRINT_H__

#if defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

// forward delcare
namespace renderer { class OpenGL; }

namespace dbg
{
	/// DebugCreateFont - Creates a GL 8x8 font texture
	void DebugCreateFont( renderer::OpenGL* openGLContext=0 );
	/// DebugSetFontColour - Sets the colour of the debug font
	/// \param r -  red component
	/// \param g -  green component
	/// \param b -  blue component
	/// \param a -  alpha component
	void DebugSetFontColour( unsigned char r, unsigned char g, unsigned char b, unsigned char a );
	/// DebugPrint - Displays a string buffer of text in Orthographic mode
	/// \param x - x window position of string
	/// \param y - y window position of string
	/// \param szString - string data
	/// \param ... - variable string options
	void DebugPrint( int x, int y, const char* szString, ... );
	/// DebugDestroyFont - deletes the GL font texture
	void DebugDestroyFont();
}

#endif // defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

#endif // __DEBUGPRINT_H__

