
#ifndef __INPUTINCLUDE_H__
#define __INPUTINCLUDE_H__

#ifndef __INPUTDEVICECONTROLLER_H__
    #include "Input/InputDeviceController.h"
#endif // __INPUTDEVICECONTROLLER_H__

#ifndef __INPUTMANAGER_H__
	#include "Input/InputManager.h"
#endif // __INPUTMANAGER_H__

#endif // __INPUTINCLUDE_H__