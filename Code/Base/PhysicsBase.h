
#ifndef __PHYSICSBASE_H__
#define __PHYSICSBASE_H__

#ifndef __DEBUGBASE_H__
	#include "DebugBase.h"
#endif // __DEBUGBASE_H__

#ifndef __PHYSICSCONSTS_H__
	#include "Physics/PhysicsConsts.h"
#endif // __PHYSICSCONSTS_H__

#ifndef __PHYSICSIDENTIFIER_H__
	#include "Physics/PhysicsIdentifier.h"
#endif // __PHYSICSIDENTIFIER_H__

/// BOX2D SUPPORT
#ifdef BASE_SUPPORT_BOX2D
	#ifndef BOX2D_H
		#include <Box2D/box2d.h>
	#endif // BOX2D_H

	#ifndef __BOX2DDEBUGDRAW_H__
		#include "Physics/box2d/Box2DDebugDraw.h"
	#endif // __BOX2DDEBUGDRAW_H__

	#ifndef __PHYSICSWORLDB2D_H__
		#include "Physics/box2d/PhysicsWorldB2D.h"
	#endif // __PHYSICSWORLDB2D_H__
#endif // BASE_SUPPORT_BOX2D

/// BULLET SUPPORT
#ifdef BASE_SUPPORT_BULLET
	#ifdef _DEBUG
		// bullet overloads the new operator
		#undef new
	#endif // _DEBUG

	#ifndef BULLET_DYNAMICS_COMMON_H
		#include "btBulletDynamicsCommon.h"
	#endif // BULLET_DYNAMICS_COMMON_H
#endif // BASE_SUPPORT_BULLET

#endif // __PHYSICSBASE_H__