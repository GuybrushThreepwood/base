
#ifndef __AABB_H__
#define __AABB_H__

// forward declare
namespace collision { class Sphere; }

namespace collision
{
	class AABB
	{
		public:
			/// default constructor
			AABB();
			/// default destructor
			~AABB();

			/// OPERATOR 'ADDITION_ASSIGN' - add and assign the data of parameter aabb to 'this' aabb
			/// \param a - vector to subtract
			/// \return AABB - ( SUCCESS: new AABB )
			AABB				&operator += (const AABB &a);

			/// Reset - Clears out the AABB data and sets to max/min values for each point
			void Reset( void );
			/// ReCalculate - Check to see if a point is outside the AABB current range and increases the AABB size if it is
			/// \param vPos - Point to check against
			void ReCalculate(const glm::vec2 &vPos);
			/// ReCalculate - Check to see if a point is outside the AABB current range and increases the AABB size if it is
			/// \param vPos - Point to check against
			void ReCalculate(const glm::vec3 &vPos);
			/// ReCalculate - Check to see if a point is outside the AABB current range and increases the AABB size if it is
			/// \param vPosMin - Minimum point to check against
			/// \param vPosMax - Maximum point to check against
			void ReCalculate(const glm::vec3 &vPosMin, const glm::vec3 &vPosMax);
			/// ClosestPointTo - Check to see if a point is outside the AABB current range and increases the AABB size if it is
			/// \param p - Point to check against
			/// \return Vec3 - Closest point
			glm::vec3 ClosestPointTo(const glm::vec3 &p);
			/// PointInAABB - Check to see if a point is inside 'this' AABB
			/// \param p - point to check
			/// \return boolean - ( COLLISION: true or NO COLLISION: false )
			bool PointInAABB(const glm::vec3 &p);
			/// AABBCollidesWithAABB - Check to see if an AABB is intersecting 'this' AABB
			/// \param aabb - AABB to check against
			/// \return boolean - ( COLLISION: true or NO COLLISION: false )
			bool AABBCollidesWithAABB( const AABB &aabb );
			/// AABBCollidesWithSphere - Check to see if a sphere is intersecting 'this' AABB
			/// \param s - sphere to check against
			/// \return boolean - ( COLLISION: true or NO COLLISION: false )
			bool AABBCollidesWithSphere( const collision::Sphere &s );

			/// center of the pos
			glm::vec3 vCenter;
			/// minimum bounding box values
			glm::vec3 vBoxMin;
			/// maximum bounding box values
			glm::vec3 vBoxMax;

		private:

	};

	/// PointInAABB - Check to see if a point is inside 'this' AABB
	/// \param p - point to check
	/// \return boolean - ( COLLISION: true or NO COLLISION: false )
	bool PointInAABB(const glm::vec3 &p, const collision::AABB &aabb);

/////////////////////////////////////////////////////
/// Operator: ADDITION_ASSIGN (a AABB to this one)
/// Params: [in]a
///
/////////////////////////////////////////////////////
inline AABB &AABB::operator += (const AABB &a) 
{
	if( a.vBoxMin.x < vBoxMin.x )
		vBoxMin.x = a.vBoxMin.x;

	if( a.vBoxMin.y < vBoxMin.y )
		vBoxMin.y = a.vBoxMin.y;

	if( a.vBoxMin.z < vBoxMin.z )
		vBoxMin.z = a.vBoxMin.z;

	if( a.vBoxMax.x > vBoxMax.x )
		vBoxMax.x = a.vBoxMax.x;

	if( a.vBoxMax.y > vBoxMax.y )
		vBoxMax.y = a.vBoxMax.y;

	if( a.vBoxMax.z > vBoxMax.z )
		vBoxMax.z = a.vBoxMax.z;

	vCenter = ( vBoxMin + vBoxMax )*0.5f;

	return *this;
}

} // namespace collision

#endif // __AABB_H__

