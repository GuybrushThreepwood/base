
/*===================================================================
	File: TiledTestState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDTESTSTATE_H__
#define __TILEDTESTSTATE_H__

#include "Support/Tiled/TiledLevelOrthogonal.h"
#include "Support/Tiled/TiledLevelIsometric.h"
#include "Support/Tiled/TiledLevelStaggeredIsometric.h"
#include "ScriptAccess/ScriptDataHolder.h"
#include "Support/Tiled/TilesetHandler.h"

// forward declare
class IState;
class IBaseGameState;

class TiledTestState : public IBaseGameState
{
	public:
		TiledTestState( StateManager& stateManager );
		virtual ~TiledTestState();

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();

	private:
		void CheckAchievements(float deltaTime);

	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;

		support::tiled::TiledBaseLoader* m_Level;
		support::tiled::TiledLevelOrthogonal* m_LevelOrtho;
		support::tiled::TiledLevelIsometric* m_LevelIso;
		support::tiled::TiledLevelStaggeredIsometric* m_LevelStaggeredIso;

		glm::vec3 m_ViewPos;
	
		glm::vec2 m_ViewRangeMin;
		glm::vec2 m_ViewRangeMax;
	
		glm::vec2 m_LevelDims;

		renderer::EViewRotation m_CurrentRotation;
	
};

#endif // __TILEDTESTSTATE_H__

