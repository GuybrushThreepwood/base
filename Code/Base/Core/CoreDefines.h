
#ifndef __COREDEFINES_H__
#define __COREDEFINES_H__

#ifdef BASE_PLATFORM_WINDOWS

	#define MAINFUNC int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nShowCmd )

	#define _CRT_SECURE_NO_DEPRECATE // don't care about secure c functions
	#define _CRT_NONSTDC_NO_DEPRECATE // don't complain about non std c
	#define _CRT_SECURE_NO_WARNINGS // no warnings

#ifdef _DEBUG
	#define _CRTDBG_MAP_ALLOC // debug calls
	//#define _CRTDBG_MAP_ALLOC_NEW
#endif // _DEBUG

//	#define	_CRT_NON_CONFORMING_SWPRINTFS // stops complaint about non conforming swprintf without the size param

	#define WIN32_LEAN_AND_MEAN // Windows specific includes and defines
	#define NOMINMAX // disable macro min/max

	#undef MAX_PATH

	#define snprintf _snprintf

	//#define GLEW_STATIC
	//#define GLEW_NO_GLU

	//#define BASE_SUPPORT_PVRSDK
#endif // BASE_PLATFORM_WINDOWS

#ifdef BASE_PLATFORM_iOS

#define iOS_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define iOS_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define iOS_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define iOS_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define iOS_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#endif // BASE_PLATFORM_iOS

//////////
// debug try/catch memory allocations
#ifdef _DEBUG
#define DBG_MEMTRY		try \
						{

#define DBG_MEMCATCH	} \
						catch( std::bad_alloc &ba ) \
						{ \
							DBGLOG( "*ERROR* allocation failed with %s (line:%d)\n", ba.what(), __LINE__ ); \
						} 

#else // not _DEBUG
	#define DBG_MEMTRY
	#define DBG_MEMCATCH
#endif //_DEBUG
/////////////

#include <cstdio>
#include <cstdarg>
#include <cstring>
#include <ctime>
#include <cassert>
#include <cmath>
#include <cerrno>
#include <fcntl.h>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <map> // added to stop debug new operator causing compile problems 
#include <vector>
#include <string>
#include <array>
#include <queue>
#include <memory>

#include <iostream>
#include <fstream>
#include <iterator>
#include <istream>
#include <sys/stat.h>

#ifdef BASE_PLATFORM_WINDOWS
	#include <direct.h>
#endif // BASE_PLATFORM_WINDOWS

#ifndef _ZZIP_ZZIP_H
	#include <zzip/zzip.h>
#endif // _ZZIP_ZZIP_H

#ifdef BASE_SUPPORT_PVRSDK

	#include "PVRCore/PVRCore.h"

#else
	// glm
	#include <glm/glm.hpp>
	#include <glm/gtc/matrix_transform.hpp>
	#include <glm/gtc/matrix_inverse.hpp>
	#include <glm/gtc/type_ptr.hpp>
	#include <glm/gtc/quaternion.hpp>
	#include <glm/gtc/constants.hpp>
	#include <glm/gtc/random.hpp>
	#include <glm/gtx/norm.hpp>
#endif

// opengl core
#include "Render/glad-41-core/glad.h"

// sdl
#include <SDL.h>
//#include <SDL_opengl.h>

// glew

#endif // __COREDEFINES_H__

