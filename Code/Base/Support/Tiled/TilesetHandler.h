
/*===================================================================
	File: TilesetHandler.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILESETHANDLER_H__
#define __TILESETHANDLER_H__

#ifdef BASE_SUPPORT_BOX2D

namespace support
{
	namespace tiled
	{
		class TilesetHandler
		{
		public:
			struct AnimFrame
			{
				int tileId;
				float tileShowTime;
				glm::ivec2 tileSize;
				glm::vec2 scaledTileSize;
				glm::vec2 scaledHalfTileSize;
				glm::vec2 uvCoord;
			};

			struct AnimationData
			{
				int animationId;

				GLuint textureId;

				int numFrames;
				AnimFrame* pTileFrames;

				glm::vec2 uvWidthHeight;
				bool playOnce;
				bool isPlaying;

				// current data
				int currentFrame;
				float frameTime;
				glm::vec2 frameUV;
			};

		public:
			TilesetHandler();
			~TilesetHandler();

			void Release();

			int LoadAndParseTileset(const char* szFilename, const glm::ivec2& tileSize, int spacing, int margin);

			AnimationData* CreateAnimation(int animationId, int* tileIds, int numFrames, float animSpeed, bool playOnce);

			GLuint GetTexture()			{ return m_TextureId; }

		private:
			void GetUVForTile(glm::vec2* bottomLeft, int tileIndex);
			AnimationData* AnimationAlreadyExists(int animationId);

		private:
			bool m_IsParsed;

			GLuint m_TextureId;

			int m_TotalNumTiles;
			int m_TotalTilesX;
			int m_TotalTilesY;

			int m_Spacing;
			int m_Margin;

			glm::ivec2 m_TileSize;
			glm::ivec2 m_ImageSize;

			glm::vec2 m_UVWidthHeight;

			std::vector<AnimationData*> m_Animations;

		};
	}
}

#endif // BASE_SUPPORT_BOX2D

#endif // __TILESETHANDLER_H__
