
#ifndef __COLLISIONBASE_H__
#define __COLLISIONBASE_H__

#ifndef __AABB_H__
	#include "Collision/AABB.h"
#endif // __AABB_H__

#ifndef __OBB_H__
	#include "Collision/OBB.h"
#endif // __OBB_H__

#ifndef __SPHERE_H__
	#include "Collision/Sphere.h"
#endif // __SPHERE_H__

#endif // __COLLISIONBASE_H__