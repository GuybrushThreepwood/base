
/*===================================================================
	File: FBOTestState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __FBOTESTSTATE_H__
#define __FBOTESTSTATE_H__

#include "ScriptAccess/ScriptDataHolder.h"

// forward declare
class IState;
class IBaseGameState;

class FBOTestState : public IBaseGameState
{
	public:
		FBOTestState( StateManager& stateManager );
		virtual ~FBOTestState();

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();
	
		void ChangeRTT();

	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;
};

#endif // __FBOTESTSTATE_H__

