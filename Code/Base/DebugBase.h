
#ifndef __DEBUGBASE_H__
#define __DEBUGBASE_H__

#ifndef __ASSERTION_H__
	#include "Debug/Assertion.h"
#endif // __ASSERTION_H__

#ifndef __DEBUGLOGGING_H__
	#include "Debug/DebugLogging.h"
#endif // __DEBUGLOGGING_H__

#if defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)
	#ifndef __DEBUGPRINT_H__
		#include "Debug/DebugPrint.h"
	#endif // __DEBUGPRINT_H__
#endif //defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

#endif // __DEBUGBASE_H__

