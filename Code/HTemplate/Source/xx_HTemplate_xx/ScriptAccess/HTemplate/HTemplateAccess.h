
/*===================================================================
	File: H8Access.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __HTEMPLATEACCESS_H__
#define __HTEMPLATEACCESS_H__

#include "ScriptBase.h"

// forward declare
class ScriptDataHolder;

struct GameData
{
	float ACHIEVEMENT_ICON_MAXX;
	float ACHIEVEMENT_ICON_SPEED;
	float ACHIEVEMENT_ICON_SHOWTIME;

	float ADBAR_PHONE_POSX;
	float ADBAR_PHONE_POSY;	
	float ADBAR_TABLET_POSX;
	float ADBAR_TABLET_POSY;

	// ui
	int MAX_PLAYLISTNAME_CHARACTERS;
	int MAX_SONGNAME_CHARACTERS;
};

struct InputBindings
{
	int MENU_NEXT;
	int MENU_PREVIOUS;
};

void RegisterHTemplateFunctions( ScriptDataHolder& dataHolder );

int ScriptSetGameData( lua_State* pState );
int ScriptSetInputBindings(lua_State* pState);

#endif // __H8ACCESS_H__
