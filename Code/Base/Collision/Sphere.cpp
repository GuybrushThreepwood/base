
/*===================================================================
	File: Sphere.cpp
	Library: Collision

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "CollisionBase.h"

using collision::Sphere;

/////////////////////////////////////////////////////
/// Default constructor
/// 
///
/////////////////////////////////////////////////////
Sphere::Sphere()
{

}

/////////////////////////////////////////////////////
/// Constructor
/// Params: [in]pos, [in]radius
///
/////////////////////////////////////////////////////
Sphere::Sphere(const glm::vec3 &pos, float radius)
{
	vCenterPoint = pos;
	r = radius;
}

/////////////////////////////////////////////////////
/// Default destructor
/// 
///
/////////////////////////////////////////////////////
Sphere::~Sphere()
{

}

///////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
/// Method: Reset
/// Params: None
///
/////////////////////////////////////////////////////
void Sphere::Reset( void )
{	
	r = 0.0f;
}

/////////////////////////////////////////////////////
/// Method: ReCalculate
/// Params: [in]vPos
///
/////////////////////////////////////////////////////
void Sphere::ReCalculate(const glm::vec3 &vPos)
{	
	float fNewRadius = glm::sqrt(vPos.x*vPos.x
							+ vPos.y*vPos.y
							+ vPos.z*vPos.z );
		
	if( fNewRadius > r )
		r = fNewRadius;
}

/////////////////////////////////////////////////////
/// Method: SetPosition
/// Params: [in]pos
///
/////////////////////////////////////////////////////
void Sphere::SetPosition(const glm::vec3 &pos)
{
	vCenterPoint = pos;
}

/////////////////////////////////////////////////////
/// Method: SetRadius
/// Params: [in]rad
///
/////////////////////////////////////////////////////
void Sphere::SetRadius( float rad )
{
	r = rad;
}

/////////////////////////////////////////////////////
/// Operator: ASSIGN
/// Params: [in]s
///
/////////////////////////////////////////////////////
Sphere &Sphere::operator =( const Sphere &s )
{
	vCenterPoint = s.vCenterPoint;
	r	= s.r;

	return *this;
}

/////////////////////////////////////////////////////
/// Method: SphereCollidesWithSphere
/// Params: [in]s
///
/////////////////////////////////////////////////////
bool Sphere::SphereCollidesWithSphere( const Sphere &s ) const
{
	float diffX = std::abs(vCenterPoint.x - s.vCenterPoint.x);
	float diffY = std::abs(vCenterPoint.y - s.vCenterPoint.y);
	float diffZ = std::abs(vCenterPoint.z - s.vCenterPoint.z);
	
	float distance = glm::sqrt((diffX*diffX) + (diffY*diffY) + (diffZ*diffZ));

	if( distance <= ( r + s.r ) )
		return true;

	return false;
}

/////////////////////////////////////////////////////
/// Method: SphereCollidesWithSphere
/// Params: [in]s, [out]distance
///
/////////////////////////////////////////////////////
bool Sphere::SphereCollidesWithSphere( const Sphere &s, float &distance ) const
{
	float diffX = std::abs(vCenterPoint.x - s.vCenterPoint.x);
	float diffY = std::abs(vCenterPoint.y - s.vCenterPoint.y);
	float diffZ = std::abs(vCenterPoint.z - s.vCenterPoint.z);

	distance = glm::sqrt((diffX*diffX) + (diffY*diffY) + (diffZ*diffZ));

	if( distance <= ( r + s.r ) )
		return true;

	return false;
}

/////////////////////////////////////////////////////
/// Method: SphereCollidesWithAABB
/// Params: [in]aabb
///
/////////////////////////////////////////////////////
bool Sphere::SphereCollidesWithAABB( collision::AABB &aabb )
{
	// find the closest point on box to the point
	glm::vec3 vCenter = vCenterPoint;
	glm::vec3 vPoint = aabb.ClosestPointTo(vCenter);

	// check if it's within range
	if( glm::distance2(vCenterPoint, vPoint) < (r*r) )
		return true;

	return false;
}

