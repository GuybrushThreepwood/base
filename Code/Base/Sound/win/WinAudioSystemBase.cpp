
/*===================================================================
	File: WinAudioSystem.cpp
	Game: Cabby

	(C)Hidden Games
=====================================================================*/

#ifdef BASE_PLATFORM_WINDOWS

#include "CoreBase.h"
#include "SoundBase.h"

#include "Resources/SoundResources.h"
#include "Sound/win/WinAudioSystemBase.h"

namespace
{
	snd::MusicCallback* pMusicCallback = nullptr;
	char CurrentPlaylistName[core::MAX_PATH];
	char CurrentSongName[core::MAX_PATH];
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
snd::AudioSystemBase::AudioSystemBase(snd::MusicMode mode)
{
	m_MusicMode = mode;
	m_MusicWasPlaying = false;
	
	m_IsPlaying = false;
	m_IsPaused = false;

	m_CurrentBGMTrack = -1;

	pMusicCallback = 0;

	std::memset( CurrentPlaylistName, 0, sizeof(char)*core::MAX_PATH );
	std::memset( CurrentSongName, 0, sizeof(char)*core::MAX_PATH );

	m_UIBufferId = snd::INVALID_SOUNDBUFFER;

	m_SystemPaused = false;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
snd::AudioSystemBase::~AudioSystemBase()
{
	pMusicCallback = 0;
	
	m_CurrentBGMTrack = -1;
	m_SFXState = true;

	m_BGMStream.Release();

	m_UIBufferId = snd::INVALID_SOUNDBUFFER;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::Update( float deltaTime )
{
	snd::SoundManager::GetInstance()->Update( deltaTime, false );

	if( !m_BGMStream.IsPlaying() )
		m_IsPlaying = false;

	m_BGMStream.Update();
}

/////////////////////////////////////////////////////
/// Method: UpdatePlaylists
/// Params: 
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::UpdatePlaylists()
{

}

/////////////////////////////////////////////////////
/// Method: PlayMusicTrack
/// Params: filename
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PlayBGMTrack( const char* filename, bool loop )
{
	// convert an mp3 to ogg lookup
	file::TFile fileSplit;
	char newPath[core::MAX_PATH+core::MAX_PATH];
	std::memset( newPath, 0, sizeof(char)*core::MAX_PATH+core::MAX_PATH );
		
	file::CreateFileStructure( filename, &fileSplit );

	if( std::strcmp( fileSplit.szFileExt, ".mp3" ) == 0 ||
		std::strcmp( fileSplit.szFileExt, ".MP3" ) == 0 )
	{
		snprintf( newPath, core::MAX_PATH+core::MAX_PATH, "%sogg/%s.ogg", fileSplit.szDrive, fileSplit.szFile );
		m_BGMStream.StreamOpen( newPath );
		m_BGMStream.StartPlayback(loop);
	}
	else
	{
		m_BGMStream.StreamOpen( filename );
		m_BGMStream.StartPlayback(loop);
	}

	m_IsPlaying = true;
}

/////////////////////////////////////////////////////
/// Method: StopBGMTrack
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::StopBGMTrack()
{
	m_BGMStream.Stop();	

	m_IsPlaying = false;
}

/////////////////////////////////////////////////////
/// Method: ClearBGMTrack
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::ClearBGMTrack()
{
	m_BGMStream.Release();

	m_IsPlaying = false;
	m_IsPaused = false;
}

/////////////////////////////////////////////////////
/// Method: SetBGMTrackVolume
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::SetBGMTrackVolume( float vol )
{
	vol = glm::clamp(vol, 0.0f, 1.0f);

	m_BGMStream.SetVolume( vol );
}

/////////////////////////////////////////////////////
/// Method: PauseMusicTrack
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PauseBGMTrack()
{
	m_BGMStream.Pause();
	m_IsPaused = true;
}

/////////////////////////////////////////////////////
/// Method: ResumeMusicTrack
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::ResumeBGMTrack()
{
	m_BGMStream.Play();
	m_IsPaused = false;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
/// Method: SetShuffleMode
/// Params: [in]mode
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::SetShuffleMode( int mode )
{

}

/////////////////////////////////////////////////////
/// Method: SetRepeatFlag
/// Params: [in]state
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::SetRepeatFlag( bool state )
{

}

/////////////////////////////////////////////////////
/// Method: IsPlaying
/// Params: None
///
/////////////////////////////////////////////////////
bool snd::AudioSystemBase::IsPlaying()
{
	return m_IsPlaying;
}

/////////////////////////////////////////////////////
/// Method: IsPaused
/// Params: None
///
/////////////////////////////////////////////////////
bool snd::AudioSystemBase::IsPaused()
{
	return m_IsPaused;
}

/////////////////////////////////////////////////////
/// Method: IsUsingPlaylists
/// Params: None
///
/////////////////////////////////////////////////////
bool snd::AudioSystemBase::IsUsingPlaylists()
{
	return false;
}

/////////////////////////////////////////////////////
/// Method: GetCurrentSong
/// Params: None
///
/////////////////////////////////////////////////////
const char* snd::AudioSystemBase::GetCurrentSong()
{
	return nullptr;
}

/////////////////////////////////////////////////////
/// Method: GetPlaylistName
/// Params: [in]playlistIndex
///
/////////////////////////////////////////////////////
const char* snd::AudioSystemBase::GetPlaylistName( int playlistIndex )
{
	return nullptr;
}

/////////////////////////////////////////////////////
/// Method: GetSongName
/// Params: [in]playlistIndex, [in]songIndex
///
/////////////////////////////////////////////////////
const char* snd::AudioSystemBase::GetSongName( int playlistIndex, int songIndex )
{
	return nullptr;
}

/////////////////////////////////////////////////////
/// Method: GetTotalNumberOfPlaylists
/// Params: None
///
/////////////////////////////////////////////////////
int snd::AudioSystemBase::GetTotalNumberOfPlaylists()
{
	return 0;		
}

/////////////////////////////////////////////////////
/// Method: GetTotalNumberOfSongsInPlaylist
/// Params: [in]playlistIndex
///
/////////////////////////////////////////////////////
int snd::AudioSystemBase::GetTotalNumberOfSongsInPlaylist( int playlistIndex )
{
	return 0;		
}

/////////////////////////////////////////////////////
/// Method: GetCurrentPlaylistIndex
/// Params: None
///
/////////////////////////////////////////////////////
int snd::AudioSystemBase::GetCurrentPlaylistIndex()
{
	return -1;		
}

/////////////////////////////////////////////////////
/// Method: GetCurrentSongIndex
/// Params: None
///
/////////////////////////////////////////////////////
int snd::AudioSystemBase::GetCurrentSongIndex()
{
	return -1;	
}

/////////////////////////////////////////////////////
/// Method: PlaySong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PlaySong( int playlistIndex, int songIndex )
{

}

/////////////////////////////////////////////////////
/// Method: PlaySong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PlaySong()
{

}

/////////////////////////////////////////////////////
/// Method: PlaySong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PauseSong()
{

}

/////////////////////////////////////////////////////
/// Method: ResumeSong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::ResumeSong()
{

}

/////////////////////////////////////////////////////
/// Method: StopSong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::StopSong()
{

}

/////////////////////////////////////////////////////
/// Method: SkipToPreviousSong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::SkipToPreviousSong()
{

}

/////////////////////////////////////////////////////
/// Method: SkipToNextSong
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::SkipToNextSong()
{

}

/////////////////////////////////////////////////////
/// Method: SetMusicCallback
/// Params: [in]pCallback
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::SetMusicCallback( snd::MusicCallback* pCallback )
{
	pMusicCallback = pCallback;
}

///////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
/// Method: PlayUIAudio
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PlayUIAudio()
{
	float soundMultiply = 1.0f;
	if(!m_SFXState)
		soundMultiply = 0.0f;

	ALuint sourceId = snd::INVALID_SOUNDSOURCE;
	sourceId = snd::SoundManager::GetInstance()->GetFreeSoundSource();

	if( sourceId != snd::INVALID_SOUNDSOURCE &&
		m_UIBufferId != snd::INVALID_SOUNDBUFFER )
	{
		CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_BUFFER, m_UIBufferId ) )
		CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_SOURCE_RELATIVE, AL_TRUE ) )
		CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_LOOPING, AL_FALSE ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_PITCH, 1.0f ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_GAIN, 1.0f*soundMultiply ) )
		CHECK_OPENAL_ERROR( alSource3f( sourceId, AL_POSITION, 0.0f, 0.0f, 0.0f ) )
		
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_MAX_DISTANCE, MAX_AUDIO_DISTANCE ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_ROLLOFF_FACTOR, ROLL_OFF ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_REFERENCE_DISTANCE, REF_DISTANCE ) )
		
		CHECK_OPENAL_ERROR( alSourcePlay( sourceId ) )
	}
}

/////////////////////////////////////////////////////
/// Method: PlayAudio
/// Params: [in]sourceId, [in]bufferId, [in]pos, [in]sourceRel
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PlayAudio(ALuint sourceId, ALuint bufferId, glm::vec3 pos, bool sourceRel, bool loop, float pitch, float gain)
{
	float soundMultiply = 1.0f;
	if(!m_SFXState)
		soundMultiply = 0.0f;

	if( sourceId == snd::INVALID_SOUNDSOURCE ||
		bufferId == snd::INVALID_SOUNDBUFFER )
		return;

	CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_BUFFER, bufferId ) )
	CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_SOURCE_RELATIVE, sourceRel ) )
	CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_LOOPING, loop ) )
	CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_PITCH, pitch ) )
	CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_GAIN, gain*soundMultiply ) )
	CHECK_OPENAL_ERROR( alSource3f( sourceId, AL_POSITION, pos.x, pos.y, pos.z ) )
	
	CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_MAX_DISTANCE, MAX_AUDIO_DISTANCE ) )
	CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_ROLLOFF_FACTOR, ROLL_OFF ) )
	CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_REFERENCE_DISTANCE, REF_DISTANCE ) )
	
	CHECK_OPENAL_ERROR( alSourcePlay( sourceId ) )
}

/////////////////////////////////////////////////////
/// Method: PlayAudio
/// Params: [in]bufferId, [in]pos, [in]sourceRel
///
/////////////////////////////////////////////////////
ALuint snd::AudioSystemBase::PlayAudio(ALuint bufferId, glm::vec3 pos, bool sourceRel, bool loop, float pitch, float gain)
{
	float soundMultiply = 1.0f;
	gain = 2.0f;
	if(!m_SFXState)
		soundMultiply = 0.0f;

	ALuint sourceId = snd::SoundManager::GetInstance()->GetFreeSoundSource();

	if( sourceId != snd::INVALID_SOUNDSOURCE &&
		bufferId != snd::INVALID_SOUNDBUFFER )
	{
		CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_BUFFER, bufferId ) )
		CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_SOURCE_RELATIVE, sourceRel ) )
		CHECK_OPENAL_ERROR( alSourcei( sourceId, AL_LOOPING, loop ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_PITCH, pitch ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_GAIN, gain*soundMultiply ) )
		CHECK_OPENAL_ERROR( alSource3f( sourceId, AL_POSITION, pos.x, pos.y, pos.z ) )

		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_MAX_DISTANCE, MAX_AUDIO_DISTANCE ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_ROLLOFF_FACTOR, ROLL_OFF ) )
		CHECK_OPENAL_ERROR( alSourcef( sourceId, AL_REFERENCE_DISTANCE, REF_DISTANCE ) )

		CHECK_OPENAL_ERROR( alSourcePlay( sourceId ) )
	}

	if( loop )
		return sourceId;

	return snd::INVALID_SOUNDSOURCE;
}

/////////////////////////////////////////////////////
/// Method: StopAudio
/// Params: [in]sourceId
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::StopAudio( ALuint sourceId )
{
	CHECK_OPENAL_ERROR( alSourceStop( sourceId ) )
}

/////////////////////////////////////////////////////
/// Method: StopAll
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::StopAll()
{
	snd::SoundManager::GetInstance()->StopAll();
}

/////////////////////////////////////////////////////
/// Method: Pause
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::Pause()
{
	snd::SoundManager::GetInstance()->Pause();

	m_SystemPaused = true;
}

/////////////////////////////////////////////////////
/// Method: UnPause
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::UnPause()
{
	snd::SoundManager::GetInstance()->UnPause();

	m_SystemPaused = false;
}

/////////////////////////////////////////////////////
/// Method: PauseMusic
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::PauseMusic()
{

}

/////////////////////////////////////////////////////
/// Method: UnPauseMusic
/// Params: None
///
/////////////////////////////////////////////////////
void snd::AudioSystemBase::UnPauseMusic()
{

}

#endif // BASE_PLATFORM_WINDOWS
