
/*===================================================================
	File: UIIds.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __UIIDS_H__
#define __UIIDS_H__

namespace
{
	const int UI_TITLESCREEN				= 0;

	const int ID_TITLESCREENSTATE			= 100;
	const int ID_MAINGAMESTATE				= 101;
	const int ID_BOX2DEXAMPLESTATE			= 102;
	const int ID_FBOTESTSTATE				= 109;
	const int ID_TILEDTESTSTATE				= 110;
	const int ID_FONTFORMATTESTSTATE		= 112;
	const int ID_INPUTTESTSTATE				= 115;
}

#endif // __UIIDS_H__
