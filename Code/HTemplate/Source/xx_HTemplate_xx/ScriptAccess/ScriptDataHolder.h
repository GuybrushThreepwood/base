
/*===================================================================
	File: ScriptDataHolder.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __SCRIPTDATAHOLDER_H__
#define __SCRIPTDATAHOLDER_H__

#include "ScriptAccess/HTemplate/HTemplateAccess.h"

class ScriptDataHolder
{
	public:
		struct DevScriptData
		{
			DevScriptData()
			{
				enablePhysicsDraw = false;
				enableDebugDraw = false;
				enableDebugUIDraw = false;

				isTablet = false;
				isRetina = false;
				isPCOnly = false;
				frameLock30 = false;
				frameLock60 = true;

				assetBaseWidth = 768;
				assetBaseHeight = 1024;

				screenWidth = 1024;
				screenHeight = 768;

				useVertexArrays = false;

				bootState = 0;
				developerSaveFileRoot = 0;
				userSaveFileRoot = 0;
				language = core::LANGUAGE_ENGLISH;

				allowAdvertBarScaling = false;
				appAdFilterId = 0;
				localAdvertXML = 0;
				externalAdvertXML = 0;

				allowDebugCam = false;
			}

			bool enablePhysicsDraw;
			bool enableDebugDraw;
			bool enableDebugUIDraw;
			bool isTablet;
			bool isRetina;
			bool isPCOnly;

			bool frameLock30;
			bool frameLock60;

			int assetBaseWidth;
			int assetBaseHeight;

			int screenWidth;
			int screenHeight;

			bool useVertexArrays;

			int bootState;
			const char* developerSaveFileRoot;
			const char* userSaveFileRoot;
			int language;
			bool showPCAdBar;

			bool allowAdvertBarScaling;
			const char* appAdFilterId;
			const char* localAdvertXML;
			const char* externalAdvertXML;

			bool allowDebugCam;
		};

		struct MusicTrackData
		{
			const char* musicFile;
			const char* musicName;
			float musicVolume;
		};

	public:
		ScriptDataHolder();
		~ScriptDataHolder();
		
		void Release();

		// development
		void SetDevData( DevScriptData& data );
		DevScriptData& GetDevData();

		// game data
		void SetGameData( GameData& data );
		GameData& GetGameData();

		void SetInputBinds(InputBindings& data);
		InputBindings& GetInputBinds();

	private:
		DevScriptData m_DevData;
		GameData m_GameData;
		InputBindings m_InputBinds;
};

inline void ScriptDataHolder::SetDevData( ScriptDataHolder::DevScriptData& data )
{
	m_DevData = data;
}

inline ScriptDataHolder::DevScriptData& ScriptDataHolder::GetDevData( )
{
	return m_DevData;
}

inline void ScriptDataHolder::SetGameData( GameData& data )
{
	m_GameData = data;
}

inline GameData& ScriptDataHolder::GetGameData( )
{
	return m_GameData;
}

inline void ScriptDataHolder::SetInputBinds(InputBindings& data)
{
	m_InputBinds = data;
}

inline InputBindings& ScriptDataHolder::GetInputBinds()
{
	return m_InputBinds;
}

#endif // __SCRIPTDATAHOLDER_H__
