
#ifndef __SHADER_H__
#define __SHADER_H__

namespace renderer
{
	class Shader
	{
		public:
			enum EShaderUniformName
			{
				ModelViewProjectionMatrix = 0,
				ModelViewMatrix,
				ViewMatrix,
				NormalMatrix,
				VertexColour,

				TotalUniforms
			};

			enum EShaderUniformType
			{
				UniformType_Vec2,
				UniformType_Vec3,
				UniformType_Vec4,

				UniformType_Mat4,

			};

			struct TUniformBlock
			{
				EShaderUniformName uniformIdent;
				EShaderUniformType uniformType;
				GLint uniformLocation;
				std::string uniformString;
			};

		public:
			/// Shader
			Shader();
			~Shader();

			void Initialise();
			void Shutdown();

			bool IsValid();

			void CreateFromFiles(const char* vertexShader, const char* fragmentShader, const char* geometryShader = nullptr, renderer::OpenGL* openGLContext = nullptr);
			void CreateFromStrings(const char* vertexShader, const char* fragmentShader, const char* geometryShader = nullptr, renderer::OpenGL* openGLContext = nullptr);

			void Bind();
			void UnBind();

			void SetUniform(EShaderUniformName uniform, const GLfloat *value);

			void SetUniform2fv(GLint location, GLsizei count, const GLfloat *value);
			void SetUniform3fv(GLint location, GLsizei count, const GLfloat *value);
			void SetUniform4fv(GLint location, GLsizei count, const GLfloat *value);

			void SetUniformMatrix(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value);

		private:
			void Init();
			void Release();

			void CacheUniforms();

		private:
			renderer::OpenGL* m_OpenGLContext;

			GLuint m_ProgramId;
			std::map<std::string, Shader::TUniformBlock> m_UniformMap;
			typedef std::pair< std::string, Shader::TUniformBlock> TShaderMapPair;

			GLuint m_PrevProgramId;
	};
	
} // namespace renderer

#endif // __SHADER_H__

