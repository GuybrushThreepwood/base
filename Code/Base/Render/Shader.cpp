
/*===================================================================
	File: Shader.cpp
	Library: Render

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "Debug/Assertion.h"
#include "Debug/DebugLogging.h"

#include "Render/RenderConsts.h"
#include "Render/OpenGL.h"
#include "Render/OpenGLCommon.h"
#include "Render/ShaderShared.h"
#include "Render/Shader.h"

using renderer::Shader;

namespace
{
	std::array<Shader::TUniformBlock, Shader::TotalUniforms> uniforms = 
	{
			Shader::ModelViewProjectionMatrix,
			Shader::UniformType_Mat4,
			-1,
			"ogl_ModelViewProjectionMatrix",

			Shader::ModelViewMatrix,
			Shader::UniformType_Mat4,
			-1,
			"ogl_ModelViewMatrix",

			Shader::ViewMatrix,
			Shader::UniformType_Mat4,
			-1,
			"ogl_ViewMatrix",

			Shader::NormalMatrix,
			Shader::UniformType_Mat4,
			-1,
			"ogl_NormalMatrix",

			Shader::VertexColour,
			Shader::UniformType_Vec4,
			-1,
			"ogl_VertexColour",
	};
}

/////////////////////////////////////////////////////
/// Default Constructor
/// Params: None
///
/////////////////////////////////////////////////////
Shader::Shader()
{
	m_ProgramId = renderer::INVALID_OBJECT;
	m_PrevProgramId = 0;

	m_OpenGLContext = nullptr;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
Shader::~Shader()
{	

}

/////////////////////////////////////////////////////
/// Method: Initialise
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::Initialise()
{
	Init();
}

/////////////////////////////////////////////////////
/// Method: Shutdown
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::Shutdown()
{
	Release();
}

/////////////////////////////////////////////////////
/// Method: Init
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::Init()
{
	m_ProgramId = renderer::INVALID_OBJECT;
	m_PrevProgramId = 0;

	m_OpenGLContext = nullptr;
}

/////////////////////////////////////////////////////
/// Method: Release
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::Release()
{
	if (m_ProgramId != renderer::INVALID_OBJECT)
		renderer::RemoveShaderProgram(m_ProgramId);
}

/////////////////////////////////////////////////////
/// Method: IsValid
/// Params: None
///
/////////////////////////////////////////////////////
bool Shader::IsValid()
{
	return (m_ProgramId != renderer::INVALID_OBJECT);
}

/////////////////////////////////////////////////////
/// Method: CreateFromFiles
/// Params: [in]vertexShader, [in]fragmentShader, [in]geometryShader, [in]openGLContext
///
/////////////////////////////////////////////////////
void Shader::CreateFromFiles(const char* vertexShader, const char* fragmentShader, const char* geometryShader, renderer::OpenGL* openGLContext )
{
	Release();

	if (openGLContext == nullptr)
		m_OpenGLContext = renderer::OpenGL::GetInstance();
	else
		m_OpenGLContext = openGLContext;
	DBG_ASSERT(m_OpenGLContext != nullptr);

	m_ProgramId = renderer::LoadShaderFilesForProgram(vertexShader, fragmentShader, geometryShader);
	DBG_ASSERT(m_ProgramId != renderer::INVALID_OBJECT);

	CacheUniforms();
}

/////////////////////////////////////////////////////
/// Method: CreateFromStrings
/// Params: [in]vertexShader, [in]fragmentShader, [in]geometryShader, [in]openGLContext
///
/////////////////////////////////////////////////////
void Shader::CreateFromStrings(const char* vertexShader, const char* fragmentShader, const char* geometryShader, renderer::OpenGL* openGLContext)
{
	Release();

	if (openGLContext == nullptr)
		m_OpenGLContext = renderer::OpenGL::GetInstance();
	else
		m_OpenGLContext = openGLContext;
	DBG_ASSERT(m_OpenGLContext != nullptr);

	m_ProgramId = renderer::LoadShaderStringsForProgram(vertexShader, fragmentShader, geometryShader);
	DBG_ASSERT(m_ProgramId != renderer::INVALID_OBJECT);

	CacheUniforms();
}

/////////////////////////////////////////////////////
/// Method: Bind
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::Bind()
{
	if (m_ProgramId != renderer::INVALID_OBJECT)
	{
		m_PrevProgramId = m_OpenGLContext->GetCurrentProgram();
		m_OpenGLContext->UseProgram(m_ProgramId);
	}
}
	
/////////////////////////////////////////////////////
/// Method: UnBind
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::UnBind()
{
	if (m_PrevProgramId != renderer::INVALID_OBJECT)
		m_OpenGLContext->UseProgram(m_PrevProgramId);
	else
		m_OpenGLContext->UseProgram(0);
}	

/////////////////////////////////////////////////////
/// Method: SetUniform
/// Params: [in]uniform, [in]value
///
/////////////////////////////////////////////////////
void Shader::SetUniform(EShaderUniformName uniform, const GLfloat *value)
{
	GLint location = m_UniformMap[uniforms[uniform].uniformString].uniformLocation;
	renderer::Shader::EShaderUniformType type = uniforms[uniform].uniformType;
	if (location != -1)
	{
		if (type == Shader::UniformType_Vec2)
			glUniform2fv(location, 1, value);
		else
		if (type == Shader::UniformType_Vec3)
			glUniform3fv(location, 1, value);
		else
		if (type == Shader::UniformType_Vec4)
			glUniform4fv(location, 1, value);
		else
		if (type == Shader::UniformType_Mat4)
			glUniformMatrix4fv(location, 1, GL_FALSE, value);
	}

	GL_CHECK
}
	
void Shader::SetUniform2fv(GLint location, GLsizei count, const GLfloat *value)
{
	
}
		
void Shader::SetUniform3fv(GLint location, GLsizei count, const GLfloat *value)
{
	
}
		
void Shader::SetUniform4fv(GLint location, GLsizei count, const GLfloat *value)
{
	
}
	
void Shader::SetUniformMatrix(GLint location, GLsizei count, GLboolean transpose, const GLfloat *value)
{
	
}

/////////////////////////////////////////////////////
/// Method: CacheUniforms
/// Params: None
///
/////////////////////////////////////////////////////
void Shader::CacheUniforms()
{
	m_UniformMap.empty();
	for (auto &u : uniforms) {

		Shader::TUniformBlock newBlock;
		newBlock.uniformIdent = u.uniformIdent;
		newBlock.uniformType = u.uniformType;
		newBlock.uniformLocation = glGetUniformLocation(m_ProgramId, u.uniformString.c_str());
		newBlock.uniformString = u.uniformString;

		m_UniformMap.insert(TShaderMapPair(u.uniformString, newBlock));
	}
}
