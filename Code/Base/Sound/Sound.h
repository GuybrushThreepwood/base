
#ifndef __SOUND_H__
#define __SOUND_H__

#ifdef BASE_SUPPORT_OPENAL

#ifndef AL_AL_H
	#include <OpenAL/al.h>
#endif // AL_AL_H

#ifdef BASE_PLATFORM_WINDOWS
#ifndef __WINSOUND_H__
	#include "win/WinSound.h"
#endif // _WINSSOUND_H__
#endif // BASE_PLATFORM_WINDOWS

namespace snd
{

#ifdef BASE_PLATFORM_WINDOWS
	#define PLATFORM_SOUND_CLASS WinSound
#endif // BASE_PLATFORM_WINDOWS
	
	class Sound : public PLATFORM_SOUND_CLASS
	{
			
	};

	void SetMusicPauseCall( snd::MusicPauseCall* call );
	
	snd::MusicPauseCall* GetMusicPauseCall();
	
} // namespace snd

#endif // BASE_SUPPORT_OPENAL

#endif // __SOUND_H__


