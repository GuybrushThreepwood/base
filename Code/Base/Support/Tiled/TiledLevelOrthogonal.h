
/*===================================================================
	File: TiledLevelOrthogonal.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDLEVELORTHOGONAL_H__
#define __TILEDLEVELORTHOGONAL_H__
	
#ifdef BASE_SUPPORT_BOX2D

#include "Support/Tiled/TiledOrthogonalMap.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		class TiledLevelOrthogonal : public TiledOrthogonalMap
		{
		public:
			TiledLevelOrthogonal();
			virtual ~TiledLevelOrthogonal();

			virtual void DrawLayers(int startLayer = -1, int endLayer = -1);

		private:

		};
	}
}

#endif // BASE_SUPPORT_BOX2D
#endif // __TILEDLEVELORTHOGONAL_H__

