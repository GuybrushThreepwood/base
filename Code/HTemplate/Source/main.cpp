
/*===================================================================
 File: main.cpp
 Game: HTemplate
 
 (C)Hidden Games
 =====================================================================*/

#ifdef BASE_PLATFORM_WINDOWS

#include "CoreBase.h"
#include "HTemplate.h"
#include "HTemplateConsts.h"
#include "resource.h"

const bool APP_FULLSCREEN = false;

MAINFUNC
{
	HTemplate theApp;

	core::app::SetIconID(IDI_ICON1);
	core::app::SetFrameLock( core::FPS30 );

	theApp.Execute(1024, 768, 1024, 768, APP_FULLSCREEN);

	return 0;
}

#endif // BASE_PLATFORM_WINDOWS