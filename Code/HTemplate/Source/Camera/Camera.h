
/*===================================================================
	File: Camera.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __CAMERA_H__
#define __CAMERA_H__

// forward declare

class Camera
{
	public:
		Camera();
		virtual ~Camera();
		
		virtual void PositionCamera( float posX, float posY, float posZ, float targetX, float targetY, float targetZ, float upX, float upY, float upZ);
		virtual void MoveCamera(float cameraSpeed);
		virtual void RotateView(float X, float Y, float Z);
		//virtual void RotateAroundPoint(glm::vec3& vCenter, float x, float y, float z);
		virtual void StrafeCamera(float speed);

		glm::vec3&	GetPosition();
		glm::vec3&	GetTarget();
		glm::vec3&	GetUp();
		float				GetAngle();

		virtual void SetPosition(glm::vec3& posVector);
		virtual void SetTarget(glm::vec3& targetVector);

		void SetAngle( float angle )
		{
			m_Angle = angle;
		}

	protected:
		glm::vec3 m_Pos;					// current position
		glm::vec3 m_Target;				// current target
		glm::vec3 m_Up;					// up vector

		float m_Angle;							// current angle
		float m_Speed;

	private:
		
};

/////////////////////////////////////////////////////
/// Method: GetPosition
/// 
///
/////////////////////////////////////////////////////
inline glm::vec3&	Camera::GetPosition()
{ 
	return m_Pos; 
}

/////////////////////////////////////////////////////
/// Method: GetTarget
/// 
///
/////////////////////////////////////////////////////
inline glm::vec3&	Camera::GetTarget()
{ 
	return m_Target; 
}

/////////////////////////////////////////////////////
/// Method: GetUp
/// 
///
/////////////////////////////////////////////////////
inline glm::vec3&	Camera::GetUp()
{ 
	return m_Up; 
}

/////////////////////////////////////////////////////
/// Method: GetAngle
/// 
///
/////////////////////////////////////////////////////
inline float Camera::GetAngle()
{
	return m_Angle;
}

/////////////////////////////////////////////////////
/// Method: SetPosition
/// 
///
/////////////////////////////////////////////////////
inline void Camera::SetPosition(glm::vec3& posVector)
{ 
	m_Pos = posVector; 
}

/////////////////////////////////////////////////////
/// Method: SetTarget
/// 
///
/////////////////////////////////////////////////////
inline void Camera::SetTarget(glm::vec3& targetVector)
{ 
	m_Target = targetVector; 
}

#endif // __CAMERA_H__

