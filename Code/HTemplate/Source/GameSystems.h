
/*===================================================================
	File: GameSystems.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __GAMESYSTEMS_H__
#define __GAMESYSTEMS_H__

#include "CollisionBase.h"
#include "SoundBase.h"
#include "SupportBase.h"

#include "HTemplateConsts.h"

#include "ScriptAccess/ScriptDataHolder.h"
#include "Camera/GameCamera.h"

class AchievementUI;

class GameSystems
{
	public:
		GameSystems();
		virtual ~GameSystems();

		static GameSystems *GetInstance( void ) 
		{
			DBG_ASSERT_MSG((ms_Instance != nullptr), "GameSystem instance has not been created");

			return ms_Instance;
		}
		static bool IsInitialised( void ) 
		{
			return (ms_Instance != nullptr);
		}

		void Update( float deltaTime );

		// physics
		void CreatePhysics();
		void DestroyPhysics();	

		// game camera
		void CreateGameCamera();
		void DestroyGameCamera();

		// preload resources
		void PreloadResources();
		void ClearPreloaded();

		// stage
		void SetCorrectStage();

		// audio
		void InitAudio();

		// debug
		void DrawDebug();

		// access

		GameCamera* GetGameCamera();
		const ScriptDataHolder::DevScriptData& GetDevData();
		const GameData& GetGameData();

	private:
		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;
		GameData m_GameData;
		GameCamera* m_GameCamera;

		static GameSystems* ms_Instance;
};

inline GameCamera* GameSystems::GetGameCamera()
{
	return m_GameCamera;
}

inline const ScriptDataHolder::DevScriptData& GameSystems::GetDevData()
{
	return m_DevData;
}

inline const GameData& GameSystems::GetGameData()
{
	return m_GameData;
}

#endif // __GAMESYSTEMS_H__

