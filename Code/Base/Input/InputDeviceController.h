
#ifndef __INPUTDEVICECONTROLLER_H__
#define __INPUTDEVICECONTROLLER_H__

namespace input
{
	class InputDeviceController
	{
		public:
			enum ControllerType
			{
				CONTROLLERTYPE_JOYSTICK,
				CONTROLLERTYPE_GAMEPAD
			};

		public:
			InputDeviceController(SDL_JoystickID instanceID, SDL_Joystick* joystick, SDL_Haptic* pHaptic = nullptr);
			InputDeviceController(SDL_JoystickID instanceID, SDL_GameController* gamepad, SDL_Haptic* pHaptic = nullptr);
			~InputDeviceController();

			void DoRumble(float strength, unsigned int length);

			int GetInstanceId()	const		{ return m_InstanceId; }
			ControllerType GetType() const	{ return m_ControllerType; }
			const char* GetName() const		{ return m_szName; }
			const char* GetGUID() const		{ return m_szGUID; }

			SDL_Joystick* GetJoystickPtr() const			{ return m_pJoystick; }
			SDL_GameController* GetGamePadPtr() const		{ return m_pGamepad; }

			int GetNumButtons()	const		{ return m_NumButtons; }
			int GetNumAxes() const			{ return m_NumAxes; }
			int GetNumHats() const			{ return m_NumHats; }
			int GetNumBalls() const		{ return m_NumBalls; }
			bool HasRumble() const		{ return m_pHaptic != nullptr; }

			void StartBinding()			{ m_IsBindScanning = true; }
			bool IsBinding()			{ return m_IsBindScanning; }
			void StopBinding()			{ m_IsBindScanning = false; }

		private:
			int m_InstanceId;
			ControllerType m_ControllerType;
			SDL_Joystick* m_pJoystick;
			SDL_GameController* m_pGamepad;
			SDL_Haptic* m_pHaptic;
			bool m_RumbleReady;
			unsigned int m_HapticSupportFlags;

			char m_szName[64];
			char m_szGUID[64];

			int m_NumButtons;
			int m_NumAxes;
			int m_NumHats;
			int m_NumBalls;

			bool m_IsBindScanning;
	};
}

#endif // __INPUTDEVICECONTROLLER_H__

