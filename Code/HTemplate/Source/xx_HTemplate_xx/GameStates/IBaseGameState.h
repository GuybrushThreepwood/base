
#ifndef __IBASEGAMESTATE_H__
#define __IBASEGAMESTATE_H__

#ifndef __ISTATE_H__
#include "StateManage/IState.h"
#endif // __ISTATE_H__

#ifndef __STATEMANAGER_H__
#include "StateManage/StateManager.h"
#endif // __STATEMANAGER_H__

#include "InputBase.h"

class IBaseGameState : public IState, public input::InputManager::InputEventCallback
{
	public:
		IBaseGameState( StateManager& stateManager, int stateId=0 )
			: IState( stateManager, stateId )
		{
			input::InputManager::GetInstance()->AddCallback(this);
		}

		virtual ~IBaseGameState()
		{
			input::InputManager::GetInstance()->RemoveCallback(this);
		}

		virtual void DeviceAdded(const input::InputDeviceController *pDevice) 
		{}

		virtual void DeviceRemoved( const input::InputDeviceController *pDevice)
		{}

		virtual void InputControllerButtonDown( const input::InputDeviceController *pDevice, int buttonIndex)
		{}

		virtual void InputControllerButtonUp(const input::InputDeviceController *pDevice, int buttonIndex)
		{}

		virtual void InputControllerAxisMotion(const input::InputDeviceController *pDevice, int axisIndex, signed short value)
		{}

		virtual void InputControllerHatMotion(const input::InputDeviceController *pDevice, unsigned char value)
		{}

	protected:
};

#endif // __IBASEGAMESTATE_H__
