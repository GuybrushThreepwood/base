
/*===================================================================
	File: ScriptAccess.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __SCRIPTACCESS_H__
#define __SCRIPTACCESS_H__

#include "ScriptBase.h"
#include "ScriptAccess/ScriptDataHolder.h"

struct LuaGlobal
{
	const char *szConstantName;
	double nConstantValue;
	int nLuaType;
};

void RegisterScriptFunctions( ScriptDataHolder& dataHolder );

int ScriptSetDevData( lua_State* pState );

ScriptDataHolder* GetScriptDataHolder();

#endif // __SCRIPTACCESS_H__

