
/*===================================================================
	File: HTemplateAccess.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "ScriptAccess/ScriptDataHolder.h"
#include "ScriptAccess/HTemplate/HTemplateAccess.h"

namespace hScript
{
	ScriptDataHolder* pScriptHData = 0;
}

/////////////////////////////////////////////////////
/// Function: RegisterH8Functions
/// Params: [in]dataHolder
///
/////////////////////////////////////////////////////
void RegisterHTemplateFunctions( ScriptDataHolder& dataHolder )
{
	hScript::pScriptHData = &dataHolder;

	script::LuaScripting::GetInstance()->RegisterFunction( "SetGameData", ScriptSetGameData );
	script::LuaScripting::GetInstance()->RegisterFunction("SetInputBindings", ScriptSetInputBindings);
}

/////////////////////////////////////////////////////
/// Function: ScriptSetGameData
/// Params: [in]pState
///
/////////////////////////////////////////////////////
int ScriptSetGameData( lua_State* pState )
{
	if( lua_istable( pState, 1 ) )
	{
		GameData gameData = hScript::pScriptHData->GetGameData();

		// ui
		gameData.ACHIEVEMENT_ICON_MAXX	= static_cast<float>(script::LuaGetNumberFromTableItem( "ACHIEVEMENT_ICON_MAXX", 1 ));
		gameData.ACHIEVEMENT_ICON_SPEED	= static_cast<float>(script::LuaGetNumberFromTableItem( "ACHIEVEMENT_ICON_SPEED", 1 ));
		gameData.ACHIEVEMENT_ICON_SHOWTIME	= static_cast<float>(script::LuaGetNumberFromTableItem( "ACHIEVEMENT_ICON_SHOWTIME", 1 ));

		gameData.ADBAR_PHONE_POSX	= static_cast<float>(script::LuaGetNumberFromTableItem( "ADBAR_PHONE_POSX", 1 ));
		gameData.ADBAR_PHONE_POSY	= static_cast<float>(script::LuaGetNumberFromTableItem( "ADBAR_PHONE_POSY", 1 ));
		gameData.ADBAR_TABLET_POSX	= static_cast<float>(script::LuaGetNumberFromTableItem( "ADBAR_TABLET_POSX", 1 ));
		gameData.ADBAR_TABLET_POSY	= static_cast<float>(script::LuaGetNumberFromTableItem( "ADBAR_TABLET_POSY", 1 ));

		// playlist
		gameData.MAX_PLAYLISTNAME_CHARACTERS	= static_cast<int>(script::LuaGetNumberFromTableItem( "MAX_PLAYLISTNAME_CHARACTERS", 1 ));
		gameData.MAX_SONGNAME_CHARACTERS		= static_cast<int>(script::LuaGetNumberFromTableItem( "MAX_SONGNAME_CHARACTERS", 1 ));

		hScript::pScriptHData->SetGameData( gameData );
	}

	return 0;
}

/////////////////////////////////////////////////////
/// Function: ScriptSetGameData
/// Params: [in]pState
///
/////////////////////////////////////////////////////
int ScriptSetInputBindings(lua_State* pState)
{
	if (lua_istable(pState, 1))
	{
		InputBindings inputBind = hScript::pScriptHData->GetInputBinds();

		// ui
		inputBind.MENU_NEXT = static_cast<int>(script::LuaGetNumberFromTableItem("MENU_NEXT", 1));
		inputBind.MENU_PREVIOUS = static_cast<int>(script::LuaGetNumberFromTableItem("MENU_PREVIOUS", 1));

		hScript::pScriptHData->SetInputBinds(inputBind);
	}

	return 0;
}