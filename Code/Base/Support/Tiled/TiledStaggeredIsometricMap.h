
/*===================================================================
	File: TiledStaggeredIsometricMap.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDSTAGGEREDISOMETRICMAP_H__
#define __TILEDSTAGGEREDISOMETRICMAP_H__
	
#ifdef BASE_SUPPORT_BOX2D

#include "Support/Tiled/TiledBaseLoader.h"
#include "Support/Tiled/TiledCommon.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		class TiledStaggeredIsometricMap : public TiledBaseLoader
		{
		public:
			TiledStaggeredIsometricMap();
			virtual ~TiledStaggeredIsometricMap();

			virtual void DrawLayers(int startLayer = -1, int endLayer = -1);

		protected:
			GLuint m_ShaderProg;

			GLint m_nVertexAttribLocation;
			GLint m_nTexCoordsAttribLocation;
			GLint m_nTexSamplerUniform;
		};
	}
}

#endif // BASE_SUPPORT_BOX2D

#endif // __TILEDSTAGGEREDISOMETRICMAP_H__

