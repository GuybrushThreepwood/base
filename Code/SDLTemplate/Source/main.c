/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL and standard IO
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <GL/glew.h>
#include <windows.h>
#include <SDL.h>
#include <SDL_opengl.h>


//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;

//The window we'll be rendering to
SDL_Window* window = 0;

SDL_GLContext context = 0;

//The surface contained by the window
//SDL_Surface* screenSurface = 0;

//int main( int argc, char* args[] )
int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
{
	SDL_GameController *controller = 0;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0)
	{
		printf( "SDL could not initialize! SDL_Error: %s\n", SDL_GetError() );
	}
	else
	{
		//Use OpenGL 4.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 5);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		//Create window
		window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);
		if( window == NULL )
		{
			printf( "Window could not be created! SDL_Error: %s\n", SDL_GetError() );
		}
		else
		{
			context = SDL_GL_CreateContext(window);
			if (context == NULL)
			{
				printf("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
			}
			else
			{
				//Initialize GLEW
				glewExperimental = GL_TRUE;
				GLenum glewError = glewInit();
				if (glewError != GLEW_OK)
				{
					printf("Error initializing GLEW! %s\n", glewGetErrorString(glewError));
				}

				// GL Core 3.0+ extension handling
				GLint numExt, i;
				glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
				for (i = 0; i < numExt; i++) {

					const char* currExtension = (const char*)glGetStringi(GL_EXTENSIONS, i);
					printf("%s\n", glGetStringi(GL_EXTENSIONS, i));
					OutputDebugString(currExtension);
				}


				//Use Vsync
				/*if (SDL_GL_SetSwapInterval(1) < 0)
				{
					printf("Warning: Unable to set VSync! SDL Error: %s\n", SDL_GetError());
				}*/

				//Initialize OpenGL
				/*if (!initGL())
				{
					printf("Unable to initialize OpenGL!\n");
					success = false;
				}*/
			}

			//Get window surface
			//screenSurface = SDL_GetWindowSurface(window);

			//Main loop flag
			bool quit = false; 

			//Event handler
			SDL_Event e;

			//While application is running
			while (!quit) 
			{
				while (SDL_PollEvent(&e) != 0)
				{
					switch (e.type)
					{
						//User requests quit 
						case SDL_QUIT:
						{
							quit = true;
						}break;
						case SDL_KEYDOWN:
						{
							// key press 
							switch (e.key.keysym.sym)
							{
							case SDLK_ESCAPE:
							{
								quit = true;
							}break;

							default:
								break;
							}
						}break;
						default:
							break;
					}
				}

				glClearColor(1.0f, 1.0f, 0.0f, 0.0f);
				glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

				SDL_GL_SwapWindow(window);
			}
		}


	}

	SDL_GL_DeleteContext(context);

	//Destroy window
	SDL_DestroyWindow( window );

	//Quit SDL subsystems
	SDL_Quit();

	return 0;
}
