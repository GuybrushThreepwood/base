
/*===================================================================
	File: InputAccess.h
	Library: Script

	(C)Hidden Games
=====================================================================*/

#ifndef __INPUTACCESS_H__
#define __INPUTACCESS_H__

namespace script
{
	void RegisterInputFunctions();

} // namespace script

#endif // __INPUTACCESS_H__
