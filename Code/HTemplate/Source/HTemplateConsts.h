
/*===================================================================
	File: HTemplateConsts.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __HTEMPLATECONSTS_H__
#define __HTEMPLATECONSTS_H__

	const float NEAR_CLIP	= 1.0f;
	const float FAR_CLIP	= 100.0f;

	const float NEAR_CLIP_ORTHO	= -6.0f;
	const float FAR_CLIP_ORTHO	= 6.0f;

	const float TOUCH_SIZE_MENU	= 8.0f;
	const float TOUCH_SIZE_GAME	= 8.0f;

	const float PHYSICS_TIMESTEP	= 1.0f / 60.0f;
	const int PHYSICS_ITERATIONS	= 10;

	const int MAX_CONTACTS		= 5;

#endif // __HTEMPLATECONSTS_H__
