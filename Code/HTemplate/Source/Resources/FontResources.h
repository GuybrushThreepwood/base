
#ifndef __FONTRESOURCES_H__
#define __FONTRESOURCES_H__

namespace res
{
	// font info store
	struct FontResourceStore
	{
		const char* resName;
		int fontId;
		renderer::FreetypeFont* fontRender;
		bool loadAllStyles;
	};

	void CreateFontResourceMap();

	void ClearFontResources();

	const FontResourceStore* GetFontResource( int index );

	const FontResourceStore* GetFontResource( int index, int fontSize, bool dropShadow, const glm::vec4& colour, bool fixedWidth );
	
	void RemoveFontResource( const renderer::FreetypeFont* pResource );
}

#endif // __FONTRESOURCES_H__
