
/*===================================================================
	File: MainGameState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __MAINGAMESTATE_H__
#define __MAINGAMESTATE_H__

#include "CoreBase.h"
#include "ScriptAccess/ScriptDataHolder.h"

// forward declare
class IState;
class IBaseGameState;


class MainGameState : public IBaseGameState
{
	public:
		MainGameState( StateManager& stateManager );
		virtual ~MainGameState();

		virtual void DeviceAdded(const input::InputDeviceController *pDevice);
		virtual void DeviceRemoved(const input::InputDeviceController *pDevice);
		virtual void InputControllerButtonDown(const input::InputDeviceController *pDevice, int buttonIndex);
		virtual void InputControllerButtonUp(const input::InputDeviceController *pDevice, int buttonIndex);

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();

	private:
		void CheckAchievements(float deltaTime);

	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;
};

#endif // __MAINGAMESTATE_H__

