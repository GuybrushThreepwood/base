
/*===================================================================
	File: InputManager.cpp
	Library: Input

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "Script/input/InputAccess.h"
#include "Input/InputManager.h"

using input::InputManager;

//InputManager* InputManager::ms_Instance = nullptr;
std::unique_ptr<InputManager> InputManager::ms_Instance = nullptr;

/////////////////////////////////////////////////////
/// Method: Initialise
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::Initialise(void)
{
	DBG_ASSERT((ms_Instance == nullptr));

	DBG_MEMTRY
		//ms_Instance = new InputManager;
		ms_Instance.reset(new InputManager);
	DBG_MEMCATCH
}

/////////////////////////////////////////////////////
/// Method: Shutdown
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::Shutdown(void)
{
	/*if (ms_Instance)
	{
		delete ms_Instance;
		ms_Instance = nullptr;
	}*/

	ms_Instance.reset(nullptr);
	ms_Instance = nullptr;
}

/////////////////////////////////////////////////////
/// Default constructor
/// 
///
/////////////////////////////////////////////////////
InputManager::InputManager()
{
	m_EventCallbacks.clear();
	//int* bleh = new int(2);
}

/////////////////////////////////////////////////////
/// Default destructor
/// 
///
/////////////////////////////////////////////////////
InputManager::~InputManager()
{
	Release();
}

/////////////////////////////////////////////////////
/// Method: Init
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::Init(void)
{
	Release();

	//script::RegisterInputFunctions();
}

/////////////////////////////////////////////////////
/// Method: Release
/// Params:
///
/////////////////////////////////////////////////////
void InputManager::Release(void)
{
	for (int i = 0; i < m_DeviceList.size(); ++i)
	{
		InputDeviceController *pObj = m_DeviceList[i];
		if (pObj != nullptr)
			delete pObj;
	}

	for (auto i = 0; i < m_EventCallbacks.size(); i++)
		delete m_EventCallbacks[i];
	
	m_EventCallbacks.clear();

	m_DeviceList.clear();
}


/////////////////////////////////////////////////////
/// Method: Update
/// Params:
///
/////////////////////////////////////////////////////
void InputManager::Update(float deltaTime)
{

}

/////////////////////////////////////////////////////
/// Method: AddDevice
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::AddDevice(int whichId)
{
	SDL_Joystick* joy = SDL_JoystickOpen(whichId);

	if (joy != nullptr)
	{
		SDL_JoystickID instanceID = SDL_JoystickInstanceID(joy);
		SDL_Haptic *pHaptic = nullptr;

		if (SDL_JoystickIsHaptic(joy))
			pHaptic = SDL_HapticOpen(whichId);

		if (SDL_IsGameController(whichId))
		{
			SDL_GameController* controller = SDL_GameControllerOpen(whichId);
			AddGamepad(instanceID, controller, pHaptic);

			SDL_JoystickClose(joy);
		}
		else
			AddJoystick(instanceID, joy, pHaptic);
	}
}

/////////////////////////////////////////////////////
/// Method: RemoveDevice
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::RemoveDevice(SDL_JoystickID instanceID)
{
	auto it = m_DeviceList.begin();

	InputDeviceController* pObjToDelete = nullptr;

	while (it != m_DeviceList.end())
	{
		if ((*it)->GetInstanceId() == instanceID)
		{
			pObjToDelete = *it;
			it = m_DeviceList.erase(it);
		}
		else
			it++;
	}

	if (pObjToDelete)
	{
		for (auto callback : m_EventCallbacks)
		{
			if (callback != nullptr)
				callback->DeviceRemoved(pObjToDelete);
		}

		delete pObjToDelete;
		pObjToDelete = 0;
	}
}

/////////////////////////////////////////////////////
/// Method: DeviceStillExists
/// Params: None
///
/////////////////////////////////////////////////////
bool InputManager::DeviceStillExists(input::InputDeviceController *pDevice)
{
	if (pDevice == nullptr)
		return false;

	if (m_DeviceList.size() == 0)
		return false;

	const char* szName = pDevice->GetName();
	int instanceID = pDevice->GetInstanceId();

	if (szName == nullptr)
		return false;

	auto it = m_DeviceList.begin();
	while (it != m_DeviceList.end())
	{
		if (((*it)->GetInstanceId() == instanceID) ||
			(strcmp((*it)->GetName(), szName) == 0))
		{
			return true;
		}
		it++;
	}

	return false;
}

/////////////////////////////////////////////////////
/// Method: AddJoystick
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::AddJoystick(SDL_JoystickID instanceID, SDL_Joystick* pJoy, SDL_Haptic* pHaptic)
{
	if (pJoy)
	{
		const char* szName = SDL_JoystickName(pJoy);

		if (szName == nullptr)
			return;

		bool exists = false;

		auto it = m_DeviceList.begin();
		while (it != m_DeviceList.end())
		{
			if (((*it)->GetInstanceId() == instanceID) ||
				(strcmp((*it)->GetName(), szName) == 0))
			{
				exists = true;
			}
			it++;
		}

		if (!exists)
		{
			InputDeviceController *pNewJoy = new InputDeviceController(instanceID, pJoy, pHaptic);
			m_DeviceList.push_back(pNewJoy);

			for (auto callback : m_EventCallbacks)
			{
				if (callback != nullptr)
					callback->DeviceAdded(pNewJoy);
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: AddGamepad
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::AddGamepad(SDL_JoystickID instanceID, SDL_GameController* pController, SDL_Haptic* pHaptic)
{
	if (pController)
	{
		const char* szName = SDL_GameControllerName(pController);

		if (szName == nullptr)
			return;

		bool exists = false;

		auto it = m_DeviceList.begin();
		while (it != m_DeviceList.end())
		{
			if (((*it)->GetInstanceId() == instanceID) ||
				(strcmp((*it)->GetName(), szName) == 0))
			{
				exists = true;
			}
			it++;
		}

		if (!exists)
		{
			InputDeviceController *pNewJoy = new InputDeviceController(instanceID, pController, pHaptic);
			m_DeviceList.push_back(pNewJoy);

			for (auto callback : m_EventCallbacks)
			{
				if (callback != nullptr)
					callback->DeviceAdded(pNewJoy);
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: ButtonEvent
/// Params: None
///
/////////////////////////////////////////////////////
input::InputDeviceController* InputManager::FindDevice(int instanceID)
{
	auto it = m_DeviceList.begin();
	while (it != m_DeviceList.end())
	{
		if (((*it)->GetInstanceId() == instanceID))
		{
			return (*it);
		}
		it++;
	}

	return 0;
}

/////////////////////////////////////////////////////
/// Method: ButtonEvent
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::ButtonEvent(int instanceID, int buttonIndex, bool state)
{
	for (auto callback : m_EventCallbacks)
	{
		if (callback != nullptr)
		{
			input::InputDeviceController* pDevice = FindDevice(instanceID);

			if (pDevice != nullptr)
			{
				if (state)
					callback->InputControllerButtonDown(pDevice, buttonIndex);
				else
					callback->InputControllerButtonUp(pDevice, buttonIndex);
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: AxisEvent
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::AxisEvent(int instanceID, int axisIndex, signed short value)
{
	for (auto callback : m_EventCallbacks)
	{
		if (callback != nullptr)
		{
			input::InputDeviceController* pDevice = FindDevice(instanceID);

			if (pDevice != nullptr)
				callback->InputControllerAxisMotion(pDevice, axisIndex, value);
		}
	}
}

/////////////////////////////////////////////////////
/// Method: HatEvent
/// Params: None
///
/////////////////////////////////////////////////////
void InputManager::HatEvent(int instanceID, unsigned char value)
{
	for (auto callback : m_EventCallbacks)
	{
		if (callback != nullptr)
		{
			input::InputDeviceController* pDevice = FindDevice(instanceID);

			if (pDevice != nullptr)
				callback->InputControllerHatMotion(pDevice, value);
		}
    }
}

/////////////////////////////////////////////////////
/// Method: BindCheck
/// Params: None
///
/////////////////////////////////////////////////////
bool InputManager::BindCheck(int btnBind, input::InputDeviceController *pPreferredDevice)
{
	if (pPreferredDevice != nullptr)
	{
		if (pPreferredDevice->GetType() == input::InputDeviceController::CONTROLLERTYPE_GAMEPAD)
		{
			SDL_GameController *controller = pPreferredDevice->GetGamePadPtr();

			if (controller != nullptr)
			{
				if (btnBind >= SDL_CONTROLLER_BUTTON_A &&
					btnBind < SDL_CONTROLLER_BUTTON_MAX)
					return (SDL_GameControllerGetButton(controller, static_cast<SDL_GameControllerButton>(btnBind)) != 0);
			}
		}
		else
		if (pPreferredDevice->GetType() == input::InputDeviceController::CONTROLLERTYPE_JOYSTICK)
		{
			SDL_Joystick *joy = pPreferredDevice->GetJoystickPtr();

			if (joy != nullptr)
			{
				int numButtons = SDL_JoystickNumButtons(joy);
				if (btnBind >= 0 &&
					btnBind < numButtons)
				{
					int state = SDL_JoystickGetButton(joy, btnBind);
					return (state != 0);
				}
			}
		}
	}
	else
	{
		auto it = m_DeviceList.begin();
		while (it != m_DeviceList.end())
		{
			InputDeviceController *pCurrentDevice = (*it);
			if (pCurrentDevice != nullptr)
			{
				if (pCurrentDevice->GetType() == input::InputDeviceController::CONTROLLERTYPE_GAMEPAD)
				{
					SDL_GameController *controller = pCurrentDevice->GetGamePadPtr();

					if (controller != nullptr)
					{
						if (btnBind >= SDL_CONTROLLER_BUTTON_A &&
							btnBind < SDL_CONTROLLER_BUTTON_MAX)
						{
							if (SDL_GameControllerGetButton(controller, static_cast<SDL_GameControllerButton>(btnBind)))
								return true;
						}
					}
				}
				else
				if (pCurrentDevice->GetType() == input::InputDeviceController::CONTROLLERTYPE_JOYSTICK)
				{
					SDL_Joystick *joy = pCurrentDevice->GetJoystickPtr();

					if (joy != nullptr)
					{
						int numButtons = SDL_JoystickNumButtons(joy);
						if (btnBind >= 0 &&
							btnBind < numButtons)
						{
							if (SDL_JoystickGetButton(joy, btnBind))
								return true;							
						}
					}
				}
			}
			it++;
		}
	}
	return false;
}

