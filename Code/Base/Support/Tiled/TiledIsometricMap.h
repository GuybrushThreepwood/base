
/*===================================================================
	File: TiledIsometricMap.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDISOMETRICMAP_H__
#define __TILEDISOMETRICMAP_H__
	
#ifdef BASE_SUPPORT_BOX2D

#include "Support/Tiled/TiledBaseLoader.h"
#include "Support/Tiled/TiledCommon.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		class TiledIsometricMap : public TiledBaseLoader
		{
		public:
			TiledIsometricMap();
			virtual ~TiledIsometricMap();

			virtual void DrawLayers(int startLayer = -1, int endLayer = -1);

		protected:
			GLuint m_ShaderProg;

			GLint m_nVertexAttribLocation;
			GLint m_nTexCoordsAttribLocation;
			GLint m_nTexSamplerUniform;
		};
	}
}

#endif // BASE_SUPPORT_BOX2D
#endif // __TILEDISOMETRICMAP_H__

