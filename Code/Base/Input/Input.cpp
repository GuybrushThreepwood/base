
/*===================================================================
	File: Input.cpp
	Library: Input

	(C)Hidden Games
=====================================================================*/

#include "Core/CoreDefines.h"

#include "Core/App.h"
#include "Debug/Assertion.h"
#include "Core/CoreFunctions.h"

#include "Input/Input.h"

using input::Input;

/////////////////////////////////////////////////////
/// Default constructor
/// 
///
/////////////////////////////////////////////////////
Input::Input()
{		
}

/////////////////////////////////////////////////////
/// Default destructor
/// 
///
/////////////////////////////////////////////////////
Input::~Input()
{

}

/////////////////////////////////////////////////////
/// Method: IsKeyPressed
/// Params: [in]nKeyCode, [in]bIgnoreHeld
///
/////////////////////////////////////////////////////
bool Input::IsKeyState(int keyCode, input::InputEventType state)
{
	return false;
}

/////////////////////////////////////////////////////
/// Method: IsLeftMouseButtonPressed
/// Params: None
///
/////////////////////////////////////////////////////
bool Input::IsMouseButtonState(input::MouseButtons buttonId, input::InputEventType state)
{
	return false;
}
