
#ifndef __STRINGRESOURCES_H__
#define __STRINGRESOURCES_H__

namespace res
{
	void CreateStringResourceMap();

	void ClearStringResources();

	const char* GetScriptString( int index );
}

#endif // __STRINGRESOURCES_H__

