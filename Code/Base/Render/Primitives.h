
#ifndef __PRIMITIVES_H__
#define __PRIMITIVES_H__

namespace renderer
{
	/// InitialisePrimitives - loads the shaders for rendering primitives
	void InitialisePrimitives();
	/// ShutdownPrimitives - releases the shaders for rendering primitives
	void ShutdownPrimitives();

	/// GetBox2DProgram - get the shader program for box2d
	/// \return GLuint - program id
	GLuint GetBox2DProgram();

	/// GetODEProgram - gets the shader program for ode
	/// \return GLuint - program id
	GLuint GetODEProgram();

	/// DrawSphere - draws a sphere
	/// \param radius - radius of the sphere
	void DrawSphere( float radius );

	/// DrawAABB - draws an AABB
	/// \param vBoxMin - min bounds of box
	/// \param vBoxMax - max bounds of box
	/// \param fillBox - whether to draw as a filled box
	void DrawAABB(const glm::vec3& vBoxMin, const glm::vec3& vBoxMax, bool fillBox = false);

	/// DrawOBB - draws an OBB
	/// \param vCenter - center of box
	/// \param vAxis - axis rotation
	/// \param vHalfWidths - max bounds of box
	void DrawOBB(const glm::vec3& vCenter, const glm::vec3 vAxis[3], const glm::vec3& vHalfWidths);

	/// DrawLine - draws a line
	/// \param start - start point of the line
	/// \param end - end point of the line
	/// \param colour - colour of the colour
	void DrawLine(const glm::vec3 &start, const glm::vec3 &end, const glm::vec4& colour);

} // namespace renderer

#endif // __PRIMITIVES_H__
