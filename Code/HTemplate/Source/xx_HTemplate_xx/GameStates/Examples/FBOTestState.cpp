
/*===================================================================
	File: FBOTestState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "Resources/StringResources.h"
#include "GameStates/IBaseGameState.h"
#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/Examples/FBOTestState.h"

namespace
{
	GLuint progId0 = renderer::INVALID_OBJECT;

	renderer::RenderToTexture rttColour;
	renderer::RenderToTexture rttDepth;
	
	glm::vec4 lightDir(0.0f, 1.0f, 1.0f, 1.0f);
	glm::vec3 lightSpotDir(0.0f, -1.0f, 0.0f);
	bool isSpot=false;

	const int FBO_SIZE = 256;
	renderer::RenderToTexture* pActiveRTT = 0;

	const char* description = "This is a render to texture example, it renders the scene to a low res texture and displays a fullscreen quad\nwith that texture.\n\n- A/D or first touch or mouse x + left button held rotates the scene\n- Press space, right click or second touch to change the render type between colour+depth and depth only \n(if depth texture is supported)";
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
FBOTestState::FBOTestState( StateManager& stateManager )
: IBaseGameState( stateManager, ID_FBOTESTSTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
FBOTestState::~FBOTestState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void FBOTestState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

	renderer::OpenGL::GetInstance()->ClearColour( 0.5f, 0.5f, 0.5f, 1.0f );

	glm::vec3 camPos = glm::vec3(1.0f, 1.0f, 2.0f);
	glm::vec3 camTarget = glm::vec3(0.0f, 1.0f, 0.0f);
	m_GameCamera->SetLerp(false);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );

	rttColour.Initialise();
	rttColour.Create( FBO_SIZE,FBO_SIZE, (renderer::RTTFLAG_CREATE_COLOURBUFFER | renderer::RTTFLAG_CREATE_DEPTHBUFFER), GL_RGBA, GL_RGBA, GL_UNSIGNED_BYTE, GL_CLAMP_TO_EDGE, false );
	
	rttDepth.Initialise();
	rttDepth.CreateDepthOnly( FBO_SIZE,FBO_SIZE );

	pActiveRTT = &rttColour;
	
	// load from script
	renderer::InititaliseFBOShader();

	progId0 = renderer::LoadShaderFilesForProgram( "shaders/perpixel_pointlight.vert", "shaders/perpixel_pointlight.frag" );
	renderer::OpenGL::GetInstance()->UseProgram(progId0);


	GL_CHECK

	glm::vec4 lightAmbient( 1.0f, 1.0f, 1.0f, 1.0f );
	glm::vec4 lightDiffuse(1.0f, 1.0f, 1.0f, 1.0f);
	glm::vec4 lightSpecular(1.0f, 1.0f, 1.0f, 1.0f);

	renderer::OpenGL::GetInstance()->SetLightAmbient( 0, lightAmbient );
	renderer::OpenGL::GetInstance()->SetLightDiffuse( 0, lightDiffuse );
	renderer::OpenGL::GetInstance()->SetLightSpecular( 0, lightSpecular ); 

	renderer::OpenGL::GetInstance()->SetLightAttenuation( 0, GL_CONSTANT_ATTENUATION, 1.0f );
	renderer::OpenGL::GetInstance()->SetLightAttenuation( 0, GL_LINEAR_ATTENUATION, 0.0f );
	renderer::OpenGL::GetInstance()->SetLightAttenuation( 0, GL_QUADRATIC_ATTENUATION, 0.0f );

	GL_CHECK

	// set the description
	/*IState* pCurrState = m_pStateManager->GetSecondaryStateManager()->GetCurrentState();
	if( pCurrState->GetId() == UI_TITLESCREEN )
	{
		FrontendTitleScreenUI* pState = static_cast<FrontendTitleScreenUI*>( pCurrState );
		if( pState != 0 )
		{
			pState->SetDescriptionString( description );
		}
	}*/
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void FBOTestState::Exit()
{
	m_GameCamera = 0;

	rttColour.Release();
	rttDepth.Release();
	
	pActiveRTT = 0;

}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int FBOTestState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int FBOTestState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void FBOTestState::Update( float deltaTime )
{
	m_LastDelta = deltaTime;

	m_GameCamera->Update( deltaTime );

}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void FBOTestState::Draw()
{
	// default colour
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	if( m_DevData.allowDebugCam && gDebugCamera.IsEnabled() )
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 0.1f, 100000.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		renderer::OpenGL::GetInstance()->SetLookAt( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z, 
												gDebugCamera.GetTarget().x, gDebugCamera.GetTarget().y, gDebugCamera.GetTarget().z );


		snd::SoundManager::GetInstance()->SetListenerPosition( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z );
		snd::SoundManager::GetInstance()->SetListenerOrientation((float)std::sin(glm::radians(-gDebugCamera.GetAngle())), 0.0f, (float)std::cos(glm::radians(-gDebugCamera.GetAngle())),
																	0.0f, 1.0f, 0.0f );
	}
	else
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 0.1f, 100.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		m_GameCamera->SetupCamera();

		gDebugCamera.SetPosition( m_GameCamera->GetPosition() );
		gDebugCamera.SetTarget( m_GameCamera->GetTarget() );

		snd::SoundManager::GetInstance()->SetListenerOrientation(std::sin(glm::radians(180.0f)), 0.0f, std::cos(glm::radians(180.0f)), 0.0f, 1.0f, 0.0f);
	}
	
	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LESS );
	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->EnableTexturing();
	renderer::OpenGL::GetInstance()->EnableLighting();
	renderer::OpenGL::GetInstance()->EnableLight(0);

	pActiveRTT->StartRenderToTexture();

		renderer::OpenGL::GetInstance()->SetNearFarClip( 1.0f, 10.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( FBO_SIZE, FBO_SIZE, true );
		m_GameCamera->SetupCamera();
		
	static float angleY = 0.0f;

	renderer::OpenGL::GetInstance()->UseProgram(progId0);

	renderer::OpenGL::GetInstance()->SetLightPosition( 0, lightDir );
	if( isSpot )
	{
		renderer::OpenGL::GetInstance()->SetLightSpotDirection( 0, lightSpotDir ); 
		renderer::OpenGL::GetInstance()->SetLightSpotCutoff( 0, 45.0 ); 
		renderer::OpenGL::GetInstance()->SetLightSpotExponent( 0, 10.0 ); 
	}
	glm::mat4 mdlMatrix = glm::mat4(1.0f);
	mdlMatrix = glm::rotate( mdlMatrix, angleY, glm::vec3(0.0f, 1.0f, 0.0f) );
	renderer::OpenGL::GetInstance()->SetModelMatrix( mdlMatrix );
	
	pActiveRTT->EndRenderToTexture();

	renderer::OpenGL::GetInstance()->EnableTexturing();
	renderer::OpenGL::GetInstance()->SetNearFarClip( NEAR_CLIP_ORTHO, FAR_CLIP_ORTHO );
	renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );
	renderer::OpenGL::GetInstance()->DisableLighting();
	
	pActiveRTT->RenderToScreen(0);

	GL_CHECK;
		
	int offset = 10;
	int xPos;
	int yPos;

	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;

	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	DBGPRINT( xPos, yPos-=offset, "DELTA (%.6f)", m_LastDelta );
	DBGPRINT( xPos, yPos-=offset, "CAM POS (%.2f  %.2f  %.2f)", m_GameCamera->GetPosition().x, m_GameCamera->GetPosition().y, m_GameCamera->GetPosition().z );
	DBGPRINT( xPos, yPos-=offset, "CAM TARGET (%.2f  %.2f  %.2f)", m_GameCamera->GetTarget().x, m_GameCamera->GetTarget().y, m_GameCamera->GetTarget().z );
	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

/////////////////////////////////////////////////////
/// Method: ChangeRTT
/// Params: None
///
/////////////////////////////////////////////////////
void FBOTestState::ChangeRTT()
{
	if( pActiveRTT == &rttColour )
	{
		pActiveRTT = &rttDepth;
	}
	else
	{
		pActiveRTT = &rttColour;
	}
}

