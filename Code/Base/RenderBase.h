
#ifndef __RENDERBASE_H__
#define __RENDERBASE_H__

#ifndef __DEBUGBASE_H__
	#include "DebugBase.h"
#endif // __DEBUGBASE_H__

#ifndef __PLATFORMRENDERBASE_H__
	#include "Render/PlatformRenderBase.h"
#endif // __PLATFORMRENDERBASE_H__

#ifndef __RENDERCONSTS_H__
	#include "Render/RenderConsts.h"
#endif // __RENDERCONSTS_H__

#ifndef __OPENGLCOMMON_H__
	#include "Render/OpenGLCommon.h"
#endif // __OPENGLCOMMON_H__

#ifndef __TEXTURESHARED_H__
	#include "Render/TextureShared.h"
#endif // __TEXTURESHARED_H__

#ifndef __TEXTURE_H__
	#include "Render/Texture.h"
#endif // __TEXTURE_H__

#ifndef __TEXTUREATLAS_H__
	#include "Render/TextureAtlas.h"
#endif // __TEXTUREATLAS_H__
	
#ifndef __SHADERSHARED_H__
	#include "Render/ShaderShared.h"
#endif // __SHADERSHARED_H__

#ifndef __SHADER_H__
#include "Render/Shader.h"
#endif // __SHADER_H__

#ifndef __OPENGL_H__
	#include "Render/OpenGL.h"
#endif // __OPENGL_H__

#ifndef __PRIMITIVES_H__
	#include "Render/Primitives.h"
#endif // __PRIMITIVES_H__

#ifndef __TEXTURELOADANDUPLOAD_H__
	#include "Render/TextureLoadAndUpload.h"
#endif // __TEXTURELOADANDUPLOAD_H__

#ifndef __GLEWES_H__
	#include "Render/glewES.h"
#endif // __GLEWES_H__

#ifndef __RENDERTOTEXTURE_H__
	#include "Render/RenderToTexture.h"
#endif // __RENDERTOTEXTURE_H__

#ifdef BASE_SUPPORT_FREETYPE
	#ifndef __FREETYPEFONT_H__
		#include "Render/FreetypeCommon.h"
	#endif // __FREETYPEFONT_H__

	#ifndef __FREETYPEFONT_H__
		#include "Render/FreetypeFont.h"
	#endif // __FREETYPEFONT_H__

    #ifndef __FREETYPEFONTZZIP_H__
        #include "Render/FreetypeFontZZip.h"
    #endif // __FREETYPEFONTZZIP_H__

    #ifndef __FREETYPEZZIPACCESS_H__
        #include "Render/FreetypeZZipAccess.h"
    #endif // __FREETYPEZZIPACCESS_H__
#endif // BASE_SUPPORT_FREETYPE

#endif // __RENDERBASE_H__
