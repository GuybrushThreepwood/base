
#ifndef __SOUNDBASE_H__
#define __SOUNDBASE_H__

#ifndef __SOUNDCOMMON_H__
	#include "Sound/SoundCommon.h"
#endif // __SOUNDCOMMON_H__

#ifndef __SOUNDCODECS_H__
	#include "Sound/SoundCodecs.h"
#endif // __SOUNDCODECS_H__

#ifndef __OPENAL_H__
	#include "Sound/OpenAL.h"
#endif // __OPENAL_H__

#ifndef __SOUND_H__
	#include "Sound/Sound.h"
#endif // __SOUND_H__

#ifndef __SOUNDSTREAM_H__
	#include "Sound/SoundStream.h"
#endif // __SOUNDSTREAM_H__

#ifndef __SOUNDLOADANDUPLOAD_H__
	#include "Sound/SoundLoadAndUpload.h"
#endif // __SOUNDLOADANDUPLOAD_H__

#ifndef __SOUNDMANAGER_H__
	#include "Sound/SoundManager.h"
#endif // __SOUNDMANAGER_H__

#ifndef __AUDIOSYSTEM_H__
	#include "Sound/AudioSystem.h"
#endif // __AUDIOSYSTEM_H__

#endif // __SOUNDBASE_H__