
/*===================================================================
	File: FontFormatExampleState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"
#include "Camera/GameCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "Resources/FontResources.h"
#include "GameStates/IBaseGameState.h"
#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/Examples/FontFormatTestState.h"

namespace
{
	const int FONT_SIZE = 48;

	const char* testText = "This is an example of some dynamic font changes. The system now has the ability to auto word wrap and also change formatting features such as colour, bold, italic and underline.";
	//const char* testText = "auto word wrap and also change formatting";

	const char* description = "- Space, right click or second touch to change the font style";

	std::vector<TFormatChangeBlock> changeList;
}

/////////////////////////////////////////////////////
/// Method: WordWrap
/// Params: None
///
/////////////////////////////////////////////////////
std::string FontFormatTestState::WordWrap( std::string& text, renderer::FreetypeFont* pFont, std::vector<TFormatChangeBlock>& formatChanges, int maxWidth )
{
    int numPixelsSinceNewLine = 0;
    std::string output = text;
    std::size_t i=0, j=0;
    int numInserts = 0;

    int totalHeightUsed = 0;

	// initial block
	int style = FONT_STYLE_NORMAL;
	TFormatChangeBlock block;
	block.style = FONT_STYLE_NORMAL;
    block.numCharsInBlockText = static_cast<int>(text.length());

	bool hasChanges = false;
	int currentChangeIndex = 0;
    
	if( formatChanges.size() != 0 )
	{
		hasChanges = true;

		style = formatChanges[currentChangeIndex].style;
		block.numCharsInBlockText = formatChanges[currentChangeIndex].numCharsInBlockText;
	}

	int currentCharChangeOffset = 0;

	// go through all the text
    for( i=0; i < text.length(); ++i )
    {
		if( hasChanges )
		{
			if( currentCharChangeOffset >= block.numCharsInBlockText ) // work out how many characters are in this formatting block
			{
				currentChangeIndex++;

				if( currentChangeIndex < static_cast<int>( formatChanges.size() ) )
				{
					style = formatChanges[currentChangeIndex].style;
					block.numCharsInBlockText = formatChanges[currentChangeIndex].numCharsInBlockText;
					currentCharChangeOffset = 0;
				}
			}
		}

		// get the current character width
        int curW = 0;
        pFont->GetCharacterWidth( text[i], &curW, style );

        // manual newline
        if( text[i] == '\n' )
        {
            totalHeightUsed += m_CurrentFontSize;

            numPixelsSinceNewLine = 0;
            continue;
        }

        // keep a space on the previous line
        if( text[i] == ' ' &&
            numPixelsSinceNewLine >= maxWidth )
        {
            output.insert( (i+1)+numInserts, 1, '\n' );
            numInserts++;

            totalHeightUsed += m_CurrentFontSize;

            numPixelsSinceNewLine = 0;
            continue;
        }

		currentCharChangeOffset++;

        // filled the line with enough characters
        if( numPixelsSinceNewLine >= maxWidth )
        {
            if( (i+1) < text.length() &&
                text[i+1] != '\n' )
            {
                // next character is not a newline or the end

                // trace back to find the start of the current word
                int totalBackWidth = 0;
                bool toInsert = false;

                for( j = (i+numInserts); ; j-- )
                {
                    int prevW = 0;
					pFont->GetCharacterWidth( output[j], &prevW );

                    if( j <= 0 || // hit the start of all the text
                        totalBackWidth >= maxWidth || // hit enough to be a newline
                        output[j] == '\n') // hit a previous newline
                    {
                        toInsert = true;
                        break;
                    }

                    // found the previous space without hitting the logic above
                    if( output[j] == ' ')
                    {
                        toInsert = false;

                        // add a newline
                        output.insert( j+1, 1, '\n' );
                        numInserts++;

                        totalHeightUsed += m_CurrentFontSize;

                        numPixelsSinceNewLine = totalBackWidth;
                        break;
                    }

                    totalBackWidth += prevW;
                }

                // insert a newline
                if( toInsert )
                {
                    output.insert( i+numInserts, 1, '\n' );
                    numInserts++;

                    totalHeightUsed += m_CurrentFontSize;
                }
                else
                {
                    // we inserted, continue to stop the reset of numCharsSinceNewLine

                    continue;
                }
            }
            else
            {
                // the next char is a newline or space will autobreak
            }

            // reset the count
            numPixelsSinceNewLine = 0;
            continue;
        }

        // increase characters used on this line
        numPixelsSinceNewLine += curW;
    }

    //m_MaxTextHeight = static_cast<float>(totalHeightUsed);

    return output;
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
FontFormatTestState::FontFormatTestState( StateManager& stateManager )
: IBaseGameState( stateManager, ID_FONTFORMATTESTSTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;

	m_CurrentFontSize = FONT_SIZE;
	m_MaxTextHeight = 0;

	m_ViewPos.x = 0;
	m_ViewPos.y = 0;
	m_ViewPos.z = 0;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
FontFormatTestState::~FontFormatTestState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void FontFormatTestState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

	renderer::OpenGL::GetInstance()->ClearColour( 0.5f, 0.5f, 0.5f, 1.0f );

	glm::vec3 camPos = glm::vec3(0, 0, 20);
	glm::vec3 camTarget = glm::vec3(0, 0, -20);
	m_GameCamera->SetLerp(true);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );

	glm::vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
	const res::FontResourceStore* pFR = res:: GetFontResource( 1, m_CurrentFontSize, 0, white, false );
	m_FontTest = pFR->fontRender;

	TFormatChangeBlock block;
	block.style = FONT_STYLE_NORMAL;
	block.underline = false;
	block.textColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
	block.numCharsInBlockText = static_cast<int>( strlen(testText) );
	block.bgColour = false;
	block.textBGColour = glm::vec4(0.0f, 0.0f, 0.0f, 1.0f);
	changeList.push_back(block);
	m_ChangeIndex = 0;

	/*IState* pCurrState = m_pStateManager->GetSecondaryStateManager()->GetCurrentState();
	if( pCurrState->GetId() == UI_TITLESCREEN )
	{
		FrontendTitleScreenUI* pState = static_cast<FrontendTitleScreenUI*>( pCurrState );
		if( pState != 0 )
		{
			pState->SetDescriptionString( description );
		}
	}*/

	// TEST
	int xPos;
	int yPos;

	int maxWidth = 0;

	xPos = 5;
	yPos = core::app::GetFrameHeight() - m_CurrentFontSize;

	maxWidth = (core::app::GetFrameWidth() - m_CurrentFontSize * 2) - xPos;

	std::string in = std::string(testText);
	std::string out = WordWrap(in, m_FontTest, changeList, maxWidth);
	m_FontTest->BuildBufferCache(xPos, yPos, false, changeList, out);

	//renderer::OpenGL::GetInstance()->SetMirrored(true);
	//renderer::OpenGL::GetInstance()->SetMirrorStyle( static_cast<renderer::EViewMirror>(renderer::VIEWMIRROR_HORIZONTAL | renderer::VIEWMIRROR_VERTICAL) );
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void FontFormatTestState::Exit()
{
	m_GameCamera = 0;
	
	changeList.clear();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int FontFormatTestState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int FontFormatTestState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void FontFormatTestState::Update( float deltaTime )
{
	m_LastDelta = deltaTime;

	const float SCROLL_SPEED = 200.0f;
	//const float TOUCH_DELTA = 3;

	/*if( input::gInputState.TouchesData[input::SECOND_TOUCH].bRelease ||
	   debugInput.IsKeyPressed( input::KEY_SPACE, true, true ) )
	{
		changeList.clear();

		TFormatChangeBlock block;
		m_ChangeIndex++;
		if( m_ChangeIndex > 4 )
			m_ChangeIndex = 0;

		if( m_ChangeIndex == 0 )
		{
			block.style = FONT_STYLE_NORMAL;
			block.underline = false;
			block.textColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			block.numCharsInBlockText = static_cast<int>( strlen(testText) );
			block.bgColour = false;
			changeList.push_back(block);
		}
		else
		if( m_ChangeIndex == 1 )
		{
			block.style = FONT_STYLE_BOLD;
			block.underline = false;
			block.textColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			block.numCharsInBlockText = static_cast<int>( strlen(testText) );
			block.bgColour = false;
			changeList.push_back(block);
		}
		else
		if( m_ChangeIndex == 2 )
		{
			block.style = FONT_STYLE_ITALIC;
			block.underline = false;
			block.textColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			block.numCharsInBlockText = static_cast<int>( strlen(testText) );
			block.bgColour = false;
			changeList.push_back(block);
		}
		else
		if( m_ChangeIndex == 3 )
		{
			block.style = FONT_STYLE_NORMAL;
			block.underline = true;
			block.textColour = glm::vec4(1.0f, 1.0f, 1.0f, 1.0f);
			block.numCharsInBlockText = static_cast<int>( strlen(testText) );
			block.bgColour = false;
			changeList.push_back(block);
		}
		else
		if( m_ChangeIndex == 4 )
		{
			for( int i=0; i < 10; ++i )
			{
				block.style = static_cast<EFontStyle>(glm::linearRand(FONT_STYLE_NORMAL, FONT_STYLE_ITALIC_BOLD));
				block.underline = rand()%2 != 0;
				block.textColour = glm::vec4(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), 1.0f );
				block.numCharsInBlockText = glm::linearRand(10, 20);
				block.bgColour = rand()%2 != 0;
				block.textBGColour = glm::vec4(glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), glm::linearRand(0.0f, 1.0f), 1.0f);
				changeList.push_back(block);
			}
		}

		int xPos;
		int yPos;

		int maxWidth = 0;

		xPos = 5;
		yPos = core::app::GetFrameHeight() - m_CurrentFontSize;

		maxWidth = (core::app::GetFrameWidth() - m_CurrentFontSize * 2) - xPos;

		std::string in = std::string(testText);
		std::string out = WordWrap(in, m_FontTest, changeList, maxWidth);
		m_FontTest->BuildBufferCache(xPos, yPos, false, changeList, out);

	}*/

	m_GameCamera->Update( deltaTime );
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void FontFormatTestState::Draw()
{
	// default colour
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	renderer::OpenGL::GetInstance()->SetNearFarClip( -6.0f, 6.0f );
    renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );
    renderer::OpenGL::GetInstance()->DisableLighting();

    renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->SetCullState(false, GL_FRONT_AND_BACK);

    int xPos;
    int yPos;

	int maxWidth = 0;
	
	xPos = 5;
	yPos = core::app::GetFrameHeight() - m_CurrentFontSize;
		
	maxWidth = (core::app::GetFrameWidth()-m_CurrentFontSize*2)-xPos;

	//std::string in = std::string( testText );
	//std::string out = WordWrap( in, m_FontTest, changeList, maxWidth );

	glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(-m_ViewPos.x, m_ViewPos.y, 0.0f));
	renderer::OpenGL::GetInstance()->SetModelMatrix(modelMatrix);

	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LEQUAL );
	m_FontTest->RenderBatches();
	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LESS );

	xPos = core::app::GetFrameWidth() - 50;
	yPos = core::app::GetFrameHeight() - 60;

	GL_CHECK;
		
	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;
	int offset = 10;

    modelMatrix = glm::mat4(1.0f);
	renderer::OpenGL::GetInstance()->SetModelMatrix(modelMatrix);

	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	DBGPRINT( xPos, yPos-=offset, "DELTA (%.6f)", m_LastDelta );
	DBGPRINT( xPos, yPos-=offset, "CAM POS (%.2f  %.2f  %.2f)", m_GameCamera->GetPosition().x, m_GameCamera->GetPosition().y, m_GameCamera->GetPosition().z );
	DBGPRINT( xPos, yPos-=offset, "CAM TARGET (%.2f  %.2f  %.2f)", m_GameCamera->GetTarget().x, m_GameCamera->GetTarget().y, m_GameCamera->GetTarget().z );
	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}

