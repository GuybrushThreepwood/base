
/*===================================================================
	File: InputTestState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __INPUTTESTSTATE_H__
#define __INPUTTESTSTATE_H__

#include "ScriptAccess/ScriptDataHolder.h"

// forward declare
class IState;
class IBaseGameState;

class InputTestState : public IBaseGameState
{
	public:
		InputTestState(StateManager& stateManager);
		virtual ~InputTestState();

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();

		/*virtual void DeviceAdded(input::InputDeviceController *pDevice);
		virtual void DeviceRemoved(input::InputDeviceController *pDevice);

		virtual void InputControllerButtonDown(input::InputDeviceController *pDevice, int buttonIndex);
		virtual void InputControllerButtonUp(input::InputDeviceController *pDevice, int buttonIndex);
		virtual void InputControllerAxisMotion(input::InputDeviceController *pDevice, int axisIndex, signed short value);
        virtual void InputControllerHatMotion(input::InputDeviceController *pDevice, unsigned char value);*/
    
	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;

		renderer::FreetypeFont* m_FontTest;
		int m_CurrentFontSize;

		int m_SelectedIndex;
		input::InputDeviceController* m_pSelectedJoypad;

		std::vector<input::InputDeviceController *> m_DeviceList;
};

#endif // __INPUTTESTSTATE_H__

