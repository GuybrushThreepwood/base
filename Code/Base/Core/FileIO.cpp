
/*===================================================================
	File: FileIO.cpp
	Library: File

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "DebugBase.h"

#include "Core/FileIO.h"

// some trackers to make sure files are correctly opened/closed
static unsigned int nFilesOpened = 0;
static unsigned int nFilesClosed = 0;

namespace
{
	// zzip file opening
	static zzip_strings_t zipFileExt[] = { ".zip", ".ZIP", "", nullptr };
	static char szConvertedFilename[core::MAX_PATH+core::MAX_PATH];
	static int o_flags = O_RDONLY;	// rb
	static int o_modes = 0664;
}

/////////////////////////////////////////////////////
/// Function: GetZipExtensions
/// Params: None
///
/////////////////////////////////////////////////////
zzip_strings_t* file::GetZipExtensions()
{
	return zipFileExt;
}

/////////////////////////////////////////////////////
/// Function: ReportFileCounts
/// Params: None
///
/////////////////////////////////////////////////////
void file::ReportFileCounts()
{
	DBGLOG( "FILEIO: Files opened %d\n", nFilesOpened );
	DBGLOG( "FILEIO: Files closed %d\n", nFilesClosed );

	if( nFilesClosed != nFilesOpened )
	{
		DBGLOG( "FILEIO: *WARNING* Not all file handles were closed\n" );
	}
}

/////////////////////////////////////////////////////
/// Function: CreateFileStructure
/// Params: [in]szFilename, [in/out]pFileStruct
///
/////////////////////////////////////////////////////
void file::CreateFileStructure( const char *szFilename, TFile *pFileStruct )
{
	if( !core::IsEmptyString( szFilename ) && pFileStruct )
	{
		std::memset( pFileStruct, 0, sizeof(TFile) );

		core::SplitPath( szFilename, pFileStruct->szDrive, pFileStruct->szPath, pFileStruct->szFile, pFileStruct->szFileExt );

		if( !core::IsEmptyString( szFilename ) )
			snprintf( pFileStruct->szFilename, core::MAX_PATH+core::MAX_PATH, "%s", szFilename );
		if( !core::IsEmptyString( pFileStruct->szFile ) && !core::IsEmptyString( pFileStruct->szFileExt ) )
			snprintf( pFileStruct->szFileAndExt, core::MAX_PATH, "%s%s", pFileStruct->szFile, pFileStruct->szFileExt );
	}
	else
	{
		DBGLOG( "FILEIO: Could not create file structure for '%s'\n", szFilename );
	}

/*	if( !core::IsEmptyString( szFilename ) && pFileStruct )
	{
		std::memset( pFileStruct, 0, sizeof(TFile) );

		wchar_t* pWideString = 0;

		// convert to wide
		std::size_t len = std::strlen( szFilename );
		if( len > 0 )
		{
			pWideString = new wchar_t[(len+1)];

			std::size_t size = std::mbstowcs( pWideString, szFilename, len+1 );

			if( size != len )
			{
				DBGLOG( L"FILEIO: Incorrect converted size of wide character in CreateFileStructure\n" );
			}
		
			CreateFileStructure( pWideString, pFileStruct );

			if( pWideString )
			{
				delete[] pWideString;
				pWideString = 0;
			}
		}
	}
	else
	{
		DBGLOG( L"FILEIO: Could not create file structure\n" );
	}
*/
}

/////////////////////////////////////////////////////
/// Function: FileIsOpen
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
bool file::FileIsOpen( TFileHandle* fileHandle )
{
	if( fileHandle->nState == FILESTATE_OPEN )
		return true;

	return false;
}

/////////////////////////////////////////////////////
/// Function: FileOpen
/// Params: [in]szFilename, [in]nType, [in]fileHandle
///
/////////////////////////////////////////////////////
bool file::FileOpen( const char *szFilename, EFileType nType, TFileHandle* fileHandle )
{
	if( core::IsEmptyString( szFilename ) )
		return false;

	if( fileHandle->nState == FILESTATE_OPEN )
	{
		DBGLOG( "FILEIO: *WARNING* trying to open an already open file\n" );
		return true;
	}

	std::streamoff nFileStart = 0;
	std::streamoff nFileEnd = 0;

	if( nType == FILETYPE_BINARY_READ )
	{
		fileHandle->zipped = false;

		if( core::app::GetLoadFilesFromZip() )
		{
			// normal file doesn't exist
			fileHandle->zfp = zzip_fopen (szFilename, "rb");
			if (fileHandle->zfp == nullptr)
			{
				snprintf( szConvertedFilename, core::MAX_PATH+core::MAX_PATH, "%s/%s", core::app::GetRootZipFile(), szFilename );
			
				// try and load from the zip
				fileHandle->zfp = zzip_open_shared_io( 0, szConvertedFilename, o_flags, o_modes, file::GetZipExtensions(), 0 );

				if (fileHandle->zfp == nullptr)
					return false;	

				fileHandle->zipped = true;
			}
			else
				fileHandle->zipped = true;
		
			zzip_seek( fileHandle->zfp, 0, SEEK_END );
			fileHandle->nFileLength = static_cast<std::size_t>( zzip_tell( fileHandle->zfp ) );
			zzip_seek( fileHandle->zfp, 0, SEEK_SET );
		}
		else
		{
			fileHandle->fp = fopen( szFilename, "rb" );

			if( !fileHandle->fp )
				return false;

			fseek( fileHandle->fp, 0, SEEK_END );
			fileHandle->nFileLength = ftell( fileHandle->fp );
			fseek( fileHandle->fp, 0, SEEK_SET );
		}
	}
	else if( nType == FILETYPE_BINARY_WRITE )
	{
		fileHandle->fp = fopen( szFilename, "wb" );

		if( !fileHandle->fp )
			return false;

		fileHandle->zipped = false;
		fseek( fileHandle->fp, 0, SEEK_END );
		fileHandle->nFileLength = ftell( fileHandle->fp );
		fseek( fileHandle->fp, 0, SEEK_SET );
	}
	else if( nType == FILETYPE_BINARY_STREAM_READ )
	{
		fileHandle->input.open( szFilename, std::ios::in | std::ios::binary );

		if( !fileHandle->input.is_open() || fileHandle->input.fail() )
			return false;

		nFileStart = fileHandle->input.tellg();
		fileHandle->input.seekg( 0, std::ios::end );
		nFileEnd = fileHandle->input.tellg();
		fileHandle->input.seekg( 0, std::ios::beg );

		fileHandle->zipped = false;
		fileHandle->nFileLength = static_cast<std::size_t>(nFileEnd - nFileStart);
	}
	else if( nType == FILETYPE_BINARY_STREAM_WRITE )
	{
		fileHandle->output.open( szFilename, std::ios::out | std::ios::binary );

		if( !fileHandle->output.is_open() || fileHandle->output.fail() )
			return false;

		nFileStart = fileHandle->output.tellp();
		fileHandle->output.seekp( 0, std::ios::end );
		nFileEnd = fileHandle->output.tellp();
		fileHandle->output.seekp( 0, std::ios::beg );

		fileHandle->zipped = false;
		fileHandle->nFileLength = static_cast<std::size_t>(nFileEnd - nFileStart);
	}
	else if( nType == FILETYPE_TEXT_READ )
	{
		if( core::app::GetLoadFilesFromZip() )
		{
			// normal file doesn't exist
			fileHandle->zfp = zzip_fopen (szFilename, "rt");
			if( fileHandle->zfp == 0 )
			{
				snprintf( szConvertedFilename, core::MAX_PATH+core::MAX_PATH, "%s/%s", core::app::GetRootZipFile(), szFilename );
			
				// try and load from the zip
				fileHandle->zfp = zzip_open_shared_io( 0, szConvertedFilename, o_flags, o_modes, file::GetZipExtensions(), 0 );

				if (fileHandle->zfp == nullptr)
					return false;	

				fileHandle->zipped = true;
			}
			else
				fileHandle->zipped = true;
		
			zzip_seek( fileHandle->zfp, 0, SEEK_END );
			fileHandle->nFileLength = static_cast<std::size_t>( zzip_tell( fileHandle->zfp ) );
			zzip_seek( fileHandle->zfp, 0, SEEK_SET );
		}
		else
		{
			fileHandle->fp = fopen( szFilename, "rt" );

			if (fileHandle->fp == nullptr)
				return false;

			fileHandle->zipped = false;
			fseek( fileHandle->fp, 0, SEEK_END );
			fileHandle->nFileLength = ftell( fileHandle->fp );
			fseek( fileHandle->fp, 0, SEEK_SET );
		}
	}
	else if( nType == FILETYPE_TEXT_WRITE )
	{
		fileHandle->fp = fopen( szFilename, "wt" );

		if (fileHandle->fp == nullptr)
			return false;

		fileHandle->zipped = false;
		fseek( fileHandle->fp, 0, SEEK_END );
		fileHandle->nFileLength = ftell( fileHandle->fp );
		fseek( fileHandle->fp, 0, SEEK_SET );
	}
	else
	{
		// unknown type
		DBGLOG( "FILEIO: *ERROR* unrecognised file type in FileOpen\n" );
		return false;
	}

	fileHandle->nState = FILESTATE_OPEN;
	fileHandle->nFileType = nType;

	// track it
	nFilesOpened++;

	return true;
}

/////////////////////////////////////////////////////
/// Function: IsEOF
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
bool file::IsEOF( TFileHandle* fileHandle )
{
	if( fileHandle->nState == FILESTATE_CLOSED )
		return true;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ ||
		fileHandle->nFileType == FILETYPE_BINARY_WRITE )
	{
		if( fileHandle->zipped )
		{
			std::size_t curPos = static_cast<std::size_t>( zzip_tell( fileHandle->zfp ) );			

			if( curPos < fileHandle->nFileLength  )
				return false;
			else
				return true;
		}
		else
		{
			std::size_t curPos = static_cast<std::size_t>( ftell( fileHandle->fp ) );

			if( curPos < fileHandle->nFileLength  )
				return false;
			else
				return true;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ )
	{
		if( !fileHandle->input.is_open() )
			return true;	

		return fileHandle->input.eof();
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE )
	{
		if( !fileHandle->output.is_open() )
			return true;	

		return fileHandle->output.eof();
	}

	return true;
}

/////////////////////////////////////////////////////
/// Function: FileSeek
/// Params: [in]nOffset, [in]eOrigin, [in]fileHandle
///
/////////////////////////////////////////////////////
int file::FileSeek( long nOffset, EFileSeekOrigin eOrigin, TFileHandle* fileHandle )
{
	if( fileHandle->nState == FILESTATE_CLOSED )
		return 1;

	//std::ios::seekdir dir = std::ios::beg;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ )
	{
		if( fileHandle->zipped )
			zzip_seek( fileHandle->zfp, nOffset, static_cast<int>(eOrigin) );
		else
			fseek( fileHandle->fp, nOffset, static_cast<int>(eOrigin) );
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_WRITE )
	{
		fseek( fileHandle->fp, nOffset, static_cast<int>(eOrigin) );
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ )
	{
		if( !fileHandle->input.is_open() )
			return 1;	

		fileHandle->input.seekg( static_cast<std::streamoff>(nOffset), static_cast<std::ios::seekdir>(eOrigin) );
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE )
	{
		if( !fileHandle->output.is_open() )
			return 1;	

		fileHandle->output.seekp( static_cast<std::streamoff>(nOffset), static_cast<std::ios::seekdir>(eOrigin) );
	}

	return 0;
}

/////////////////////////////////////////////////////
/// Function: FileTell
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileTell( TFileHandle* fileHandle )
{
	std::size_t result = 0;

	if( fileHandle->nState == FILESTATE_CLOSED )
		return 0;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ )
	{
		if( fileHandle->zipped )
			result = static_cast<std::size_t>(zzip_tell( fileHandle->zfp ));
		else
			result = static_cast<std::size_t>(ftell( fileHandle->fp ));
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_WRITE )
	{
		result = static_cast<std::size_t>(ftell( fileHandle->fp ));
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ )
	{
		if( !fileHandle->input.is_open() )
			return 1;	

		result = static_cast<std::size_t>(fileHandle->input.tellg());
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE )
	{
		if( !fileHandle->output.is_open() )
			return 1;	

		result = static_cast<std::size_t>(fileHandle->output.tellp());
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FilePosition
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FilePosition( TFileHandle* fileHandle )
{
	std::size_t result = 0;
	fpos_t pos;

#ifdef BASE_PLATFORM_RASPBERRYPI
	#define POS_STRUCT(x) x.__pos
#else
	#define POS_STRUCT(x) x
#endif

	if( fileHandle->nState == FILESTATE_CLOSED )
		return 0;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ )
	{
		if( fileHandle->zipped )
		{
			result = static_cast<std::size_t>(zzip_tell( fileHandle->zfp ));
		}
		else
		{
			fgetpos( fileHandle->fp, &pos );
			result = static_cast<std::size_t>(POS_STRUCT(pos));
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_WRITE )
	{
		fgetpos( fileHandle->fp, &pos );
		result = static_cast<std::size_t>(POS_STRUCT(pos));
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ )
	{
		if( !fileHandle->input.is_open() )
			return 1;	

		result = static_cast<std::size_t>(fileHandle->input.tellg());
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE )
	{
		if( !fileHandle->output.is_open() )
			return 1;	

		result = static_cast<std::size_t>(fileHandle->output.tellp());
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileSize
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileSize( TFileHandle* fileHandle )
{
	return fileHandle->nFileLength;
}

/////////////////////////////////////////////////////
/// Function: FileSize
/// Params: [in]szFilename
///
/////////////////////////////////////////////////////
std::size_t file::FileSize( const char* szFilename )
{
	std::size_t fileSize = 0;
	struct stat st;
	if( !stat(szFilename, &st) )
		fileSize = st.st_size;

	return fileSize;
}

/////////////////////////////////////////////////////
/// Function: FileClose
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
bool file::FileClose( TFileHandle* fileHandle )
{
	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if( fileHandle->zfp != nullptr)
			{
				zzip_close( fileHandle->zfp );
				fileHandle->zfp = nullptr;
			}
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{
				fclose( fileHandle->fp );
				fileHandle->fp = nullptr;
			}
		}

		fileHandle->nState = FILESTATE_CLOSED;
		nFilesClosed++;
		
		return true;
		
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			fclose( fileHandle->fp );
			fileHandle->fp = nullptr;
			fileHandle->nState = FILESTATE_CLOSED;

			nFilesClosed++;
			return true;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			fileHandle->input.close();
			fileHandle->nState = FILESTATE_CLOSED;

			nFilesClosed++;
			return true;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->output.is_open() )
		{
			fileHandle->output.close();
			fileHandle->nState = FILESTATE_CLOSED;

			nFilesClosed++;
			return true;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_TEXT_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			fclose( fileHandle->fp );
			fileHandle->fp = nullptr;
			fileHandle->nState = FILESTATE_CLOSED;

			nFilesClosed++;
			return true;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_TEXT_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			fclose( fileHandle->fp );
			fileHandle->fp = nullptr;
			fileHandle->nState = FILESTATE_CLOSED;

			nFilesClosed++;
			return true;
		}
	}
	return false;
}

/////////////////////////////////////////////////////
/// Function: FileExists
/// Params: [in]szFilename
///
/////////////////////////////////////////////////////
bool file::FileExists( const char *szFilename )
{
	if( core::IsEmptyString(szFilename) )
		return false;

	file::TFileHandle fileHandle;

	struct stat   buffer;   
	return (stat (szFilename, &buffer) == 0);
}

/////////////////////////////////////////////////////
/// Function: FileCopy
/// Params: [in]szInFilename, [in]szOutFilename, [in]bFailIfExists
///
/////////////////////////////////////////////////////
int file::FileCopy( const char *szInFilename, const char *szOutFilename, bool bFailIfExists )
{
	std::fstream f(szInFilename, std::fstream::in | std::fstream::binary);
	f << std::noskipws;
	std::istream_iterator<unsigned char> begin(f);
	std::istream_iterator<unsigned char> end;
 
	std::fstream f2(szOutFilename, std::fstream::out|std::fstream::trunc|std::fstream::binary);
	std::ostream_iterator<char> begin2(f2);
 
	std::copy(begin, end, begin2);

	return 0;
}

/////////////////////////////////////////////////////
/// Function: FileDelete
/// Params: [in]szFilename
///
/////////////////////////////////////////////////////
int file::FileDelete( const char *szFilename )
{
	int err = 0;

	err = remove( szFilename );

	if( err )
		return 1;

	return 0;
}

/////////////////////////////////////////////////////
/// Function: FileGetC
/// Params: [in]fileHandle
///
/////////////////////////////////////////////////////
int file::FileGetC( TFileHandle* fileHandle )
{
	int result = 0;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			DBG_ASSERT(0);
			return 0;
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{
				result = fgetc( fileHandle->fp );
				return result;
			}
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			result = static_cast<int>( fileHandle->input.get() );

			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileGetC\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileGetPos
/// Params: [in/out]pos, [in]fileHandle
///
/////////////////////////////////////////////////////
int file::FileGetPos( fpos_t* pos, TFileHandle* fileHandle )
{
	int result = 0;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			DBG_ASSERT(0);
			return 0;
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{
				result = fgetpos( fileHandle->fp, pos );
				return result;
			}
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileGetPos\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileRead
/// Params: [in]buffer, [in]size, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileRead( void *buffer, std::size_t size, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char reads are endian safe

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && 
		fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				result += zzip_fread( buffer, 1, size, fileHandle->zfp );

				return result;
			}
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{

#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileRead\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileRead" );
				}
#endif // _DEBUG

				result += fread( buffer, 1, size, fileHandle->fp );
				return result;
			}
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && 
		fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size) );
			result += static_cast<std::size_t>(fileHandle->input.gcount());

			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileRead\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileRead
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileRead( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char reads are endian safe

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && 
		fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				result += zzip_fread( buffer, size, count, fileHandle->zfp );

				return result;
			}
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileRead\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileRead" );
				}
#endif // _DEBUG

				result += fread( buffer, size, count, fileHandle->fp );

				return result;
			}
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && 
		fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			result += static_cast<std::size_t>(fileHandle->input.gcount());

			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_TEXT_READ  && 
		fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				result += zzip_fread( buffer, size, count, fileHandle->zfp );

				return result;
			}
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileRead\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileRead" );
				}
#endif // _DEBUG

				result += fread( buffer, size, count, fileHandle->fp );

				return result;
			}
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileRead\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadUChar
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadUChar( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char reads are endian safe

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				result += zzip_fread( buffer, size, count, fileHandle->zfp );

				return result;
			}
		}
		else
		{
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadUChar\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadUChar" );
				}
#endif // _DEBUG

				result += fread( buffer, size, count, fileHandle->fp );
				return result;
			}
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			result += static_cast<std::size_t>(fileHandle->input.gcount());

			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadUChar\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadChar
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadChar( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char reads are endian safe

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				result += zzip_fread( buffer, size, count, fileHandle->zfp );

				return result;
			}
		}
		else
		if (fileHandle->fp != nullptr)
		{
#ifdef _DEBUG
			// always check that the file is not read past EOF
			if( feof(fileHandle->fp ) )
			{
				DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadChar\n" );
				DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadChar" );
			}
#endif // _DEBUG

			result += fread( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			result += static_cast<std::size_t>(fileHandle->input.gcount());
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadChar\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadBool
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadBool( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				result += zzip_fread( buffer, size, count, fileHandle->zfp );

				return result;
			}
		}
		else
		if (fileHandle->fp != nullptr)
		{
#ifdef _DEBUG
			// always check that the file is not read past EOF
			if( feof(fileHandle->fp ) )
			{
				DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadBool\n" );
				DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadBool" );
			}
#endif // _DEBUG

			result += fread( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			result += static_cast<std::size_t>(fileHandle->input.gcount());
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadBool\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadShort
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadShort( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	short nTempShort = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &nTempShort, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadShort\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadShort" );
					}
#endif // _DEBUG
					result += fread( &nTempShort, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadShort\n" );
				return 0;
			}

			short *pTmpbuffer = reinterpret_cast<short *>(buffer);

			pTmpbuffer[i] = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &nTempShort, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadShort\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadShort" );
				}
#endif // _DEBUG
				result += fread( &nTempShort, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadShort\n" );
			return 0;
		}

		short *pTmpbuffer = reinterpret_cast<short *>(buffer);

		*pTmpbuffer = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );	

	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadUShort
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadUShort( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	unsigned short nTempShort = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &nTempShort, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadUShort\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadUShort" );
					}
#endif // _DEBUG
					result += fread( &nTempShort, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadUShort\n" );
				return 0;
			}

			unsigned short *pTmpbuffer = reinterpret_cast<unsigned short *>(buffer);

			pTmpbuffer[i] = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &nTempShort, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadUShort\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadUShort" );
				}
#endif // _DEBUG
				result += fread( &nTempShort, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadUShort\n" );
			return 0;
		}

		unsigned short *pTmpbuffer = reinterpret_cast<unsigned short *>(buffer);

		*pTmpbuffer = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );	

	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadInt
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadInt( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	int nTempInt = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &nTempInt, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadInt\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadInt" );
					}
#endif // _DEBUG
					result += fread( &nTempInt, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadInt\n" );
				return 0;
			}

			int *pTmpbuffer = reinterpret_cast<int *>(buffer);

			pTmpbuffer[i] = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &nTempInt, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadInt\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadInt" );
				}
#endif // _DEBUG
				result += fread( &nTempInt, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadInt\n" );
			return 0;
		}

		int *pTmpbuffer = reinterpret_cast<int *>(buffer);

		*pTmpbuffer = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadUInt
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadUInt( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	unsigned int nTempInt = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &nTempInt, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadUInt\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadUInt" );
					}
#endif // _DEBUG
					result += fread( &nTempInt, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadUInt\n" );
				return 0;
			}

			unsigned int *pTmpbuffer = reinterpret_cast<unsigned int *>(buffer);

			pTmpbuffer[i] = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &nTempInt, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadUInt\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadUInt" );
				}
#endif // _DEBUG
				result += fread( &nTempInt, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadUInt\n" );
			return 0;
		}

		unsigned int *pTmpbuffer = reinterpret_cast<unsigned int *>(buffer);

		*pTmpbuffer = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadFloat
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadFloat( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	float fTempFloat = 0.0f;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &fTempFloat, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadFloat\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadFloat" );
					}
#endif // _DEBUG
					result += fread( &fTempFloat, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&fTempFloat), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadFloat\n" );
				return 0;
			}

			float *pTmpbuffer = reinterpret_cast<float *>(buffer);

			pTmpbuffer[i] = core::EndianSwapFloat( fTempFloat, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &fTempFloat, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadFloat\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadFloat" );
				}
#endif // _DEBUG
				result += fread( &fTempFloat, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&fTempFloat), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadFloat\n" );
			return 0;
		}

		float *pTmpbuffer = reinterpret_cast<float *>(buffer);

		*pTmpbuffer = core::EndianSwapFloat( fTempFloat, core::MACHINE_LITTLE_ENDIAN );
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadDouble
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadDouble( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	double dTempDouble = 0.0f;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &dTempDouble, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadFloat\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadFloat" );
					}
#endif // _DEBUG
					result += fread( &dTempDouble, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&dTempDouble), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadFloat\n" );
				return 0;
			}

			float *pTmpbuffer = reinterpret_cast<float *>(buffer);

			pTmpbuffer[i] = static_cast<float>(dTempDouble);//core::EndianSwapFloat( dTempDouble, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &dTempDouble, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadFloat\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadFloat" );
				}
#endif // _DEBUG
				result += fread( &dTempDouble, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&dTempDouble), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadFloat\n" );
			return 0;
		}

		float *pTmpbuffer = reinterpret_cast<float *>(buffer);

		*pTmpbuffer = static_cast<float>(dTempDouble);//core::EndianSwapFloat( dTempDouble, core::MACHINE_LITTLE_ENDIAN );
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadVec2D
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadTerminatedString( char *buffer, unsigned int maxCount, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char reads are endian safe

	unsigned int bufferIndex = 0;
	bool completedRead = false;

	if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->zipped )
		{
			if (fileHandle->zfp != nullptr)
			{
				while( !completedRead )
				{
					result += zzip_fread( &buffer[bufferIndex], sizeof(char), 1, fileHandle->zfp );

					if( buffer[bufferIndex] == '\0' )
						return result;
					else
					{
						bufferIndex++;

						if( bufferIndex >= maxCount )
							return result;
					}
				}
			}
		}
		else
		if (fileHandle->fp != nullptr)
		{
			while( !completedRead )
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadChar\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadChar" );
				}
#endif // _DEBUG

				result += fread( &buffer[bufferIndex], sizeof(char), 1, fileHandle->fp );

				if( buffer[bufferIndex] == '\0' )
					return result;
				else
				{
					bufferIndex++;

					if( bufferIndex >= maxCount )
						return result;
				}
			}
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->input.is_open() )
		{
			while( !completedRead )
			{
				fileHandle->input.read( &buffer[bufferIndex], static_cast<std::streamsize>(sizeof(char)) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());

				if( buffer[bufferIndex] == '\0' )
					return result;
				else
				{
					bufferIndex++;

					if( bufferIndex >= maxCount )
						return result;
				}
			}
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadChar\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadVec2
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadVec2( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	glm::vec2 vTempVec(0.0f, 0.0f);

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &vTempVec, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadVec2D\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadVec2D" );
					}
#endif // _DEBUG
					result += fread( &vTempVec, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadVec2D\n" );
				return 0;
			}

			glm::vec2 *pTmpbuffer = reinterpret_cast<glm::vec2 *>(buffer);

			pTmpbuffer[i].x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
			pTmpbuffer[i].y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{
		
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &vTempVec, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadVec2D\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadVec2D" );
				}
#endif // _DEBUG
				result += fread( &vTempVec, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadVec2D\n" );
			return 0;
		}

		glm::vec2 *pTmpbuffer = reinterpret_cast<glm::vec2 *>(buffer);

		pTmpbuffer->x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
		pTmpbuffer->y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );

	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadVec3
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadVec3( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	glm::vec3 vTempVec(0.0f, 0.0f, 0.0f);

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &vTempVec, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadVec3D\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadVec3D" );
					}
#endif // _DEBUG
					result += fread( &vTempVec, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadVec3D\n" );
				return 0;
			}

			glm::vec3 *pTmpbuffer = reinterpret_cast<glm::vec3 *>(buffer);

			pTmpbuffer[i].x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
			pTmpbuffer[i].y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
			pTmpbuffer[i].z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{		
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &vTempVec, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadVec3D\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadVec3D" );
				}
#endif // _DEBUG
				result += fread( &vTempVec, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadVec3D\n" );
			return 0;
		}

		glm::vec3 *pTmpbuffer = reinterpret_cast<glm::vec3 *>(buffer);

		pTmpbuffer->x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
		pTmpbuffer->y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
		pTmpbuffer->z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadVec4
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadVec4( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
	glm::vec4 vTempVec( 0.0f, 0.0f, 0.0f, 0.0f );

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( &vTempVec, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadVec4D\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadVec4D" );
					}
#endif // _DEBUG
					result += fread( &vTempVec, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadVec4D\n" );
				return 0;
			}

			glm::vec4 *pTmpbuffer = reinterpret_cast<glm::vec4 *>(buffer);

			pTmpbuffer[i].x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
			pTmpbuffer[i].y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
			pTmpbuffer[i].z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );
			pTmpbuffer[i].w = core::EndianSwapFloat( vTempVec.w, core::MACHINE_LITTLE_ENDIAN );
		}
	}
	else
	{	
	
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( &vTempVec, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadVec4D\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadVec4D" );
				}
#endif // _DEBUG
				result += fread( &vTempVec, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadVec4D\n" );
			return 0;
		}

		glm::vec4 *pTmpbuffer = reinterpret_cast<glm::vec4 *>(buffer);

		pTmpbuffer->x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
		pTmpbuffer->y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
		pTmpbuffer->z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );
		pTmpbuffer->w = core::EndianSwapFloat( vTempVec.w, core::MACHINE_LITTLE_ENDIAN );
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileReadFileStructure
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileReadFileStructure( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->zipped )
				{
					if (fileHandle->zfp != nullptr)
					{
						result += zzip_fread( buffer, size, 1, fileHandle->zfp );
					}
				}
				else
				if (fileHandle->fp != nullptr)
				{
#ifdef _DEBUG
					// always check that the file is not read past EOF
					if( feof(fileHandle->fp ) )
					{
						DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadFileStructure\n" );
						DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadFileStructure" );
					}
#endif // _DEBUG
					result += fread( buffer, size, 1, fileHandle->fp );
				}
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->input.is_open() )
				{
					fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size) );
					result += static_cast<std::size_t>(fileHandle->input.gcount());
				}
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadFileStructure\n" );
				return 0;
			}
		}
	}
	else
	{	
	
		if( fileHandle->nFileType == FILETYPE_BINARY_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->zipped )
			{
				if (fileHandle->zfp != nullptr)
				{
					result += zzip_fread( buffer, size, 1, fileHandle->zfp );
				}
			}
			else
			if (fileHandle->fp != nullptr)
			{
#ifdef _DEBUG
				// always check that the file is not read past EOF
				if( feof(fileHandle->fp ) )
				{
					DBGLOG( "FILEIO: *ERROR* reading past the EOF in FileReadFileStructure\n" );
					DBG_ASSERT_MSG( 0, "FILEIO: *ERROR* reading past the EOF in FileReadFileStructure" );
				}
#endif // _DEBUG
				result += fread( buffer, size, 1, fileHandle->fp );
			}
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_READ && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->input.is_open() )
			{
				fileHandle->input.read( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size) );
				result += static_cast<std::size_t>(fileHandle->input.gcount());
			}
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised read type in FileReadFileStructure\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////
/// Function: FileWrite
/// Params: [in]buffer, [in]size, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWrite( void *buffer, std::size_t size, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char writes are endian safe
	if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			result += fwrite( buffer, size, 1, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->output.is_open() )
		{
			fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size) );
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWrite\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWrite
/// Params: [in]buffer, [in]size, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWrite( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char writes are endian safe
	if( (fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN) )
	{
		if (fileHandle->fp != nullptr)
		{
			result += fwrite( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else
	if( (fileHandle->nFileType == FILETYPE_TEXT_WRITE && fileHandle->nState == FILESTATE_OPEN) )
	{
		if (fileHandle->fp != nullptr)
		{
			result += fwrite( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->output.is_open() )
		{
			fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWrite\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteUChar
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteUChar( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char writes are endian safe
	if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			result += fwrite( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->output.is_open() )
		{
			fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteUChar\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteChar
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteChar( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	std::size_t result = 0;

	// char writes are endian safe
	if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			result += fwrite( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->output.is_open() )
		{
			fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteChar\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteBool
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteBool( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle  )
{
	std::size_t result = 0;

	// bool writes are endian safe
	if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if (fileHandle->fp != nullptr)
		{
			result += fwrite( buffer, size, count, fileHandle->fp );
			return result;
		}
	}
	else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
	{
		if( fileHandle->output.is_open() )
		{
			fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size*count) );
			return result;
		}
	}
	else
	{
		DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteBool\n" );
		return 0;
	}

	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteShort
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteShort( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle  )
{
	unsigned int i = 0;
	std::size_t result = 0;

	short nTempShort = 0;
	
	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			short *pTmpbuffer = reinterpret_cast<short *>(buffer);

			nTempShort = pTmpbuffer[i];

			nTempShort = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &nTempShort, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteShort\n" );
				return 0;
			}
		}
	}
	else
	{
		short *pTmpbuffer = reinterpret_cast<short *>(buffer);

		nTempShort = *pTmpbuffer;

		nTempShort = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );
		
		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &nTempShort, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteShort\n" );
			return 0;
		}

	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteUShort
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteUShort( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle  )
{
	unsigned int i = 0;
	std::size_t result = 0;

	unsigned short nTempShort = 0;
	
	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			unsigned short *pTmpbuffer = reinterpret_cast<unsigned short *>(buffer);

			nTempShort = pTmpbuffer[i];

			nTempShort = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &nTempShort, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteUShort\n" );
				return 0;
			}
		}
	}
	else
	{
		unsigned short *pTmpbuffer = reinterpret_cast<unsigned short *>(buffer);

		nTempShort = *pTmpbuffer;

		nTempShort = core::EndianSwapShort( nTempShort, core::MACHINE_LITTLE_ENDIAN );
		
		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &nTempShort, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&nTempShort), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteUShort\n" );
			return 0;
		}

	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteInt
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteInt( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	int nTempInt = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			int *pTmpbuffer = reinterpret_cast<int *>(buffer);

			nTempInt = pTmpbuffer[i];

			nTempInt = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &nTempInt, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteInt\n" );
				return 0;
			}
		}
	}
	else
	{
		int *pTmpbuffer = reinterpret_cast<int *>(buffer);

		nTempInt = *pTmpbuffer;

		nTempInt = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &nTempInt, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteInt\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteUInt
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteUInt( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle  )
{
	unsigned int i = 0;
	std::size_t result = 0;

	unsigned int nTempInt = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			unsigned int *pTmpbuffer = reinterpret_cast<unsigned int *>(buffer);

			nTempInt = pTmpbuffer[i];

			nTempInt = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &nTempInt, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteUInt\n" );
				return 0;
			}
		}
	}
	else
	{
		unsigned int *pTmpbuffer = reinterpret_cast<unsigned int *>(buffer);

		nTempInt = *pTmpbuffer;

		nTempInt = core::EndianSwapInt( nTempInt, core::MACHINE_LITTLE_ENDIAN );

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &nTempInt, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&nTempInt), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteUInt\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteFloat
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteFloat( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	float fTempFloat = 0.0f;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			float *pTmpbuffer = reinterpret_cast<float *>(buffer);

			fTempFloat = pTmpbuffer[i];

			fTempFloat = core::EndianSwapFloat( fTempFloat, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &fTempFloat, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&fTempFloat), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteFloat\n" );
				return 0;
			}
		}
	}
	else
	{
		float *pTmpbuffer = reinterpret_cast<float *>(buffer);

		fTempFloat = *pTmpbuffer;

		fTempFloat = core::EndianSwapFloat( fTempFloat, core::MACHINE_LITTLE_ENDIAN );

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &fTempFloat, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&fTempFloat), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteFloat\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteVec2
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteVec2( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	glm::vec2 vTempVec(0.0f, 0.0f);

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			
			glm::vec2 *pTmpbuffer = reinterpret_cast<glm::vec2 *>(buffer);

			vTempVec.x = pTmpbuffer[i].x;
			vTempVec.y = pTmpbuffer[i].y;

			vTempVec.x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
			vTempVec.y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &vTempVec, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteVec2D\n" );
				return 0;
			}
		}
	}
	else
	{
		glm::vec2 *pTmpbuffer = reinterpret_cast<glm::vec2 *>(buffer);

		vTempVec.x = pTmpbuffer->x;
		vTempVec.y = pTmpbuffer->y;

		vTempVec.x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
		vTempVec.y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &vTempVec, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteVec2D\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteVec3
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteVec3( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;
			
	glm::vec3 vTempVec(0.0f, 0.0f, 0.0f);

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{

			glm::vec3 *pTmpbuffer = reinterpret_cast<glm::vec3 *>(buffer);

			vTempVec.x = pTmpbuffer[i].x;
			vTempVec.y = pTmpbuffer[i].y;
			vTempVec.z = pTmpbuffer[i].z;

			vTempVec.x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
			vTempVec.y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
			vTempVec.z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &vTempVec, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteVec3D\n" );
				return 0;
			}		
		}
	}
	else
	{	
		glm::vec3 *pTmpbuffer = reinterpret_cast<glm::vec3 *>(buffer);

		vTempVec.x = pTmpbuffer->x;
		vTempVec.y = pTmpbuffer->y;
		vTempVec.z = pTmpbuffer->z;

		vTempVec.x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
		vTempVec.y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
		vTempVec.z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );	

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &vTempVec, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteVec3D\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteVec4
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteVec4( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	glm::vec4 vTempVec(0.0f, 0.0f, 0.0f, 0.0f);

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			glm::vec4 *pTmpbuffer = reinterpret_cast<glm::vec4 *>(buffer);

			vTempVec.x = pTmpbuffer[i].x;
			vTempVec.y = pTmpbuffer[i].y;
			vTempVec.z = pTmpbuffer[i].z;
			vTempVec.w = pTmpbuffer[i].w;

			vTempVec.x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
			vTempVec.y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
			vTempVec.z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );
			vTempVec.w = core::EndianSwapFloat( vTempVec.w, core::MACHINE_LITTLE_ENDIAN );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &vTempVec, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteVec4D\n" );
				return 0;
			}		
		}
	}
	else
	{	
		glm::vec4 *pTmpbuffer = reinterpret_cast<glm::vec4 *>(buffer);

		vTempVec.x = pTmpbuffer->x;
		vTempVec.y = pTmpbuffer->y;
		vTempVec.z = pTmpbuffer->z;
		vTempVec.w = pTmpbuffer->w;

		vTempVec.x = core::EndianSwapFloat( vTempVec.x, core::MACHINE_LITTLE_ENDIAN );
		vTempVec.y = core::EndianSwapFloat( vTempVec.y, core::MACHINE_LITTLE_ENDIAN );
		vTempVec.z = core::EndianSwapFloat( vTempVec.z, core::MACHINE_LITTLE_ENDIAN );
		vTempVec.w = core::EndianSwapFloat( vTempVec.w, core::MACHINE_LITTLE_ENDIAN );

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &vTempVec, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&vTempVec), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteVec4D\n" );
			return 0;
		}
	}
	
	return result;
}


/////////////////////////////////////////////////////
/// Function: FileWriteFileStructure
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteFileStructure( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( buffer, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteFileStructure\n" );
				return 0;
			}		
		}
	}
	else
	{	
		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( buffer, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(buffer), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteFileStructure\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteFileStore
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteFileStore( void *buffer, std::size_t size, std::size_t count, TFileHandle* fileHandle )
{
	unsigned int i = 0;
	std::size_t result = 0;

	file::TFileStoreInformation tmpFileStore;
	std::memset( &tmpFileStore, 0, sizeof(file::TFileStoreInformation) );

	if( count > 1 )
	{
		for( i = 0; i < count; i++ )
		{
			file::TFileStoreInformation *pTmpbuffer = reinterpret_cast<file::TFileStoreInformation *>(buffer);

			tmpFileStore.nFileStartPos = pTmpbuffer[i].nFileStartPos;
			tmpFileStore.nFileEndPos = pTmpbuffer[i].nFileEndPos;
			tmpFileStore.nFileLength = pTmpbuffer[i].nFileLength;

			tmpFileStore.nFileStartPos = core::EndianSwapInt( tmpFileStore.nFileStartPos, core::MACHINE_LITTLE_ENDIAN );
			tmpFileStore.nFileEndPos = core::EndianSwapInt( tmpFileStore.nFileEndPos, core::MACHINE_LITTLE_ENDIAN );
			tmpFileStore.nFileLength = static_cast<std::size_t>( core::EndianSwapInt( static_cast<unsigned int>(tmpFileStore.nFileLength), core::MACHINE_LITTLE_ENDIAN ) );

			if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if (fileHandle->fp != nullptr)
					result += fwrite( &tmpFileStore, size, 1, fileHandle->fp );
			}
			else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
			{
				if( fileHandle->output.is_open() )
					fileHandle->output.write( reinterpret_cast<char *>(&tmpFileStore), static_cast<std::streamsize>(size) );
			}
			else
			{
				DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteFileStore\n" );
				return 0;
			}		
		}
	}
	else
	{	
		file::TFileStoreInformation *pTmpbuffer = reinterpret_cast<file::TFileStoreInformation *>(buffer);

		tmpFileStore.nFileStartPos = pTmpbuffer->nFileStartPos;
		tmpFileStore.nFileEndPos = pTmpbuffer->nFileEndPos;
		tmpFileStore.nFileLength = pTmpbuffer->nFileLength;

		tmpFileStore.nFileStartPos = core::EndianSwapInt( tmpFileStore.nFileStartPos, core::MACHINE_LITTLE_ENDIAN );
		tmpFileStore.nFileEndPos = core::EndianSwapInt( tmpFileStore.nFileEndPos, core::MACHINE_LITTLE_ENDIAN );
		tmpFileStore.nFileLength = core::EndianSwapInt( static_cast<unsigned int>(tmpFileStore.nFileLength), core::MACHINE_LITTLE_ENDIAN );

		if( fileHandle->nFileType == FILETYPE_BINARY_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if (fileHandle->fp != nullptr)
				result += fwrite( &tmpFileStore, size, 1, fileHandle->fp );
		}
		else if( fileHandle->nFileType == FILETYPE_BINARY_STREAM_WRITE && fileHandle->nState == FILESTATE_OPEN )
		{
			if( fileHandle->output.is_open() )
				fileHandle->output.write( reinterpret_cast<char *>(&tmpFileStore), static_cast<std::streamsize>(size) );
		}
		else
		{
			DBGLOG( "FILEIO: *ERROR* unrecognised write type in FileWriteFileStore\n" );
			return 0;
		}
	}
	
	return result;
}

/////////////////////////////////////////////////////
/// Function: FileWriteCURL
/// Params: [in]buffer, [in]size, [in]count, [in]fileHandle
///
/////////////////////////////////////////////////////
std::size_t file::FileWriteCURL( char *buffer, std::size_t size, std::size_t count, file::TFileHandle* fileHandle )
{
	if( fileHandle != 0 )
	{
		if( file::FileIsOpen( fileHandle ) )
			return file::FileWrite( buffer, size, count, fileHandle );
	}

	return 0;
}

/////////////////////////////////////////////////////
/// Function: CreateDirectory
/// Params: [in]szDirectory
///
/////////////////////////////////////////////////////
int file::CreateDirectory( const char *szDirectory )
{
#ifdef BASE_PLATFORM_WINDOWS
		return( _mkdir( szDirectory ) );
#elif defined(BASE_PLATFORM_MAC) || defined(BASE_PLATFORM_iOS)
	#ifdef BASE_DUMMY_APP // running QT so has to be C++ only
	
	#else
		NSError* error = nil;
		NSString* convertedPath = [[NSString alloc] initWithCString:szDirectory encoding:NSUTF8StringEncoding];
		[[NSFileManager defaultManager] createDirectoryAtPath:convertedPath withIntermediateDirectories:NO attributes:nil error:&error];
		if( error != nil )
			return 1;
		else
			return 0;
	#endif
#else
	return( mkdir( szDirectory, 0777 ) );
#endif 
	
	return 1;
}

/////////////////////////////////////////////////////
/// Function: DeleteDirectory
/// Params: [in]szDirectory
///
/////////////////////////////////////////////////////
int file::DeleteDirectory( const char *szDirectory, bool deleteSubDirectories )
{
	return 1;
}

