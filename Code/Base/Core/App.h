
#ifndef __APP_H__
#define __APP_H__

#ifndef __CORECONSTS_H__
	#include "Core/CoreConsts.h"
#endif // __CORECONSTS_H__

namespace core
{
	enum EPlatform
	{
		PLATFORM_WINDOWS=0,
		PLATFORM_iOS,
		PLATFORM_ANDROID,
		PLATFORM_MAC
	};
	
	namespace app
	{

		class App 
		{	
			public:
				App();
				virtual ~App();

				// override
				virtual int Initialise(void) = 0;
				virtual int Update(void) = 0;
				virtual int Render(void) = 0;
				virtual int Shutdown(void) = 0;

				// main launch
				int Execute(int windowWidth, int windowHeight, int frameWidth, int frameHeight, bool fullscreen);

			private:
				void ToggleFullscreen();

				void UpdateFrame();
				void RenderFrame();

			private:
				SDL_Window* m_Window;
				SDL_GLContext m_Context;

				int m_TotalFrames;
				Uint32 m_StartTicks;
				Uint32 m_LastTicks;

			protected:
				float m_ElapsedTime;
				float m_FPS;

				std::wstring m_SaveFilePath;

				glm::ivec2 m_WindowDims;
				glm::ivec2 m_FrameDims;
		};

		/// SetPlatform - runtime platform details
		/// \param platform - which platform are we running
		void SetPlatform( core::EPlatform platform );
		/// GetPlatform - get the current known platform
		/// \return EPlatform - current platform
		core::EPlatform GetPlatform();
		
		/// SupportsHardwareKeyboard - does this platform have a hardware keyboard
		/// \return boolean - flag whether the current platform supports a hardware keyboard
		bool SupportsHardwareKeyboard();
		
		/// SetArgumentCount - set the argument count from main call
		/// \param count - number of arguments
		void SetArgumentCount( int count );
		/// GetArgumentCount - get the argument count from main call
		/// \return integer - number of arguments
		int GetArgumentCount();

		/// SetArgumentVariables - set the argument strings from main call
		/// \param argv - number of arguments
		void SetArgumentVariables( char** argv );
		/// GetArgumentVariables - get the argument strings from main call
		/// \return char pointer to pointer - all arguments
		char** GetArgumentVariables();

		/// SetUseZipFiles - set the flag to load everything from a zip file
		/// \param flag - flag state
		void SetLoadFilesFromZip( bool flag );
		/// GetLoadFilesFromZip - get the flag to check if everything is loaded from a zip file
		/// \return boolean - current flag state
		bool GetLoadFilesFromZip();

		/// SetRootZipFile - set the name of the root zip file if all files are stored in a zip
		/// \param rootFile - root zip file all files will load from
		void SetRootZipFile( const char* rootFile );
		/// GetRootZipFile - get the name of the root zip file if all files are stored in a zip
		/// \return const char* - get the root file name
		const char* GetRootZipFile();

		/// SetFPS - set the current fps
		/// \param fps - frame rate to set
		void SetFPS( float fps );
		/// GetFPS - get the current fps
		/// \return float - current app frame rate
		float GetFPS();

		/// SetFrameLock - set the current frame rate lock
		/// \param fps - frame rate lock to set
		void SetFrameLock( float fps );
		/// GetFrameLock - get the current fps
		/// \return float - current app locked frame rate
		float GetFrameLock();

		/// SetInBackground - set the boolean if this app is in the background
        /// \param inBackground - in background state       
		void SetInBackground(bool inBackground);
		/// IsInBackground - is this app is in the background
		/// \return boolean - true if in background
		bool IsInBackground();
		
        /// SetLanguage - current language
        /// \param language - set the current language id
        void SetLanguage( int language );
        /// GetLanguage - get the current language
        /// \return integer - current language id
        int GetLanguage();
        
		/// SetFrameWidth - set the default width of the application
		/// \param width - width to set
		void SetFrameWidth( int width );
		/// GetFrameWidth - get the default width of the application
		/// \return integer - current app width 
		int GetFrameWidth();

		/// SetFrameHeight - set the default height of the application
		/// \param height - height to set
		void SetFrameHeight( int height );
		/// GetFrameHeight - get the default height of the application
		/// \return integer - current app height 
		int GetFrameHeight();

		/// SetBaseAssetsWidth - set the default width of the assets (they can be scaled up/down)
		/// \param width - width to set
		void SetBaseAssetsWidth( int width );
		/// GetBaseAssetsWidth - get the default width of the assets
		/// \return integer - base assets width
		int GetBaseAssetsWidth();

		/// SetBaseAssetsHeight - set the default height of the assets (they can be scaled up/down)
		/// \param height - height to set
		void SetBaseAssetsHeight( int height );
		/// GetBaseAssetsHeight - get the default height of the assets
		/// \return integer - base assets height
		int GetBaseAssetsHeight();

		/// GetWidthScale - get the width scaling factor for assets
		/// \return float - assets width scale
		float GetWidthScale();
		/// GetHeightScale - get the height scaling factor for assets
		/// \return float - assets height scale
		float GetHeightScale();

		/// SetSavePath - set the app save path
		/// \param path - app save path
		void SetSavePath(const std::wstring& path);
		/// GetSavePath - get the app save path
		/// \return const char - app save path
		std::wstring& GetSavePath();

		////////////////////////////////////////////////////////////////////////////////////////////////////
		
		/// SetIconID - Sets the application icon (should be called in main.cpp)
		/// \param resourceId = which resource in the rc/header file
		void SetIconID( int resourceId );
		/// GetIconID - Gets the set application icon
		/// \return integer - app icon id
		int GetIconID();
		
		/// SupportiPodMusic - Sets the state of the allowing ipod music to play (should be called in main.m)
		/// \param state - true or false for support/do not support
		void SupportiPodMusic( bool state );
		/// GetSupportiPodMusic - Gets the state of the allowing ipod music to play
		/// \return boolean - true or false for support/do not support		
		bool GetSupportiPodMusic();	
		
		/// OpenWebLink - opens a web link
		/// \param url - URL of the page to open
		void OpenWebLink( const char* url );
		
		/// QuitApplication - Gracefully quits the application
		void QuitApplication();

	} // namespace app

} // namespace core

#endif // __APP_H__

