
/*===================================================================
	File: HTemplate.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "InputBase.h"
#include "ScriptBase.h"
#include "CollisionBase.h"
#include "SupportBase.h"

// app includes
#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "ScriptAccess/ScriptDataHolder.h"
#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/TitleScreenState.h"
#include "GameStates/MainGameState.h"
#include "GameStates/Examples/Box2DTestState.h"
#include "GameStates/Examples/FBOTestState.h"
#include "GameStates/Examples/TiledTestState.h"
#include "GameStates/Examples/FontFormatTestState.h"

#include "Resources/SpriteResources.h"
#include "Resources/SoundResources.h"
#include "Resources/FontResources.h"
#include "Resources/StringResources.h"
#include "Resources/TextureResources.h"


/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
HTemplate::HTemplate()
{
	m_pScriptHolder = 0;
	m_pGameSystems = 0;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
HTemplate::~HTemplate()
{

}

/////////////////////////////////////////////////////
/// Method: Initialise
/// Params: None
///
/////////////////////////////////////////////////////
int HTemplate::Initialise( void )
{	
	//core::app::SetSavePath( m_SaveFilePath );
	//file::CreateDirectory( core::app::GetSavePath() );
	
	snd::AudioSystem::Initialise( snd::MusicMode_PlaylistPlayer );
	//snd::SetMusicPauseCall( AudioSystem::GetInstance() );

	script::LuaScripting::Initialise();
	script::RegisterInputFunctions();
	
	m_pScriptHolder = new ScriptDataHolder();
	DBG_ASSERT_MSG( (m_pScriptHolder != 0), "Could not allocate script data holder" );
	
	// load the profiles
	RegisterScriptFunctions( *m_pScriptHolder );
	script::LuaScripting::GetInstance()->LoadScript( "lua/boot.lua" );
	script::LuaCallFunction( "BootInitialise", 0, 0 );

	ScriptDataHolder::DevScriptData devData = m_pScriptHolder->GetDevData();

	//core::app::SetFrameWidth( devData.screenWidth );
	//core::app::SetFrameHeight( devData.screenHeight );

	core::app::SetBaseAssetsWidth( devData.assetBaseWidth );
	core::app::SetBaseAssetsHeight( devData.assetBaseHeight );

    // set and load the correct language file
	int language = devData.language;
    devData.language = core::app::GetLanguage();

	// in debug use the script
#ifdef _DEBUG
	#ifdef BASE_PLATFORM_WINDOWS 
	if( language != devData.language )
		devData.language = language;
	#endif // BASE_PLATFORM_WINDOWS
#endif // _DEBUG

    lua_getglobal( script::LuaScripting::GetState(), "LoadLanguageFile" );
    lua_pushnumber( script::LuaScripting::GetState(), devData.language );
    lua_pcall( script::LuaScripting::GetState(), 1, 0, 0 );

	// register the resource list(s)
	res::CreateSpriteResourceMap();
	res::CreateSoundResourceMap();
	res::CreateFontResourceMap();
	res::CreateStringResourceMap();
	res::CreateTextureResourceMap();

    lua_getglobal( script::LuaScripting::GetState(), "HTemplateInitialise" );
    lua_pcall( script::LuaScripting::GetState(), 0, 0, 0 );

	// setup GL
	renderer::OpenGL::GetInstance()->ClearColour( 0.0f, 0.0f, 0.0f, 1.0f );
	renderer::OpenGL::GetInstance()->SetPerspective( 75.0f, static_cast<float>(core::app::GetFrameWidth())/static_cast<float>(core::app::GetFrameHeight()), 1.0f, 50.0f );
	renderer::OpenGL::GetInstance()->SetClearBits( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
	
	glm::vec3 pos(0.0f, 0.0f, 0.0f);
	glm::vec3 target(0.0f, 0.0f, -100.0f);
	gDebugCamera.SetPosition( pos ); 
	gDebugCamera.SetTarget( target );
	gDebugCamera.Disable();

	// create gamesystems
	m_pGameSystems = new GameSystems();
	DBG_ASSERT_MSG( (m_pGameSystems != 0), "Could not allocate GameSystems" );

	m_pGameSystems->CreateGameCamera();

	// initialise

	GameData gameData = m_pScriptHolder->GetGameData();

	DBG_MEMTRY
		m_UIStateManager.ChangeState( 0 /*new FrontendTitleScreenUI(m_UIStateManager)*/ );	
	DBG_MEMCATCH

	m_MainStateManager.SetSecondaryManager( &m_UIStateManager );

	DBG_MEMTRY
		m_MainStateManager.ChangeState( new MainGameState(m_MainStateManager) );
	DBG_MEMCATCH

	m_UIStateManager.SetSecondaryManager( &m_MainStateManager );

	return 0;
}

/////////////////////////////////////////////////////
/// Method: FrameMove
/// Params: None
///
/////////////////////////////////////////////////////
int HTemplate::Update( void )
{
	//AudioSystem::GetInstance()->Update( m_ElapsedTime );
	
	if( GameSystems::IsInitialised() )
	{
		GameSystems::GetInstance()->Update( m_ElapsedTime );
	}

	m_MainStateManager.Update( m_ElapsedTime );
	m_UIStateManager.Update( m_ElapsedTime );

	return 0;
}

/////////////////////////////////////////////////////
/// Method: Render
/// Params: None
///
/////////////////////////////////////////////////////
int HTemplate::Render( void )
{
	m_MainStateManager.Draw();
	m_UIStateManager.Draw();

	return 0;
}

/////////////////////////////////////////////////////
/// Method: Cleanup
/// Params: None
///
/////////////////////////////////////////////////////
int HTemplate::Shutdown( void )
{
	m_MainStateManager.ChangeState( 0 );
	m_UIStateManager.ChangeState( 0 );

	res::ClearSpriteResources();
	res::ClearSoundResources();
	res::ClearFontResources();
	res::ClearStringResources();
	res::ClearTextureResources();

	if( m_pGameSystems != 0 )
	{
		m_pGameSystems->DestroyGameCamera();

		delete m_pGameSystems;
		m_pGameSystems = 0;
	}

	if( m_pScriptHolder != 0 )
	{
		delete m_pScriptHolder;
		m_pScriptHolder = 0;
	}

	snd::AudioSystem::Shutdown();

	script::LuaScripting::Shutdown();

	return 0;
}

