
/*===================================================================
	File: GameCamera.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "CollisionBase.h"
#include "RenderBase.h"
#include "ScriptBase.h"

#include "Input/InputInclude.h"
#include "Input/Input.h"

#include "ScriptAccess/ScriptDataHolder.h"

#include "Camera.h"

#include "GameCamera.h"

namespace
{
	const float CAM_CATCHUP = 6.0f;
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
GameCamera::GameCamera( )
	: Camera()
	, m_LerpEnabled(true)
	, m_LerpSpeed(CAM_CATCHUP)
{
	//ScriptDataHolder* pScriptData = GetScriptDataHolder();
	//m_CameraData = pScriptData->GetCameraData();

	m_CurrentZoom = 50.0f;

	m_LerpPos = glm::vec3(0.0f, 0.0f, 100.0f);
	m_LerpTarget = glm::vec3(0.0f, 0.0f, 0.0f);

	// look down the positive Z
	m_Dir = glm::vec3(0.0f, 0.0f, 1.0f);

	m_UpVector = glm::vec3(0.0f, 1.0f, 0.0f);
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
GameCamera::~GameCamera()
{

}

/////////////////////////////////////////////////////
/// Method: SetupCamera
/// 
///
/////////////////////////////////////////////////////
void GameCamera::SetupCamera()
{
	
	//glRotatef( m_Rot.Z, 0.0f, 0.0f, 1.0f );

	renderer::OpenGL::GetInstance()->SetLookAt( m_Pos.x, m_Pos.y, m_Pos.z, 
													m_Target.x, m_Target.y, m_Target.z, m_UpVector.x, m_UpVector.y, m_UpVector.z );
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: deltaTime
///
/////////////////////////////////////////////////////
void GameCamera::Update( float deltaTime )
{
	glm::vec3 newPos(0.0f, 0.0f, 0.0f);
	glm::vec3 newLookAt(0.0f, 0.0f, 0.0f);

	if( m_LerpEnabled )
	{
		/*m_Pos = math::Lerp( m_Pos, m_LerpPos, deltaTime*m_LerpSpeed );

		m_Target = math::Lerp( m_Target, m_LerpTarget, deltaTime*m_LerpSpeed );*/
	}
}

/////////////////////////////////////////////////////
/// Method: RotateAroundPoint
/// 
///
/////////////////////////////////////////////////////
void GameCamera::RotateAroundPoint(glm::vec3& vCenter, float x, float y, float z)
{	
	glm::vec3 vVector = m_LerpPos - vCenter;
	float sinVal;
	float cosVal;

	if(x != 0.0f)
	{
		sinVal = glm::sin(x);
		cosVal = glm::cos(x);

		m_LerpPos.y = (float)(vCenter.y + sinVal*vVector.y + cosVal*vVector.x);
		m_LerpPos.z = (float)(vCenter.z - sinVal*vVector.x + cosVal*vVector.z);
	}
	if(y != 0.0f) 
	{
		sinVal = glm::sin(y);
		cosVal = glm::cos(y);

		m_LerpPos.x = (float)(vCenter.x + cosVal*vVector.x + sinVal*vVector.z);
		m_LerpPos.z = (float)(vCenter.z - sinVal*vVector.x + cosVal*vVector.z);

		m_Angle += glm::degrees(y);
	}
	if(z != 0.0f)
	{
		sinVal = glm::sin(z);
		cosVal = glm::cos(z);

		m_LerpPos.x = (float)(vCenter.x + cosVal*vVector.y - sinVal*vVector.x);		
		m_LerpPos.y = (float)(vCenter.y + sinVal*vVector.y + cosVal*vVector.x);
	}

	if( !m_LerpEnabled )
		m_Pos = m_LerpPos;
}