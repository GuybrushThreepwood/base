
/*===================================================================
	File: TiledTestState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "Resources/StringResources.h"
#include "GameStates/IBaseGameState.h"
#include "ScriptAccess/ScriptAccess.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/Examples/TiledTestState.h"

namespace
{
	bool showCollision = false;

	const char* description = "This is an example of a Tiled 2D level with Box2D physics \n\n- Press arrows or hold first touch and move, to move around the scene\n- Press space to change map style\n- Press t to toggle physics show";
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
TiledTestState::TiledTestState( StateManager& stateManager )
: IBaseGameState( stateManager, ID_TILEDTESTSTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;

	m_Level = 0;
	m_LevelIso = 0;
	m_LevelStaggeredIso = 0;
	m_LevelOrtho = 0;

	m_ViewPos.x = 0;
	m_ViewPos.y = 0;
	m_ViewPos.z = 0;
	
	m_ViewRangeMin.x = 0.0f;
	m_ViewRangeMin.y = 0.0f;
	
	m_ViewRangeMax.x = 0.0f;
	m_ViewRangeMax.y = 0.0f;
	
	m_CurrentRotation = renderer::VIEWROTATION_UNKNOWN;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
TiledTestState::~TiledTestState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void TiledTestState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

	renderer::OpenGL::GetInstance()->ClearColour( 135.0f/255.0f, 206.0f/255.0f, 235.0f/255.0f, 1.0f );

	b2Vec2 gravity( 0.0f, 0.0f );
	b2Vec2 minBounds( -100000.0f, -100000.0f );
	b2Vec2 maxBounds( 100000.0f, 100000.0f );

	physics::PhysicsWorldB2D::Create( minBounds, maxBounds, gravity, true );

	glm::vec3 camPos = glm::vec3(10, 10, 20);
	glm::vec3 camTarget = glm::vec3(0, 0, 0);
	m_GameCamera->SetLerp(true);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );

	m_LevelOrtho = new support::tiled::TiledLevelOrthogonal;
	DBG_ASSERT( m_LevelOrtho != 0 );

	m_LevelOrtho->LoadLevel( "assets/tests/tiled/landscape_scroll.lua" );
	m_Level = m_LevelOrtho;

	/*m_LevelStaggeredIso = new TiledLevelStaggeredIsometric;
	DBG_ASSERT( m_LevelStaggeredIso != 0 );
				
	m_LevelStaggeredIso->LoadLevel( "assets/tests/tiled/isometric_staggered_level.lua" );
	m_Level = m_LevelStaggeredIso;*/
	
	m_ViewPos.x = 0.0f;
	m_ViewPos.y = 0.0f;

	glm::vec2 levelScale = m_Level->GetScaleFactor();

	m_LevelDims.x = static_cast<int>( static_cast<float>( (m_Level->GetMapWidth()*m_Level->GetMapTileWidth()) * levelScale.x ) );
	m_LevelDims.y = static_cast<int>( static_cast<float>( (m_Level->GetMapHeight()*m_Level->GetMapTileHeight()) * levelScale.y ) );

	// set the description
	/*IState* pCurrState = m_pStateManager->GetSecondaryStateManager()->GetCurrentState();
	if( pCurrState->GetId() == UI_TITLESCREEN )
	{
		FrontendTitleScreenUI* pState = static_cast<FrontendTitleScreenUI*>( pCurrState );
		if( pState != 0 )
		{
			pState->SetDescriptionString( description );
		}
	}*/
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void TiledTestState::Exit()
{	
	m_GameCamera = 0;

	physics::PhysicsWorldB2D::Destroy();

	m_Level = 0;

	if( m_LevelOrtho != 0 )
	{
		delete m_LevelOrtho;
		m_LevelOrtho = 0;
	}

	if( m_LevelIso != 0 )
	{
		delete m_LevelIso;
		m_LevelIso = 0;
	}

	if( m_LevelStaggeredIso != 0 )
	{
		delete m_LevelStaggeredIso;
		m_LevelStaggeredIso = 0;
	}
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int TiledTestState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int TiledTestState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void TiledTestState::Update( float deltaTime )
{
	const int PHYSICS_VEL_ITERATIONS = 8;
	const int PHYSICS_POS_ITERATIONS = 3;

	m_LastDelta = deltaTime;
	
	// level change
/*	if( input::gInputState.TouchesData[input::SECOND_TOUCH].bRelease ||
	   debugInput.IsKeyPressed( input::KEY_SPACE, true, true ) )
	{
		if( m_Level == m_LevelOrtho )
		{
			m_CurrentRotation = renderer::VIEWROTATION_UNKNOWN;
			
			delete m_LevelOrtho;
			m_LevelOrtho = 0;
			
			m_LevelIso = new TiledLevelIsometric;
			DBG_ASSERT( m_LevelIso != 0 );
			
			m_LevelIso->LoadLevel( "assets/tests/tiled/isometric_level.lua" );
			m_Level = m_LevelIso;
			
			m_ViewPos.x = 0;
			m_ViewPos.y = 0;
			
			glm::vec2 levelScale = m_Level->GetScaleFactor();
			
			m_LevelDims.x = static_cast<int>( static_cast<float>( (m_Level->GetMapWidth()*m_Level->GetMapTileWidth()) * levelScale.x ) );
			m_LevelDims.y = static_cast<int>( static_cast<float>( (m_Level->GetMapHeight()*m_Level->GetMapTileHeight()) * levelScale.y ) );
		}
		else
		if( m_Level == m_LevelIso )
		{
			m_CurrentRotation = renderer::VIEWROTATION_UNKNOWN;
				
			delete m_LevelIso;
			m_LevelIso = 0;
				
			m_LevelStaggeredIso = new TiledLevelStaggeredIsometric;
			DBG_ASSERT( m_LevelStaggeredIso != 0 );
				
			m_LevelStaggeredIso->LoadLevel( "assets/tests/tiled/isometric_staggered_level.lua" );
			m_Level = m_LevelStaggeredIso;
				
			m_ViewPos.x = 0;
			m_ViewPos.y = 0;
				
			glm::vec2 levelScale = m_Level->GetScaleFactor();
				
			m_LevelDims.x = static_cast<int>( static_cast<float>( (m_Level->GetMapWidth()*m_Level->GetMapTileWidth()) * levelScale.x ) );
			m_LevelDims.y = static_cast<int>( static_cast<float>( (m_Level->GetMapHeight()*m_Level->GetMapTileHeight()) * levelScale.y ) );
		}
		else
		if( m_Level == m_LevelStaggeredIso )
		{
			m_CurrentRotation = renderer::VIEWROTATION_UNKNOWN;
				
			delete m_LevelStaggeredIso;
			m_LevelStaggeredIso = 0;
				
			m_LevelOrtho = new TiledLevelOrthogonal;
			DBG_ASSERT( m_LevelOrtho != 0 );
				
			m_LevelOrtho->LoadLevel( "assets/tests/tiled/landscape_scroll.lua" );
			m_Level = m_LevelOrtho;
				
			m_ViewPos.x = 0;
			m_ViewPos.y = 0;
				
			glm::vec2 levelScale = m_Level->GetScaleFactor();
				
			m_LevelDims.x = static_cast<int>( static_cast<float>( (m_Level->GetMapWidth()*m_Level->GetMapTileWidth()) * levelScale.x ) );
			m_LevelDims.y = static_cast<int>( static_cast<float>( (m_Level->GetMapHeight()*m_Level->GetMapTileHeight()) * levelScale.y ) );
		}

		return;
	}

	m_GameCamera->Update( deltaTime );

	const float SCROLL_SPEED = 200.0f;
	const float TOUCH_DELTA = 3;

	if( debugInput.IsKeyPressed( input::KEY_UPARROW, false, true ) )
		m_ViewPos.y -= static_cast<int>( SCROLL_SPEED*deltaTime );
	if( debugInput.IsKeyPressed( input::KEY_DOWNARROW, false, true ) )
		m_ViewPos.y += static_cast<int>( SCROLL_SPEED*deltaTime );
	if( debugInput.IsKeyPressed( input::KEY_LEFTARROW, false, true ) )
		m_ViewPos.x += static_cast<int>( SCROLL_SPEED*deltaTime );
	if( debugInput.IsKeyPressed( input::KEY_RIGHTARROW, false, true ) )
		m_ViewPos.x -= static_cast<int>( SCROLL_SPEED*deltaTime );

	if((input::gInputState.TouchesData[input::FIRST_TOUCH].bPress || input::gInputState.TouchesData[input::FIRST_TOUCH].bHeld))
	{
		if( input::gInputState.TouchesData[input::FIRST_TOUCH].nXDelta <= -TOUCH_DELTA )
			m_ViewPos.x += static_cast<int>( SCROLL_SPEED*deltaTime ) * static_cast<int>(-input::gInputState.TouchesData[input::FIRST_TOUCH].nXDelta);
		else 
		if( input::gInputState.TouchesData[input::FIRST_TOUCH].nXDelta >= TOUCH_DELTA )
			m_ViewPos.x -= static_cast<int>( SCROLL_SPEED*deltaTime ) * static_cast<int>(input::gInputState.TouchesData[input::FIRST_TOUCH].nXDelta);

		if( input::gInputState.TouchesData[input::FIRST_TOUCH].nYDelta <= -TOUCH_DELTA )
			m_ViewPos.y -= static_cast<int>( SCROLL_SPEED*deltaTime ) * static_cast<int>(-input::gInputState.TouchesData[input::FIRST_TOUCH].nYDelta);
		else 
		if( input::gInputState.TouchesData[input::FIRST_TOUCH].nYDelta >= TOUCH_DELTA )
			m_ViewPos.y += static_cast<int>( SCROLL_SPEED*deltaTime ) * static_cast<int>(input::gInputState.TouchesData[input::FIRST_TOUCH].nYDelta);
	}

	if( debugInput.IsKeyPressed( input::KEY_T, true, false ) )
		showCollision = !showCollision;

	/*if( m_ViewPos.X < static_cast<int>(m_ViewRangeMin.X) )
		m_ViewPos.X = static_cast<int>(m_ViewRangeMin.X);
	else if( m_ViewPos.X > static_cast<int>(m_ViewRangeMax.X) )
		m_ViewPos.X = static_cast<int>(m_ViewRangeMax.X);

	if( m_ViewPos.Y < static_cast<int>(m_ViewRangeMin.Y) )
		m_ViewPos.Y = static_cast<int>(m_ViewRangeMin.Y);
	else if( m_ViewPos.Y > static_cast<int>(m_ViewRangeMax.Y) )
		m_ViewPos.Y = static_cast<int>(m_ViewRangeMax.Y);*/

	if( m_Level != 0 )
		m_Level->Update( deltaTime );

	physics::PhysicsWorldB2D::GetWorld()->Step( PHYSICS_TIMESTEP, PHYSICS_VEL_ITERATIONS, PHYSICS_POS_ITERATIONS );

	//CheckAchievements( deltaTime );
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void TiledTestState::Draw()
{
	// default colour
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	renderer::OpenGL::GetInstance()->SetNearFarClip( NEAR_CLIP_ORTHO, FAR_CLIP_ORTHO );
	renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

	snd::SoundManager::GetInstance()->SetListenerOrientation(std::sin(glm::radians(180.0f)), 0.0f, std::cos(glm::radians(180.0f)), 0.0f, 1.0f, 0.0f);

	renderer::OpenGL::GetInstance()->DepthMode( false, GL_LESS );
	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->DisableLighting();

	glm::mat4 modelMatrix = glm::translate( glm::mat4(1.0f), glm::vec3( -m_ViewPos.x, m_ViewPos.y, 0.0f ) );
	renderer::OpenGL::GetInstance()->SetModelMatrix( modelMatrix );

	if( m_Level != 0 )
		m_Level->DrawLayers( -1, -1 );

	renderer::OpenGL::GetInstance()->DepthMode( true, GL_ALWAYS );
	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->DisableTexturing();
	renderer::OpenGL::GetInstance()->DisableLighting();

	if( showCollision )
		physics::PhysicsWorldB2D::DrawDebugData();

	renderer::OpenGL::GetInstance()->EnableTexturing();
	renderer::OpenGL::GetInstance()->SetNearFarClip( NEAR_CLIP_ORTHO, FAR_CLIP_ORTHO );
	renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );
	renderer::OpenGL::GetInstance()->DisableLighting();
	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	
	GL_CHECK;
		
	int offset = 10;
	int xPos;
	int yPos;
		
	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;

	DBGPRINT( xPos, yPos-=offset, "DELTA (%.6f)", m_LastDelta );
	DBGPRINT( xPos, yPos-=offset, "VIEW POS (%.2f  %.2f  %.2f)", m_ViewPos.x, m_ViewPos.y, m_ViewPos.z );
	DBGPRINT( xPos, yPos-=offset, "VIEW RANGE MIN ( %.2f  %.2f )", m_ViewRangeMin.x, m_ViewRangeMin.y );
	DBGPRINT( xPos, yPos-=offset, "VIEW RANGE MAX ( %.2f  %.2f )", m_ViewRangeMax.x, m_ViewRangeMax.y );

	DBGPRINT( xPos, yPos-=offset, "LEVEL DIMS ( %d  %d )", m_LevelDims.x, m_LevelDims.y );
}
