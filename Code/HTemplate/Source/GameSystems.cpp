
/*===================================================================
	File: GameSystems.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "CollisionBase.h"
#include "SoundBase.h"
#include "SupportBase.h"

#include "HTemplate.h"
#include "HTemplateConsts.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "ScriptAccess/ScriptAccess.h"
#include "Resources/StringResources.h"

#include "GameSystems.h"

GameSystems* GameSystems::ms_Instance = 0;

namespace
{

}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
GameSystems::GameSystems( )
{
	DBG_ASSERT_MSG( (ms_Instance == 0), "GameSystems instance already created" );

	ms_Instance = this;

	m_pScriptData = 0;

	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	//m_CameraData = m_pScriptData->GetCameraData();
	m_GameData = m_pScriptData->GetGameData();

	// update some values before use
	glm::vec3 scaleFactor(core::app::GetWidthScale(), core::app::GetHeightScale(), 1.0f);

	m_GameData.ACHIEVEMENT_ICON_MAXX	*= scaleFactor.x;
	m_GameData.ACHIEVEMENT_ICON_SPEED	*= scaleFactor.x;

	//m_GameData.ANALOGUE_DEAD_ZONE		*= scaleFactor.X;
	
	// adbar assumes landscape
/*	m_GameData.ADBAR_PHONE_POSX		*= scaleFactor.Y;
	m_GameData.ADBAR_PHONE_POSY		*= scaleFactor.X;
	m_GameData.ADBAR_PHONEHD_POSX	*= scaleFactor.Y;
	m_GameData.ADBAR_PHONEHD_POSY	*= scaleFactor.X;
	m_GameData.ADBAR_TABLET_POSX	*= scaleFactor.Y;
	m_GameData.ADBAR_TABLET_POSY	*= scaleFactor.X;
	m_GameData.ADBAR_TABLETHD_POSX	*= scaleFactor.Y;
	m_GameData.ADBAR_TABLETHD_POSY	*= scaleFactor.X;
*/
	m_pScriptData->SetGameData(m_GameData);
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
GameSystems::~GameSystems()
{
	ms_Instance = 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void GameSystems::Update( float deltaTime )
{
}

/////////////////////////////////////////////////////
/// Method: CreatePhysics
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::CreatePhysics()
{
	//physics::PhysicsWorldODE::Create();

}

/////////////////////////////////////////////////////
/// Method: DestroyPhysics
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::DestroyPhysics()
{
	
	//physics::PhysicsWorldODE::Destroy();
}

/////////////////////////////////////////////////////
/// Method: CreateGameCamera
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::CreateGameCamera()
{
	m_GameCamera = new GameCamera();
	DBG_ASSERT_MSG( (m_GameCamera != 0), "Could not create GameCamera class" );

}

/////////////////////////////////////////////////////
/// Method: DestroyGameCamera
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::DestroyGameCamera()
{
	if(m_GameCamera != 0)
	{
		delete m_GameCamera;
		m_GameCamera = 0;
	}
}

/////////////////////////////////////////////////////
/// Method: PreloadResources
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::PreloadResources()
{

}
		
/////////////////////////////////////////////////////
/// Method: ClearPreloaded
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::ClearPreloaded()
{

}

/////////////////////////////////////////////////////
/// Method: DrawDebug
/// Params: None
///
/////////////////////////////////////////////////////
void GameSystems::DrawDebug()
{

}

/////////////////////////////////////////////////////
/// Method: InitAudio
/// Params: 
///
/////////////////////////////////////////////////////
void GameSystems::InitAudio()
{

}
