
/*===================================================================
	File: PVRTestState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __PVRTESTSTATE_H__
#define __PVRTESTSTATE_H__

#include "CoreBase.h"

#ifdef BASE_SUPPORT_PVRSDK

#include "ScriptAccess/ScriptDataHolder.h"

// forward declare
class IState;
class IBaseGameState;

#include "PVRCore/IGraphicsContext.h"
#include "PVRCore/IAssetProvider.h"
#include "PVRCore/StringHash.h"
#include "PVRAssets/Texture/TextureHeader.h"
#include "PVRAssets/Model.h"

class PVRTestState : public IBaseGameState
{
	public:
		PVRTestState(StateManager& stateManager);
		virtual ~PVRTestState();

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();

	private:
		void CheckAchievements(float deltaTime);

		bool loadTextures();
		bool loadShaders();
		bool LoadVbos();

		pvr::Result::Enum loadTexturePVR(const pvr::StringHash& filename, GLuint& outTexHandle, pvr::assets::Texture* outTexture, pvr::assets::TextureHeader* outDescriptor);

		void drawMesh(int i32NodeIndex);

	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;
		
		pvr::assets::ModelHandle m_Scene;
};

#endif // BASE_SUPPORT_PVRSDK

#endif // __PVRTESTSTATE_H__

