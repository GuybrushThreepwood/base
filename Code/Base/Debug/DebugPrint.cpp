
/*===================================================================
	File: DebugPrint.cpp
	Library: Debug

	(C)Hidden Games
=====================================================================*/

#if defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

#include "CoreBase.h"

#include "Render/RenderConsts.h"
#include "Render/OpenGLCommon.h"
#include "Render/OpenGL.h"
#include "Render/glewES.h"

#include "Render/TextureShared.h"
#include "Render/Texture.h"
#include "Render/TextureLoadAndUpload.h"
#include "Render/ShaderShared.h"
#include "Render/Shader.h"

#include "Debug/DebugFont.h"
#include "Debug/DebugPrint.h"

namespace DbgPrint
{
	const int FONT_WIDTH = 8+1;
	const int FONT_HEIGHT = 8+1;
	const int FONT_CHAR_PER_ROW = 16;
	const int FONT_CHAR_PER_COLUMN = 16;
	const float FONT_DEPTH		= 0.5f;
	const float FONT_SPACING = 4.0f;

	struct FontVert
	{
		glm::vec2 v;
		glm::vec2 uv;
	};
	
	FontVert FontQuad[(FONT_CHAR_PER_ROW * FONT_CHAR_PER_COLUMN) * 4];

	GLuint DebugPrintVAO = renderer::INVALID_OBJECT;
	GLuint DebugPrintBO = renderer::INVALID_OBJECT;

	GLuint TexID = renderer::INVALID_OBJECT;
	GLfloat fontRed = 1.0f, fontGreen = 1.0f, fontBlue = 1.0f, fontAlpha = 1.0f;

	renderer::Shader DebugPrintShader;
    renderer::OpenGL* pOpenGLContext=0;
    
	const char DbgPrintVertexShader[] = 
	"	//_DBGPRINT_ precision highp float;\n\
		layout(location=0) in vec3 base_v;\n\
		layout(location=1) in vec2 base_uv0;\n\
		out vec2 tu0;\n\
		out vec4 colour0;\n\
		uniform mat4 ogl_ModelViewProjectionMatrix;\n\
		uniform vec4 ogl_VertexColour;\n\
		void main()\n\
		{\n\
			tu0 = base_uv0;\n\
			colour0 = ogl_VertexColour;\n\
			vec4 vInVertex = ogl_ModelViewProjectionMatrix * vec4(base_v, 1.0);\n\
			gl_Position = vInVertex;\n\
		}\n\
	";
	const char DbgPrintFragmentShader[] = 
	"	//_DBGPRINT_ precision highp float;\n\
		uniform sampler2D texUnit0;\n\
		in vec2 tu0;\n\
		in vec4 colour0;\n\
		out vec4 fragColour;\n\
		void main()\n\
		{\n\
			vec4 color = colour0 * texture2D(texUnit0, tu0.xy);\n\
			if( color.a < 0.4 )\n\
				discard;\n\
			fragColour = color;\n\
		}\n\
	";
}

/////////////////////////////////////////////////////
/// Function: DebugCreateFont
/// Params: None
///
/////////////////////////////////////////////////////
void dbg::DebugCreateFont( renderer::OpenGL* openGLContext )
{
    if( openGLContext == 0 )
        DbgPrint::pOpenGLContext = renderer::OpenGL::GetInstance();
    else
        DbgPrint::pOpenGLContext = openGLContext;
    
	unsigned int i=0;
	
	float cx, cy;
	float cwx, cwy;
    
	dbg::DebugDestroyFont();

	glGenTextures( 1, &DbgPrint::TexID );
	DbgPrint::pOpenGLContext->BindTexture( DbgPrint::TexID );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST );
	glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST );
	glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, 128, 128, 0, GL_RGBA, GL_UNSIGNED_BYTE, debug_font8x8 );

	cwx = (1.0f / (DbgPrint::FONT_CHAR_PER_ROW*DbgPrint::FONT_WIDTH)) * DbgPrint::FONT_WIDTH;
	cwy = (1.0f / (DbgPrint::FONT_CHAR_PER_COLUMN*DbgPrint::FONT_HEIGHT)) * DbgPrint::FONT_HEIGHT;

	glGenVertexArrays(1, &DbgPrint::DebugPrintVAO);
	glBindVertexArray(DbgPrint::DebugPrintVAO);

	GL_CHECK

	if (DbgPrint::DebugPrintBO == renderer::INVALID_OBJECT)
		glGenBuffers(1, &DbgPrint::DebugPrintBO);

	int vertOffset = 0;
	for( i=0; i < (DbgPrint::FONT_CHAR_PER_ROW * DbgPrint::FONT_CHAR_PER_COLUMN); i++, vertOffset+=4 )
	{
		// X position of the char
		cx = (float)(i % DbgPrint::FONT_CHAR_PER_ROW) * cwx;
			
		// Y position of the char
		cy = (float)(i / DbgPrint::FONT_CHAR_PER_COLUMN) * cwy;

		DbgPrint::FontQuad[vertOffset].uv = glm::vec2(cx, 1.0f - cy - cwy);
		DbgPrint::FontQuad[vertOffset].v = glm::vec2(0.0f, 0.0f);

		DbgPrint::FontQuad[vertOffset+1].uv = glm::vec2(cx + cwx, 1.0f - cy - cwy);
		DbgPrint::FontQuad[vertOffset+1].v = glm::vec2(DbgPrint::FONT_WIDTH - 1.0f, 0.0f);

		DbgPrint::FontQuad[vertOffset+2].uv = glm::vec2(cx, 1.0f - cy);
		DbgPrint::FontQuad[vertOffset+2].v = glm::vec2(0.0f, DbgPrint::FONT_HEIGHT - 1.0f);

		DbgPrint::FontQuad[vertOffset+3].uv = glm::vec2(cx + cwx, 1.0f - cy);
		DbgPrint::FontQuad[vertOffset+3].v = glm::vec2(DbgPrint::FONT_WIDTH - 1.0f, DbgPrint::FONT_HEIGHT - 1.0f);
	}

	int totalElements = (DbgPrint::FONT_CHAR_PER_ROW * DbgPrint::FONT_CHAR_PER_COLUMN)*4;

	glBindBuffer(GL_ARRAY_BUFFER, DbgPrint::DebugPrintBO);
	glBufferData(GL_ARRAY_BUFFER, totalElements*(sizeof(DbgPrint::FontVert)), &DbgPrint::FontQuad[0].v, GL_STATIC_DRAW);

	GL_CHECK

	DbgPrint::DebugPrintShader.CreateFromStrings(DbgPrint::DbgPrintVertexShader, DbgPrint::DbgPrintFragmentShader, nullptr, DbgPrint::pOpenGLContext);

	glEnableVertexAttribArray(renderer::SHADERATTRIB_VERT);
	glVertexAttribPointer(renderer::SHADERATTRIB_VERT, 2, GL_FLOAT, GL_FALSE, sizeof(DbgPrint::FontVert), VBO_OFFSET(0));

	GL_CHECK

	glEnableVertexAttribArray(renderer::SHADERATTRIB_TEXCOORDS);
	glVertexAttribPointer(renderer::SHADERATTRIB_TEXCOORDS, 2, GL_FLOAT, GL_FALSE, sizeof(DbgPrint::FontVert), VBO_OFFSET(sizeof(glm::vec2)));

	GL_CHECK

	glBindVertexArray(0);

	// check for errors
	GL_CHECK
}

/////////////////////////////////////////////////////
/// Method: DebugSetFontColour
/// Params: [in]r, [in]g, [in]b, [in]a
///
/////////////////////////////////////////////////////
void dbg::DebugSetFontColour( unsigned char r, unsigned char g, unsigned char b, unsigned char a )
{
	DbgPrint::fontRed = r;
	DbgPrint::fontGreen = g;
	DbgPrint::fontBlue = b;
	DbgPrint::fontAlpha = a;
}

/////////////////////////////////////////////////////
/// Function: DebugPrint
/// Params: [in]x, [in]y, [in]szString
///
/////////////////////////////////////////////////////
void dbg::DebugPrint( int x, int y, const char* szString, ... )
{
	char szText[256];
	unsigned int i=0;
	std::va_list ap;				

	if( szString == 0 )		
		return;				

	if (!DbgPrint::DebugPrintShader.IsValid())
		return;

	va_start( ap, szString );					
		std::vsprintf( szText, szString, ap );			
	va_end( ap );

	DbgPrint::pOpenGLContext->DepthMode( true, GL_ALWAYS );
	DbgPrint::pOpenGLContext->DisableVBO();

	DbgPrint::DebugPrintShader.Bind();

	// grab all matrices
	glm::mat4 projMat	= DbgPrint::pOpenGLContext->GetProjectionMatrix();
	glm::mat4 modelMat	= DbgPrint::pOpenGLContext->GetModelMatrix();

	if (DbgPrint::DebugPrintVAO != renderer::INVALID_OBJECT)
		glBindVertexArray(DbgPrint::DebugPrintVAO);

	if (DbgPrint::DebugPrintBO != renderer::INVALID_OBJECT)
		glBindBuffer(GL_ARRAY_BUFFER, DbgPrint::DebugPrintBO);

	// check for errors
	GL_CHECK

	if (DbgPrint::TexID != renderer::INVALID_OBJECT)
		DbgPrint::pOpenGLContext->BindTexture( DbgPrint::TexID );

	// set the font colour
	DbgPrint::pOpenGLContext->SetColour( DbgPrint::fontRed, DbgPrint::fontGreen, DbgPrint::fontBlue, DbgPrint::fontAlpha );
		
	glm::mat4 objMatrix = glm::translate( modelMat, glm::vec3(static_cast<float>(x), static_cast<float>(y), DbgPrint::FONT_DEPTH) );

	for( i=0; i < (int)std::strlen(szText); ++i )
	{
		char index = szText[i];

		// don't want lower case
		index -= 32;

		DbgPrint::DebugPrintShader.SetUniform(renderer::Shader::ModelViewProjectionMatrix, glm::value_ptr(projMat*objMatrix) );

		glDrawArrays( GL_TRIANGLE_STRIP, (int)index*4, 4 );	

		GL_CHECK

		objMatrix = glm::translate( objMatrix, glm::vec3(DbgPrint::FONT_SPACING, 0.0f, 0.0f) );
	}

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	DbgPrint::pOpenGLContext->DepthMode( true, GL_LESS );

	DbgPrint::DebugPrintShader.UnBind();
}

/////////////////////////////////////////////////////
/// Function: DebugDestroyFont
/// Params: None
///
/////////////////////////////////////////////////////
void dbg::DebugDestroyFont()
{
	if( DbgPrint::TexID != renderer::INVALID_OBJECT )
	{
		renderer::RemoveTexture( DbgPrint::TexID );
		DbgPrint::TexID = renderer::INVALID_OBJECT;
	}

	if (DbgPrint::DebugPrintBO != renderer::INVALID_OBJECT)
	{
		glDeleteBuffers(1, &DbgPrint::DebugPrintBO);
		DbgPrint::DebugPrintBO = renderer::INVALID_OBJECT;
	}

	if (DbgPrint::DebugPrintVAO != renderer::INVALID_OBJECT)
	{
		glDeleteVertexArrays(1, &DbgPrint::DebugPrintVAO);
		DbgPrint::DebugPrintVAO = renderer::INVALID_OBJECT;
	}

	DbgPrint::DebugPrintShader.Shutdown();
}

#endif // defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)
