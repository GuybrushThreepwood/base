
/*===================================================================
	File: TiledCommon.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDCOMMON_H__
#define __TILEDCOMMON_H__

#ifdef BASE_SUPPORT_BOX2D

#include "CoreBase.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		const int MAX_PROPERTY_STRING = 256;

		struct Property
		{
			const char* name;
			const char* value;
		};

		struct TileAnimationFrame
		{
			int tileid;
			float duration;
		};

		struct Tile
		{
			int tileId;

			int numProperties;
			Property* propertyList;

			int numAnimationFrames;
			TileAnimationFrame* animationFrameList;
		};

		struct Tileset
		{
			const char* name;
			int firstgid;
			int tileWidth;
			int tileHeight;
			int spacing;
			int margin;
			const char* image;
			int imageWidth;
			int imageHeight;
			const char* transparentColor;
			int tileCount;

			int numProperties;
			Property* propertyList;

			int numTileData;
			Tile* tileDataList;

			int totalTilesX;
			int totalTilesY;
			int totalTiles;

			float uvTileWidth;
			float uvTileHeight;

			glm::vec2 scaledTileSize;
			glm::vec2 scaledHalfTileSize;

			GLuint tilesetTexture;
		};

		struct TileData
		{
			bool enabled;

			int tileId;
			int realId;
			TilesetHandler::AnimationData* anim;
			glm::ivec2 tileCenter;

			int tileX;
			int tileY;
		};

		struct ObjectData
		{
			int objectid;
			const char* name;
			const char* type;
			const char* shape;
			int x;
			int y;
			int width;
			int height;
			float rotation;
			int gid;
			bool visible;

			int numProperties;
			Property* propertyList;

			int numPoints;
			glm::ivec2* polygonPoints;
		};

		struct Layer
		{
			const char* type;
			const char* name;
			int x;
			int y;
			int width;
			int height;
			bool visible;
			int opacity;
			const char* transparentColor;
			const char* image;

			int numProperties;
			Property* propertyList;

			int numObjects;
			ObjectData* objectList;

			const char* encoding;
			TileData* data;
		};

		struct PropertyValue
		{
			char text[MAX_PROPERTY_STRING];
			float floatNumber;
			int integerNumber;
		};

		struct AnimationFiles
		{
			const char* animationFile;
			TilesetHandler* pTilesetHandler;
		};
	}
}
#endif // BASE_SUPPORT_BOX2D

#endif // __TILEDCOMMON_H__

