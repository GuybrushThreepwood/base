
/*===================================================================
	File: ProfileSelectState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "GameStates/IBaseGameState.h"
#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/TitleScreenState.h"

namespace
{

}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
TitleScreenState::TitleScreenState( StateManager& stateManager )
: IBaseGameState( stateManager, ID_TITLESCREENSTATE )
{
	m_LastDelta = 0.0f;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
TitleScreenState::~TitleScreenState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void TitleScreenState::Enter()
{

}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void TitleScreenState::Exit()
{

}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int TitleScreenState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int TitleScreenState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void TitleScreenState::Update( float deltaTime )
{
	m_LastDelta = deltaTime;
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void TitleScreenState::Draw()
{
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	if( gDebugCamera.IsEnabled() )
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 0.5f, 10000.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		renderer::OpenGL::GetInstance()->SetLookAt( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z, 
												gDebugCamera.GetTarget().x, gDebugCamera.GetTarget().y, gDebugCamera.GetTarget().z );
	
		snd::SoundManager::GetInstance()->SetListenerPosition( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z );
		snd::SoundManager::GetInstance()->SetListenerOrientation((float)std::sin(glm::radians(-gDebugCamera.GetAngle())), 0.0f, (float)std::cos(glm::radians(-gDebugCamera.GetAngle())),
																	0.0f, 1.0f, 0.0f );
	}
	else
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 1.0f, 10.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		renderer::OpenGL::GetInstance()->SetLookAt( -0.0f, 5.0f, 12.0f, 0.0f, 0.0f, 0.0f );

		snd::SoundManager::GetInstance()->SetListenerPosition( 0.0f, 2.0f, 10.0f );
		snd::SoundManager::GetInstance()->SetListenerOrientation(std::sin(glm::radians(180.0f)), 0.0f, std::cos(glm::radians(180.0f)), 0.0f, 1.0f, 0.0f);
	}

	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LESS );
	renderer::OpenGL::GetInstance()->BlendMode( true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

	renderer::OpenGL::GetInstance()->DisableLighting();


	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
}
