
/*===================================================================
	File: ScriptAccess.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "HTemplateConsts.h"

#include "GameSystems.h"

#include "Resources/StringResources.h"
#include "Resources/TextureResources.h"

#include "ScriptAccess/ScriptDataHolder.h"
#include "ScriptAccess/ScriptAccess.h"
#include "ScriptAccess/HTemplate/HTemplateAccess.h"

namespace hScript
{
	ScriptDataHolder* pScriptData = 0;
}

LuaGlobal scriptGlobals[] = 
{
	// core languages
	{ "LANGUAGE_ENGLISH",				static_cast<double>(core::LANGUAGE_ENGLISH),			LUA_TNUMBER },
	{ "LANGUAGE_FRENCH",				static_cast<double>(core::LANGUAGE_FRENCH),				LUA_TNUMBER },
	{ "LANGUAGE_ITALIAN",				static_cast<double>(core::LANGUAGE_ITALIAN),			LUA_TNUMBER },
	{ "LANGUAGE_GERMAN",				static_cast<double>(core::LANGUAGE_GERMAN),				LUA_TNUMBER },
	{ "LANGUAGE_SPANISH",				static_cast<double>(core::LANGUAGE_SPANISH),			LUA_TNUMBER },

#if defined(BASE_SUPPORT_OPENGL) || defined(BASE_SUPPORT_OPENGLES)
	// used by emitters
	{ "GL_EXP",		static_cast<double>(GL_EXP),	LUA_TNUMBER },
	{ "GL_EXP2",	static_cast<double>(GL_EXP2),	LUA_TNUMBER },
#endif // (BASE_SUPPORT_OPENGL) || defined(BASE_SUPPORT_OPENGLES)

	// depth/alpha
	{ "GL_NEVER",		static_cast<double>(GL_NEVER),		LUA_TNUMBER }, 
	{ "GL_LESS",		static_cast<double>(GL_LESS),		LUA_TNUMBER },
	{ "GL_LEQUAL",		static_cast<double>(GL_LEQUAL),		LUA_TNUMBER },
	{ "GL_EQUAL",		static_cast<double>(GL_EQUAL),		LUA_TNUMBER },
	{ "GL_GREATER",		static_cast<double>(GL_GREATER),	LUA_TNUMBER },
	{ "GL_NOTEQUAL",	static_cast<double>(GL_NOTEQUAL),	LUA_TNUMBER },
	{ "GL_GEQUAL",		static_cast<double>(GL_GEQUAL),		LUA_TNUMBER },
	{ "GL_ALWAYS",		static_cast<double>(GL_ALWAYS),		LUA_TNUMBER },

	// blend
	{ "GL_ZERO",				static_cast<double>(GL_ZERO),				LUA_TNUMBER },
	{ "GL_ONE",					static_cast<double>(GL_ONE),				LUA_TNUMBER },
	{ "GL_SRC_COLOR",			static_cast<double>(GL_SRC_COLOR),			LUA_TNUMBER },
	{ "GL_ONE_MINUS_SRC_COLOR",	static_cast<double>(GL_ONE_MINUS_SRC_COLOR),LUA_TNUMBER },
	{ "GL_DST_COLOR",			static_cast<double>(GL_DST_COLOR),			LUA_TNUMBER },
	{ "GL_ONE_MINUS_DST_COLOR",	static_cast<double>(GL_ONE_MINUS_DST_COLOR),LUA_TNUMBER },
	{ "GL_SRC_ALPHA",			static_cast<double>(GL_SRC_ALPHA),			LUA_TNUMBER },
	{ "GL_ONE_MINUS_SRC_ALPHA",	static_cast<double>(GL_ONE_MINUS_SRC_ALPHA),LUA_TNUMBER },
	{ "GL_DST_ALPHA",			static_cast<double>(GL_DST_ALPHA),			LUA_TNUMBER },
	{ "GL_ONE_MINUS_DST_ALPHA",	static_cast<double>(GL_ONE_MINUS_DST_ALPHA),LUA_TNUMBER },
	{ "GL_SRC_ALPHA_SATURATE",	static_cast<double>(GL_SRC_ALPHA_SATURATE),	LUA_TNUMBER },


	// texture filters
	{ "GL_NEAREST",	static_cast<double>(GL_NEAREST),	LUA_TNUMBER },
	{ "GL_LINEAR",	static_cast<double>(GL_LINEAR),		LUA_TNUMBER },

	{ "GL_NEAREST_MIPMAP_NEAREST",	static_cast<double>(GL_NEAREST_MIPMAP_NEAREST),	LUA_TNUMBER },
	{ "GL_LINEAR_MIPMAP_NEAREST",	static_cast<double>(GL_LINEAR_MIPMAP_NEAREST),	LUA_TNUMBER },
	{ "GL_NEAREST_MIPMAP_LINEAR",	static_cast<double>(GL_NEAREST_MIPMAP_LINEAR),	LUA_TNUMBER },
	{ "GL_LINEAR_MIPMAP_LINEAR",	static_cast<double>(GL_LINEAR_MIPMAP_LINEAR),	LUA_TNUMBER },

};


/////////////////////////////////////////////////////
/// Function: RegisterScriptFunctions
/// Params: [in]dataHolder
///
/////////////////////////////////////////////////////
void RegisterScriptFunctions( ScriptDataHolder& dataHolder )
{
	int i=0;
	hScript::pScriptData = &dataHolder;

	// setup globals
	for( i=0; i < sizeof(scriptGlobals)/sizeof(LuaGlobal); ++i )
	{
		lua_pushnumber( script::LuaScripting::GetState(), scriptGlobals[i].nConstantValue );
		lua_setglobal( script::LuaScripting::GetState(), scriptGlobals[i].szConstantName );
	}
	script::LuaScripting::GetInstance()->RegisterFunction( "SetDevData",		ScriptSetDevData );

	RegisterHTemplateFunctions( dataHolder );
}

/////////////////////////////////////////////////////
/// Function: ScriptSetDevData
/// Params: [in]pState
///
/////////////////////////////////////////////////////
int ScriptSetDevData( lua_State* pState )
{
	if( lua_istable( pState, 1 ) )
	{
		ScriptDataHolder::DevScriptData devData = hScript::pScriptData->GetDevData();

		// grab the data
		devData.enablePhysicsDraw	= static_cast<bool>(script::LuaGetBoolFromTableItem( "enablePhysicsDraw", 1 )); 
		devData.enableDebugDraw		= static_cast<bool>(script::LuaGetBoolFromTableItem( "enableDebugDraw", 1 ));
		devData.enableDebugUIDraw	= static_cast<bool>(script::LuaGetBoolFromTableItem( "enableDebugUIDraw", 1 ));

		devData.isTablet			= static_cast<bool>(script::LuaGetBoolFromTableItem( "isTablet", 1, false ));
		devData.isRetina			= static_cast<bool>(script::LuaGetBoolFromTableItem( "isRetina", 1, false ));
		devData.isPCOnly			= static_cast<bool>(script::LuaGetBoolFromTableItem( "isPCOnly", 1, false ));
	
		devData.frameLock30			= static_cast<bool>(script::LuaGetBoolFromTableItem( "frameLock30", 1 )); 
		devData.frameLock60			= static_cast<bool>(script::LuaGetBoolFromTableItem( "frameLock60", 1 )); 

		devData.assetBaseWidth		= static_cast<int>(script::LuaGetNumberFromTableItem( "assetBaseWidth", 1 ));
		devData.assetBaseHeight		= static_cast<int>(script::LuaGetNumberFromTableItem( "assetBaseHeight", 1 ));
		devData.screenWidth			= static_cast<int>(script::LuaGetNumberFromTableItem( "screenWidth", 1 ));
		devData.screenHeight		= static_cast<int>(script::LuaGetNumberFromTableItem( "screenHeight", 1 ));
		devData.bootState			= static_cast<int>(script::LuaGetNumberFromTableItem( "bootState", 1 ));

		devData.showPCAdBar				= static_cast<bool>(script::LuaGetBoolFromTableItem( "showPCAdBar", 1 ));
		devData.useVertexArrays			= static_cast<bool>(script::LuaGetBoolFromTableItem( "useVertexArrays", 1 ));
		devData.developerSaveFileRoot	= script::LuaGetStringFromTableItem( "developerSaveFileRoot", 1 );
		devData.userSaveFileRoot		= script::LuaGetStringFromTableItem( "userSaveFileRoot", 1 );
		devData.language				= static_cast<int>(script::LuaGetNumberFromTableItem( "language", 1 ));

		devData.allowAdvertBarScaling	= static_cast<bool>(script::LuaGetBoolFromTableItem( "allowAdvertBarScaling", 1 ));
		devData.appAdFilterId			= script::LuaGetStringFromTableItem( "appAdFilterId", 1 );
		devData.localAdvertXML			= script::LuaGetStringFromTableItem( "localAdvertXML", 1 );
		devData.externalAdvertXML		= script::LuaGetStringFromTableItem( "externalAdvertXML", 1 );
		
		devData.allowDebugCam		= static_cast<bool>(script::LuaGetBoolFromTableItem( "allowDebugCam", 1 ));

		hScript::pScriptData->SetDevData( devData );
	}

	return 0;
}

/////////////////////////////////////////////////////
/// Function: GetScriptDataHolder
/// Params: None
///
/////////////////////////////////////////////////////
ScriptDataHolder* GetScriptDataHolder()
{
	return hScript::pScriptData;
}
