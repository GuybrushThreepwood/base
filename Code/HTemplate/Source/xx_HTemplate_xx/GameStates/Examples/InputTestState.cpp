
/*===================================================================
	File: InputTestState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"


#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"

#include "GameStates/IBaseGameState.h"
#include "ScriptAccess/ScriptAccess.h"
#include "Resources/FontResources.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/Examples/InputTestState.h"

namespace
{
	const char* description = "Input Tests\n\n * Use PGUP and PGDOWN to cycle connected joysticks/gamepads";
	const int DEAD_ZONE = 6000;
	
	char* HatToString[] =
	{
		"SDL_HAT_CENTERED",
		"SDL_HAT_UP",
		"SDL_HAT_RIGHT",
		"SDL_HAT_DOWN",
		"SDL_HAT_LEFT",
	};

	unsigned char HatToValue[] =
	{
		SDL_HAT_CENTERED,
		SDL_HAT_UP,
		SDL_HAT_RIGHT,
		SDL_HAT_DOWN,
		SDL_HAT_LEFT,
	};
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
InputTestState::InputTestState(StateManager& stateManager)
: IBaseGameState( stateManager, ID_INPUTTESTSTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;

	m_CurrentFontSize = 24;
	m_pSelectedJoypad = 0;
	m_SelectedIndex = -1;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
InputTestState::~InputTestState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void InputTestState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

//	if (input::InputManager::IsInitialised())
//		input::InputManager::GetInstance()->SetCallback(this);

	renderer::OpenGL::GetInstance()->ClearColour( 0.5f, 0.5f, 0.5f, 1.0f );

	glm::vec3 camPos = glm::vec3(0, 0, 20);
	glm::vec3 camTarget = glm::vec3(0, 0, -20);
	m_GameCamera->SetLerp(true);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );

	// set the description
	/*IState* pCurrState = m_pStateManager->GetSecondaryStateManager()->GetCurrentState();
	if( pCurrState->GetId() == UI_TITLESCREEN )
	{
		FrontendTitleScreenUI* pState = static_cast<FrontendTitleScreenUI*>( pCurrState );
		if( pState != 0 )
		{
			pState->SetDescriptionString( description );
		}
	}*/

	glm::vec4 white(1.0f, 1.0f, 1.0f, 1.0f);
	const res::FontResourceStore* pFR = res::GetFontResource(1, m_CurrentFontSize, 0, white, false);
	m_FontTest = pFR->fontRender;
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void InputTestState::Exit()
{
	//if (input::InputManager::IsInitialised())
	//	input::InputManager::GetInstance()->SetCallback(0);

	m_GameCamera = 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int InputTestState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int InputTestState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void InputTestState::Update(float deltaTime)
{
	m_DeviceList = input::InputManager::GetInstance()->GetDeviceList();

	if (m_pSelectedJoypad != 0)
	{
		if (!input::InputManager::GetInstance()->DeviceStillExists(m_pSelectedJoypad))
		{
			m_pSelectedJoypad = 0;
			m_SelectedIndex = -1;
		}
	}

	m_LastDelta = deltaTime;

	/*if (debugInput.IsKeyPressed(input::KEY_PAGEDOWN, true, true))
	{
		if (m_SelectedIndex == -1)
		{
			if (m_DeviceList.size() > 0)
			{
				m_SelectedIndex = 0;
				m_pSelectedJoypad = m_DeviceList[m_SelectedIndex];
			}
		}
		else
		{
			m_SelectedIndex++;
			if (m_DeviceList.size() > 0)
			{
				if (m_SelectedIndex >= m_DeviceList.size())
					m_SelectedIndex = 0;
					
				m_pSelectedJoypad = m_DeviceList[m_SelectedIndex];
			}
		}
	}
	else
	if (debugInput.IsKeyPressed(input::KEY_PAGEUP, true, true))
	{
		if (m_SelectedIndex == -1)
		{
			if (m_DeviceList.size() > 0)
			{
				m_SelectedIndex = 0;
				m_pSelectedJoypad = m_DeviceList[m_SelectedIndex];
			}
		}
		else
		{
			m_SelectedIndex--;
			if (m_DeviceList.size() > 0)
			{
				if (m_SelectedIndex < 0)
					m_SelectedIndex = (int)m_DeviceList.size() - 1;

				m_pSelectedJoypad = m_DeviceList[m_SelectedIndex];
			}
		}
	}*/

	m_GameCamera->Update( deltaTime );
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void InputTestState::Draw()
{
	int i = 0, j=0;

	// default colour
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	renderer::OpenGL::GetInstance()->SetNearFarClip(-6.0f, 6.0f);
	renderer::OpenGL::GetInstance()->SetupOrthographicView(core::app::GetFrameWidth(), core::app::GetFrameHeight(), true);
	renderer::OpenGL::GetInstance()->DisableLighting();

	renderer::OpenGL::GetInstance()->BlendMode(true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	renderer::OpenGL::GetInstance()->SetCullState(false, GL_FRONT_AND_BACK);

	int xPos;
	int yPos;

	glm::mat4 modelMatrix = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
	renderer::OpenGL::GetInstance()->SetModelMatrix(modelMatrix);

	xPos = 10;
	yPos = core::app::GetFrameHeight() - m_CurrentFontSize;

	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LEQUAL);

	m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Total Joystick/Gamepads Found %d", m_DeviceList.size());
	yPos -= m_CurrentFontSize;
    
	if (m_pSelectedJoypad != 0)
	{
		xPos += m_CurrentFontSize*2;

		m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Current Controller: %s", m_pSelectedJoypad->GetName());
		yPos -= m_CurrentFontSize;

		int numAxis = m_pSelectedJoypad->GetNumAxes();
		m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Number of Axes: %d\n", numAxis);
		yPos -= m_CurrentFontSize;
		int numButtons = m_pSelectedJoypad->GetNumButtons();
		m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Number of Buttons: %d\n", numButtons);
		yPos -= m_CurrentFontSize;
		int numHats = m_pSelectedJoypad->GetNumHats();
		m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Number of Hats: %d\n", numHats);
		yPos -= m_CurrentFontSize;
		int numBalls = m_pSelectedJoypad->GetNumBalls();
		m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Number of Balls: %d\n", numBalls);
		yPos -= m_CurrentFontSize;
		bool hasRumble = m_pSelectedJoypad->HasRumble();
		m_FontTest->Print(xPos, yPos, false, GL_NEAREST, "Has Rumble: %s\n", hasRumble ? "YES" : "NO" );
		yPos -= m_CurrentFontSize;

		xPos += m_CurrentFontSize*2;
        if (m_pSelectedJoypad->GetType() == input::InputDeviceController::CONTROLLERTYPE_GAMEPAD)
		{
			SDL_GameController *controller = m_pSelectedJoypad->GetGamePadPtr();

			if (controller)
			{
				for (i = SDL_CONTROLLER_BUTTON_A; i < SDL_CONTROLLER_BUTTON_MAX; ++i)
				{
					if (SDL_GameControllerGetButton(controller, static_cast<SDL_GameControllerButton>(i)))
					{
						m_FontTest->Print(xPos, yPos, false, GL_NEAREST, SDL_GameControllerGetStringForButton(static_cast<SDL_GameControllerButton>(i)));
						yPos -= m_CurrentFontSize;
					}
				}

				for (i = SDL_CONTROLLER_AXIS_LEFTX; i < SDL_CONTROLLER_AXIS_MAX; ++i)
				{
					if (SDL_GameControllerGetAxis(controller, static_cast<SDL_GameControllerAxis>(i)) > DEAD_ZONE ||
						SDL_GameControllerGetAxis(controller, static_cast<SDL_GameControllerAxis>(i)) < -DEAD_ZONE)
					{
						char szAxisStr[64];
						snprintf(szAxisStr, 64, "Axis %s = %d", SDL_GameControllerGetStringForAxis(static_cast<SDL_GameControllerAxis>(i)), SDL_GameControllerGetAxis(controller, static_cast<SDL_GameControllerAxis>(i)));

						m_FontTest->Print(xPos, yPos, false, GL_NEAREST, szAxisStr);
						yPos -= m_CurrentFontSize;
					}
				}
			}
		}
		else
			if (m_pSelectedJoypad->GetType() == input::InputDeviceController::CONTROLLERTYPE_JOYSTICK)
		{
			SDL_Joystick *joy = m_pSelectedJoypad->GetJoystickPtr();

			if (joy)
			{
				int numButtons = SDL_JoystickNumButtons(joy);
				for (i = 0; i < numButtons; ++i)
				{
					int state = SDL_JoystickGetButton(joy, i);
					if (state)
					{
						char szButtonStr[64];
						snprintf(szButtonStr, 64, "Button %d pressed", i);
						m_FontTest->Print(xPos, yPos, false, GL_NEAREST, szButtonStr);
						yPos -= m_CurrentFontSize;
					}
				}
				int numHats = SDL_JoystickNumHats(joy);
				for (i = 0; i < numHats; ++i)
				{
					Uint8 state = SDL_JoystickGetHat(joy, i);
					for (j = 0; j < 5; ++j)
					{
						if (state & HatToValue[j])
						{
							char szButtonStr[64];
							snprintf(szButtonStr, 64, "Hat %s pressed", HatToString[j]);
							m_FontTest->Print(xPos, yPos, false, GL_NEAREST, szButtonStr);
							yPos -= m_CurrentFontSize;
						}
					}
				}
				int numAxes = SDL_JoystickNumAxes(joy);
				for (i = 0; i < numAxes; ++i)
				{
					Sint16 val = SDL_JoystickGetAxis(joy, i);
					if (val > DEAD_ZONE ||
						val < -DEAD_ZONE)
					{
						char szAxisStr[64];
						snprintf(szAxisStr, 64, "Axis %d = %d", i, val);
						m_FontTest->Print(xPos, yPos, false, GL_NEAREST, szAxisStr);
						yPos -= m_CurrentFontSize;
					}
				}
			}
		}
	}
    
	renderer::OpenGL::GetInstance()->DepthMode(true, GL_LESS);

	xPos = core::app::GetFrameWidth() - 50;
	yPos = core::app::GetFrameHeight() - 60;

	GL_CHECK;

	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;

	modelMatrix = glm::mat4(1.0f);
	renderer::OpenGL::GetInstance()->SetModelMatrix(modelMatrix);

	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);
	renderer::OpenGL::GetInstance()->BlendMode(true, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	DBGPRINT(xPos, yPos, "DELTA (%.6f)", m_LastDelta);
	renderer::OpenGL::GetInstance()->BlendMode(false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
/*
void InputTestState::DeviceAdded(input::InputDeviceController *pDevice)
{

}

void InputTestState::DeviceRemoved(input::InputDeviceController *pDevice)
{

}

void InputTestState::InputControllerButtonDown(input::InputDeviceController *pDevice, int buttonIndex)
{
	if (pDevice == m_pSelectedJoypad &&
		m_pSelectedJoypad->IsBinding())
	{
		m_pSelectedJoypad->StopBinding();
	}
}

void InputTestState::InputControllerButtonUp(input::InputDeviceController *pDevice, int buttonIndex)
{

}

void InputTestState::InputControllerAxisMotion(input::InputDeviceController *pDevice, int axisIndex, signed short value)
{
	if (pDevice == m_pSelectedJoypad &&
		m_pSelectedJoypad->IsBinding())
	{
		m_pSelectedJoypad->StopBinding();
	}
}

void InputTestState::InputControllerHatMotion(input::InputDeviceController *pDevice, unsigned char value)
{
	if (pDevice == m_pSelectedJoypad &&
		m_pSelectedJoypad->IsBinding())
	{
		if (value & SDL_HAT_UP)
		{

		}
		else
		if (value & SDL_HAT_DOWN)
		{

		}
		else
		if (value & SDL_HAT_LEFT)
		{

		}
		else
		if (value & SDL_HAT_RIGHT)
		{

		}

		m_pSelectedJoypad->StopBinding();
	}
}*/

