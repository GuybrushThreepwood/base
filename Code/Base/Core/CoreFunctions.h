
#ifndef __COREFUNCTIONS_H__
#define __COREFUNCTIONS_H__

namespace core
{
	/// SplitPath - Splits a path into its drive/directory/filename/extension
	/// \param path - The path you want split
	/// \param drive - pointer to store the returned drive string
	/// \param dir - pointer to store the returned directory string
	/// \param fname - pointer to store the returned filename string
	/// \param ext - pointer to store the returned file extension string
	void SplitPath( const char *path, char *drive, char *dir, char *fname, char *ext );
	/// GetDateAndTime - Returns the date and time as a string
	/// \return char * - string containing time and date
	char *GetDateAndTime( void );
	/// IsEmptyString - Checks to see is a string pointer is 0
	/// \param szString - string to test
	/// \return boolean - true or false
	bool IsEmptyString( const char *szString );

} // namespace core

#endif // __COREFUNCTIONS_H__

