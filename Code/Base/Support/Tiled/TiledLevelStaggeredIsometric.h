
/*===================================================================
	File: TiledLevelStaggeredIsometric.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDLEVELSTAGGEREDISOMETRIC_H__
#define __TILEDLEVELSTAGGEREDISOMETRIC_H__
	
#ifdef BASE_SUPPORT_BOX2D

#include "Support/Tiled/TiledStaggeredIsometricMap.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		class TiledLevelStaggeredIsometric : public TiledStaggeredIsometricMap
		{
		public:
			TiledLevelStaggeredIsometric();
			virtual ~TiledLevelStaggeredIsometric();

			virtual void DrawLayers(int startLayer = -1, int endLayer = -1);

		private:

		};
	}
}

#endif // BASE_SUPPORT_BOX2D
#endif // __TILEDLEVELSTAGGEREDISOMETRIC_H__

