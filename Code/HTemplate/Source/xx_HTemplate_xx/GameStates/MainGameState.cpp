
/*===================================================================
	File: MainGameState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"
#include "Resources/StringResources.h"
#include "GameStates/IBaseGameState.h"

#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/MainGameState.h"

namespace
{

}


/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
MainGameState::MainGameState( StateManager& stateManager )
: IBaseGameState( stateManager, ID_MAINGAMESTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;

}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
MainGameState::~MainGameState()
{

}

void MainGameState::DeviceAdded(const input::InputDeviceController *pDevice)
{
	DBGLOG("Device %s Added\n", pDevice->GetName());
}

void MainGameState::DeviceRemoved(const input::InputDeviceController *pDevice)
{
	DBGLOG("Device %s Removed\n", pDevice->GetName());
}

void MainGameState::InputControllerButtonDown(const input::InputDeviceController *pDevice, int buttonIndex)
{
	DBGLOG("Device %s Button %d DOWN\n", pDevice->GetName(), buttonIndex);
}

void MainGameState::InputControllerButtonUp(const input::InputDeviceController *pDevice, int buttonIndex)
{
	DBGLOG("Device %s Button %d UP\n", pDevice->GetName(), buttonIndex);
}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void MainGameState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

	renderer::OpenGL::GetInstance()->ClearColour( 0.5f, 0.5f, 0.5f, 1.0f );

	GameSystems::GetInstance()->CreatePhysics();

	glm::vec3 camPos = glm::vec3(10, 10, 20);
	glm::vec3 camTarget = glm::vec3(0, 0, 0);
	m_GameCamera->SetLerp(true);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void MainGameState::Exit()
{
	m_GameCamera = 0;

	GameSystems::GetInstance()->DestroyPhysics();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int MainGameState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int MainGameState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void MainGameState::Update( float deltaTime )
{
	m_LastDelta = deltaTime;	

	m_GameCamera->Update( deltaTime );

	//CheckAchievements( deltaTime );
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void MainGameState::Draw()
{
	// default colour
	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	if( m_DevData.allowDebugCam && gDebugCamera.IsEnabled() )
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 0.5f, 100000.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		renderer::OpenGL::GetInstance()->SetLookAt( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z, 
												gDebugCamera.GetTarget().x, gDebugCamera.GetTarget().y, gDebugCamera.GetTarget().z );
	
		snd::SoundManager::GetInstance()->SetListenerPosition( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z );
		snd::SoundManager::GetInstance()->SetListenerOrientation((float)std::sin(glm::radians(-gDebugCamera.GetAngle())), 0.0f, (float)std::cos(glm::radians(-gDebugCamera.GetAngle())),
																	0.0f, 1.0f, 0.0f );
	}
	else
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 1.0f, 100.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		m_GameCamera->SetupCamera();

		snd::SoundManager::GetInstance()->SetListenerOrientation(std::sin(glm::radians(180.0f)), 0.0f, std::cos(glm::radians(180.0f)), 0.0f, 1.0f, 0.0f);
	}
	
	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LESS );
	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->DisableLighting();

	renderer::OpenGL::GetInstance()->SetNearFarClip( NEAR_CLIP_ORTHO, FAR_CLIP_ORTHO );
	renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );
	renderer::OpenGL::GetInstance()->DisableLighting();

	GL_CHECK;
		
	int offset = 10;
	int xPos;
	int yPos;

	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;

	DBGPRINT(xPos, yPos -= offset, "DELTA (%.6f)", m_LastDelta);
	//DBGPRINT( xPos, yPos-=offset, "CAM POS (%.2f  %.2f  %.2f)", m_GameCamera->GetPosition().x, m_GameCamera->GetPosition().y, m_GameCamera->GetPosition().z );
	//DBGPRINT( xPos, yPos-=offset, "CAM TARGET (%.2f  %.2f  %.2f)", m_GameCamera->GetTarget().x, m_GameCamera->GetTarget().y, m_GameCamera->GetTarget().z );
}
