
/*===================================================================
	File: App.cpp
	Library: Core

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "CollisionBase.h"
#include "RenderBase.h"
#include "SoundBase.h"
#include "InputBase.h"

extern "C" 
{
	#include "Render/FreetypeZZipAccess.h"
}
#include "App.h"

using namespace core::app;

static core::EPlatform s_Platform = core::PLATFORM_WINDOWS;

static bool s_LoadFilesFromZip = false;
static file::TFile s_RootZipFile;

static float s_FPS = 0.0f;
static float s_FrameLock = core::FPS60;

static int s_Language = core::LANGUAGE_ENGLISH;
static bool s_SupportHardwareKeyboard = false;

static int s_ArgumentCount = 0;
static char** s_ArgumentVars = 0;

static bool s_IsInBackground = false;

static int s_FrameWidth = 1024;
static int s_FrameHeight = 768;

static int s_BaseAssetWidth = 1024;
static int s_BaseAssetHeight = 768;

static float s_WidthScale = 1.0f;
static float s_HeightScale = 1.0f;

static std::wstring s_SavePath;

static bool s_AllowiPodMusic = false;

static int s_AppIconID = 32512; // IDI_APPLICATION

/////////////////////////////////////////////////////
/// Function: SetPlatform
/// Params: [in]platform
///
/////////////////////////////////////////////////////
void core::app::SetPlatform( core::EPlatform platform )
{
	s_Platform = platform;

	if( s_Platform == PLATFORM_WINDOWS ||
	   s_Platform == PLATFORM_MAC )
		s_SupportHardwareKeyboard = true;
}

/////////////////////////////////////////////////////
/// Function: GetPlatform
/// Params: None
///
/////////////////////////////////////////////////////
core::EPlatform core::app::GetPlatform()
{
	return s_Platform;
}

/////////////////////////////////////////////////////
/// Function: SupportsHardwareKeyboard
/// Params: None
///
/////////////////////////////////////////////////////
bool core::app::SupportsHardwareKeyboard()
{
	return s_SupportHardwareKeyboard;
}

/////////////////////////////////////////////////////
/// Function: SetArgumentCount
/// Params: None
///
/////////////////////////////////////////////////////
void core::app::SetArgumentCount( int count )
{
	s_ArgumentCount = count;
}

/////////////////////////////////////////////////////
/// Function: GetArgumentCount
/// Params: None
///
/////////////////////////////////////////////////////
int core::app::GetArgumentCount()
{
	return s_ArgumentCount;
}

/////////////////////////////////////////////////////
/// Function: SetArgumentVariables
/// Params: None
///
/////////////////////////////////////////////////////
void core::app::SetArgumentVariables( char** argv )
{
	s_ArgumentVars = argv;
}

/////////////////////////////////////////////////////
/// Function: GetArgumentVariables
/// Params: None
///
/////////////////////////////////////////////////////
char** core::app::GetArgumentVariables()
{
	return s_ArgumentVars;
}

/////////////////////////////////////////////////////
/// Function: SetLoadFilesFromZip
/// Params: [in]flag 
///
/////////////////////////////////////////////////////		
void core::app::SetLoadFilesFromZip( bool flag )
{
	s_LoadFilesFromZip = flag;
}

/////////////////////////////////////////////////////
/// Function: GetLoadFilesFromZip
/// 
///
/////////////////////////////////////////////////////
bool core::app::GetLoadFilesFromZip()
{
	return s_LoadFilesFromZip;
}

/////////////////////////////////////////////////////
/// Function: SetRootZipFile
/// Params: [in]rootFile
///
/////////////////////////////////////////////////////		
void core::app::SetRootZipFile( const char* rootFile )
{
	file::CreateFileStructure( rootFile, &s_RootZipFile );

#ifdef BASE_SUPPORT_FREETYPE
	SetFreeTypeZipFile( s_RootZipFile.szFilename );
#endif // BASE_SUPPORT_FREETYPE
}

/////////////////////////////////////////////////////
/// Function: GetRootZipFile
/// 
///
/////////////////////////////////////////////////////
const char* core::app::GetRootZipFile()
{
	return s_RootZipFile.szFilename;
}

/////////////////////////////////////////////////////
/// Function: SetFPS
/// Params: [in]fps
///
/////////////////////////////////////////////////////
void core::app::SetFrameLock( float fps )
{
	s_FrameLock = fps;
}

/////////////////////////////////////////////////////
/// Function: GetFrameLock
/// 
///
/////////////////////////////////////////////////////
float core::app::GetFrameLock()
{
	return s_FrameLock;
}

/////////////////////////////////////////////////////
/// Function: SetFPS
/// Params: [in]fps
///
/////////////////////////////////////////////////////
void core::app::SetFPS( float fps )
{
	s_FPS = fps;
}

/////////////////////////////////////////////////////
/// Function: GetFPS
/// 
///
/////////////////////////////////////////////////////
float core::app::GetFPS()
{
	return s_FPS;
}

/////////////////////////////////////////////////////
/// Function: IsStoreAvailable
/// 
///
/////////////////////////////////////////////////////   
void core::app::SetInBackground(bool inBackground)
{
	s_IsInBackground = inBackground;
}

/////////////////////////////////////////////////////
/// Function: IsStoreAvailable
/// 
///
/////////////////////////////////////////////////////   
bool core::app::IsInBackground()
{
	return s_IsInBackground;
}

/////////////////////////////////////////////////////
/// Function: SetLanguage
/// Params: [in] language
///
/////////////////////////////////////////////////////
void core::app::SetLanguage( int language )
{
    switch( language )
    {           
        case LANGUAGE_ENGLISH:
        {
            s_Language = core::LANGUAGE_ENGLISH;
        }break;
        case LANGUAGE_FRENCH:
        {
            s_Language = core::LANGUAGE_FRENCH;
        }break;
        case LANGUAGE_ITALIAN:
        {
            s_Language = core::LANGUAGE_ITALIAN;
        }break;
        case LANGUAGE_GERMAN:
        {
            s_Language = core::LANGUAGE_GERMAN;
        }break;
        case LANGUAGE_SPANISH:
        {
            s_Language = core::LANGUAGE_SPANISH;
        }break;    
        default:
            // default to english
            s_Language = core::LANGUAGE_ENGLISH;
            break;
    }
}

/////////////////////////////////////////////////////
/// Function: GetLanguage
/// 
///
/////////////////////////////////////////////////////
int core::app::GetLanguage()
{
    return s_Language;
}

/////////////////////////////////////////////////////
/// Function: SetAppWidth
/// 
///
/////////////////////////////////////////////////////
void core::app::SetFrameWidth( int width )
{
	s_FrameWidth = width;
}

/////////////////////////////////////////////////////
/// Function: GetFrameWidth
/// 
///
/////////////////////////////////////////////////////
int core::app::GetFrameWidth()
{
	return s_FrameWidth;
}

/////////////////////////////////////////////////////
/// Function: SetFrameHeight
/// 
///
/////////////////////////////////////////////////////
void core::app::SetFrameHeight( int height )
{
	s_FrameHeight = height;
}

/////////////////////////////////////////////////////
/// Function: GetFrameHeight
/// 
///
/////////////////////////////////////////////////////
int core::app::GetFrameHeight()
{
	return s_FrameHeight;
}

/////////////////////////////////////////////////////
/// Function: SetBaseAssetsWidth
/// 
///
/////////////////////////////////////////////////////
void core::app::SetBaseAssetsWidth( int width )
{
	s_BaseAssetWidth = width;
}

/////////////////////////////////////////////////////
/// Function: GetBaseAssetsWidth
/// 
///
/////////////////////////////////////////////////////
int core::app::GetBaseAssetsWidth()
{
	return s_BaseAssetWidth;
}

/////////////////////////////////////////////////////
/// Function: SetBaseAssetsHeight
/// 
///
/////////////////////////////////////////////////////
void core::app::SetBaseAssetsHeight( int height )
{
	s_BaseAssetHeight = height;
}

/////////////////////////////////////////////////////
/// Function: GetBaseAssetsHeight
/// 
///
/////////////////////////////////////////////////////
int core::app::GetBaseAssetsHeight()
{
	return s_BaseAssetHeight;
}

/////////////////////////////////////////////////////
/// Function: GetWidthScale
/// 
///
/////////////////////////////////////////////////////
float core::app::GetWidthScale()
{
	return s_WidthScale;
}

/////////////////////////////////////////////////////
/// Function: GetHeightScale
/// 
///
/////////////////////////////////////////////////////
float core::app::GetHeightScale()
{
	return s_HeightScale;
}

/////////////////////////////////////////////////////
/// Function: SetSavePath
/// 
///
/////////////////////////////////////////////////////
void core::app::SetSavePath(const std::wstring& path)
{
	s_SavePath = path;
}

/////////////////////////////////////////////////////
/// Function: GetSavePath
/// 
///
/////////////////////////////////////////////////////
std::wstring& core::app::GetSavePath()
{
	return s_SavePath;
}

/////////////////////////////////////////////////////
/// Function: SupportiPodMusic
/// 
///
/////////////////////////////////////////////////////
void core::app::SupportiPodMusic( bool state )
{
	s_AllowiPodMusic = state;
}

/////////////////////////////////////////////////////
/// Function: GetSupportiPodMusic
/// 
///
/////////////////////////////////////////////////////
bool core::app::GetSupportiPodMusic()
{
	return s_AllowiPodMusic;
}

/////////////////////////////////////////////////////
/// Function: SetIconID
/// 
///
/////////////////////////////////////////////////////
void core::app::SetIconID( int resourceId )
{
	s_AppIconID = resourceId;
}
		
/////////////////////////////////////////////////////
/// Function: GetIconID
/// 
///
/////////////////////////////////////////////////////
int core::app::GetIconID()
{
	return s_AppIconID;
}

/////////////////////////////////////////////////////
/// Function: QuitApplication
/// Params: None
///
/////////////////////////////////////////////////////
void core::app::QuitApplication()
{
	//SDL_Quit();

	//PostQuitMessage(0);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

App::App()
	: m_Window(nullptr)
	, m_Context(nullptr)
	, m_TotalFrames(0)
	, m_StartTicks(0)
	, m_FPS(0)
	, m_ElapsedTime(0.0f)
{

}

App::~App()
{

}

void App::ToggleFullscreen() 
{
	Uint32 fullscreenFlag = SDL_WINDOW_FULLSCREEN;

	bool IsFullscreen = (SDL_GetWindowFlags(m_Window) & fullscreenFlag) != 0;

	SDL_SetWindowFullscreen(m_Window, IsFullscreen ? 0 : fullscreenFlag);

	//SDL_ShowCursor(IsFullscreen);
}

void App::UpdateFrame()
{
	Uint32 currTicks = SDL_GetTicks();

	m_ElapsedTime = static_cast<float>( (currTicks - m_LastTicks) ) / 1000.0f;
	m_FPS = (m_TotalFrames / (float)(currTicks - m_StartTicks)) * 1000.0f;

	m_LastTicks = currTicks;

	Update();
}

void App::RenderFrame()
{
	Render();
}

int App::Execute(int windowWidth, int windowHeight, int frameWidth, int frameHeight, bool fullscreen)
{
	m_WindowDims = glm::ivec2( windowWidth, windowHeight );
	m_FrameDims = glm::ivec2( frameWidth, frameHeight );

#if defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)
	if (!dbg::Debug::IsInitialised())
	{
		dbg::Debug::Initialise();
		dbg::Debug::GetInstance()->CreateLogFile();
	}
#endif // defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_GAMECONTROLLER) < 0)
	{
		DBGLOG("SDL could not initialize! SDL_Error: %s\n", SDL_GetError());
		return 1;
	}
	else
	{
		// Use OpenGL 4.1 core
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 1);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		// Create window
		Uint32 flags = SDL_WINDOW_OPENGL;
		if (fullscreen)
			flags |= SDL_WINDOW_FULLSCREEN;

		m_Window = SDL_CreateWindow("SDL Tutorial", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, m_WindowDims.x, m_WindowDims.y, flags);
		if (m_Window == NULL)
		{
			DBGLOG("Window could not be created! SDL_Error: %s\n", SDL_GetError());
			return 1;
		}
		else
		{
			m_Context = SDL_GL_CreateContext(m_Window);
			if (m_Context == NULL)
			{
				DBGLOG("OpenGL context could not be created! SDL Error: %s\n", SDL_GetError());
				return 1;
			}
			else
			{
				// initialise OpenGL
				renderer::OpenGL::Initialise();

				// some default GL values
				renderer::OpenGL::GetInstance()->Init();

				renderer::OpenGL::GetInstance()->SetupPerspectiveView(m_FrameDims.x, m_FrameDims.y);
				renderer::OpenGL::GetInstance()->SetNearFarClip(1.0f, 10000.0f);
				renderer::OpenGL::GetInstance()->ClearColour(0.0f, 0.282f, 0.415f, 1.0f);

				//SDL_GL_SetSwapInterval(0);

#if defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)
				dbg::DebugCreateFont();
#endif // defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

#ifdef BASE_SUPPORT_OPENAL
				// initialise OpenAL
				snd::OpenAL::Initialise();
#endif // BASE_SUPPORT_OPENAL

				input::InputManager::Initialise();

				Initialise();
			}

			// Main loop flag
			bool quit = false;

			// Event handler
			SDL_Event e;

			m_TotalFrames = 0;
			m_LastTicks = m_StartTicks = SDL_GetTicks();

			//While application is running
			while (!quit)
			{
				while (SDL_PollEvent(&e) != 0)
				{
					switch (e.type)
					{
						//User requests quit 
						case SDL_QUIT:
						{
							quit = true;
						}break;
						case SDL_KEYDOWN:
						{
							// key press 
							switch (e.key.keysym.sym)
							{
								case SDLK_RETURN:
								{
									ToggleFullscreen();
								}break;
								case SDLK_ESCAPE:
								{
									quit = true;
								}break;

								default:
									break;
							}
						}break;
						case SDL_KEYUP:
						{

						}break;
						case SDL_JOYHATMOTION:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->HatEvent(e.jhat.which, e.jhat.value);

						}break;
						case SDL_JOYAXISMOTION:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->AxisEvent(e.jaxis.which, e.jaxis.axis, e.jaxis.value);
						}break;
						case SDL_JOYBUTTONDOWN:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->ButtonEvent(e.jbutton.which, e.jbutton.button, true);
						}break;
						case SDL_CONTROLLERBUTTONDOWN:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->ButtonEvent(e.cbutton.which, e.cbutton.button, true);
						}break;

						case SDL_JOYBUTTONUP:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->ButtonEvent(e.jbutton.which, e.jbutton.button, false);
						}break;
						case SDL_CONTROLLERBUTTONUP:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->ButtonEvent(e.cbutton.which, e.cbutton.button, false);
						}break;

						case SDL_JOYDEVICEADDED:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->AddDevice(e.jdevice.which);
						}break;
						case SDL_JOYDEVICEREMOVED:
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->RemoveDevice(e.jdevice.which);
						}break;
						case SDL_CONTROLLERDEVICEADDED:         // A new Game controller has been inserted into the system
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->AddDevice(e.cdevice.which);
						}break;
						case SDL_CONTROLLERDEVICEREMOVED:       // An opened Game controller has been removed
						{
							if (input::InputManager::IsInitialised())
								input::InputManager::GetInstance()->RemoveDevice(e.cdevice.which);
						}break;

						default:
							break;
					}
				}

				UpdateFrame();

				renderer::OpenGL::GetInstance()->ClearScreen();

				RenderFrame();

				SDL_GL_SwapWindow(m_Window);

				std::string title = "FPS: " + std::to_string(m_FPS) + " | FrameTime: " + std::to_string(m_ElapsedTime);
				SDL_SetWindowTitle(m_Window, title.c_str());
				m_TotalFrames++;
			}
		}


	}

	Shutdown();

#if defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)
	dbg::DebugDestroyFont();
#endif // defined(_DEBUG) || defined(BASE_USE_DEBUG_FONT)

	renderer::ClearShaderMap();
	renderer::ClearTextureMap();

	input::InputManager::Shutdown();

	// release OpenGL
	renderer::OpenGL::Shutdown();

	SDL_GL_DeleteContext(m_Context);

	// Destroy window
	SDL_DestroyWindow(m_Window);

	// Quit SDL subsystems
	SDL_Quit();

	return 0;
}
