
/*===================================================================
	File: AABB.cpp
	Library: Collision

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "CollisionBase.h"

using collision::AABB;

/////////////////////////////////////////////////////
/// Default constructor
/// 
///
/////////////////////////////////////////////////////
AABB::AABB()
{
	Reset();
}

/////////////////////////////////////////////////////
/// Default destructor
/// 
///
/////////////////////////////////////////////////////
AABB::~AABB()
{

}

/////////////////////////////////////////////////////
/// Method: Reset
/// Params: None
///
/////////////////////////////////////////////////////
void AABB::Reset( void )
{
	float fBigNumber = 1e37f;
	vCenter = glm::vec3(0.0f, 0.0f, 0.0f);
	vBoxMin.x = vBoxMin.y = vBoxMin.z = fBigNumber;
	vBoxMax.x = vBoxMax.y = vBoxMax.z = -fBigNumber;
}

/////////////////////////////////////////////////////
/// Method: ReCalculate
/// Params: [in]vPos
///
/////////////////////////////////////////////////////
void AABB::ReCalculate(const glm::vec2 &vPos)
{
	if (vPos.x < vBoxMin.x)
		vBoxMin.x = vPos.x;

	if (vPos.y < vBoxMin.y)
		vBoxMin.y = vPos.y;

	if (vPos.x > vBoxMax.x)
		vBoxMax.x = vPos.x;

	if (vPos.y > vBoxMax.y)
		vBoxMax.y = vPos.y;
}

/////////////////////////////////////////////////////
/// Method: ReCalculate
/// Params: [in]vPos
///
/////////////////////////////////////////////////////
void AABB::ReCalculate( const glm::vec3 &vPos )
{
	if( vPos.x < vBoxMin.x )
		vBoxMin.x = vPos.x;

	if( vPos.y < vBoxMin.y )
		vBoxMin.y = vPos.y;

	if( vPos.z < vBoxMin.z )
		vBoxMin.z = vPos.z;


	if( vPos.x > vBoxMax.x )
		vBoxMax.x  = vPos.x ;

	if( vPos.y > vBoxMax.y  )
		vBoxMax.y = vPos.y;

	if( vPos.z > vBoxMax.z )
		vBoxMax.z = vPos.z;
}

/////////////////////////////////////////////////////
/// Method: ReCalculate
/// Params: [in]vPosMin, [in]vPosMax
///
/////////////////////////////////////////////////////
void AABB::ReCalculate(const glm::vec3 &vPosMin, const glm::vec3 &vPosMax)
{
	if( vPosMin.x < vBoxMin.x )
		vBoxMin.x = vPosMin.x;

	if( vPosMin.y < vBoxMin.y )
		vBoxMin.y = vPosMin.y;

	if( vPosMin.z < vBoxMin.z )
		vBoxMin.z = vPosMin.z;


	if( vPosMax.x > vBoxMax.x )
		vBoxMax.x  = vPosMax.x ;

	if( vPosMax.y > vBoxMax.y  )
		vBoxMax.y = vPosMax.y;

	if( vPosMax.z > vBoxMax.z )
		vBoxMax.z = vPosMax.z;
}

/////////////////////////////////////////////////////
/// Method: ClosestPointTo
/// Params: [in]p
///
/////////////////////////////////////////////////////
glm::vec3 AABB::ClosestPointTo(const glm::vec3 &p)
{
	// "Push" p into the box, on each dimension
	glm::vec3 r;

	if( p.x < vBoxMin.x ) 
	{
		r.x = vBoxMin.x;
	} 
	else if(p.x > vBoxMax.x) 
	{
		r.x = vBoxMax.x;
	} 
	else 
	{
		r.x = p.x;
	}

	if (p.y < vBoxMin.y) 
	{
		r.y = vBoxMin.y;
	} 
	else if (p.y > vBoxMax.y) 
	{
		r.y = vBoxMax.y;
	} 
	else 
	{
		r.y = p.y;
	}

	if (p.z < vBoxMin.z) 
	{
		r.z = vBoxMin.z;
	} 
	else if (p.z > vBoxMax.z) 
	{
		r.z = vBoxMax.z;
	} 
	else 
	{
		r.z = p.z;
	}

	return r;
}

/////////////////////////////////////////////////////
/// Method: PointInAABB
/// Params: [in]p
///
/////////////////////////////////////////////////////
bool AABB::PointInAABB(const glm::vec3 &p)
{
	if( p.x < vBoxMin.x || p.x > vBoxMax.x )
		return false;
	if( p.y < vBoxMin.y || p.y > vBoxMax.y )
		return false;
	if( p.z < vBoxMin.z || p.z > vBoxMax.z )
		return false;

	return true;
}

/////////////////////////////////////////////////////
/// Method: AABBCollidesWithAABB
/// Params: [in]aabb
///
/////////////////////////////////////////////////////
bool AABB::AABBCollidesWithAABB( const AABB &aabb )
{
	if(vBoxMin.x > aabb.vBoxMax.x) 
		return false;
	if(vBoxMax.x < aabb.vBoxMin.x) 
		return false;
	if(vBoxMin.y > aabb.vBoxMax.y) 
		return false;
	if(vBoxMax.y < aabb.vBoxMin.y) 
		return false;
	if(vBoxMin.z > aabb.vBoxMax.z) 
		return false;
	if(vBoxMax.z < aabb.vBoxMin.z) 
		return false;

	return true;
}

/////////////////////////////////////////////////////
/// Method: AABBCollidesWithSphere
/// Params: [in]s
///
/////////////////////////////////////////////////////
bool AABB::AABBCollidesWithSphere( const collision::Sphere &s )
{
	// find the closest point on box to the point
	glm::vec3 vPoint = ClosestPointTo(s.vCenterPoint);

	// check if it's within range
	if (glm::distance2(s.vCenterPoint, vPoint) < (s.r*s.r))
		return true;

	return false;
}

/////////////////////////////////////////////////////
/// Method: PointInAABB
/// Params: [in]p
///
/////////////////////////////////////////////////////
bool collision::PointInAABB(const glm::vec3 &p, const collision::AABB &aabb)
{
	if( p.x < aabb.vBoxMin.x || p.x > aabb.vBoxMax.x )
		return false;
	if( p.y < aabb.vBoxMin.y || p.y > aabb.vBoxMax.y )
		return false;
	if( p.z < aabb.vBoxMin.z || p.z > aabb.vBoxMax.z )
		return false;

	return true;
}
