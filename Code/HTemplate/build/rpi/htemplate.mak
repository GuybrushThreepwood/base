# Copyright (C) 2012 Hidden Games
#

#LOCAL_PATH := $(call my-dir)

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

PROJECT_PATH := ../..
SDK_ROOT := $(PROJECT_PATH)/../SDK

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

OPENAL_FOLDER := $(SDK_ROOT)/openal-soft-1.15.1
LUA_FOLDER := $(SDK_ROOT)/lua
ZLIB_FOLDER := $(SDK_ROOT)/zlib
PNG_FOLDER := $(SDK_ROOT)/png
FREETYPE_FOLDER := $(SDK_ROOT)/freetype
VORBIS_FOLDER := $(SDK_ROOT)/vorbis
OGG_FOLDER := $(SDK_ROOT)/ogg
ZZIPLIB_FOLDER := $(SDK_ROOT)/zziplib

BOX2D_FOLDER := $(SDK_ROOT)/box2d

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

OPENAL_INCLUDE := $(OPENAL_FOLDER)/include
GLM_INCLUDE := $(SDK_ROOT)/glm-0.9.4.6
LUA_INCLUDE := $(LUA_FOLDER)/include
ZLIB_INCLUDE := $(ZLIB_FOLDER)/include
PNG_INCLUDE := $(PNG_FOLDER)/include
FREETYPE_INCLUDE := $(FREETYPE_FOLDER)/include
VORBIS_INCLUDE := $(VORBIS_FOLDER)/include
OGG_INCLUDE := $(OGG_FOLDER)/include
ZZIPLIB_INCLUDE := $(ZZIPLIB_FOLDER)/include

BOX2D_INCLUDE := $(BOX2D_FOLDER)

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

# HTemplate 
include $(CLEAR_VARS)

LOCAL_MODULE    := htemplate

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

BASE_FOLDER := $(PROJECT_PATH)/../Base
BASE_INCLUDE := -I$(PROJECT_PATH)/../Base -I$(OPENAL_INCLUDE) -I$(GLM_INCLUDE) -I$(LUA_INCLUDE) -I$(ZLIB_INCLUDE) -I$(PNG_INCLUDE) -I$(FREETYPE_INCLUDE) -I$(VORBIS_INCLUDE) -I$(OGG_INCLUDE) -I$(ZZIPLIB_INCLUDE) -I$(CURL_INCLUDE) -I$(TINYXML2_INCLUDE)
BASE_INCLUDE += -I$(BOX2D_INCLUDE)

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

BASE_SRC := $(BASE_FOLDER)/Collision/AABB.cpp \
$(BASE_FOLDER)/Collision/OBB.cpp \
$(BASE_FOLDER)/Collision/Sphere.cpp \
$(BASE_FOLDER)/Core/rpi/RaspberryPiApp.cpp \
$(BASE_FOLDER)/Core/rpi/RaspberryPiMutex.cpp \
$(BASE_FOLDER)/Core/rpi/RaspberryPiThread.cpp \
$(BASE_FOLDER)/Core/App.cpp \
$(BASE_FOLDER)/Core/CoreFunctions.cpp \
$(BASE_FOLDER)/Core/Endian.cpp \
$(BASE_FOLDER)/Core/FileIO.cpp \
$(BASE_FOLDER)/Debug/FF/DebugPrint.cpp \
$(BASE_FOLDER)/Debug/GLSL/DebugPrintGLSL.cpp \
$(BASE_FOLDER)/Debug/Assertion.cpp \
$(BASE_FOLDER)/Debug/DebugLogging.cpp \
$(BASE_FOLDER)/Input/Input.cpp \
$(BASE_FOLDER)/Math/MathFunctions.cpp \
$(BASE_FOLDER)/Math/Matrix.cpp \
$(BASE_FOLDER)/Math/Quaternion.cpp \
$(BASE_FOLDER)/Math/RandomTables.cpp \
$(BASE_FOLDER)/Math/Vectors.cpp \
$(BASE_FOLDER)/Model/FF/Mesh.cpp \
$(BASE_FOLDER)/Model/FF/ModelHGA.cpp \
$(BASE_FOLDER)/Model/FF/ModelHGM.cpp \
$(BASE_FOLDER)/Model/FF/SkinMesh.cpp \
$(BASE_FOLDER)/Model/GLSL/MeshGLSL.cpp \
$(BASE_FOLDER)/Model/GLSL/ModelHGAGLSL.cpp \
$(BASE_FOLDER)/Model/GLSL/ModelHGMGLSL.cpp \
$(BASE_FOLDER)/Model/GLSL/SkinMeshGLSL.cpp \
$(BASE_FOLDER)/Model/Model.cpp \
$(BASE_FOLDER)/Physics/box2d/FF/Box2DDebugDraw.cpp \
$(BASE_FOLDER)/Physics/box2d/GLSL/Box2DDebugDrawGLSL.cpp \
$(BASE_FOLDER)/Physics/box2d/PhysicsWorldB2D.cpp \
$(BASE_FOLDER)/Physics/ode/FF/ODEPhysicsPrimitives.cpp \
$(BASE_FOLDER)/Physics/ode/GLSL/ODEPhysicsPrimitivesGLSL.cpp \
$(BASE_FOLDER)/Physics/ode/PhysicsWorldODE.cpp \
$(BASE_FOLDER)/Render/FF/OpenGLES/ExtensionsOES.cpp \
$(BASE_FOLDER)/Render/FF/OpenGLES/gluSupport.cpp \
$(BASE_FOLDER)/Render/FF/OpenGLES/OpenGLES.cpp \
$(BASE_FOLDER)/Render/FF/OpenGLES/RenderToTextureOES.cpp \
$(BASE_FOLDER)/Render/FF/OpenGLES/SpriteOES.cpp \
$(BASE_FOLDER)/Render/FF/OpenGLES/TextureLoadAndUploadOES.cpp \
$(BASE_FOLDER)/Render/FF/AdvertBar.cpp \
$(BASE_FOLDER)/Render/FF/FreetypeFont.cpp \
$(BASE_FOLDER)/Render/FF/Primitives.cpp \
$(BASE_FOLDER)/Render/GLSL/AdvertBarGLSL.cpp \
$(BASE_FOLDER)/Render/GLSL/FreetypeFontGLSL.cpp \
$(BASE_FOLDER)/Render/GLSL/glewES.cpp \
$(BASE_FOLDER)/Render/GLSL/gluSupportGLSL.cpp \
$(BASE_FOLDER)/Render/GLSL/OpenGLSL.cpp \
$(BASE_FOLDER)/Render/GLSL/PrimitivesGLSL.cpp \
$(BASE_FOLDER)/Render/GLSL/RenderToTextureGLSL.cpp \
$(BASE_FOLDER)/Render/GLSL/ShaderShared.cpp \
$(BASE_FOLDER)/Render/GLSL/TextureLoadAndUploadGLSL.cpp \
$(BASE_FOLDER)/Render/FreetypeFontZZip.c \
$(BASE_FOLDER)/Render/FreetypeZZipAccess.c \
$(BASE_FOLDER)/Render/OpenGLCommon.cpp \
$(BASE_FOLDER)/Render/Texture.cpp \
$(BASE_FOLDER)/Render/TextureAtlas.cpp \
$(BASE_FOLDER)/Render/TextureShared.cpp \
$(BASE_FOLDER)/Script/box2d/PhysicsAccessB2D.cpp \
$(BASE_FOLDER)/Script/BaseScriptSupport.cpp \
$(BASE_FOLDER)/Script/LuaScripting.cpp \
$(BASE_FOLDER)/Sound/rpi/RaspberryPiSound.cpp \
$(BASE_FOLDER)/Sound/rpi/RaspberryPiSoundStream.cpp \
$(BASE_FOLDER)/Sound/OpenAL.cpp \
$(BASE_FOLDER)/Sound/Sound.cpp \
$(BASE_FOLDER)/Sound/SoundCodecs.cpp \
$(BASE_FOLDER)/Sound/SoundLoadAndUpload.cpp \
$(BASE_FOLDER)/Sound/SoundManager.cpp \
$(BASE_FOLDER)/Sound/SoundStream.cpp \
$(BASE_FOLDER)/Support/rpi/RaspberryPiPurchase.cpp \
$(BASE_FOLDER)/Support/rpi/RaspberryPiScores.cpp \
$(BASE_FOLDER)/Support/Leaderboards.cpp \
$(BASE_FOLDER)/Support/Purchase.cpp \
$(BASE_FOLDER)/Support/Scores.cpp

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GAME_FOLDER := $(PROJECT_PATH)/../HTemplate
GAME_INCLUDE := -I$(PROJECT_PATH)/../HTemplate -I$(PROJECT_PATH)/../HTemplate/Source -I$(PROJECT_PATH)/../HTemplate/Source/xx_HTemplate_xx

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

GAME_SRC := $(GAME_FOLDER)/Source/Audio/rpi/RaspberryPiAudioSystemBase.cpp \
$(GAME_FOLDER)/Source/Audio/AudioSystem.cpp \
$(GAME_FOLDER)/Source/Camera/Camera.cpp \
$(GAME_FOLDER)/Source/Camera/DebugCamera.cpp \
$(GAME_FOLDER)/Source/Camera/GameCamera.cpp \
$(GAME_FOLDER)/Source/Effects/GLSL/EmitterGLSL.cpp \
$(GAME_FOLDER)/Source/Effects/FF/Emitter.cpp \
$(GAME_FOLDER)/Source/GameEffects/FullscreenEffects.cpp \
$(GAME_FOLDER)/Source/InputSystem/InputSystem.cpp \
$(GAME_FOLDER)/Source/Profiles/ProfileManager.cpp \
$(GAME_FOLDER)/Source/Resources/AchievementList.cpp \
$(GAME_FOLDER)/Source/Resources/AnimatedResources.cpp \
$(GAME_FOLDER)/Source/Resources/EmitterResources.cpp \
$(GAME_FOLDER)/Source/Resources/FontResources.cpp \
$(GAME_FOLDER)/Source/Resources/IAPList.cpp \
$(GAME_FOLDER)/Source/Resources/ModelResources.cpp \
$(GAME_FOLDER)/Source/Resources/ResourceHelper.cpp \
$(GAME_FOLDER)/Source/Resources/SoundResources.cpp \
$(GAME_FOLDER)/Source/Resources/SpriteResources.cpp \
$(GAME_FOLDER)/Source/Resources/StringResources.cpp \
$(GAME_FOLDER)/Source/Resources/TextureResources.cpp \
$(GAME_FOLDER)/Source/StateManage/StateManager.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/EffectPool/EffectPool.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/UI/AchievementUI.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/UI/FrontendTitleScreenUI.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/UI/TextFormattingFuncs.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/UI/UIBaseState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/UI/UIFileLoader.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/AdvertBarTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/Box2DTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/BulletTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/EmitterTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/FBOTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/FontFormatTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/HGMTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/HGATestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/HUITestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/ODETestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/PlaylistTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/ShaderTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/Examples/TiledTestState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/MainGameState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/GameStates/TitleScreenState.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Player/Player.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/ScriptAccess/HTemplate/HTemplateAccess.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/ScriptAccess/ScriptAccess.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/ScriptAccess/ScriptDataHolder.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledBaseLoader.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledIsometricMap.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledLevelIsometric.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledLevelOrthogonal.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledLevelStaggeredIsometric.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledOrthogonalMap.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TiledStaggeredIsometricMap.cpp \
$(GAME_FOLDER)/Source/xx_HTemplate_xx/Tiled/TilesetHandler.cpp \
$(GAME_FOLDER)/Source/HTemplate.cpp \
$(GAME_FOLDER)/Source/GameSystems.cpp \
$(GAME_FOLDER)/Source/mainRaspberryPi.cpp

## ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------

LOCAL_CFLAGS := -Wno-psabi -DGL_GLEXT_PROTOTYPES=1 -DBASE_PLATFORM_RASPBERRYPI -DBASE_INPUT_NO_TOUCH_SUPPORT -DBASE_SUPPORT_OGG -DBASE_SUPPORT_FREETYPE -DBASE_SUPPORT_SCRIPTING -DBASE_SUPPORT_ADBAR
#LOCAL_CFLAGS += -DBASE_SUPPORT_OPENGLES
LOCAL_CFLAGS += -DBASE_SUPPORT_OPENGL_GLSL
LOCAL_CFLAGS += -DBASE_SUPPORT_BOX2D -DBASE_SUPPORT_ODE -DBASE_SUPPORT_BULLET
LOCAL_CFLAGS += -ffast-math -O3 -funroll-loops -Wmultichar
LOCAL_CFLAGS += -DBASE_USE_DEBUG_FONT
#LOCAL_CFLAGS += -D_DEBUG 

LOCAL_C_INCLUDES := $(GAME_INCLUDE) $(BASE_INCLUDE)
LOCAL_SRC_FILES :=  $(GAME_SRC) $(BASE_SRC)

#LOCAL_LDLIBS := -L/opt/vc/lib -lGLESv1_CM
LOCAL_LDLIBS := -L/opt/vc/lib -lGLESv2

LOCAL_STATIC_LIBRARIES := $(LUA_FOLDER)/libs/liblua.a $(PNG_FOLDER)/libs/libpng.a $(ZLIB_FOLDER)/libs/libzlib.a $(FREETYPE_FOLDER)/libs/libfreetype.a $(VORBIS_FOLDER)/libs/libvorbis.a $(OGG_FOLDER)/libs/libogg.a $(ZZIPLIB_FOLDER)/libs/libzziplib.a $(CURL_FOLDER)/libs/libcurl.a $(TINYXML2_FOLDER)/libs/libtinyxml2.a
LOCAL_STATIC_LIBRARIES += $(BOX2D_FOLDER)/libs/libbox2d.a $(ODE_FOLDER)/libs/libode.a $(BULLET_FOLDER)/libs/libbullet.a

LOCAL_SHARED_LIBRARIES := -L$(OPENAL_FOLDER)/libs -lopenal

include $(BUILD_SHARED_LIBRARY)

#################

OBJDIR := obj

LOCAL_CFLAGS += -DHAVE_LIBBCM_HOST -DUSE_EXTERNAL_LIBBCM_HOST -DUSE_VCHIQ_ARM
LOCAL_CFLAGS += -march=armv6 -mfpu=vfp -mfloat-abi=hard
LOCAL_C_INCLUDES += -I/opt/vc/include -I/opt/vc/include/interface/vcos/pthreads -I/opt/vc/include/interface/vmcs_host/linux
LOCAL_LDLIBS += -Wl -lEGL -lbcm_host -lvcos -lvchiq_arm -lpthread -lrt

FILTERED_OBJECTS := $(patsubst $(GAME_FOLDER)/%, $(OBJDIR)/%, $(LOCAL_SRC_FILES) ) 
TEMP_OBJECTS := $(FILTERED_OBJECTS)
FILTERED_OBJECTS = $(patsubst $(BASE_FOLDER)/%, $(OBJDIR)/%, $(TEMP_OBJECTS) ) 

C_OBJECTS := $(FILTERED_OBJECTS:.c=.o)
OBJECTS := $(C_OBJECTS:.cpp=.o)
OUTFOLDERS := $(dir $(OBJECTS) ) 

APPLICATION := ../../bin/$(LOCAL_MODULE)
MKDIR := mkdir

all: $(OBJDIR) $(APPLICATION)
		
$(APPLICATION): $(OBJECTS) 
	g++ -o $@ $(OBJECTS) $(LOCAL_LDLIBS) $(LOCAL_STATIC_LIBRARIES) $(LOCAL_SHARED_LIBRARIES)
	
$(OBJDIR): 
	$(MKDIR) -p $@
	$(MKDIR) -p $(OUTFOLDERS)
	
$(OBJDIR)/%.o : $(GAME_FOLDER)/%.c
	gcc $(LOCAL_CFLAGS) $(LOCAL_C_INCLUDES) -g -c $< -o $@
	
$(OBJDIR)/%.o : $(GAME_FOLDER)/%.cpp
	gcc $(LOCAL_CFLAGS) $(LOCAL_C_INCLUDES) -g -c $< -o $@

$(OBJDIR)/%.o : $(BASE_FOLDER)/%.c
	gcc $(LOCAL_CFLAGS) $(LOCAL_C_INCLUDES) -g -c $< -o $@
	
$(OBJDIR)/%.o : $(BASE_FOLDER)/%.cpp
	gcc $(LOCAL_CFLAGS) $(LOCAL_C_INCLUDES) -g -c $< -o $@
	
clean:
	$(RM) $(APPLICATION)
	$(RM) -rf $(OBJDIR)
