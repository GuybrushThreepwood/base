
/*===================================================================
	File: OBB.cpp
	Library: Collision

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"
#include "CollisionBase.h"

using collision::OBB;

/////////////////////////////////////////////////////
/// Default constructor
/// 
///
/////////////////////////////////////////////////////
OBB::OBB()
{
	Reset();
}

/////////////////////////////////////////////////////
/// Default destructor
/// 
///
/////////////////////////////////////////////////////
OBB::~OBB()
{

}

/////////////////////////////////////////////////////
/// Method: Reset
/// Params: None
///
/////////////////////////////////////////////////////
void OBB::Reset( void )
{
	unsigned int i=0;
	vCenter = glm::vec3(0.0f, 0.0f, 0.0f);

	for( i=0; i<3; ++i )
		vAxis[i] = glm::vec3(0.0f, 0.0f, 0.0f);

	vHalfWidths = glm::vec3(1.0f, 1.0f, 1.0f);
}

/////////////////////////////////////////////////////
/// Method: ClosestPointTo
/// Params: [in]p
///
/////////////////////////////////////////////////////
glm::vec3 OBB::ClosestPointTo(const glm::vec3 &p)
{
	unsigned int i=0;
	glm::vec3 d = p - vCenter;

	// start at the center of the box
	glm::vec3 closestP = vCenter;

	for( i=0; i<3; ++i )
	{
		// project d onto that axis to get the distance along the axis of d from the box center
		float dist = glm::dot(d, vAxis[i]);

		// if distance is further than the box extends, clamp to the box
		if( dist > vHalfWidths[i] )
			dist = vHalfWidths[i];
		if( dist < -vHalfWidths[i] )
			dist = -vHalfWidths[i];

		// step that distance along the axis to get world coodinate
		closestP += vAxis[i] * dist;
	}

	return closestP;
}

/////////////////////////////////////////////////////
/// Method: OBBCollidesWithSphere
/// Params: [in]s
///
/////////////////////////////////////////////////////
bool OBB::OBBCollidesWithSphere( const collision::Sphere &s )
{
	glm::vec3 p = ClosestPointTo(s.vCenterPoint);

	// sphere and OBB intersect if the squared distance from the sphere center to point p is less than
	// the squared sphere radius
	glm::vec3 v = p - s.vCenterPoint;

	return (glm::dot(v, v) <= (s.r*s.r));
}

