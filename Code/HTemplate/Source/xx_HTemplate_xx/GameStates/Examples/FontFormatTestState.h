
/*===================================================================
	File: FontFormatTestState.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __FONTFORMATTESTSTATE_H__
#define __FONTFORMATTESTSTATE_H__

#include "ScriptAccess/ScriptDataHolder.h"

// forward declare
class IState;
class IBaseGameState;

class FontFormatTestState : public IBaseGameState
{
	public:
		FontFormatTestState( StateManager& stateManager );
		virtual ~FontFormatTestState();

		virtual void Enter();
		virtual void Exit();
		virtual int TransitionIn();
		virtual int TransitionOut();

		virtual void Update( float deltaTime );
		virtual void Draw();

		std::string WordWrap( std::string& text, renderer::FreetypeFont* pFont, std::vector<TFormatChangeBlock>& formatChanges, int maxWidth );

	private:
		float m_LastDelta;

		IState* m_pState;

		ScriptDataHolder* m_pScriptData;
		ScriptDataHolder::DevScriptData m_DevData;	

		renderer::FreetypeFont* m_FontTest;

		GameData m_GameData;

		GameCamera* m_GameCamera;
		glm::vec3 m_CamPos;
		glm::vec3 m_CamLookAt;

		int m_CurrentFontSize;
		float m_MaxTextHeight;

		int m_ChangeIndex;

		glm::vec3 m_ViewPos;
};

#endif // __FONTFORMATTESTSTATE_H__

