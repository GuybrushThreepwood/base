
#ifndef __INPUTBASE_H__
#define __INPUTBASE_H__

#ifndef __INPUT_H__
	#include "Input/Input.h"
#endif // __INPUT_H__

#ifndef __INPUTINCLUDE_H__
	#include "Input/InputInclude.h"
#endif // __INPUTINCLUDE_H__

#endif // __INPUTBASE_H__