
#ifndef __INPUT_H__
#define __INPUT_H__

namespace input
{
	const int MAX_TOUCHES = 10;
	const int FIRST_TOUCH = 0;
	const int SECOND_TOUCH = 1;
	const float HOLD_TIME = 0.2f;

	enum InputEventType
	{
		InputEventType_Press=0,
		InputEventType_Release,
		InputEventType_Held,
		InputEventType_Move,
		InputEventType_Tap,
	};

	enum MouseButtons
	{
		MouseButton_Left = 0,
		MouseButton_Right,
		MouseButton_Middle,

		MouseButton_Total
	};

	struct TInputEvent
	{
		InputEventType type;
			
		int touchId;

		long touchX;
		long touchY;
	};

	struct TInputState
	{

		InputEventType keyStates[SDL_NUM_SCANCODES];
		float keyHoldTimes[SDL_NUM_SCANCODES];

		InputEventType mouseStates[MouseButton_Total];
		float mouseHoldTimes[MouseButton_Total];

		long mouseX;
		long mouseY;
		long lastMouseX;
		long lastMouseY;

		long mouseXDelta;
		long mouseYDelta;

		bool mouseWheel;
		long mouseWheelDelta;
		
		int nTouchCount;
		struct TouchData
		{
			bool active;

			bool press;
			bool release;
			bool held;

			int taps;
			long touchX;
			long touchY;
		
			long lastTouchX;
			long lastTouchY;
		
			long xDelta;
			long yDelta;

			int specialId;

			glm::vec2 vAccumulatedVec;

			float ticks;
		};
		
		// first index can be counted for mouse touch
		TouchData TouchesData[MAX_TOUCHES];

		// extends a box around the touch point
		int nTouchWidth;
		int nTouchHeight;
		
		// sphere extends from the touch
		int nTouchRadius;

		float Accelerometers[3];
	};

	class Input
	{
		public:
			/// default constructor
			Input();
			/// default destructor
			~Input();

			/// IsKey - Is a certain key in a state
			/// \param nKeyCode - key to check
			/// \param state - returns false if a key was held
			/// \return bool - ( SUCCESS: true or FAIL: false )
			bool IsKeyState(int keyCode, InputEventType state);
			/// IsMouseButtonState - mouse button state
			/// \return bool - ( SUCCESS: true or FAIL: false )
			bool IsMouseButtonState(MouseButtons buttonId, InputEventType state);
			
		private:

	};

} // namespace input

#endif // __INPUT_H__

