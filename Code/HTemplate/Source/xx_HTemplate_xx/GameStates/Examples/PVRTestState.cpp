
/*===================================================================
	File: PVRTestState.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#ifdef BASE_SUPPORT_PVRSDK

#include "RenderBase.h"
#include "SoundBase.h"
#include "CollisionBase.h"
#include "InputBase.h"
#include "ScriptBase.h"

#include "Camera/Camera.h"
#include "Camera/DebugCamera.h"

#include "HTemplateConsts.h"
#include "HTemplate.h"
#include "Resources/StringResources.h"
#include "GameStates/IBaseGameState.h"

#include "ScriptAccess/ScriptAccess.h"

#include "GameStates/UI/UIIds.h"
#include "GameStates/PVRTestState.h"


#include "PVRShell/PVRShellNoPVRApi.h"
//Main include file for the PVRAssets library.
#include "PVRAssets/PVRAssets.h"
//Used to manually decompress PVRTC compressed textures on platforms that do not support them.
#include "PVRAssets/Texture/PVRTDecompress.h"

namespace
{
	//using namespace pvr;
	//using namespace pvr::types;

	// Index to bind the attributes to vertex shaders
	const pvr::uint32 VertexArray = 0;
	const pvr::uint32 NormalArray = 1;
	const pvr::uint32 TexCoordArray = 2;

	//Shader files
	const char FragShaderSrcFile[] = "FragShader.fsh";
	const char VertShaderSrcFile[] = "VertShader.vsh";

	//POD scene file
	const char SceneFile[] = "test.pod";

	const pvr::StringHash AttribNames[] =
	{
		"POSITION",
		"NORMAL",
		"UV0",
	};

	// OpenGL handles for shaders, textures and VBOs

	GLuint vao;
	std::vector<GLuint> vbo;
	std::vector<GLuint> indexVbo;
	std::vector<GLuint> texDiffuse;
	// Group shader programs and their uniform locations together
	struct
	{
		GLuint handle;
		pvr::uint32 uiMVPMatrixLoc;
		pvr::uint32 uiLightDirLoc;
		pvr::uint32 uiWorldViewITLoc;
	} ShaderProgram;

	// Variables to handle the animation in a time-based manner
	pvr::float32 frame;
	glm::mat4 projection;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////

bool getOpenGLFormat(pvr::PixelFormat pixelFormat, pvr::types::ColorSpace::Enum colorSpace, pvr::VariableType::Enum dataType,
	pvr::uint32& glInternalFormat, pvr::uint32& glFormat, pvr::uint32& glType, pvr::uint32& glTypeSize,
	bool& isCompressedFormat);


bool isGLExtensionSupported(const char *extName)
{
	GLint numExt, i;
	glGetIntegerv(GL_NUM_EXTENSIONS, &numExt);
	for (i = 0; i < numExt; i++) {

		const char* currExtension = (const char*)glGetStringi(GL_EXTENSIONS, i);
		if (strcmp(currExtension, extName) == 0)
			return true;
		//DBGLOG("%s\n", glGetStringi(GL_EXTENSIONS, i));
	}

	return false;
}

/*!*********************************************************************************************************************
\brief	Get an OpenGl format from the description of a Texture format in PVR Assets (Code extracted from PVRApi)
\return	Return true on success
\param	pixelFormat The pixel/texel format, as a pvr::PixelFormat (RGBA 8888 etc.)
\param	colorSpace	The colorspace as a pvr::Colorspaces (linear RGB or SRGB)
\param	dataType	The variable type (Float, Unsigned Byte etc.)
\param[out] glInternalFormat	Output: The GLenum that should be passed as the "internalFormat" parameter in GLES calls
\param[out] glFormat			Output: The GLenum that should be passed as the "format" parameter in GLES calls
\param[out] glType				Output: The GLenum that should be passed as the "glType" parameter in GLES calls
\param[out] glTypeSize			Output: The GLenum that should be passed as the "glTypeSize" parameter in GLES calls
\param[out] isCompressedFormat	Output: True is the format is compressed, false otherwise.
***********************************************************************************************************************/
bool getOpenGLFormat(pvr::PixelFormat pixelFormat, pvr::types::ColorSpace::Enum colorSpace, pvr::VariableType::Enum dataType,
	pvr::uint32& glInternalFormat, pvr::uint32& glFormat, pvr::uint32& glType, pvr::uint32& glTypeSize,
	bool& isCompressedFormat)
{
	isCompressedFormat = (pixelFormat.getPart().High == 0)
		&& (pixelFormat.getPixelTypeId() != pvr::CompressedPixelFormat::SharedExponentR9G9B9E5);
	if (pixelFormat.getPart().High == 0)
	{
		//Format and type == 0 for compressed textures.
		glFormat = 0;
		glType = 0;
		glTypeSize = 1;
		switch (pixelFormat.getPixelTypeId())
		{
		case pvr::CompressedPixelFormat::PVRTCI_2bpp_RGB:
		{
			glInternalFormat = GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG;
			return true;
		}
		case pvr::CompressedPixelFormat::PVRTCI_2bpp_RGBA:
		{
			glInternalFormat = GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG;
			return true;
		}
		case pvr::CompressedPixelFormat::PVRTCI_4bpp_RGB:
		{
			glInternalFormat = GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG;
			return true;
		}
		case pvr::CompressedPixelFormat::PVRTCI_4bpp_RGBA:
		{
			glInternalFormat = GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG;
			return true;
		}
#if !defined(TARGET_OS_IPHONE)
		case pvr::CompressedPixelFormat::PVRTCII_2bpp:
		{
			//JM:glInternalFormat = GL_COMPRESSED_RGBA_PVRTC_2BPPV2_IMG;
			return true;
		}
		case pvr::CompressedPixelFormat::PVRTCII_4bpp:
		{
			//JM:glInternalFormat = GL_COMPRESSED_RGBA_PVRTC_4BPPV2_IMG;
			return true;
		}
#endif
		case pvr::CompressedPixelFormat::SharedExponentR9G9B9E5:
		{
			//Not technically a compressed format by OpenGL ES standards.
			glType = GL_UNSIGNED_INT_5_9_9_9_REV;
			glTypeSize = 4;
			glFormat = GL_RGB;
			glInternalFormat = GL_RGB9_E5;
			return true;
		}
		case pvr::CompressedPixelFormat::ETC2_RGB:
		{
			if (colorSpace == pvr::types::ColorSpace::sRGB)
			{
				//JM:glInternalFormat = GL_COMPRESSED_SRGB8_ETC2;
			}
			else
			{
				//JM:glInternalFormat = GL_COMPRESSED_RGB8_ETC2;
			};
			return true;
		}
		case pvr::CompressedPixelFormat::ETC2_RGBA:
		{
			if (colorSpace == pvr::types::ColorSpace::sRGB)
			{
				//JM:glInternalFormat = GL_COMPRESSED_SRGB8_ALPHA8_ETC2_EAC;
			}
			else
			{
				//JM:glInternalFormat = GL_COMPRESSED_RGBA8_ETC2_EAC;
			}
			return true;
		}
		case pvr::CompressedPixelFormat::ETC2_RGB_A1:
		{
			if (colorSpace == pvr::types::ColorSpace::sRGB)
			{
				//JM:glInternalFormat = GL_COMPRESSED_SRGB8_PUNCHTHROUGH_ALPHA1_ETC2;
			}
			else
			{
				//JM:glInternalFormat = GL_COMPRESSED_RGB8_PUNCHTHROUGH_ALPHA1_ETC2;
			}
			return true;
		}
		case pvr::CompressedPixelFormat::EAC_R11:
		{
			if (dataType == pvr::VariableType::SignedInteger || dataType == pvr::VariableType::SignedIntegerNorm ||
				dataType == pvr::VariableType::SignedShort || dataType == pvr::VariableType::SignedShortNorm ||
				dataType == pvr::VariableType::SignedByte || dataType == pvr::VariableType::SignedByteNorm ||
				dataType == pvr::VariableType::SignedFloat)
			{
				//JM:glInternalFormat = GL_COMPRESSED_SIGNED_R11_EAC;
			}
			else
			{
				//JM:glInternalFormat = GL_COMPRESSED_R11_EAC;
			}
			return true;
		}
		case pvr::CompressedPixelFormat::EAC_RG11:
		{
			if (dataType == pvr::VariableType::SignedInteger || dataType == pvr::VariableType::SignedIntegerNorm ||
				dataType == pvr::VariableType::SignedShort || dataType == pvr::VariableType::SignedShortNorm ||
				dataType == pvr::VariableType::SignedByte || dataType == pvr::VariableType::SignedByteNorm ||
				dataType == pvr::VariableType::SignedFloat)
			{
				//JM:glInternalFormat = GL_COMPRESSED_SIGNED_RG11_EAC;
			}
			else
			{
				//JM:glInternalFormat = GL_COMPRESSED_RG11_EAC;
			}
			return true;
		}
		//Formats not supported by opengl/opengles
		case pvr::CompressedPixelFormat::BC4:
		case pvr::CompressedPixelFormat::BC5:
		case pvr::CompressedPixelFormat::BC6:
		case pvr::CompressedPixelFormat::BC7:
		case pvr::CompressedPixelFormat::RGBG8888:
		case pvr::CompressedPixelFormat::GRGB8888:
		case pvr::CompressedPixelFormat::UYVY:
		case pvr::CompressedPixelFormat::YUY2:
		case pvr::CompressedPixelFormat::BW1bpp:
		case pvr::CompressedPixelFormat::NumCompressedPFs:
			return false;
		}
	}
	else
	{
		switch (dataType)
		{
		case pvr::VariableType::UnsignedFloat:
			if (pixelFormat.getPixelTypeId() == pvr::assets::GeneratePixelType3<'r', 'g', 'b', 11, 11, 10>::ID)
			{
				glTypeSize = 4;
				glType = GL_UNSIGNED_INT_10F_11F_11F_REV;
				glFormat = GL_RGB;
				glInternalFormat = GL_R11F_G11F_B10F;
				return true;
			}
			break;
		case pvr::VariableType::SignedFloat:
		{
			switch (pixelFormat.getPixelTypeId())
			{
				//HALF_FLOAT
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 16, 16, 16, 16>::ID:
			{
				glTypeSize = 2;
				glType = GL_HALF_FLOAT;
				glFormat = GL_RGBA;
				glInternalFormat = GL_RGBA16F;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 16, 16, 16>::ID:
			{
				glTypeSize = 2;
				glType = GL_HALF_FLOAT;
				glFormat = GL_RGB;
				glInternalFormat = GL_RGB16F;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 16, 16>::ID:
			{
				glTypeSize = 2;
				glType = GL_HALF_FLOAT;
				glFormat = GL_RG;
				glInternalFormat = GL_RG16F;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 16>::ID:
			{
				glTypeSize = 2;
				glType = GL_HALF_FLOAT;
				glFormat = GL_RED;
				glInternalFormat = GL_R16F;
				return true;
			}
			//FLOAT
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 32, 32, 32, 32>::ID:
			{
				glTypeSize = 4;
				glType = GL_FLOAT;
				glFormat = GL_RGBA;
				glInternalFormat = GL_RGBA32F;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 32, 32, 32>::ID:
			{
				glTypeSize = 4;
				glType = GL_FLOAT;
				glFormat = GL_RGB;
				glInternalFormat = GL_RGB32F;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 32, 32>::ID:
			{
				glTypeSize = 4;
				glType = GL_FLOAT;
				glFormat = GL_RG;
				glInternalFormat = GL_RG32F;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 32>::ID:
			{
				glTypeSize = 4;
				glType = GL_FLOAT;
				glFormat = GL_RED;
				glInternalFormat = GL_R32F;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'d', 24>::ID:
			{
				glType = GL_UNSIGNED_INT;
				glTypeSize = 3;
				//JM:glInternalFormat = GL_DEPTH_COMPONENT24_OES;
				glFormat = GL_DEPTH_COMPONENT;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::UnsignedByteNorm:
		{
			glType = GL_UNSIGNED_BYTE;
			glTypeSize = 1;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 8, 8, 8, 8>::ID:
			{
				glFormat = GL_RGBA;
				if (colorSpace == pvr::types::ColorSpace::sRGB)
				{
					glInternalFormat = GL_SRGB8_ALPHA8;
				}
				else
				{
					glInternalFormat = GL_RGBA8;
				}
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 8, 8, 8>::ID:
			{
				glFormat = glInternalFormat = GL_RGB;
				if (colorSpace == pvr::types::ColorSpace::sRGB)
				{
					glInternalFormat = GL_SRGB8;
				}
				else
				{
					glInternalFormat = GL_RGB8;
				}
				return true;
			}

			case pvr::assets::GeneratePixelType2<'r', 'g', 8, 8>::ID:
			{
				glFormat = GL_RG;
				glInternalFormat = GL_RG8;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 8>::ID:
			{
				glFormat = GL_RED;
				glInternalFormat = GL_R8;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'l', 'a', 8, 8>::ID:
			{
				//JM:glFormat = GL_LUMINANCE_ALPHA;
				//JM:glInternalFormat = GL_LUMINANCE_ALPHA;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'l', 8>::ID:
			{
				//JM:glFormat = GL_LUMINANCE;
				//JM:glInternalFormat = GL_LUMINANCE;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'a', 8>::ID:
			{
				glFormat = GL_ALPHA;
				glInternalFormat = GL_ALPHA;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::SignedByteNorm:
		{
			glType = GL_BYTE;
			glTypeSize = 1;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 8, 8, 8, 8>::ID:
			{
				glFormat = GL_RGBA;
				glInternalFormat = GL_RGBA8_SNORM;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 8, 8, 8>::ID:
			{
				glFormat = GL_RGB;
				glInternalFormat = GL_RGB8_SNORM;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 8, 8>::ID:
			{
				glFormat = GL_RG;
				glInternalFormat = GL_RG8_SNORM;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 8>::ID:
			{
				glFormat = GL_RED;
				glInternalFormat = GL_R8_SNORM;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'l', 'a', 8, 8>::ID:
			{
				//JM:glFormat = GL_LUMINANCE_ALPHA;
				//JM:glInternalFormat = GL_LUMINANCE_ALPHA;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'l', 8>::ID:
			{
				//JM:glFormat = GL_LUMINANCE;
				//JM:glInternalFormat = GL_LUMINANCE;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::UnsignedByte:
		{
			glType = GL_UNSIGNED_BYTE;
			glTypeSize = 1;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 8, 8, 8, 8>::ID:
			{
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGBA8UI;
				//glInternalFormat = GL_RGBA;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 8, 8, 8>::ID:
			{
				glFormat = GL_RGB_INTEGER;
				glInternalFormat = GL_RGB8UI;
				//glInternalFormat = GL_RGB;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 8, 8>::ID:
			{
				glFormat = GL_RG_INTEGER;
				glInternalFormat = GL_RG8UI;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 8>::ID:
			{
				glFormat = GL_RED_INTEGER;
				glInternalFormat = GL_R8UI;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::SignedByte:
		{
			glType = GL_BYTE;
			glTypeSize = 1;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 8, 8, 8, 8>::ID:
			{
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGBA8I;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 8, 8, 8>::ID:
			{
				glFormat = GL_RGB_INTEGER;
				glInternalFormat = GL_RGB8I;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 8, 8>::ID:
			{
				glFormat = GL_RG_INTEGER;
				glInternalFormat = GL_RG8I;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 8>::ID:
			{
				glFormat = GL_RED_INTEGER;
				glInternalFormat = GL_R8I;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::UnsignedShortNorm:
		{
			glType = GL_UNSIGNED_SHORT;
			glTypeSize = 2;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 4, 4, 4, 4>::ID:
			{
				glType = GL_UNSIGNED_SHORT_4_4_4_4;
				glFormat = GL_RGBA;
				glInternalFormat = GL_RGBA4;
				return true;
			}
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 5, 5, 5, 1>::ID:
			{
				glType = GL_UNSIGNED_SHORT_5_5_5_1;
				glFormat = GL_RGBA;
				glInternalFormat = GL_RGB5_A1;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 5, 6, 5>::ID:
			{
				glType = GL_UNSIGNED_SHORT_5_6_5;
				glFormat = GL_RGB;
				glInternalFormat = GL_RGB565;
				return true;
			}

			case pvr::assets::GeneratePixelType2<'l', 'a', 16, 16>::ID:
			{
				//JM:glFormat = GL_LUMINANCE_ALPHA;
				//JM:glInternalFormat = GL_LUMINANCE_ALPHA;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'l', 16>::ID:
			{
				//JM:glFormat = GL_LUMINANCE;
				//JM:glInternalFormat = GL_LUMINANCE;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::SignedShortNorm:
		{
			glTypeSize = 2;
			glType = GL_SHORT;
			switch (pixelFormat.getPixelTypeId())
			{

			case pvr::assets::GeneratePixelType2<'l', 'a', 16, 16>::ID:
			{
				//JM:glFormat = GL_LUMINANCE_ALPHA;
				//JM:glInternalFormat = GL_LUMINANCE_ALPHA;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'l', 16>::ID:
			{
				//JM:glFormat = GL_LUMINANCE;
				//JM:glInternalFormat = GL_LUMINANCE;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::UnsignedShort:
		{
			glType = GL_UNSIGNED_SHORT;
			glTypeSize = 2;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 16, 16, 16, 16>::ID:
			{
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGBA16UI;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 16, 16, 16>::ID:
			{
				glFormat = GL_RGB_INTEGER;
				glInternalFormat = GL_RGB16UI;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 16, 16>::ID:
			{
				glFormat = GL_RG_INTEGER;
				glInternalFormat = GL_RG16UI;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 16>::ID:
			{
				glFormat = GL_RED_INTEGER;
				glInternalFormat = GL_R16UI;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'d', 16>::ID:
			{
				glFormat = GL_DEPTH_COMPONENT;
				glInternalFormat = GL_DEPTH_COMPONENT16;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::SignedShort:
		{
			glType = GL_SHORT;
			glTypeSize = 2;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 16, 16, 16, 16>::ID:
			{
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGBA16I;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 16, 16, 16>::ID:
			{
				glFormat = GL_RGB_INTEGER;
				glInternalFormat = GL_RGB16I;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 16, 16>::ID:
			{
				glFormat = GL_RG_INTEGER;
				glInternalFormat = GL_RG16I;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 16>::ID:
			{
				glFormat = GL_RED_INTEGER;
				glInternalFormat = GL_R16I;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::UnsignedIntegerNorm:
		{
			glTypeSize = 4;
			if (pixelFormat.getPixelTypeId() == pvr::assets::GeneratePixelType4<'a', 'b', 'g', 'r', 2, 10, 10, 10>::ID)
			{
				glType = GL_UNSIGNED_INT_2_10_10_10_REV;
				glFormat = GL_RGBA;
				glInternalFormat = GL_RGB10_A2;
				return true;
			}
			break;
		}
		case pvr::VariableType::UnsignedInteger:
		{
			glType = GL_UNSIGNED_INT;
			glTypeSize = 4;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 32, 32, 32, 32>::ID:
			{
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGBA32UI;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 32, 32, 32>::ID:
			{
				glFormat = GL_RGB_INTEGER;
				glInternalFormat = GL_RGB32UI;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 32, 32>::ID:
			{
				glFormat = GL_RG_INTEGER;
				glInternalFormat = GL_RG32UI;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 32>::ID:
			{
				glFormat = GL_RED_INTEGER;
				glInternalFormat = GL_R32UI;
				return true;
			}
			case pvr::assets::GeneratePixelType4<'a', 'b', 'g', 'r', 2, 10, 10, 10>::ID:
			{
				glType = GL_UNSIGNED_INT_2_10_10_10_REV;
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGB10_A2UI;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'d', 24>::ID:
			{
				glFormat = GL_DEPTH_COMPONENT;
				//JM:glInternalFormat = GL_DEPTH_COMPONENT24_OES;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'d', 's', 24, 8>::ID:
			{
				//JM:glFormat = GL_DEPTH_STENCIL_OES;
				//JM:glInternalFormat = GL_DEPTH24_STENCIL8_OES;
				return true;
			}
			}
			break;
		}
		case pvr::VariableType::SignedInteger:
		{
			glType = GL_INT;
			glTypeSize = 4;
			switch (pixelFormat.getPixelTypeId())
			{
			case pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 32, 32, 32, 32>::ID:
			{
				glFormat = GL_RGBA_INTEGER;
				glInternalFormat = GL_RGBA32I;
				return true;
			}
			case pvr::assets::GeneratePixelType3<'r', 'g', 'b', 32, 32, 32>::ID:
			{
				glFormat = GL_RGB_INTEGER;
				glInternalFormat = GL_RGB32I;
				return true;
			}
			case pvr::assets::GeneratePixelType2<'r', 'g', 32, 32>::ID:
			{
				glFormat = GL_RG_INTEGER;
				glInternalFormat = GL_RG32I;
				return true;
			}
			case pvr::assets::GeneratePixelType1<'r', 32>::ID:
			{
				glFormat = GL_RED_INTEGER;
				glInternalFormat = GL_R32I;
				return true;
			}
			}
			break;
		}
		default: {}
		}
	}
	//Default (erroneous) return values.
	glTypeSize = glType = glFormat = glInternalFormat = 0;
	return false;
}

/*!*********************************************************************************************************************
\brief	Upload the texture (extracted from PVRApi)
\return	Return pvr::Result::Success on success
\param	texture Texture to upload
\param	outTextureName Return a handle to uploaded opengles texture
\param	apiType What api used to upload the texture to
\param	allowDecompress Allow decompress the texture if it is a compressed texture when uploading
***********************************************************************************************************************/
pvr::Result::Enum textureUpload(const pvr::assets::Texture& texture, GLuint& outTextureName, pvr::Api::Enum apiType, bool allowDecompress/*=true*/)
{
	/*****************************************************************************
	* Initial error checks
	*****************************************************************************/
	// Check for any glError occurring prior to loading the texture, and warn the user.
	pvr::assertion(&texture != NULL, "TextureUtils.h:textureUpload:: Invalid Texture");

	// Check that the texture is valid.
	if (!texture.getDataSize())
	{
		pvr::Log(pvr::Log.Error, "TextureUtils.h:textureUpload:: Invalid texture supplied, please verify inputs.\n");

		return pvr::Result::UnsupportedRequest;
	}

	/*****************************************************************************
	* Setup code to get various state
	*****************************************************************************/
	// Generic error strings for textures being unsupported.
	const pvr::char8* cszUnsupportedFormat = "TextureUtils.h:textureUpload:: Texture format %s is not supported in this implementation.\n";
	const pvr::char8* cszUnsupportedFormatDecompressionAvailable = "TextureUtils.h:textureUpload:: Texture format %s is not supported in this implementation. Allowing software decompression (allowDecompress=true) will enable you to use this format.\n";

	// Get the texture format for the API.
	GLenum glInternalFormat = 0;
	GLenum glFormat = 0;
	GLenum glType = 0;
	GLenum glTypeSize = 0;
	bool unused;

	// Check that the format is a valid format for this API - Doesn't check specifically between
	// OpenGL/ES, it simply gets the values that would be set for a KTX file.
	if (!getOpenGLFormat(texture.getPixelFormat(), texture.getColorSpace(), texture.getChannelType(),
		glInternalFormat, glFormat, glType, glTypeSize, unused))
	{
		pvr::Log(pvr::Log.Error, "TextureUtils.h:textureUpload:: Texture's pixel type is not supported by this API.\n");
		return pvr::Result::UnsupportedRequest;
	}

	// Is the texture compressed? RGB9E5 is treated as an uncompressed texture in OpenGL/ES so
	// is a special case.
	bool isCompressedFormat = (texture.getPixelFormat().getPart().High == 0)
		&& (texture.getPixelFormat().getPixelTypeId() != pvr::CompressedPixelFormat::SharedExponentR9G9B9E5);

	//Whether we should use TexStorage or not.

	bool isEs2 = apiType < pvr::Api::OpenGLES3;
	bool useTexStorage = !isEs2;
	bool needsSwizzling = false;
	GLenum swizzle_r, swizzle_g, swizzle_b, swizzle_a;

	//Texture to use if we decompress in software.
	pvr::assets::Texture cDecompressedTexture;

	// Texture pointer which points at the texture we should use for the function. Allows
	// switching to, for example, a decompressed version of the texture.
	const pvr::assets::Texture* textureToUse = &texture;

	//Default texture target, modified as necessary as the texture type is determined.
	GLenum texTarget = GL_TEXTURE_2D;

	/*****************************************************************************
	* Check that extension support exists for formats supported in this way.
	*****************************************************************************/
	{
		//Check for formats that cannot be supported by this context version
		switch (glFormat)
		{
			//JM:
			/*case GL_LUMINANCE:
			if (!isEs2)
			{
				Log(Log.Information, "LUMINANCE texture format detected in OpenGL ES 3+ context. "
					"Remapping to RED texture with swizzling (r,r,r,1) enabled.");
				needsSwizzling = true;
				glFormat = GL_RED;
				glInternalFormat = GL_R8;
				swizzle_r = GL_RED;
				swizzle_g = GL_RED;
				swizzle_b = GL_RED;
				swizzle_a = GL_ONE;
			}
			break;*/
		case GL_ALPHA:
			if (!isEs2)
			{
				pvr::Log(pvr::Log.Information, "ALPHA format texture detected in OpenGL ES 3+ context. "
					"Remapping to RED texture with swizzling (0,0,0,r) enabled in order to allow"
					" Texture Storage.");
				needsSwizzling = true;
				glFormat = GL_RED;
				glInternalFormat = GL_R8;
				swizzle_r = GL_ZERO;
				swizzle_g = GL_ZERO;
				swizzle_b = GL_ZERO;
				swizzle_a = GL_RED;
			}
			break;
			//JM:
		/*case GL_LUMINANCE_ALPHA:
			if (!isEs2)
			{
				Log(Log.Information, "LUMINANCE/ALPHA format texture detected in OpenGL ES 3+ "
					"context. Remapping to RED texture with swizzling (r,r,r,g) enabled in order"
					" to allow Texture Storage.");
				needsSwizzling = true;
				glFormat = GL_RG;
				glInternalFormat = GL_RG8;
				swizzle_r = GL_RED;
				swizzle_g = GL_RED;
				swizzle_b = GL_RED;
				swizzle_a = GL_GREEN;
			} break;*/
		case GL_RED:
			if (isEs2)
			{
				pvr::Log(pvr::Log.Warning, "RED channel texture format texture detected in OpenGL ES 2+ "
					"context. Remapping to LUMINANCE texture to avoid errors. Ensure shaders "
					"are compatible with a LUMINANCE swizzle (r,r,r,1)");
				//JM:glFormat = GL_LUMINANCE;
				//JM:glInternalFormat = GL_LUMINANCE;
			} break;
		case GL_RG:
			if (isEs2)
			{
				pvr::Log(pvr::Log.Warning, "RED/GREEN channel texture format texture detected in OpenGL ES"
					" 2+ context. Remapping to LUMINANCE_ALPJA texture to avoid errors. Ensure "
					"shaders are compatible with a LUMINANCE/ALPHA swizzle (r,r,r,g)");
				//JM:glFormat = GL_LUMINANCE_ALPHA;
				//JM:glInternalFormat = GL_LUMINANCE_ALPHA;
			} break;
		}

		// Check for formats only supported by extensions.
		switch (glInternalFormat)
		{
		case GL_COMPRESSED_RGB_PVRTC_2BPPV1_IMG:
		case GL_COMPRESSED_RGBA_PVRTC_2BPPV1_IMG:
		case GL_COMPRESSED_RGB_PVRTC_4BPPV1_IMG:
		case GL_COMPRESSED_RGBA_PVRTC_4BPPV1_IMG:
		{
			//useTexStorage = false;
			if (!isGLExtensionSupported("GL_IMG_texture_compression_pvrtc"))
			{
				if (allowDecompress)
				{
					//No longer compressed if this is the case.
					isCompressedFormat = false;

					//Set up the new texture and header.
					pvr::assets::TextureHeader cDecompressedHeader(texture);
					// robin: not sure what should happen here. The PVRTGENPIXELID4 macro is used in the old SDK.
					cDecompressedHeader.setPixelFormat(pvr::assets::GeneratePixelType4<'r', 'g', 'b', 'a', 8, 8, 8, 8>::ID);

					cDecompressedHeader.setChannelType(pvr::VariableType::UnsignedByteNorm);
					cDecompressedTexture = pvr::assets::Texture(cDecompressedHeader);

					//Update the texture format.
					getOpenGLFormat(cDecompressedTexture.getPixelFormat(), cDecompressedTexture.getColorSpace(),
						cDecompressedTexture.getChannelType(), glInternalFormat, glFormat, glType,
						glTypeSize, unused);

					//Do decompression, one surface at a time.
					for (pvr::uint32 uiMIPLevel = 0; uiMIPLevel < textureToUse->getNumberOfMIPLevels(); ++uiMIPLevel)
					{
						for (pvr::uint32 uiArray = 0; uiArray < textureToUse->getNumberOfArrayMembers(); ++uiArray)
						{
							for (pvr::uint32 uiFace = 0; uiFace < textureToUse->getNumberOfFaces(); ++uiFace)
							{
								pvr::PVRTDecompressPVRTC(textureToUse->getDataPointer(uiMIPLevel, uiArray, uiFace),
									(textureToUse->getBitsPerPixel() == 2 ? 1 : 0),
									textureToUse->getWidth(uiMIPLevel),
									textureToUse->getHeight(uiMIPLevel),
									cDecompressedTexture.getDataPointer(uiMIPLevel, uiArray, uiFace));
							}
						}
					}

					//Make sure the function knows to use a decompressed texture instead.
					textureToUse = &cDecompressedTexture;
				}
				else
				{
					pvr::Log(pvr::Log.Error, cszUnsupportedFormatDecompressionAvailable, "PVRTC1");

					return pvr::Result::UnsupportedRequest;
				}
			}
			break;
		}
#if !defined(TARGET_OS_IPHONE)
		//JM:
		/*
		case GL_COMPRESSED_RGBA_PVRTC_2BPPV2_IMG:
		case GL_COMPRESSED_RGBA_PVRTC_4BPPV2_IMG:
		{
			//useTexStorage = false;
			if (!isGLExtensionSupported("GL_IMG_texture_compression_pvrtc2"))
			{
				Log(Log.Error, cszUnsupportedFormat, "PVRTC2");
				return Result::UnsupportedRequest;
			}
			break;
		}


		case GL_COMPRESSED_RGB_S3TC_DXT1_EXT:
		case GL_COMPRESSED_RGBA_S3TC_DXT1_EXT:
		{
			//useTexStorage = false;
			if (!isGLExtensionSupported("GL_EXT_texture_compression_dxt1"))
			{
				Log(Log.Error, cszUnsupportedFormat, "DXT1");
				return Result::UnsupportedRequest;
			}
			break;
		}
		case GL_COMPRESSED_RGBA_S3TC_DXT3_ANGLE:
		{
			//useTexStorage = false;
			if (!isGLExtensionSupported("GL_ANGLE_texture_compression_dxt3"))
			{
				Log(Log.Error, cszUnsupportedFormat, "DXT3");
				return Result::UnsupportedRequest;
			}
			break;
		}
		case GL_COMPRESSED_RGBA_S3TC_DXT5_ANGLE:
		{
			//useTexStorage = false;
			if (!isGLExtensionSupported("GL_ANGLE_texture_compression_dxt5"))
			{
				Log(Log.Error, cszUnsupportedFormat, "DXT5");
				return Result::UnsupportedRequest;
			}
			break;
		}*/
#endif

		default:
		{}
		}
	}

	/*****************************************************************************
	* Check the type of texture (e.g. 3D textures).
	*****************************************************************************/
	{
		// Only 2D Arrays are supported in this API.
		if (textureToUse->getNumberOfArrayMembers() > 1)
		{
			pvr::Log(pvr::Log.Error,"TextureUtils.h:textureUpload:: Texture arrays are not supported by this implementation.\n");

			return pvr::Result::UnsupportedRequest;
		}

		// 3D Cubemaps aren't supported
		if (textureToUse->getDepth() > 1)
		{
			pvr::Log(pvr::Log.Error, "TextureUtils.h:textureUpload:: 3-Dimensional textures are not supported by this implementation.\n");

			return pvr::Result::UnsupportedRequest;
		}

		//Check if it's a Cube Map.
		if (textureToUse->getNumberOfFaces() > 1)
		{
			//Make sure it's a complete cube, otherwise warn the user. We've already checked if it's a 3D texture or a texture array as well.
			if (textureToUse->getNumberOfFaces() < 6)
			{
				pvr::Log(pvr::Log.Warning, "TextureUtils.h:textureUpload:: Textures with between 2 and 5 faces are unsupported. Faces up to 6 will be allocated in a cube map as undefined surfaces.\n");
			}
			else if (textureToUse->getNumberOfFaces() > 6)
			{
				pvr::Log(pvr::Log.Warning, "TextureUtils.h:textureUpload:: Textures with more than 6 faces are unsupported. Only the first 6 faces will be loaded into the API.\n");
			}
			texTarget = GL_TEXTURE_CUBE_MAP;
		}
	}

	/*****************************************************************************
	* Setup the texture object.
	*****************************************************************************/
	{
		// Check the error here, in case the extension loader or anything else raised any errors.
		//Generate a new texture name.
		glGenTextures(1, &outTextureName);

		//Bind the texture to edit it.
		glBindTexture(texTarget, outTextureName);

		//Set the unpack alignment to 1 - PVR textures are not stored as padded.
		glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

		if (needsSwizzling)
		{
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, swizzle_r);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, swizzle_g);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, swizzle_b);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, swizzle_a);
		}


	}

	/*****************************************************************************
	* Load the texture.
	*****************************************************************************/
	{
		// 2D textures.
		if (texTarget == GL_TEXTURE_2D)
		{
			for (pvr::uint32 uiMIPLevel = 0; uiMIPLevel < textureToUse->getNumberOfMIPLevels(); ++uiMIPLevel)
			{
				if (isCompressedFormat)
				{
					glCompressedTexImage2D(texTarget, uiMIPLevel, glInternalFormat,
						textureToUse->getWidth(uiMIPLevel),
						textureToUse->getHeight(uiMIPLevel), 0,
						textureToUse->getDataSize(uiMIPLevel, false, false),
						textureToUse->getDataPointer(uiMIPLevel, 0, 0));
				}
				else
				{
					if (isEs2) { glInternalFormat = glFormat; }
					glTexImage2D(texTarget, uiMIPLevel, glInternalFormat, textureToUse->getWidth(uiMIPLevel),
						textureToUse->getHeight(uiMIPLevel), 0, glFormat, glType, textureToUse->getDataPointer(uiMIPLevel, 0, 0));

				}
			}
		}
		// Cube maps.
		else if (texTarget == GL_TEXTURE_CUBE_MAP)
		{
			for (pvr::uint32 uiMIPLevel = 0; uiMIPLevel < textureToUse->getNumberOfMIPLevels(); ++uiMIPLevel)
			{
				//Iterate through 6 faces regardless, as these should always be iterated through. We fill in the blanks with uninitialized data for uncompressed textures, or repeat faces for compressed data.
				for (pvr::uint32 uiFace = 0; uiFace < 6; ++uiFace)
				{
					GLenum eTexImageTarget = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
					if (isCompressedFormat)
					{
						//Make sure to wrap texture faces around if there are fewer faces than 6 in a compressed texture.
						glCompressedTexImage2D(eTexImageTarget + uiFace, uiMIPLevel, glInternalFormat,
							textureToUse->getWidth(uiMIPLevel),
							textureToUse->getHeight(uiMIPLevel), 0, textureToUse->getDataSize(uiMIPLevel, false, false),
							textureToUse->getDataPointer(uiMIPLevel, 0, uiFace % textureToUse->getNumberOfFaces()));
					}
					else
					{
						//No need to wrap faces for uncompressed textures, as gl will handle a NULL pointer, which Texture::getDataPtr will do when requesting a non-existant face.
						glTexImage2D(eTexImageTarget + uiFace, uiMIPLevel, glInternalFormat,
							textureToUse->getWidth(uiMIPLevel),
							textureToUse->getHeight(uiMIPLevel), 0, glFormat, glType, textureToUse->getDataPointer(uiMIPLevel, 0,
							uiFace % textureToUse->getNumberOfFaces()));
					}
				}
			}
		}
		else
		{
			pvr::Log(pvr::Log.Debug, "TextureUtilsGLES3 : TextureUpload : File corrupted or suspected bug : unknown texture target type.");
		}
	}

	return pvr::Result::Success;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////
// Example specific methods
//////////////////////////////////////////////////////////////////////////////////////////////////////

/*!*********************************************************************************************************************
\brief	load pvr texture
\return	Result::Success on success
\param	const StringHash & filename
\param	GLuint & outTexHandle
\param	pvr::assets::Texture * outTexture
\param	assets::TextureHeader * outDescriptor
***********************************************************************************************************************/
pvr::Result::Enum PVRTestState::loadTexturePVR(const pvr::StringHash& filename, GLuint& outTexHandle, pvr::assets::Texture* outTexture, pvr::assets::TextureHeader* outDescriptor)
{
	pvr::assets::Texture tempTexture;
	pvr::Result::Enum result = pvr::Result::UnableToOpen;
	GLuint textureHandle;

	char *base_path = SDL_GetBasePath();

	pvr::Stream::ptr_type stream;
	//for (size_t i = 0; i < paths.size(); ++i)
	{
		std::string filepath(base_path);
		filepath += filename;
		stream.reset(new pvr::FileStream(filepath, "rb"));

		if (stream->open())
		{
			if (!stream.get())
			{
				pvr::Log(pvr::Log.Error, "AssetStore.loadModel  error for filename %s : File not found", filename);

				return pvr::Result::NotFound;
			}
			
			result = pvr::assets::textureLoad(stream, pvr::assets::TextureFileFormat::PVR, tempTexture);

			if (result == pvr::Result::Success)
			{
				textureUpload(tempTexture, textureHandle, pvr::Api::OpenGLES2, true);
			}
			if (result != pvr::Result::Success)
			{
				stream.release();
				stream.reset();

				pvr::Log(pvr::Log.Error, "AssetStore.loadTexture error for filename %s : Failed to load texture with code %s.", filename.c_str(), pvr::Log.getResultCodeString(result));

				return result;
			}
			if (outTexture)
			{ 
				*outTexture = tempTexture;
			}

			outTexHandle = textureHandle;

			stream.release();
			stream.reset();
		}
	}
	return result;
}

/*!*********************************************************************************************************************
\brief	Load model
\return	Return Result::Success on success
\param	assetProvider Assets stream provider
\param	filename Name of the model file
\param	outModel Returned loaded model
***********************************************************************************************************************/
pvr::Result::Enum loadModel( const char* filename, pvr::assets::ModelHandle& outModel)
{
	char *base_path = SDL_GetBasePath();

	pvr::Stream::ptr_type stream;
	//for (size_t i = 0; i < paths.size(); ++i)
	{
		std::string filepath(base_path);
		filepath += filename;
		stream.reset(new pvr::FileStream(filepath, "rb"));

		if (stream->open())
		{
			if (!stream.get())
			{
				pvr::Log(pvr::Log.Error, "AssetStore.loadModel  error for filename %s : File not found", filename);

				return pvr::Result::NotFound;
			}

			pvr::assets::PODReader reader(stream);
			outModel = pvr::assets::Model::createWithReader(reader);

			if (outModel.isNull())
			{
				pvr::Log(pvr::Log.Error, "AssetStore.loadModel error : Failed to load model %s.", filename);

				return pvr::Result::UnableToOpen;
			}

			reader.closeAssetStream();
			stream.release();
			stream.reset(0);
		}
	}

	return pvr::Result::Success;
}

/*!*********************************************************************************************************************
\brief	Load the material's textures
\return	Return true if success
***********************************************************************************************************************/
bool PVRTestState::loadTextures()
{
	pvr::uint32 numMaterials = m_Scene->getNumMaterials();
	texDiffuse.resize(numMaterials);
	for (pvr::uint32 i = 0; i < numMaterials; ++i)
	{
		const pvr::assets::Model::Material& material = m_Scene->getMaterial(i);
		if (material.getDiffuseTextureIndex() != -1)
		{
			// Load the diffuse texture map
			if (loadTexturePVR(m_Scene->getTexture(material.getDiffuseTextureIndex()).getName(),
				texDiffuse[i], NULL, 0) != pvr::Result::Success)
			{
				pvr::Log("Failed to load texture %s", m_Scene->getTexture(material.getDiffuseTextureIndex()).getName().c_str());
				return false;
			}
			glBindTexture(GL_TEXTURE_2D, texDiffuse[i]);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		}
	}// next material

	GL_CHECK

	return true;
}

/*!*********************************************************************************************************************
\brief	Loads and compiles the shaders and links the shader programs required for this training course
\return	Return true if no error occurred
***********************************************************************************************************************/
bool PVRTestState::loadShaders()
{
	// Load and compile the shaders from files.

	const char* attributes[] = { "inVertex", "inNormal", "inTexCoord" };

	ShaderProgram.handle = renderer::LoadShaderFilesForProgram(VertShaderSrcFile, FragShaderSrcFile);

	// Set the sampler2D variable to the first texture unit
	glUseProgram(ShaderProgram.handle);
	glUniform1i(glGetUniformLocation(ShaderProgram.handle, "sTexture"), 0);

	// Store the location of uniforms for later use
	ShaderProgram.uiMVPMatrixLoc = glGetUniformLocation(ShaderProgram.handle, "MVPMatrix");
	ShaderProgram.uiLightDirLoc = glGetUniformLocation(ShaderProgram.handle, "LightDirection");
	ShaderProgram.uiWorldViewITLoc = glGetUniformLocation(ShaderProgram.handle, "WorldViewIT");

	GL_CHECK

	return true;
}

/*!*********************************************************************************************************************
\brief	Loads the mesh data required for this training course into vertex buffer objects
\return	Return true if no error occurred
***********************************************************************************************************************/
bool PVRTestState::LoadVbos()
{
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	GL_CHECK

	vbo.resize(m_Scene->getNumMeshes());
	indexVbo.resize(m_Scene->getNumMeshes());
	glGenBuffers(m_Scene->getNumMeshes(), &vbo[0]);

	// Load vertex data of all meshes in the scene into VBOs
	// The meshes have been exported with the "Interleave Vectors" option,
	// so all data is interleaved in the buffer at pMesh->pInterleaved.
	// Interleaving data improves the memory access pattern and cache efficiency,
	// thus it can be read faster by the hardware.

	for (unsigned int i = 0; i < m_Scene->getNumMeshes(); ++i)
	{
		// Load vertex data into buffer object
		const pvr::assets::Mesh& mesh = m_Scene->getMesh(i);
		pvr::uint32 size = mesh.getDataSize(0);
		//PVRTuint32 uiSize = Mesh.nNumVertex * Mesh.sVertex.nStride;
		glBindBuffer(GL_ARRAY_BUFFER, vbo[i]);
		glBufferData(GL_ARRAY_BUFFER, size, mesh.getData(0), GL_STATIC_DRAW);

		// Load index data into buffer object if available
		indexVbo[i] = 0;
		if (mesh.getFaces().getData())
		{
			glGenBuffers(1, &indexVbo[i]);
			size = mesh.getFaces().getDataSize();
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVbo[i]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, size, mesh.getFaces().getData(), GL_STATIC_DRAW);
		}
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	GL_CHECK

	return true;
}

/*!*********************************************************************************************************************
\param	nodeIndex		Node index of the mesh to draw
\brief	Draws a mesh after the model view matrix has been set and the material prepared.
***********************************************************************************************************************/
void PVRTestState::drawMesh(int nodeIndex)
{
	int meshIndex = m_Scene->getMeshNode(nodeIndex).getObjectId();
	const pvr::assets::Mesh& mesh = m_Scene->getMesh(meshIndex);
	const pvr::int32 matId = m_Scene->getMeshNode(nodeIndex).getMaterialIndex();

	glBindVertexArray(vao);

	glBindTexture(GL_TEXTURE_2D, texDiffuse[matId]);
	// bind the VBO for the mesh
	glBindBuffer(GL_ARRAY_BUFFER, vbo[meshIndex]);
	// bind the index buffer, won't hurt if the handle is 0
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexVbo[meshIndex]);

	// Enable the vertex attribute arrays
	glEnableVertexAttribArray(VertexArray);
	glEnableVertexAttribArray(NormalArray);
	glEnableVertexAttribArray(TexCoordArray);

	// Set the vertex attribute offsets
	const pvr::assets::VertexAttributeData* posAttrib = mesh.getVertexAttributeByName(AttribNames[0]);
	const pvr::assets::VertexAttributeData* normalAttrib = mesh.getVertexAttributeByName(AttribNames[1]);
	const pvr::assets::VertexAttributeData* texCoordAttrib = mesh.getVertexAttributeByName(AttribNames[2]);

	glVertexAttribPointer(VertexArray, posAttrib->getN(), GL_FLOAT, GL_FALSE, mesh.getStride(0), (void*)posAttrib->getOffset());
	glVertexAttribPointer(NormalArray, normalAttrib->getN(), GL_FLOAT, GL_FALSE, mesh.getStride(0), (void*)normalAttrib->getOffset());
	glVertexAttribPointer(TexCoordArray, texCoordAttrib->getN(), GL_FLOAT, GL_FALSE, mesh.getStride(0), (void*)texCoordAttrib->getOffset());

	//	The geometry can be exported in 4 ways:
	//	- Indexed Triangle list
	//	- Non-Indexed Triangle list
	//	- Indexed Triangle strips
	//	- Non-Indexed Triangle strips
	if (mesh.getNumStrips() == 0)
	{
		if (indexVbo[meshIndex])
		{
			// Indexed Triangle list
			// Are our face indices unsigned shorts? If they aren't, then they are unsigned ints
			GLenum type = (mesh.getFaces().getDataType() == pvr::types::IndexType::IndexType16Bit) ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT;
			glDrawElements(GL_TRIANGLES, mesh.getNumFaces() * 3, type, 0);
		}
		else
		{
			// Non-Indexed Triangle list
			glDrawArrays(GL_TRIANGLES, 0, mesh.getNumFaces() * 3);
		}
	}
	else
	{
		pvr::uint32 offset = 0;
		// Are our face indices unsigned shorts? If they aren't, then they are unsigned ints
		GLenum type = (mesh.getFaces().getDataType() == pvr::types::IndexType::IndexType16Bit) ? GL_UNSIGNED_SHORT : GL_UNSIGNED_INT;

		for (int i = 0; i < (int)mesh.getNumStrips(); ++i)
		{
			if (indexVbo[meshIndex])
			{
				// Indexed Triangle strips
				glDrawElements(GL_TRIANGLE_STRIP, mesh.getStripLength(i) + 2, type, (void*)(size_t)(offset * mesh.getFaces().getDataSize()));
			}
			else
			{
				// Non-Indexed Triangle strips
				glDrawArrays(GL_TRIANGLE_STRIP, offset, mesh.getStripLength(i) + 2);
			}
			offset += mesh.getStripLength(i) + 2;
		}
	}

	// Safely disable the vertex attribute arrays
	glDisableVertexAttribArray(VertexArray);
	glDisableVertexAttribArray(NormalArray);
	glDisableVertexAttribArray(TexCoordArray);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glBindVertexArray(0);

	GL_CHECK
}

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
PVRTestState::PVRTestState(StateManager& stateManager)
: IBaseGameState( stateManager, ID_MAINGAMESTATE )
, m_pScriptData(0)
{
	m_LastDelta = 0.0f;

}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
PVRTestState::~PVRTestState()
{

}

/////////////////////////////////////////////////////
/// Method: Enter
/// Params: None
///
/////////////////////////////////////////////////////
void PVRTestState::Enter()
{
	m_pScriptData = GetScriptDataHolder();
	m_DevData = m_pScriptData->GetDevData();
	m_GameData = m_pScriptData->GetGameData();

	m_GameCamera = GameSystems::GetInstance()->GetGameCamera();

	renderer::OpenGL::GetInstance()->ClearColour( 0.5f, 0.5f, 0.5f, 1.0f );

	GameSystems::GetInstance()->CreatePhysics();

	glm::vec3 camPos = glm::vec3(10, 10, 20);
	glm::vec3 camTarget = glm::vec3(0, 0, 0);
	m_GameCamera->SetLerp(true);
	m_GameCamera->SetPosition( camPos );
	m_GameCamera->SetTarget( camTarget );

	//context = pvr::createGraphicsContext();

	// Load the scene
	pvr::Result::Enum rslt = pvr::Result::Success;
	if ((rslt = loadModel(SceneFile, m_Scene)) != pvr::Result::Success)
	{
		DBGLOG("ERROR: Couldn't load the .pod file\n");
		//return rslt;
	}

	// The cameras are stored in the file. We check it contains at least one.
	if (m_Scene->getNumCameras() == 0)
	{
		DBGLOG("ERROR: The scene does not contain a camera. Please add one and re-export.\n");
		//return pvr::Result::InvalidData;
	}

	// We also check that the scene contains at least one light
	if (m_Scene->getNumLights() == 0)
	{
		DBGLOG("ERROR: The scene does not contain a light. Please add one and re-export.\n");
		//return pvr::Result::InvalidData;
	}

	// Initialize variables used for the animation
	frame = 0;

	pvr::string ErrorStr;

	//	Initialize VBO data
	if (!LoadVbos())
	{
		DBGLOG(ErrorStr.c_str());
		//return pvr::Result::UnknownError;
	}

	//	Load textures
	if (!loadTextures())
	{
		DBGLOG(ErrorStr.c_str());
		//return pvr::Result::UnknownError;
	}

	//	Load and compile the shaders & link programs
	if (!loadShaders())
	{
		DBGLOG(ErrorStr.c_str());
		//return pvr::Result::UnknownError;
	}

	//	Set OpenGL ES render states needed for this training course
	// Enable backface culling and depth test
	glCullFace(GL_BACK);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);

	// Use a nice bright blue as clear color
	glClearColor(0.00, 0.70, 0.67, 1.0f);
	// Calculate the projection matrix

	//JM:
	/*bool isRotated = this->isScreenRotated() && this->isFullScreen();
	if (isRotated)
	{
		projection = pvr::math::perspectiveFov(pvr::Api::OpenGLES2, scene->getCamera(0).getFOV(), (float)this->getHeight(),
			(float)this->getWidth(), scene->getCamera(0).getNear(), scene->getCamera(0).getFar(),
			glm::pi<pvr::float32>() * .5f);// rotate by 90 degree
	}
	else*/
	{
		projection = glm::perspectiveFov(m_Scene->getCamera(0).getFOV(), (float)/*this->getWidth()*/core::app::GetFrameWidth(),
			(float)/*this->getHeight()*/core::app::GetFrameHeight(), m_Scene->getCamera(0).getNear(), m_Scene->getCamera(0).getFar());
	}

	GL_CHECK
}

/////////////////////////////////////////////////////
/// Method: Exit
/// Params: None
///
/////////////////////////////////////////////////////
void PVRTestState::Exit()
{
	// Deletes the textures
	glDeleteTextures(texDiffuse.size(), &texDiffuse[0]);

	// Delete program and shader objects
	renderer::RemoveShaderProgram(ShaderProgram.handle);

	glDeleteBuffers(vbo.size(), &vbo[0]);
	glDeleteBuffers(indexVbo.size(), &indexVbo[0]);

	glDeleteVertexArrays(1, &vao);

	// Delete buffer objects
	m_Scene->destroy();

	GL_CHECK
	
	m_GameCamera = 0;

	GameSystems::GetInstance()->DestroyPhysics();
}

/////////////////////////////////////////////////////
/// Method: TransitionIn
/// Params: None
///
/////////////////////////////////////////////////////
int PVRTestState::TransitionIn()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: TransitionOut
/// Params: None
///
/////////////////////////////////////////////////////
int PVRTestState::TransitionOut()
{
	return 0;
}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: None
///
/////////////////////////////////////////////////////
void PVRTestState::Update(float deltaTime)
{
	m_LastDelta = deltaTime;	

	m_GameCamera->Update( deltaTime );

	//CheckAchievements( deltaTime );
}

/////////////////////////////////////////////////////
/// Method: Draw
/// Params: None
///
/////////////////////////////////////////////////////
void PVRTestState::Draw()
{
	// default colour
/*	renderer::OpenGL::GetInstance()->SetColour(1.0f, 1.0f, 1.0f, 1.0f);

	if( m_DevData.allowDebugCam && gDebugCamera.IsEnabled() )
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 0.5f, 100000.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		renderer::OpenGL::GetInstance()->SetLookAt( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z, 
												gDebugCamera.GetTarget().x, gDebugCamera.GetTarget().y, gDebugCamera.GetTarget().z );
	
		snd::SoundManager::GetInstance()->SetListenerPosition( gDebugCamera.GetPosition().x, gDebugCamera.GetPosition().y, gDebugCamera.GetPosition().z );
		snd::SoundManager::GetInstance()->SetListenerOrientation((float)std::sin(glm::radians(-gDebugCamera.GetAngle())), 0.0f, (float)std::cos(glm::radians(-gDebugCamera.GetAngle())),
																	0.0f, 1.0f, 0.0f );
	}
	else
	{
		renderer::OpenGL::GetInstance()->SetNearFarClip( 1.0f, 100.0f );
		renderer::OpenGL::GetInstance()->SetupPerspectiveView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );

		m_GameCamera->SetupCamera();

		snd::SoundManager::GetInstance()->SetListenerOrientation(std::sin(glm::radians(180.0f)), 0.0f, std::cos(glm::radians(180.0f)), 0.0f, 1.0f, 0.0f);
	}
	
	renderer::OpenGL::GetInstance()->DepthMode( true, GL_LESS );
	renderer::OpenGL::GetInstance()->BlendMode( false, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	renderer::OpenGL::GetInstance()->DisableLighting();

	renderer::OpenGL::GetInstance()->SetNearFarClip( NEAR_CLIP_ORTHO, FAR_CLIP_ORTHO );
	renderer::OpenGL::GetInstance()->SetupOrthographicView( core::app::GetFrameWidth(), core::app::GetFrameHeight(), true );
	renderer::OpenGL::GetInstance()->DisableLighting();

	GL_CHECK;
		
	int offset = 10;
	int xPos;
	int yPos;

	xPos = core::app::GetFrameWidth() - 150;
	yPos = core::app::GetFrameHeight() - 60;

	DBGPRINT(xPos, yPos -= offset, "DELTA (%.6f)", m_LastDelta);
	//DBGPRINT( xPos, yPos-=offset, "CAM POS (%.2f  %.2f  %.2f)", m_GameCamera->GetPosition().x, m_GameCamera->GetPosition().y, m_GameCamera->GetPosition().z );
	//DBGPRINT( xPos, yPos-=offset, "CAM TARGET (%.2f  %.2f  %.2f)", m_GameCamera->GetTarget().x, m_GameCamera->GetTarget().y, m_GameCamera->GetTarget().z );
*/
	// Clear the color and depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// Use shader program
	glUseProgram(ShaderProgram.handle);

	// Calculates the frame number to animate in a time-based manner.
	// get the time in milliseconds.
	frame += (float)m_LastDelta * 30.f; /*design-time target fps for animation*/

	if (frame > m_Scene->getNumFrames() - 1) 
	{ 
		frame = 0; 
	}

	// Sets the scene animation to this frame
	m_Scene->setCurrentFrame(frame);

	// Get the direction of the first light from the scene.
	glm::vec3 lightDirVec3;
	m_Scene->getLightDirection(0, lightDirVec3);
	// For direction vectors, w should be 0
	glm::vec4 lightDirVec4(glm::normalize(lightDirVec3), 1.f);

	// Set up the view and projection matrices from the camera
	glm::mat4 mView;
	glm::vec3	vFrom, vTo, vUp;
	float fFOV;

	// Camera nodes are after the mesh and light nodes in the array
	m_Scene->getCameraProperties(0, fFOV, vFrom, vTo, vUp);

	// We can build the model view matrix from the camera position, target and an up vector.
	// For this we use glm::lookAt()
	mView = glm::lookAt(vFrom, vTo, vUp);

	//	A scene is composed of nodes. There are 3 types of nodes:
	//	- MeshNodes :
	//		references a mesh in the pMesh[].
	//		These nodes are at the beginning of the pNode[] array.
	//		And there are getNumMeshNodes() number of them.
	//		This way the .pod format can instantiate several times the same mesh
	//		with different attributes.
	//	- lights
	//	- cameras
	//	To draw a scene, you must go through all the MeshNodes and draw the referenced meshes.
	for (unsigned int i = 0; i < m_Scene->getNumMeshNodes(); ++i)
	{
		// Get the node model matrix
		glm::mat4 mWorld = m_Scene->getWorldMatrix(i);

		// Pass the model-view-projection matrix (MVP) to the shader to transform the vertices
		glm::mat4 mModelView, mMVP;
		mModelView = mView * mWorld;
		mMVP = projection * mModelView;
		glm::mat4 worldIT = glm::inverseTranspose(mModelView);
		glUniformMatrix4fv(ShaderProgram.uiMVPMatrixLoc, 1, GL_FALSE, glm::value_ptr(mMVP));
		glUniformMatrix4fv(ShaderProgram.uiWorldViewITLoc, 1, GL_FALSE, glm::value_ptr(worldIT));

		// Pass the light direction in view space to the shader
		glm::vec4 vLightDir = mView * lightDirVec4;
		glm::vec3 dirModelVec3 = glm::normalize(*(glm::vec3*)&vLightDir);

		glUniform3f(ShaderProgram.uiLightDirLoc, dirModelVec3.x, dirModelVec3.y, dirModelVec3.z);
		//	Now that the model-view matrix is set and the materials are ready,
		//	call another function to actually draw the mesh.
		drawMesh(i);
	}
}

#endif // BASE_SUPPORT_PVRSDK
