
/*===================================================================
	File: TiledBaseLoader.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifdef BASE_SUPPORT_BOX2D

#include "CoreBase.h"

#include "RenderBase.h"
#include "SoundBase.h"
#include "PhysicsBase.h"
#include "ScriptBase.h"

#include "Support/Tiled/TiledBaseLoader.h"

namespace
{
	const char* ORIENTATION_ORTHOGONAL = "orthogonal";
	const char* ORIENTATION_ISOMETRIC = "isometric";
	const char* ORIENTATION_ISOMETRIC_STAGGERED = "staggered";

	const char* OBJECTS_TABLE = "objects";
	const char* DATA_TABLE = "data";
	const char* LAYERS_TABLE = "layers";
	const char* TILES_TABLE = "tiles";
	const char* TILESETS_TABLE = "tilesets";
	const char* PROPERTIES_TABLE = "properties";
	const char* ANIMATION_TABLE = "animation";

	const char* LAYER_TYPE_TILE = "tilelayer";
	const char* LAYER_TYPE_OBJECT = "objectgroup";

	const char* POLYLINE_TABLE = "polyline";
	const char* POLYGON_TABLE = "polygon";

	const char* OBJECTLAYER_TYPE_RECTANGLE = "rectangle";
	const char* OBJECTLAYER_TYPE_ELLIPSE = "ellipse";
	const char* OBJECTLAYER_TYPE_POLYGON = "polygon";
	const char* OBJECTLAYER_TYPE_POLYLINE = "polyline";

	// custom
	const char* COLLISION_LAYERS_PROP = "collision_layers";
	const char* ANIMATION_FILE_PROP = "animation_files";

	const int MAX_ANIM_FRAMES = 32;
}

using support::tiled::TiledBaseLoader;

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
TiledBaseLoader::TiledBaseLoader()
{
	m_TileScale = glm::vec2(core::app::GetWidthScale(), core::app::GetHeightScale());

	m_MapWidth = 32;
	m_MapHeight = 24;
	m_MapTileWidth = 32;
	m_MapTileHeight = 32;

	m_MapOrientation = MapOrientation_Orthogonal;

	m_NumMapProperties = 0;
	m_MapProperties = 0;

	m_NumTilesets = 0;
	m_Tilesets = 0;

	m_NumLayers = 0;
	m_Layers = 0;

	m_pBody = 0;
	m_Box2DScaleFactor = 1.0f;

	b2BodyDef bd;
	bd.type = b2_staticBody;
		
	m_pBody = physics::PhysicsWorldB2D::GetWorld()->CreateBody(&bd);
	DBG_ASSERT_MSG( (m_pBody != 0), "Could not create level generic holding body" );

	m_pBody->SetUserData( reinterpret_cast<void *>(this) );
	m_pBody->SetGravityScale( 0.0f );
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
TiledBaseLoader::~TiledBaseLoader()
{
	int i=0, j=0;

	if( m_pBody != 0 )
	{
		if( physics::PhysicsWorldB2D::GetWorld() )
			physics::PhysicsWorldB2D::GetWorld()->DestroyBody(m_pBody);

		m_pBody = 0;
	}

	if( m_MapProperties != 0 )
	{
		delete[] m_MapProperties;
		m_MapProperties = 0;
	}

	for( i=0; i < m_NumLayers; ++i )
	{
		if( m_Layers[i].propertyList != 0 )
		{
			delete[] m_Layers[i].propertyList;
			m_Layers[i].propertyList = 0;
		}

		for( j=0; j < m_Layers[i].numObjects; ++j )
		{
			if( m_Layers[i].objectList[j].propertyList != 0 )
			{
				delete[] m_Layers[i].objectList[j].propertyList;
				m_Layers[i].objectList[j].propertyList = 0;
			}

			if( m_Layers[i].objectList[j].polygonPoints != 0 )
			{
				delete[] m_Layers[i].objectList[j].polygonPoints;
				m_Layers[i].objectList[j].polygonPoints = 0;
			}
		}

		if( m_Layers[i].objectList != 0 )
		{
			delete[] m_Layers[i].objectList;
			m_Layers[i].objectList = 0;
		}

		if( m_Layers[i].data != 0 )
		{
			delete[] m_Layers[i].data;
			m_Layers[i].data = 0;
		}
	}

	if( m_Layers != 0 )
	{
		delete[] m_Layers;
		m_Layers = 0;
	}

	for( i=0; i < m_NumTilesets; ++i )
	{
		if( m_Tilesets[i].tileDataList != 0 )
		{
			for( j=0; j < m_Tilesets[i].numTileData; ++j )
			{
				if( m_Tilesets[i].tileDataList[j].propertyList != 0 )
				{
					delete[] m_Tilesets[i].tileDataList[j].propertyList;
					m_Tilesets[i].tileDataList[j].propertyList = 0;
				}

				if (m_Tilesets[i].tileDataList[j].animationFrameList != 0)
				{
					delete[] m_Tilesets[i].tileDataList[j].animationFrameList;
					m_Tilesets[i].tileDataList[j].animationFrameList = 0;
				}
			}

			delete[] m_Tilesets[i].tileDataList;
			m_Tilesets[i].tileDataList = 0;
		}

		if( m_Tilesets[i].propertyList != 0 )
		{
			delete[] m_Tilesets[i].propertyList;
			m_Tilesets[i].propertyList = 0;
		}

		if( m_Tilesets[i].tilesetTexture != renderer::INVALID_OBJECT )
		{
			renderer::RemoveTexture( m_Tilesets[i].tilesetTexture ); 
			m_Tilesets[i].tilesetTexture = renderer::INVALID_OBJECT;
		}
	}

	if( m_Tilesets != 0 )
	{
		delete[] m_Tilesets;
		m_Tilesets = 0;
	}

	for( i=0; i < static_cast<int>(m_AnimationFiles.size()); ++i )
	{
		if( m_AnimationFiles[i]->pTilesetHandler != 0 )
		{
			delete m_AnimationFiles[i]->pTilesetHandler;
			m_AnimationFiles[i]->pTilesetHandler = 0;
		}

		delete m_AnimationFiles[i];
	}

	m_AnimationFiles.clear();

	m_TileAnimations.clear();
}

/////////////////////////////////////////////////////
/// Method: LoadLevel
/// Params: [in]szFilename
///
/////////////////////////////////////////////////////
void TiledBaseLoader::LoadLevel( const char* szFilename )
{
	const int MAX_BUFFER_SIZE = 1024*500; // 500k
	char maxFileBuffer[MAX_BUFFER_SIZE];
		
	int err = 0;
	
#ifdef _DEBUG
	CHECK_LUASTATE()
#endif // _DEBUG
	
	int errorFuncIndex;
	errorFuncIndex = script::GetErrorFuncIndex();
	
	if( core::app::GetLoadFilesFromZip() )
	{
		int o_flags = O_RDONLY;	// rb
		int o_modes = 0664;
		
		ZZIP_FILE* file = zzip_fopen (szFilename, "rb");
		if( file == 0 )
		{
			char newFileName[core::MAX_PATH+core::MAX_PATH];
			snprintf( newFileName, core::MAX_PATH+core::MAX_PATH, "%s/%s", core::app::GetRootZipFile(), szFilename );
			
			file = zzip_open_shared_io( 0, newFileName, o_flags, o_modes, file::GetZipExtensions(), 0 );//zzip_fopen (newFileName, "rt");
			if( file == 0 )
			{
				DBG_ASSERT_MSG( 0, "LUASCRIPTING: *ERROR* could not load script '%s'", szFilename );
				return;
			}
		}
		
		zzip_seek( file, 0, SEEK_END );
		zzip_off_t fileSize = zzip_tell( file );
		zzip_rewind( file );
		
		zzip_fread( maxFileBuffer, fileSize, sizeof(char), file );
		
		zzip_fclose( file );
		
		err = luaL_loadbuffer( script::LuaScripting::GetState(), maxFileBuffer, fileSize, szFilename );
	}
	else
	{
		// load the script file
		err = luaL_loadfile( script::LuaScripting::GetState(), szFilename );
	}
	
	// 0 --- no errors;
	// LUA_ERRSYNTAX --- syntax error during pre-compilation.
	// LUA_ERRMEM --- memory allocation error.
	// LUA_ERRFILE --- cannot open/read the file
	
	if( err == LUA_ERRSYNTAX || err == LUA_ERRMEM || err == LUA_ERRFILE )
	{
		DBGLOG( "LUASCRIPTING: *ERROR* could not load script, luaL_loadfile failed\n" );
		DBGLOG( "\tLUA_TRACEBACK: %s\n", lua_tostring( script::LuaScripting::GetState(), -1 ) );
		
		script::StackDump(script::LuaScripting::GetState());
		
		DBG_ASSERT_MSG( 0, "LUASCRIPTING: *ERROR* could not load script '%s'", szFilename );
		
		return;
	}
	
	err = lua_pcall( script::LuaScripting::GetState(), 0, 1, errorFuncIndex );
	
	// LUA_ERRRUN --- a runtime error.
	// LUA_ERRMEM --- memory allocation error. For such errors, Lua does not call the error handler function.
	// LUA_ERRERR --- error while running the error handler function.
	
	if( err == LUA_ERRRUN || err == LUA_ERRMEM || err == LUA_ERRERR )
	{
		DBGLOG( "LUASCRIPTING: *ERROR* Calling script '%s' failed\n", szFilename );
		DBGLOG( "\tLUA_TRACEBACK: %s\n", lua_tostring( script::LuaScripting::GetState(), -1 ) );
		
		script::StackDump(script::LuaScripting::GetState());
		
		DBG_ASSERT_MSG( 0, "LUASCRIPTING: *ERROR* Calling script '%s'", szFilename );
		
		return;
	}
	
	// should be a table
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		int paramIndex = 1;
		
		file::CreateFileStructure( szFilename, &m_FilePath );
		
		const char* version = script::LuaGetStringFromTableItem( "version", paramIndex );
		const char* luaversion = script::LuaGetStringFromTableItem( "luaversion", paramIndex );
		const char* orientation = script::LuaGetStringFromTableItem( "orientation", paramIndex );

		if(strcmp(orientation, ORIENTATION_ORTHOGONAL) == 0 )
			m_MapOrientation = MapOrientation_Orthogonal;
		else
		if(strcmp(orientation, ORIENTATION_ISOMETRIC) == 0 )
			m_MapOrientation = MapOrientation_Isometric;
		else
		if(strcmp(orientation, ORIENTATION_ISOMETRIC_STAGGERED) == 0 )
			m_MapOrientation = MapOrientation_StaggeredIsometric;
	
		m_MapWidth = static_cast<int>( script::LuaGetNumberFromTableItem( "width", paramIndex ) );
		m_MapHeight = static_cast<int>( script::LuaGetNumberFromTableItem( "height", paramIndex ) );
		m_MapTileWidth = static_cast<int>( script::LuaGetNumberFromTableItem( "tilewidth", paramIndex ) );
		m_MapTileHeight = static_cast<int>( script::LuaGetNumberFromTableItem( "tileheight", paramIndex ) );

		m_MapTileSize.x = static_cast<float>(m_MapTileWidth);
		m_MapTileSize.y = static_cast<float>(m_MapTileHeight);

		m_MapHalfTileSize.x = m_MapTileSize.x*0.5f;
		m_MapHalfTileSize.y = m_MapTileSize.y*0.5f;

		m_ScaledMapTileSize.x = static_cast<float>(m_MapTileWidth)*m_TileScale.x;
		m_ScaledMapTileSize.y = static_cast<float>(m_MapTileHeight)*m_TileScale.y;
		
		m_ScaledMapHalfTileSize.x = m_ScaledMapTileSize.x*0.5f;
		m_ScaledMapHalfTileSize.y = m_ScaledMapTileSize.y*0.5f;

		ParseMapProperties();
		
		ParseTilesets();
		
		ParseLayers();
	}
	lua_pop( script::LuaScripting::GetState(), 1 );
	
	ProcessLevel();
}

/////////////////////////////////////////////////////
/// Method: DrawLayers
/// Params: [in]startLayer, [in]endLayer
///
/////////////////////////////////////////////////////
void TiledBaseLoader::DrawLayers( int startLayer, int endLayer )
{

}

/////////////////////////////////////////////////////
/// Method: Update
/// Params: [in]deltaTime
///
/////////////////////////////////////////////////////
void TiledBaseLoader::Update( float deltaTime )
{
	auto it = m_TileAnimations.begin();
	while( it != m_TileAnimations.end() )
	{

		(*it)->frameTime -= deltaTime;
		if( (*it)->frameTime <= 0.0f )
		{
			(*it)->currentFrame++;
			
			if( (*it)->currentFrame >= (*it)->numFrames )
			{
				if(!(*it)->playOnce )
					(*it)->currentFrame = 0;
				else
					(*it)->currentFrame = (*it)->numFrames-1;
			}
			(*it)->frameTime = (*it)->pTileFrames[(*it)->currentFrame].tileShowTime;
			(*it)->frameUV = (*it)->pTileFrames[(*it)->currentFrame].uvCoord;
		}

		// next
		++it;
	}
}

/////////////////////////////////////////////////////
/// Method: GetTileIndexFromPosition
/// Params: [in]pos, [in/out]tileX, [in/out]tileY
///
/////////////////////////////////////////////////////
void TiledBaseLoader::GetTileIndexFromPosition( const glm::vec3& pos, int* tileX, int* tileY )
{
	DBG_ASSERT( tileX != 0 );
	DBG_ASSERT( tileY != 0 );

	if( m_MapOrientation == MapOrientation_Orthogonal )
	{
		int totalSizeX = static_cast<int>(m_ScaledMapTileSize.x) * m_MapWidth;
		int totalSizeY = static_cast<int>(m_ScaledMapTileSize.y) * m_MapHeight;

		glm::vec2 currentPos( std::abs( pos.x ), std::abs( pos.y ) );

		// init 
		*tileX = 0;
		*tileY = 0;

		// check limits
		if( currentPos.x < 0.0f || currentPos.x > static_cast<float>(totalSizeX) )
		{
			// off the map so inform my setting to negative
			*tileX = -1;
		}

		if( currentPos.y < 0.0f || currentPos.y > static_cast<float>(totalSizeY) )
		{
			// off the map so inform my setting to negative
			*tileY = -1;
		}

		if( *tileX != -1 )
		{
			int xPos = static_cast<int>( currentPos.x );
			int divX = xPos / static_cast<int>(m_ScaledMapTileSize.x);
			if( divX == 0 )
				*tileX = 0;
			else
				*tileX = divX % m_MapWidth;
		}

		if( *tileY != -1 )
		{
			int yPos = static_cast<int>( currentPos.y );
			int divY = yPos / static_cast<int>(m_ScaledMapTileSize.y);
	
			if( divY == 0 )
				*tileY = 0;
			else
				*tileY = divY % m_MapHeight;
		}
	}
	else
	if( m_MapOrientation == MapOrientation_Isometric )
	{

	}
	else
	if( m_MapOrientation == MapOrientation_StaggeredIsometric )
	{

	}
}

/////////////////////////////////////////////////////
/// Method: GetTileDataFromPosition
/// Params: [in]whichLayer, [in]pos
///
/////////////////////////////////////////////////////
support::tiled::TileData* TiledBaseLoader::GetTileDataFromPosition( int whichLayer, const glm::vec3& pos )
{
	DBG_ASSERT( (whichLayer >= 0 && whichLayer < m_NumLayers) );

	int tileX = 0;
	int tileY = 0;

	int xPos = static_cast<int>( std::abs(pos.x) );
	int yPos = static_cast<int>( std::abs(pos.y) );

	if( m_MapOrientation == MapOrientation_Orthogonal )
	{
		int divX = xPos / static_cast<int>(m_ScaledMapTileSize.x);
		int divY = yPos / static_cast<int>(m_ScaledMapTileSize.y);
	
		if( divX == 0 )
			tileX = 0;
		else
			tileX = divX % m_MapWidth;

		if( divY == 0 )
			tileY = 0;
		else
			tileY = divY % m_MapHeight;

		return &m_Layers[whichLayer].data[tileX + (tileY*m_MapWidth)];
	}
	else
	if( m_MapOrientation == MapOrientation_Isometric )
	{

	}
	else
	if( m_MapOrientation == MapOrientation_StaggeredIsometric )
	{

	}

	return 0;
}

/////////////////////////////////////////////////////
/// Method: GetTileIdInLayer
/// Params: [in]whichLayer, [in]whichId, [in/out]numFound, [in/out]dataOut
///
/////////////////////////////////////////////////////
support::tiled::TileData** TiledBaseLoader::GetTileIdInLayer(int whichLayer, int realId, int* numFound)
{
	DBG_ASSERT( (whichLayer >= 0 && whichLayer < m_NumLayers) );
	DBG_ASSERT( numFound != 0 );

	int y=0, x=0;
	int index=0;

	TileData** dataOut = 0;
	*numFound = 0;

	// get counts first
	for( y=0; y < m_Layers[whichLayer].height; ++y )
	{
		for( x=0; x < m_Layers[whichLayer].width; ++x )
		{
			if( m_Layers[whichLayer].data[x + (y*m_Layers[whichLayer].width)].realId == realId )
			{
				*numFound += 1;
			}
		}
	}
	
	if( *numFound > 0 )
	{
		// allocate
		dataOut = new TileData*[*numFound];

		for( y=0; y < m_Layers[whichLayer].height; ++y )
		{
			for( x=0; x < m_Layers[whichLayer].width; ++x )
			{
				if( m_Layers[whichLayer].data[x + (y*m_Layers[whichLayer].width)].realId == realId )
				{
					dataOut[index] = &m_Layers[whichLayer].data[x + (y*m_Layers[whichLayer].width)];
					index++;
				}
			}
		}
	}

	return dataOut;
}

/////////////////////////////////////////////////////
/// Method: GetTilesetForId
/// Params: [in]tileId
///
/////////////////////////////////////////////////////
support::tiled::Tileset* TiledBaseLoader::GetTilesetForId(int tileId)
{
	int i=0;

	for( i=0; i < m_NumTilesets; ++i )
	{
		if( (tileId >= m_Tilesets[i].firstgid) &&
			(tileId <= ((m_Tilesets[i].firstgid+m_Tilesets[i].totalTiles)-1) ) )
			return &m_Tilesets[i];
	}

	// tileset not found or it's an empty tile
	return 0;
}

/////////////////////////////////////////////////////
/// Method: ParseMapProperties
/// Params: None
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseMapProperties()
{
	int i=0;
	int n = 0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		lua_pushnil(script::LuaScripting::GetState());
		while(lua_next(script::LuaScripting::GetState(), -2)) 
		{    
			if(lua_isstring(script::LuaScripting::GetState(), -1)) 
			{
				//const char* name = lua_tostring(script::LuaScripting::GetState(), -2);
				//const char* value = lua_tostring(script::LuaScripting::GetState(), -1);

				//DBGLOG( "%s %s\n", name, value );
				n++;
			}
			lua_pop(script::LuaScripting::GetState(), 1);
		}
	}
	// pop property table
	lua_pop( script::LuaScripting::GetState(), 1 );

	// now allocate
	m_NumMapProperties = n;

	if( n != 0 )
	{
		m_MapProperties = new Property[m_NumMapProperties];
		DBG_ASSERT( m_MapProperties != 0 );

		// put the table back on top
		lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
		lua_gettable( script::LuaScripting::GetState(), -2 ); // key

		// new table is top
		if( lua_istable( script::LuaScripting::GetState(), -1 ) )
		{
			i = 0;
			lua_pushnil(script::LuaScripting::GetState());
			while(lua_next(script::LuaScripting::GetState(), -2)) 
			{ 
				if(lua_isstring(script::LuaScripting::GetState(), -1)) 
				{
					m_MapProperties[i].name = lua_tostring(script::LuaScripting::GetState(), -2);
					m_MapProperties[i].value = lua_tostring(script::LuaScripting::GetState(), -1);
					i++;
				}
				lua_pop(script::LuaScripting::GetState(), 1);
			}
		}
		// pop property table
		lua_pop( script::LuaScripting::GetState(), 1 );
	}
}

/////////////////////////////////////////////////////
/// Method: ParseTilesets
/// Params: None
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseTilesets()
{
	int i=0;
	renderer::Texture texLoader;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), TILESETS_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		// get number of tables (tilsets)
		int n = static_cast<int>( luaL_len( script::LuaScripting::GetState(), -1 ) );

		m_NumTilesets = n;
		if( n != 0 )
		{
			m_Tilesets = new Tileset[m_NumTilesets];
			DBG_ASSERT( m_Tilesets != 0 );

			// go through all the tables in this table
			for( i = 1; i <= n; ++i )
			{
				lua_rawgeti( script::LuaScripting::GetState(), -1, i );
				if( lua_istable( script::LuaScripting::GetState(), -1 ) )
				{
					int idx = i-1;
					int paramIndex = 3;
					
					m_Tilesets[idx].name = script::LuaGetStringFromTableItem( "name", paramIndex );
					m_Tilesets[idx].firstgid = static_cast<int>( script::LuaGetNumberFromTableItem( "firstgid", paramIndex ) );
					m_Tilesets[idx].tileWidth = static_cast<int>( script::LuaGetNumberFromTableItem( "tilewidth", paramIndex ) );
					m_Tilesets[idx].tileHeight = static_cast<int>( script::LuaGetNumberFromTableItem( "tileheight", paramIndex ) );
					m_Tilesets[idx].spacing = static_cast<int>( script::LuaGetNumberFromTableItem( "spacing", paramIndex ) );
					m_Tilesets[idx].margin = static_cast<int>( script::LuaGetNumberFromTableItem( "margin", paramIndex ) );
					m_Tilesets[idx].image = script::LuaGetStringFromTableItem( "image", paramIndex );

					m_Tilesets[idx].imageWidth = static_cast<int>( script::LuaGetNumberFromTableItem( "imagewidth", paramIndex ) );
					m_Tilesets[idx].imageHeight = static_cast<int>( script::LuaGetNumberFromTableItem( "imageheight", paramIndex ) );

					m_Tilesets[idx].transparentColor = script::LuaGetStringFromTableItem( "transparentcolor", paramIndex );

					m_Tilesets[idx].tileCount = static_cast<int>(script::LuaGetNumberFromTableItem("tilecount", paramIndex));

					m_Tilesets[idx].scaledTileSize.x = m_Tilesets[idx].tileWidth*m_TileScale.x;
					m_Tilesets[idx].scaledTileSize.y = m_Tilesets[idx].tileHeight*m_TileScale.y;

					m_Tilesets[idx].scaledHalfTileSize.x = m_Tilesets[idx].scaledTileSize.x*0.5f;
					m_Tilesets[idx].scaledHalfTileSize.y = m_Tilesets[idx].scaledTileSize.y*0.5f;

					m_Tilesets[idx].numProperties = 0;
					m_Tilesets[idx].propertyList = 0;
					m_Tilesets[idx].numTileData = 0;
					m_Tilesets[idx].tileDataList = 0;

					ParseTilesetProperties(idx);
					ParseTilesetTilePropertiesAndAnimation(idx);

					int spacedTileWidth = m_Tilesets[idx].tileWidth + m_Tilesets[idx].margin /*+ m_Tilesets[idx].spacing*/;
					int spacedTileHeight = m_Tilesets[idx].tileHeight /*+ m_Tilesets[idx].margin*/ + m_Tilesets[idx].spacing;

					m_Tilesets[idx].totalTilesX = (m_Tilesets[idx].imageWidth / spacedTileWidth);
					m_Tilesets[idx].totalTilesY = (m_Tilesets[idx].imageHeight / spacedTileHeight);

					m_Tilesets[idx].totalTiles = m_Tilesets[idx].totalTilesX * m_Tilesets[idx].totalTilesY;

					// load the texture
					m_Tilesets[idx].tilesetTexture = renderer::INVALID_OBJECT;
					if( m_Tilesets[idx].image != 0 )
					{
						char tempPath[core::MAX_PATH+core::MAX_PATH];
						snprintf( tempPath, core::MAX_PATH+core::MAX_PATH, "%s%s", m_FilePath.szPath, m_Tilesets[idx].image );

						m_Tilesets[idx].tilesetTexture = renderer::TextureLoad( tempPath, texLoader, renderer::TEXTURE_UNKNOWN, 0, false, GL_NEAREST, GL_NEAREST );

						DBG_ASSERT( texLoader.nOriginalWidth == m_Tilesets[idx].imageWidth );
						DBG_ASSERT( texLoader.nOriginalHeight == m_Tilesets[idx].imageHeight );
						
						// just in case the image was resized by the loader
						m_Tilesets[idx].imageWidth = texLoader.nWidth;
						m_Tilesets[idx].imageHeight = texLoader.nHeight;
					}
					
					m_Tilesets[idx].uvTileWidth = static_cast<float>( m_Tilesets[idx].tileWidth ) / static_cast<float>( m_Tilesets[idx].imageWidth );
					m_Tilesets[idx].uvTileHeight = static_cast<float>( m_Tilesets[idx].tileHeight ) / static_cast<float>( m_Tilesets[idx].imageHeight );
				}
				lua_pop( script::LuaScripting::GetState(), 1 );
			}
		}
	}

	// pop tileset table
	lua_pop( script::LuaScripting::GetState(), 1 );
}

/////////////////////////////////////////////////////
/// Method: ParseTilesetProperties
/// Params: None
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseTilesetProperties( int tileSetIndex )
{
	int i=0;
	int n = 0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		lua_pushnil(script::LuaScripting::GetState());
		while(lua_next(script::LuaScripting::GetState(), -2)) 
		{    
			if(lua_isstring(script::LuaScripting::GetState(), -1)) 
			{
				//const char* name = lua_tostring(script::LuaScripting::GetState(), -2);
				//const char* value = lua_tostring(script::LuaScripting::GetState(), -1);
				n++;
			}
			lua_pop(script::LuaScripting::GetState(), 1);
		}
	}
	// pop property table
	lua_pop( script::LuaScripting::GetState(), 1 );

	m_Tilesets[tileSetIndex].propertyList = 0;
	if( n != 0 )
	{
		// now allocate
		m_Tilesets[tileSetIndex].numProperties = n;
		m_Tilesets[tileSetIndex].propertyList = new Property[m_Tilesets[tileSetIndex].numProperties];
		DBG_ASSERT( m_Tilesets[tileSetIndex].propertyList != 0 );

		// put the table back on top
		lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
		lua_gettable( script::LuaScripting::GetState(), -2 ); // key

		// new table is top
		if( lua_istable( script::LuaScripting::GetState(), -1 ) )
		{
			i = 0;
			lua_pushnil(script::LuaScripting::GetState());
			while(lua_next(script::LuaScripting::GetState(), -2)) 
			{ 
				if(lua_isstring(script::LuaScripting::GetState(), -1)) 
				{
					m_Tilesets[tileSetIndex].propertyList[i].name = lua_tostring(script::LuaScripting::GetState(), -2);
					m_Tilesets[tileSetIndex].propertyList[i].value = lua_tostring(script::LuaScripting::GetState(), -1);
					i++;
				}
				lua_pop(script::LuaScripting::GetState(), 1);
			}
		}
		// pop property table
		lua_pop( script::LuaScripting::GetState(), 1 );
	}
}
		
/////////////////////////////////////////////////////
/// Method: ParseTilesetTilePropertiesAndAnimation
/// Params: None
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseTilesetTilePropertiesAndAnimation( int tileSetIndex )
{
	int i=0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), TILES_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		// get number of tables (data)
		int n = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );

		m_Tilesets[tileSetIndex].tileDataList = 0;

		if( n != 0 )
		{
			m_Tilesets[tileSetIndex].numTileData = n;
			m_Tilesets[tileSetIndex].tileDataList = new Tile[m_Tilesets[tileSetIndex].numTileData];
			DBG_ASSERT( m_Tilesets[tileSetIndex].tileDataList != 0 );

			int paramIndex = 5;

			// go through all the tables in this table
			for( i = 1; i <= m_Tilesets[tileSetIndex].numTileData; ++i )
			{
				int idx = i-1;

				lua_rawgeti( script::LuaScripting::GetState(), -1, i );
				if( lua_istable( script::LuaScripting::GetState(), -1 ) )
				{
					m_Tilesets[tileSetIndex].tileDataList[idx].tileId = static_cast<int>( script::LuaGetNumberFromTableItem( "id", paramIndex ) );

					m_Tilesets[tileSetIndex].tileDataList[idx].numAnimationFrames = 0;
					m_Tilesets[tileSetIndex].tileDataList[idx].animationFrameList = 0;

					// grab animations

					n = 0;
					lua_pushstring(script::LuaScripting::GetState(), ANIMATION_TABLE);
					lua_gettable(script::LuaScripting::GetState(), -2); // key

					// new table is top
					if (lua_istable(script::LuaScripting::GetState(), -1))
					{
						// get number of frames
						n = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );

						m_Tilesets[tileSetIndex].tileDataList[idx].numAnimationFrames = n;
						if (n != 0)
						{
							// now allocate	
							m_Tilesets[tileSetIndex].tileDataList[idx].animationFrameList = new TileAnimationFrame[m_Tilesets[tileSetIndex].tileDataList[idx].numAnimationFrames];
							DBG_ASSERT(m_Tilesets[tileSetIndex].tileDataList[idx].animationFrameList != 0);

							// go through all the layer tables
							int j = 0;
							for (j = 1; j <= n; ++j)
							{
								lua_rawgeti(script::LuaScripting::GetState(), -1, j);
								if (lua_istable(script::LuaScripting::GetState(), -1))
								{
									int frameIdx = j - 1;
									int newParamIndex = 7;

									const char* tileid = script::LuaGetStringFromTableItem("tileid", newParamIndex);
									const char* duration = script::LuaGetStringFromTableItem("duration", newParamIndex);

									m_Tilesets[tileSetIndex].tileDataList[idx].animationFrameList[frameIdx].tileid = static_cast<int>(std::atoi(tileid));
									m_Tilesets[tileSetIndex].tileDataList[idx].animationFrameList[frameIdx].duration = static_cast<float>(std::atof(duration));
								}
								lua_pop(script::LuaScripting::GetState(), 1);
							}
						}
					}
					// pop animation table
					lua_pop(script::LuaScripting::GetState(), 1);
					
					// grab the properties

					// grab table
					n = 0;
					lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
					lua_gettable( script::LuaScripting::GetState(), -2 ); // key

					// new table is top
					if( lua_istable( script::LuaScripting::GetState(), -1 ) )
					{
						lua_pushnil(script::LuaScripting::GetState());
						while(lua_next(script::LuaScripting::GetState(), -2)) 
						{    
							if(lua_isstring(script::LuaScripting::GetState(), -1)) 
							{
								//const char* name = lua_tostring(script::LuaScripting::GetState(), -2);
								//const char* value = lua_tostring(script::LuaScripting::GetState(), -1);
								n++;
							}
							lua_pop(script::LuaScripting::GetState(), 1);
						}
					}
					// pop property table
					lua_pop( script::LuaScripting::GetState(), 1 );

					m_Tilesets[tileSetIndex].tileDataList[idx].propertyList = 0;
					if( n != 0 )
					{
						// now allocate
						m_Tilesets[tileSetIndex].tileDataList[idx].numProperties = n;
						m_Tilesets[tileSetIndex].tileDataList[idx].propertyList = new Property[m_Tilesets[tileSetIndex].tileDataList[idx].numProperties];

						// put the table back on top
						lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
						lua_gettable( script::LuaScripting::GetState(), -2 ); // key

						// new table is top
						if( lua_istable( script::LuaScripting::GetState(), -1 ) )
						{
							int j = 0;
							lua_pushnil(script::LuaScripting::GetState());
							while(lua_next(script::LuaScripting::GetState(), -2)) 
							{ 
								if(lua_isstring(script::LuaScripting::GetState(), -1)) 
								{
									m_Tilesets[tileSetIndex].tileDataList[idx].propertyList[j].name = lua_tostring(script::LuaScripting::GetState(), -2);
									m_Tilesets[tileSetIndex].tileDataList[idx].propertyList[j].value = lua_tostring(script::LuaScripting::GetState(), -1);
									j++;
								}
								lua_pop(script::LuaScripting::GetState(), 1);
							}
						}
						// pop property table
						lua_pop( script::LuaScripting::GetState(), 1 );
					}
				}
				lua_pop( script::LuaScripting::GetState(), 1 );
			}
		}
	}
	// pop tiles table
	lua_pop( script::LuaScripting::GetState(), 1 );
}

/////////////////////////////////////////////////////
/// Method: ParseLayers
/// Params: None
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseLayers()
{
	int i=0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), LAYERS_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		// get number of tables (tilsets)
		int n = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );
		m_NumLayers = n;

		m_Layers = new Layer[m_NumLayers];
		DBG_ASSERT( m_Layers != 0 );

		// go through all the layer tables
		for( i = 1; i <= n; ++i )
		{
			lua_rawgeti( script::LuaScripting::GetState(), -1, i );
			if( lua_istable( script::LuaScripting::GetState(), -1 ) )
			{
				int idx = i-1;
				int paramIndex = 3;

				m_Layers[idx].type = script::LuaGetStringFromTableItem( "type", paramIndex );
				m_Layers[idx].name = script::LuaGetStringFromTableItem( "name", paramIndex );
				m_Layers[idx].x = static_cast<int>( script::LuaGetNumberFromTableItem( "x", paramIndex ) );
				m_Layers[idx].y = static_cast<int>( script::LuaGetNumberFromTableItem( "y", paramIndex ) );
				m_Layers[idx].width = static_cast<int>( script::LuaGetNumberFromTableItem( "width", paramIndex ) );
				m_Layers[idx].height = static_cast<int>( script::LuaGetNumberFromTableItem( "height", paramIndex ) );
				m_Layers[idx].visible = ( script::LuaGetBoolFromTableItem( "visible", paramIndex ) != 0 );

				m_Layers[idx].transparentColor = script::LuaGetStringFromTableItem( "transparentcolor", paramIndex );
				m_Layers[idx].image = script::LuaGetStringFromTableItem( "image", paramIndex );

				m_Layers[idx].numProperties = 0;
				m_Layers[idx].propertyList = 0;

				ParseLayerProperties( idx );

				m_Layers[idx].opacity = static_cast<int>( script::LuaGetNumberFromTableItem( "opacity", paramIndex ) );
				m_Layers[idx].encoding = script::LuaGetStringFromTableItem( "encoding", paramIndex );

				m_Layers[idx].data = 0;
				
				m_Layers[idx].numObjects = 0;
				m_Layers[idx].objectList = 0;

				if( strcmp( m_Layers[idx].type, LAYER_TYPE_TILE ) == 0 )
					ParseLayerData( idx );

				if( strcmp( m_Layers[idx].type, LAYER_TYPE_OBJECT ) == 0 )
					ParseLayerObjectData( idx );

			}
			lua_pop( script::LuaScripting::GetState(), 1 );
		}
	}

	// pop layer table
	lua_pop( script::LuaScripting::GetState(), 1 );
}

/////////////////////////////////////////////////////
/// Method: ParseLayerProperties
/// Params: [in]layerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseLayerProperties( int layerIndex )
{
	int i=0;
	int n = 0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		lua_pushnil(script::LuaScripting::GetState());
		while(lua_next(script::LuaScripting::GetState(), -2)) 
		{    
			if(lua_isstring(script::LuaScripting::GetState(), -1)) 
			{
				//const char* name = lua_tostring(script::LuaScripting::GetState(), -2);
				//const char* value = lua_tostring(script::LuaScripting::GetState(), -1);
				n++;
			}
			lua_pop(script::LuaScripting::GetState(), 1);
		}
	}
	// pop property table
	lua_pop( script::LuaScripting::GetState(), 1 );

	if( n != 0 )
	{
		// now allocate
		m_Layers[layerIndex].numProperties = n;
		m_Layers[layerIndex].propertyList = new Property[m_Layers[layerIndex].numProperties];
		DBG_ASSERT( m_Layers[layerIndex].propertyList != 0 );

		// put the table back on top
		lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
		lua_gettable( script::LuaScripting::GetState(), -2 ); // key

		// new table is top
		if( lua_istable( script::LuaScripting::GetState(), -1 ) )
		{
			i = 0;
			lua_pushnil(script::LuaScripting::GetState());
			while(lua_next(script::LuaScripting::GetState(), -2)) 
			{ 
				if(lua_isstring(script::LuaScripting::GetState(), -1)) 
				{
					m_Layers[layerIndex].propertyList[i].name = lua_tostring(script::LuaScripting::GetState(), -2);
					m_Layers[layerIndex].propertyList[i].value = lua_tostring(script::LuaScripting::GetState(), -1);
					i++;
				}
				lua_pop(script::LuaScripting::GetState(), 1);
			}
		}
		// pop property table
		lua_pop( script::LuaScripting::GetState(), 1 );
	}
}

/////////////////////////////////////////////////////
/// Method: ParseLayerData
/// Params: [in]layerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseLayerData( int layerIndex )
{
	int i=0;

	glm::vec3 tmpPos;
	
	// grab table
	lua_pushstring( script::LuaScripting::GetState(), DATA_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		// get number of tile numbers
		int n = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );
		m_Layers[layerIndex].data = new TileData[n];
		DBG_ASSERT( m_Layers[layerIndex].data != 0 );

		int isometricDivY = -1;
		glm::ivec2 isometricOffset;

		// go through all the values in this table
		for( i = 1; i <= n; ++i )
		{
			int idx = i-1;

			lua_rawgeti( script::LuaScripting::GetState(), -1, i );
			m_Layers[layerIndex].data[idx].tileId = static_cast<int>( lua_tonumber( script::LuaScripting::GetState(), -1 ) );
			lua_pop( script::LuaScripting::GetState(), 1 );

			m_Layers[layerIndex].data[idx].tileCenter.x = 0;
			m_Layers[layerIndex].data[idx].tileCenter.y = 0;
			m_Layers[layerIndex].data[idx].tileX = 0;
			m_Layers[layerIndex].data[idx].tileY = 0;

			// work out the x/y division in the array
			int divX = idx % m_Layers[layerIndex].width;
			int divY = idx / m_Layers[layerIndex].width;

			if( m_Layers[layerIndex].data[idx].tileId != 0 )
			{
				Tileset* pTileset = GetTilesetForId( m_Layers[layerIndex].data[idx].tileId );
				DBG_ASSERT( pTileset != 0 );

				if( pTileset != 0 )
				{
					// because tilesets have an offset let's remove it to get a lookup id in a single set
					m_Layers[layerIndex].data[idx].realId = (m_Layers[layerIndex].data[idx].tileId - pTileset->firstgid);

					if( m_MapOrientation == MapOrientation_Orthogonal )
					{
						// for a normal orthogonal map, work out the tile centers
						m_Layers[layerIndex].data[idx].tileCenter.x = static_cast<int>((pTileset->scaledTileSize.x * static_cast<float>(divX)) + pTileset->scaledHalfTileSize.x);
						m_Layers[layerIndex].data[idx].tileCenter.y = static_cast<int>((pTileset->scaledTileSize.y * static_cast<float>(divY)) + pTileset->scaledHalfTileSize.y);

						// for the tile index
						tmpPos = glm::vec3(0.0f,0.0f,0.0f);
						tmpPos.x = (pTileset->scaledTileSize.x * static_cast<float>(divX)) + pTileset->scaledHalfTileSize.x;
						tmpPos.y = (pTileset->scaledTileSize.y * static_cast<float>(divY)) + pTileset->scaledHalfTileSize.y;

						GetTileIndexFromPosition( tmpPos, &m_Layers[layerIndex].data[idx].tileX, &m_Layers[layerIndex].data[idx].tileY );
					}
					else
					if( m_MapOrientation == MapOrientation_Isometric )
					{
						// starting new diagonal row?
						if( isometricDivY != divY )
						{
							// change the starting X/Y
							isometricOffset.x = -static_cast<int>(m_MapHalfTileSize.x * static_cast<float>(divY));
							isometricOffset.y = static_cast<int>((m_MapHalfTileSize.y * static_cast<float>(divY)) + m_MapHalfTileSize.y); 

							isometricDivY = divY;
						}

						// isometric draws diagonally
						m_Layers[layerIndex].data[idx].tileCenter.x = static_cast<int>(static_cast<float>(isometricOffset.x)*m_TileScale.x);
						m_Layers[layerIndex].data[idx].tileCenter.y = static_cast<int>(static_cast<float>(isometricOffset.y)*m_TileScale.y);

						m_Layers[layerIndex].data[idx].tileX = divX;
						m_Layers[layerIndex].data[idx].tileY = divY;

						isometricOffset.x += static_cast<int>(m_MapHalfTileSize.x);
						isometricOffset.y += static_cast<int>(m_MapHalfTileSize.y);
					}
					else
					if( m_MapOrientation == MapOrientation_StaggeredIsometric )
					{
						// starting new diagonal row?
						if( isometricDivY != divY )
						{
							// change the starting X/Y
							if( divY % 2 )
								isometricOffset.x = static_cast<int>(m_MapHalfTileSize.x);
							else
								isometricOffset.x = 0;

							isometricOffset.y = static_cast<int>((m_MapHalfTileSize.y * static_cast<float>(divY)) + m_MapHalfTileSize.y); 

							isometricDivY = divY;
						}

						// isometric draws diagonally
						m_Layers[layerIndex].data[idx].tileCenter.x = static_cast<int>(static_cast<float>(isometricOffset.x)*m_TileScale.x);
						m_Layers[layerIndex].data[idx].tileCenter.y = static_cast<int>(static_cast<float>(isometricOffset.y)*m_TileScale.y);

						m_Layers[layerIndex].data[idx].tileX = divX;
						m_Layers[layerIndex].data[idx].tileY = divY;

						isometricOffset.x += m_MapTileWidth;
					}

					//DBGLOG( "%d x = %d  y = %d \n", idx, m_Layers[layerIndex].data[idx].tileCenter.X, m_Layers[layerIndex].data[idx].tileCenter.Y );
				}
			}
			else
			{
				m_Layers[layerIndex].data[idx].realId = -1;

				if( m_MapOrientation == MapOrientation_Orthogonal )
				{
					// for a normal orthogonal map, work out the tile centers
					m_Layers[layerIndex].data[idx].tileCenter.x = static_cast<int>((m_ScaledMapTileSize.x * static_cast<float>(divX)) + m_ScaledMapHalfTileSize.x);
					m_Layers[layerIndex].data[idx].tileCenter.y = static_cast<int>((m_ScaledMapTileSize.y * static_cast<float>(divY)) + m_ScaledMapHalfTileSize.y);

					// for the tile index
					tmpPos = glm::vec3(0.0f,0.0f,0.0f);
					tmpPos.x = (m_ScaledMapTileSize.x * static_cast<float>(divX)) + m_ScaledMapHalfTileSize.x;
					tmpPos.y = (m_ScaledMapTileSize.y * static_cast<float>(divY)) + m_ScaledMapHalfTileSize.y;

					GetTileIndexFromPosition( tmpPos, &m_Layers[layerIndex].data[idx].tileX, &m_Layers[layerIndex].data[idx].tileY );
				}
				else
				if( m_MapOrientation == MapOrientation_Isometric  )
				{
					// starting new diagonal row?
					if( isometricDivY != divY )
					{
						// change the starting X/Y
						isometricOffset.x = -static_cast<int>(m_MapHalfTileSize.x * static_cast<float>(divY));
						isometricOffset.y = static_cast<int>((m_MapHalfTileSize.y * static_cast<float>(divY)) + m_MapHalfTileSize.y); 

						isometricDivY = divY;
					}

					// isometric draws diagonally
					m_Layers[layerIndex].data[idx].tileCenter.x = static_cast<int>(static_cast<float>(isometricOffset.x)*m_TileScale.x);
					m_Layers[layerIndex].data[idx].tileCenter.y = static_cast<int>(static_cast<float>(isometricOffset.y)*m_TileScale.y);


					m_Layers[layerIndex].data[idx].tileX = divX;
					m_Layers[layerIndex].data[idx].tileY = divY;

					isometricOffset.x += static_cast<int>(m_MapHalfTileSize.x);
					isometricOffset.y += static_cast<int>(m_MapHalfTileSize.y);
				}
				else
				if( m_MapOrientation == MapOrientation_StaggeredIsometric )
				{

				}
			}

			// initialise other data parts
			m_Layers[layerIndex].data[idx].anim = 0;
			m_Layers[layerIndex].data[idx].enabled = true;
		}

	}
	// pop property table
	lua_pop( script::LuaScripting::GetState(), 1 );

}

/////////////////////////////////////////////////////
/// Method: ParseLayerObjectData
/// Params: [in]layerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseLayerObjectData( int layerIndex )
{
	int i=0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), OBJECTS_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		// get number of tile numbers
		int n = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );

		m_Layers[layerIndex].numObjects = n;
		m_Layers[layerIndex].objectList = new ObjectData[n];
		DBG_ASSERT( m_Layers[layerIndex].objectList != 0 );

		// go through all the values in this table
		for( i = 1; i <= n; ++i )
		{
			int idx = i-1;
			int paramIndex = 5;

			lua_rawgeti( script::LuaScripting::GetState(), -1, i );

			if( lua_istable(script::LuaScripting::GetState(), -1 ) )
			{
				m_Layers[layerIndex].objectList[idx].objectid = static_cast<int>(script::LuaGetNumberFromTableItem("id", paramIndex));
				m_Layers[layerIndex].objectList[idx].name = script::LuaGetStringFromTableItem( "name", paramIndex );
				m_Layers[layerIndex].objectList[idx].type = script::LuaGetStringFromTableItem( "type", paramIndex );
				m_Layers[layerIndex].objectList[idx].shape = script::LuaGetStringFromTableItem( "shape", paramIndex );
				m_Layers[layerIndex].objectList[idx].x = static_cast<int>( script::LuaGetNumberFromTableItem( "x", paramIndex ) );
				m_Layers[layerIndex].objectList[idx].y = static_cast<int>( script::LuaGetNumberFromTableItem( "y", paramIndex ) );
				m_Layers[layerIndex].objectList[idx].width = static_cast<int>( script::LuaGetNumberFromTableItem( "width", paramIndex ) );
				m_Layers[layerIndex].objectList[idx].height = static_cast<int>( script::LuaGetNumberFromTableItem( "height", paramIndex ) );
				m_Layers[layerIndex].objectList[idx].rotation = static_cast<float>(script::LuaGetNumberFromTableItem("rotation", paramIndex));
				m_Layers[layerIndex].objectList[idx].gid = static_cast<int>( script::LuaGetNumberFromTableItem( "gid", paramIndex ) );
				m_Layers[layerIndex].objectList[idx].visible = ( script::LuaGetBoolFromTableItem( "visible", paramIndex ) != 0 );

				ParseLayerObjectDataProperties( layerIndex, idx );

				if( strcmp( m_Layers[layerIndex].objectList[idx].shape, OBJECTLAYER_TYPE_POLYGON ) == 0 )
				{
					ParseLayerObjectDataPolygon( layerIndex, idx, false );
				}
				else
				if( strcmp( m_Layers[layerIndex].objectList[idx].shape, OBJECTLAYER_TYPE_POLYLINE ) == 0 )
				{
					ParseLayerObjectDataPolygon( layerIndex, idx, true );
				}
			}

			// pop table
			lua_pop( script::LuaScripting::GetState(), 1 );
		}
	}
	// pop property table
	lua_pop( script::LuaScripting::GetState(), 1 );
}

/////////////////////////////////////////////////////
/// Method: ParseLayerObjectDataProperties
/// Params: [in]layerIndex, [in]objectLayerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseLayerObjectDataProperties( int layerIndex, int objectLayerIndex )
{
	int i=0;
	int n = 0;

	// grab table
	lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		lua_pushnil(script::LuaScripting::GetState());
		while(lua_next(script::LuaScripting::GetState(), -2)) 
		{    
			if(lua_isstring(script::LuaScripting::GetState(), -1)) 
			{
				//const char* name = lua_tostring(script::LuaScripting::GetState(), -2);
				//const char* value = lua_tostring(script::LuaScripting::GetState(), -1);
				n++;
			}
			lua_pop(script::LuaScripting::GetState(), 1);
		}
	}
	// pop property table
	lua_pop( script::LuaScripting::GetState(), 1 );

	m_Layers[layerIndex].objectList[objectLayerIndex].numProperties = 0;
	m_Layers[layerIndex].objectList[objectLayerIndex].propertyList = 0;
	
	m_Layers[layerIndex].objectList[objectLayerIndex].numPoints = 0;
	m_Layers[layerIndex].objectList[objectLayerIndex].polygonPoints = 0;

	if( n != 0 )
	{
		// now allocate
		m_Layers[layerIndex].objectList[objectLayerIndex].numProperties = n;
		m_Layers[layerIndex].objectList[objectLayerIndex].propertyList = new Property[n];
		DBG_ASSERT( m_Layers[layerIndex].objectList[objectLayerIndex].propertyList != 0 );

		// put the table back on top
		lua_pushstring( script::LuaScripting::GetState(), PROPERTIES_TABLE );
		lua_gettable( script::LuaScripting::GetState(), -2 ); // key

		// new table is top
		if( lua_istable( script::LuaScripting::GetState(), -1 ) )
		{
			i = 0;
			lua_pushnil(script::LuaScripting::GetState());
			while(lua_next(script::LuaScripting::GetState(), -2)) 
			{ 
				if(lua_isstring(script::LuaScripting::GetState(), -1)) 
				{
					m_Layers[layerIndex].objectList[objectLayerIndex].propertyList[i].name = lua_tostring(script::LuaScripting::GetState(), -2);
					m_Layers[layerIndex].objectList[objectLayerIndex].propertyList[i].value = lua_tostring(script::LuaScripting::GetState(), -1);
					i++;
				}
				lua_pop(script::LuaScripting::GetState(), 1);
			}
		}
		// pop property table
		lua_pop( script::LuaScripting::GetState(), 1 );
	}
}

/////////////////////////////////////////////////////
/// Method: ParseLayerObjectDataPolygon
/// Params: [in]layerIndex, [in]objectLayerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ParseLayerObjectDataPolygon( int layerIndex, int objectLayerIndex, bool polyLine )
{
	int i=0;

	// grab table
	if( polyLine )
		lua_pushstring( script::LuaScripting::GetState(), POLYLINE_TABLE );
	else
		lua_pushstring( script::LuaScripting::GetState(), POLYGON_TABLE );

	lua_gettable( script::LuaScripting::GetState(), -2 ); // key

	// new table is top
	if( lua_istable( script::LuaScripting::GetState(), -1 ) )
	{
		// get number of tile numbers
		int n = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );

		m_Layers[layerIndex].objectList[objectLayerIndex].numPoints = n;
		m_Layers[layerIndex].objectList[objectLayerIndex].polygonPoints = new glm::ivec2[n];
		DBG_ASSERT( m_Layers[layerIndex].objectList[objectLayerIndex].polygonPoints != 0 );

		// go through all the values in this table
		for( i = 1; i <= n; ++i )
		{
			int idx = i-1;
			int paramIndex = 7;

			lua_rawgeti( script::LuaScripting::GetState(), -1, i );
			if( lua_istable(script::LuaScripting::GetState(), -1 ) )
			{
				m_Layers[layerIndex].objectList[objectLayerIndex].polygonPoints[idx].x = static_cast<int>( script::LuaGetNumberFromTableItem( "x", paramIndex ) );
				m_Layers[layerIndex].objectList[objectLayerIndex].polygonPoints[idx].y = static_cast<int>( script::LuaGetNumberFromTableItem( "y", paramIndex ) );
			}
			// pop table
			lua_pop( script::LuaScripting::GetState(), 1 );
		}
	}
	// pop table
	lua_pop( script::LuaScripting::GetState(), 1 );
}

/////////////////////////////////////////////////////
/// Method: ProcessLevel
/// Params: None
///
/////////////////////////////////////////////////////
void TiledBaseLoader::ProcessLevel()
{
	int i=0, j=0, k=0, x=0, y=0;

	int numValues = 0;
	PropertyValue* propertyValues = 0;

	// custom per app, find certain properties and process 
	for( i=0; i < m_NumMapProperties; ++i )
	{
		// collision
		if( strcmp( m_MapProperties[i].name, COLLISION_LAYERS_PROP ) == 0 )
		{
			numValues = 0;
			propertyValues = 0;

			propertyValues = ParseValuesFromString( m_MapProperties[i].value, &numValues );
			if( propertyValues != 0 )
			{
				// should contain the layer numbers for collision
				CreateCollisionData( numValues, propertyValues );

				// done and clear
				delete[] propertyValues;
				propertyValues = 0;
			}
		}

		// animations
		if( strcmp( m_MapProperties[i].name, ANIMATION_FILE_PROP ) == 0 )
		{
			numValues = 0;
			propertyValues = 0;

			propertyValues = ParseValuesFromString( m_MapProperties[i].value, &numValues );
			if( propertyValues != 0 )
			{
				LoadAnimations( numValues, propertyValues );

				// done and clear
				delete[] propertyValues;
				propertyValues = 0;
			}
		}
	}

	// assign animations to tiles
	for( i=0; i < m_NumLayers; ++i )
	{
		for( y=0; y < m_Layers[i].height; y++ )
		{
			for( x=0; x < m_Layers[i].width; x++ )
			{
				// get the tile
				int tileId = m_Layers[i].data[x + (y*m_Layers[i].width)].tileId;
				Tileset* pTileset = GetTilesetForId( tileId );

				if( pTileset != 0 )
				{
					// because tilesets have an offset let's remove it to get a lookup id in a single set
					int realId = (tileId - pTileset->firstgid);

					// check for tile data info
					if( pTileset->numTileData )
					{
						for( j=0; j < pTileset->numTileData; ++j )
						{
							// this tile has data
							if( realId == pTileset->tileDataList[j].tileId )
							{
								// check the properties for the animation tag
								for( k=0; k < pTileset->tileDataList[j].numProperties; ++k )
								{
									if( strcmp( pTileset->tileDataList[j].propertyList[k].name, "animId" ) == 0 )
									{
										numValues = 0;
										propertyValues = 0;

										propertyValues = ParseValuesFromString( pTileset->tileDataList[j].propertyList[k].value, &numValues );
										DBG_ASSERT( numValues == 1 );

										int animationId = propertyValues[0].integerNumber;

										// assign the anim pointer
										m_Layers[i].data[x + (y*m_Layers[i].width)].anim = GetTileAnimation( animationId );

										// done and clear
										delete[] propertyValues;
										propertyValues = 0;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: ParseValuesFromString
/// Params: [in]numValues
///
/////////////////////////////////////////////////////
support::tiled::PropertyValue* TiledBaseLoader::ParseValuesFromString(const char* propertyString, int *numValues)
{
	DBG_ASSERT( numValues != 0 );
	DBG_ASSERT( propertyString != 0 );

	*numValues = 0;

	// expect comma separated values
	int i=0;
	int len = static_cast<int>( std::strlen( propertyString ) );

	bool isNumber = false;
	bool isString = false;

	// get the count of parameters first
	for( i=0; i <= len; ++i )
	{
		if( !isNumber && 
			!isString )
		{
			if( propertyString[i] >= '0' && propertyString[i] <= '9' )
			{
				isNumber = true;
				*numValues += 1;
			}
			else if( (propertyString[i] >= 'a' && propertyString[i] >= 'z') ||
				(propertyString[i] >= 'A' && propertyString[i] >= 'Z') ||
				(propertyString[i] == '.' || propertyString[i] == '/') )
			{
				isString = true;
				*numValues += 1;
			}
		}
		else
		{
			if( propertyString[i] == ' ' || 
				propertyString[i] == ',' ||
				propertyString[i] == '\0' )
			{
				isNumber = false;
				isString = false;
			}
		}
	}

	int count = *numValues;
	
	// now allocate the properties
	PropertyValue* pValues = new PropertyValue[count];
	int propertyIndex = -1;
	int stringIndex = 0;

	isNumber = false;
	isString = false;

	for( i=0; i <= len; ++i )
	{
		if( !isNumber && 
			!isString )
		{
			if( (propertyString[i] >= '0' && propertyString[i] <= '9') ||
				propertyString[i] == '-' )
			{
				isNumber = true;
				propertyIndex++;
			}
			else if( (propertyString[i] >= 'a' && propertyString[i] >= 'z') ||
				(propertyString[i] >= 'A' && propertyString[i] >= 'Z') ||
				( propertyString[i] == '.' || propertyString[i] == '-' || propertyString[i] == '/' ) )
			{
				isString = true;	
				propertyIndex++;
			}

			if( isNumber || 
				isString )
			{
				pValues[propertyIndex].integerNumber = 0;
				pValues[propertyIndex].floatNumber = 0.0f;
				std::memset( pValues[propertyIndex].text, 0, sizeof(char)*MAX_PROPERTY_STRING );

				stringIndex = 0;
			}
		}

		if( propertyString[i] == ' ' || 
			propertyString[i] == ',' ||
			propertyString[i] == '\0' )
		{
			// terminate a string value
			if( isNumber ||
				isString )
			{
				pValues[propertyIndex].text[stringIndex] = '\0';
								
				pValues[propertyIndex].floatNumber = static_cast<float>( std::atof(pValues[propertyIndex].text ) );
				pValues[propertyIndex].integerNumber = static_cast<int>( std::atoi(pValues[propertyIndex].text ) );
			}

			isNumber = false;
			isString = false;
		}
		else
		{
			pValues[propertyIndex].text[stringIndex] = propertyString[i];
			stringIndex++;
		}
	}

	return pValues;
}

/////////////////////////////////////////////////////
/// Method: CreateCollisionData
/// Params: [in]numValues, [in]layerValues
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateCollisionData( int numValues, support::tiled::PropertyValue* layerValues )
{
	int i=0, x=0, y=0;

	for( i=0; i < numValues; ++i )
	{
		int layerIndex = layerValues[i].integerNumber;

		if( layerIndex < 0 || 
			layerIndex > m_NumLayers-1 )
			continue;

		// normal tile layer collision
		if( strcmp( m_Layers[layerIndex].type, LAYER_TYPE_TILE ) == 0 )
		{
			for( y=0; y < m_Layers[layerIndex].height; y++ )
			{
				for( x=0; x < m_Layers[layerIndex].width; x++ )
				{
					int tileId = m_Layers[layerIndex].data[x + (y*m_Layers[layerIndex].width)].tileId;
					float rotation = 0.0f;
					Tileset* pTileset = GetTilesetForId( tileId );
					if( pTileset != 0 )
					{
						if( m_MapOrientation == MapOrientation_Orthogonal )
						{
							// so starting from top left in the map/layer
							float posX = static_cast<float>( x*(m_MapTileWidth*m_TileScale.x) );
							float posY = static_cast<float>( -(y*(m_MapTileHeight*m_TileScale.y)) - (m_MapTileHeight*m_TileScale.y) );

							// assume box collision
							CreateB2DBox(posX, posY, static_cast<float>(pTileset->tileWidth*m_TileScale.x), static_cast<float>(pTileset->tileHeight*m_TileScale.y), rotation);
							//CreateB2DCircle( posX, posY, static_cast<float>(pTileset->tileWidth*m_TileScale.Width)*0.5f );
							//CreateB2DLine( posX, posY, posX+static_cast<float>(pTileset->tileWidth*m_TileScale.Width), posY+static_cast<float>(pTileset->tileHeight*m_TileScale.Height) );
						}
						else
						if( m_MapOrientation == MapOrientation_Isometric )
						{
							// in isometric create the diamond polygon
							int j=0, k=0;
							glm::ivec2 center = m_Layers[layerIndex].data[x + (y*m_Layers[layerIndex].width)].tileCenter;

							b2PolygonShape polygonDef;
							b2FixtureDef fd;

							fd.friction = 0.0f;
							fd.restitution = 1.0f;
							fd.density = 0.0f;

							fd.filter.categoryBits = 0xFFFF;
							fd.filter.maskBits = 0xFFFF;
							fd.filter.groupIndex = -1;

							polygonDef.m_count = 4;

							// start on point
							polygonDef.m_vertices[0].x = (static_cast<float>(center.x))*m_Box2DScaleFactor;
							polygonDef.m_vertices[0].y = (static_cast<float>(center.y) - m_ScaledMapHalfTileSize.y)*m_Box2DScaleFactor;

							// right side point
							polygonDef.m_vertices[1].x = (static_cast<float>(center.x) + m_ScaledMapHalfTileSize.x)*m_Box2DScaleFactor;
							polygonDef.m_vertices[1].y = (static_cast<float>(center.y))*m_Box2DScaleFactor;

							// lower point
							polygonDef.m_vertices[2].x = (static_cast<float>(center.x))*m_Box2DScaleFactor;
							polygonDef.m_vertices[2].y = (static_cast<float>(center.y) + m_ScaledMapHalfTileSize.y)*m_Box2DScaleFactor;

							// left side point
							polygonDef.m_vertices[3].x = (static_cast<float>(center.x) - m_ScaledMapHalfTileSize.x)*m_Box2DScaleFactor;
							polygonDef.m_vertices[3].y = (static_cast<float>(center.y))*m_Box2DScaleFactor;

							int n = polygonDef.m_count;

							polygonDef.m_vertices[0].y = -polygonDef.m_vertices[0].y;
							polygonDef.m_vertices[1].y = -polygonDef.m_vertices[1].y;
							polygonDef.m_vertices[2].y = -polygonDef.m_vertices[2].y;
							polygonDef.m_vertices[3].y = -polygonDef.m_vertices[3].y;

							float area = 0.0f;
							for (j = 0; j < n; ++j)
							{
								k = (j + 1) % n;
								area += (polygonDef.m_vertices[k].x * polygonDef.m_vertices[j].y) - (polygonDef.m_vertices[j].x * polygonDef.m_vertices[k].y);
							}
							if (area > 0)
							{
								// reverse order of vertices, because winding is clockwise
								b2PolygonShape reverseShape;
								reverseShape.m_count = polygonDef.m_count;
								for (j = n - 1, k = 0; j > -1; --j, ++k)
								{
									reverseShape.m_vertices[j].Set(polygonDef.m_vertices[k].x, polygonDef.m_vertices[k].y);
								}
								polygonDef = reverseShape;
							}

							polygonDef.Set(polygonDef.m_vertices, polygonDef.m_count);
							fd.shape = &polygonDef;

							m_pBody->CreateFixture( &fd );
						}
						else
						if( m_MapOrientation == MapOrientation_StaggeredIsometric )
						{

						}
					}
				}
			}
		}
		else
		if( strcmp( m_Layers[layerIndex].type, LAYER_TYPE_OBJECT ) == 0 )
		{
			// object collisions
			CreateCollisionFromObjectLayer( layerIndex );
		}
	}
}

/////////////////////////////////////////////////////
/// Method: CreateCollisionFromObjectLayer
/// Params: [in]layerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateCollisionFromObjectLayer( int layerIndex )
{

	if( m_MapOrientation == MapOrientation_Orthogonal )
	{
		CreateCollisionFromObjectLayerOrthogonal( layerIndex );
	}
	else
	if( m_MapOrientation == MapOrientation_Isometric )
	{
		CreateCollisionFromObjectLayerIsometric( layerIndex );
	}
	else
	if( m_MapOrientation == MapOrientation_StaggeredIsometric )
	{

	}
}

/////////////////////////////////////////////////////
/// Method: CreateCollisionFromObjectLayerOrthogonal
/// Params: [in]layerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateCollisionFromObjectLayerOrthogonal( int layerIndex )
{
	int i=0, j=0, k=0;

	DBG_ASSERT( (layerIndex >= 0 && layerIndex < m_NumLayers) );

	for( i=0; i < m_Layers[layerIndex].numObjects; ++i )
	{
		if( m_Layers[layerIndex].objectList != 0 )
		{
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_RECTANGLE ) == 0 )
			{
				float posX = static_cast<float>( m_Layers[layerIndex].objectList[i].x ) * m_TileScale.x;
				float posY = static_cast<float>( m_Layers[layerIndex].objectList[i].y ) * m_TileScale.y;
				float w = static_cast<float>(m_Layers[layerIndex].objectList[i].width) * m_TileScale.x;
				float h = static_cast<float>(m_Layers[layerIndex].objectList[i].height) * m_TileScale.y;

				CreateB2DBox(posX, -(posY + h), w, h, m_Layers[layerIndex].objectList[i].rotation);
			}
			else
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_ELLIPSE ) == 0 )
			{
				float posX = static_cast<float>( m_Layers[layerIndex].objectList[i].x ) * m_TileScale.x;
				float posY = static_cast<float>( m_Layers[layerIndex].objectList[i].y ) * m_TileScale.y;

				float w = (m_Layers[layerIndex].objectList[i].width) * m_TileScale.x;
				float h = (m_Layers[layerIndex].objectList[i].height) * m_TileScale.y;

				float radius = (w > h) ? h : w; // if not a square scale on x/y choose the smaller value for the radius

				CreateB2DCircle( posX, -(posY+radius), static_cast<float>(radius*0.5f) );
			}
			else
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_POLYLINE ) == 0 )
			{
				float posX = static_cast<float>( m_Layers[layerIndex].objectList[i].x );
				float posY = static_cast<float>( m_Layers[layerIndex].objectList[i].y );

				for( j=0; j < m_Layers[layerIndex].objectList[i].numPoints-1; j++ )
				{
					float startX = posX + static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].x );
					float startY = posY + static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].y );

					float endX = posX + static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j+1].x );
					float endY = posY + static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j+1].y );

					CreateB2DLine( startX*m_TileScale.x, -(startY*m_TileScale.y), endX*m_TileScale.x, -(endY*m_TileScale.y) );
				}
			}
			else
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_POLYGON ) == 0 )
			{
				b2PolygonShape polygonDef;
				b2FixtureDef fd;

				fd.friction = 0.0f;
				fd.restitution = 1.0f;
				fd.density = 0.0f;

				fd.filter.categoryBits = 0xFFFF;
				fd.filter.maskBits = 0xFFFF;
				fd.filter.groupIndex = -1;

				polygonDef.m_count = m_Layers[layerIndex].objectList[i].numPoints;

				float posX = static_cast<float>( m_Layers[layerIndex].objectList[i].x );
				float posY = static_cast<float>( m_Layers[layerIndex].objectList[i].y );

				for( j=0; j < m_Layers[layerIndex].objectList[i].numPoints; ++j )
				{
					polygonDef.m_vertices[j].x = posX + static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].x );
					polygonDef.m_vertices[j].y = posY + static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].y );

					polygonDef.m_vertices[j].x *= m_TileScale.x;
					polygonDef.m_vertices[j].y *= m_TileScale.y;

					// scale
					polygonDef.m_vertices[j].x *= m_Box2DScaleFactor;
					polygonDef.m_vertices[j].y *= m_Box2DScaleFactor;

					polygonDef.m_vertices[j].y = -polygonDef.m_vertices[j].y;
				}

				float area = 0.0f;
				int n = polygonDef.m_count;
				for (j = 0; j < n; ++j)
				{
					k = (j + 1) % n;
					area += (polygonDef.m_vertices[k].x * polygonDef.m_vertices[j].y) - (polygonDef.m_vertices[j].x * polygonDef.m_vertices[k].y);
				}
				if (area > 0)
				{
					// reverse order of vertices, because winding is clockwise
					b2PolygonShape reverseShape;
					reverseShape.m_count = polygonDef.m_count;
					for (j = n - 1, k = 0; j > -1; --j, ++k)
					{
						reverseShape.m_vertices[j].Set(polygonDef.m_vertices[k].x, polygonDef.m_vertices[k].y);
					}
					polygonDef = reverseShape;
				}

				polygonDef.Set(polygonDef.m_vertices, polygonDef.m_count);
				fd.shape = &polygonDef;

				m_pBody->CreateFixture( &fd );
			}
		}
	}
}
	
/////////////////////////////////////////////////////
/// Method: CreateCollisionFromObjectLayerIsometric
/// Params: [in]layerIndex
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateCollisionFromObjectLayerIsometric( int layerIndex )
{
	int i=0, j=0, k=0;

	DBG_ASSERT( (layerIndex >= 0 && layerIndex < m_NumLayers) );

	for( i=0; i < m_Layers[layerIndex].numObjects; ++i )
	{
		if( m_Layers[layerIndex].objectList != 0 )
		{
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_RECTANGLE ) == 0 )
			{
				// rectangle is a polygon in isometric
				float x = static_cast<float>( m_Layers[layerIndex].objectList[i].x );
				float y = static_cast<float>( m_Layers[layerIndex].objectList[i].y );

				float w = static_cast<float>(m_Layers[layerIndex].objectList[i].width);
				float h = static_cast<float>(m_Layers[layerIndex].objectList[i].height);

				float divX = w / m_MapTileSize.y;
				float divY = h / m_MapTileSize.y;

				float startPosX = (x-y);
				float startPosY = (x*0.5f) + (y*0.5f);

				b2PolygonShape polygonDef;
				b2FixtureDef fd;

				fd.friction = 0.0f;
				fd.restitution = 1.0f;
				fd.density = 0.0f;

				fd.filter.categoryBits = 0xFFFF;
				fd.filter.maskBits = 0xFFFF;
				fd.filter.groupIndex = -1;

				polygonDef.m_count = 4;

				// start on point
				polygonDef.m_vertices[0].x = startPosX;
				polygonDef.m_vertices[0].y = startPosY;

				// right side point
				polygonDef.m_vertices[1].x = startPosX + (m_MapHalfTileSize.x * divX);
				polygonDef.m_vertices[1].y = startPosY + (m_MapHalfTileSize.y * divX);

				// left side point
				polygonDef.m_vertices[3].x = startPosX - (m_MapHalfTileSize.x * divY);
				polygonDef.m_vertices[3].y = startPosY + (m_MapHalfTileSize.y * divY);

				// lower point
				polygonDef.m_vertices[2].x = polygonDef.m_vertices[3].x + (polygonDef.m_vertices[1].x - polygonDef.m_vertices[0].x);
				polygonDef.m_vertices[2].y = polygonDef.m_vertices[3].y + (polygonDef.m_vertices[1].y - polygonDef.m_vertices[0].y);

				int n = polygonDef.m_count;
				for (j = 0; j < n; ++j)
				{
					polygonDef.m_vertices[j].x *= m_TileScale.x;
					polygonDef.m_vertices[j].y *= m_TileScale.y;

					polygonDef.m_vertices[j].x *= m_Box2DScaleFactor;
					polygonDef.m_vertices[j].y *= m_Box2DScaleFactor;
 				}

				polygonDef.m_vertices[0].y = -polygonDef.m_vertices[0].y;
				polygonDef.m_vertices[1].y = -polygonDef.m_vertices[1].y;
				polygonDef.m_vertices[2].y = -polygonDef.m_vertices[2].y;
				polygonDef.m_vertices[3].y = -polygonDef.m_vertices[3].y;


				float area = 0.0f;
				for (j = 0; j < n; ++j)
				{
					k = (j + 1) % n;
					area += (polygonDef.m_vertices[k].x * polygonDef.m_vertices[j].y) - (polygonDef.m_vertices[j].x * polygonDef.m_vertices[k].y);
				}
				if (area > 0)
				{
					// reverse order of vertices, because winding is clockwise
					b2PolygonShape reverseShape;
					reverseShape.m_count = polygonDef.m_count;
					for (j = n - 1, k = 0; j > -1; --j, ++k)
					{
						reverseShape.m_vertices[j].Set(polygonDef.m_vertices[k].x, polygonDef.m_vertices[k].y);
					}
					polygonDef = reverseShape;
				}

				polygonDef.Set(polygonDef.m_vertices, polygonDef.m_count);
				fd.shape = &polygonDef;

				m_pBody->CreateFixture( &fd );
			}
			else
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_ELLIPSE ) == 0 )
			{
				// not supported
			}
			else
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_POLYLINE ) == 0 )
			{
				float x = static_cast<float>( m_Layers[layerIndex].objectList[i].x );
				float y = static_cast<float>( m_Layers[layerIndex].objectList[i].y );

				float startPosX = x-y;
				float startPosY = (x*0.5f) + (y*0.5f);

				for( j=0; j < m_Layers[layerIndex].objectList[i].numPoints-1; j++ )
				{

					float point1X = static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].x );
					float point1Y = static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].y );

					float point2X = static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j+1].x );
					float point2Y = static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j+1].y );

					float startX = startPosX + (point1X-point1Y);
					float startY = startPosY + ((point1X*0.5f) + (point1Y*0.5f));

					float endX = startPosX + (point2X-point2Y);
					float endY = startPosY + ((point2X*0.5f) + (point2Y*0.5f));

					CreateB2DLine( startX*m_TileScale.x, -(startY*m_TileScale.y), endX*m_TileScale.x, -(endY*m_TileScale.y) );
				}
			}
			else
			if( strcmp( m_Layers[layerIndex].objectList[i].shape, OBJECTLAYER_TYPE_POLYGON ) == 0 )
			{
				b2PolygonShape polygonDef;
				b2FixtureDef fd;

				fd.friction = 0.0f;
				fd.restitution = 1.0f;
				fd.density = 0.0f;

				fd.filter.categoryBits = 0xFFFF;
				fd.filter.maskBits = 0xFFFF;
				fd.filter.groupIndex = -1;

				polygonDef.m_count = m_Layers[layerIndex].objectList[i].numPoints;

				float x = static_cast<float>( m_Layers[layerIndex].objectList[i].x );
				float y = static_cast<float>( m_Layers[layerIndex].objectList[i].y );

				float startPosX = x-y;
				float startPosY = (x*0.5f) + (y*0.5f);

				for( j=0; j < m_Layers[layerIndex].objectList[i].numPoints; ++j )
				{
					float pointX = static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].x );
					float pointY = static_cast<float>( m_Layers[layerIndex].objectList[i].polygonPoints[j].y );

					polygonDef.m_vertices[j].x = startPosX + (pointX-pointY);
					polygonDef.m_vertices[j].y = startPosY + ((pointX*0.5f) + (pointY*0.5f));

					polygonDef.m_vertices[j].x *= m_TileScale.x;
					polygonDef.m_vertices[j].y *= m_TileScale.y;

					polygonDef.m_vertices[j].x *= m_Box2DScaleFactor;
					polygonDef.m_vertices[j].y *= m_Box2DScaleFactor;

					polygonDef.m_vertices[j].y = -polygonDef.m_vertices[j].y;

				}

				float area = 0.0f;
				int n = polygonDef.m_count;
				for (j = 0; j < n; ++j)
				{
					k = (j + 1) % n;
					area += (polygonDef.m_vertices[k].x * polygonDef.m_vertices[j].y) - (polygonDef.m_vertices[j].x * polygonDef.m_vertices[k].y);
				}
				if (area > 0)
				{
					// reverse order of vertices, because winding is clockwise
					b2PolygonShape reverseShape;
					reverseShape.m_count = polygonDef.m_count;
					for (j = n - 1, k = 0; j > -1; --j, ++k)
					{
						reverseShape.m_vertices[j].Set(polygonDef.m_vertices[k].x, polygonDef.m_vertices[k].y);
					}
					polygonDef = reverseShape;
				}

				polygonDef.Set(polygonDef.m_vertices, polygonDef.m_count);
				fd.shape = &polygonDef;

				m_pBody->CreateFixture( &fd );
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: LoadAnimations
/// Params: [in]numValues, [in]layerValues
///
/////////////////////////////////////////////////////
void TiledBaseLoader::LoadAnimations(int numValues, support::tiled::PropertyValue* animationValues)
{
	if( numValues == 0 )
		return;

	int i=0;

	for( i=0; i < numValues; ++i )
	{
		if( script::LoadScript( animationValues[i].text ) == 0 )
		{
			// file was loaded and parsed, get the table of animations for this level
		#ifdef _DEBUG
			if( script::LuaFunctionCheck( "ParseAnimations" ) == 0 )
		#endif // _DEBUG
			{
				int result = 0;

				int errorFuncIndex;
				errorFuncIndex = script::GetErrorFuncIndex();

				// get the function
				lua_getglobal( script::LuaScripting::GetState(), "ParseAnimations" );

				result = lua_pcall( script::LuaScripting::GetState(), 0, 1, errorFuncIndex );

				// LUA_ERRRUN --- a runtime error. 
				// LUA_ERRMEM --- memory allocation error. For such errors, Lua does not call the error handler function. 
				// LUA_ERRERR --- error while running the error handler function. 

				if( result == LUA_ERRRUN || result == LUA_ERRMEM || result == LUA_ERRERR )
				{
					DBGLOG( "LEVEL: *ERROR* Calling function '%s' failed\n", "ParseAnimations" );
					DBGLOG( "\tLUA_TRACEBACK: %s\n", lua_tostring( script::LuaScripting::GetState(), -1 ) );
			
					DBG_ASSERT_MSG( 0, "LEVEL: *ERROR* Calling function '%s' failed", "ParseAnimations" );
					return;
				}

				// should be a table
				if( lua_istable( script::LuaScripting::GetState(), -1 ) )
				{
					int paramIndex = 1;

					glm::ivec2 tileSize;
					const char* textureFile = script::LuaGetStringFromTableItem( "textureFile", paramIndex );

					tileSize.x = static_cast<int>( script::LuaGetNumberFromTableItem( "tileSizeWidth", paramIndex ) );
					tileSize.y = static_cast<int>( script::LuaGetNumberFromTableItem( "tileSizeHeight", paramIndex ) );

					int spacing = static_cast<int>( script::LuaGetNumberFromTableItem( "spacing", paramIndex ) );
					int margin = static_cast<int>( script::LuaGetNumberFromTableItem( "margin", paramIndex ) );

					int animCount = 0;

					// grab table
					lua_pushstring( script::LuaScripting::GetState(), "animations" );
					lua_gettable( script::LuaScripting::GetState(), -2 ); // key

					// new table is top
					if( lua_istable( script::LuaScripting::GetState(), -1 ) )
					{
						int i=0;
						animCount = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );

						// go through all the tables in this table
						for( i = 1; i <= animCount; ++i )
						{
							lua_rawgeti( script::LuaScripting::GetState(), -1, i );
							if( lua_istable( script::LuaScripting::GetState(), -1 ) )
							{
								int index = 3;

								int animId = static_cast<int>( script::LuaGetNumberFromTableItem( "animId", index ) );
								float animSpeed = static_cast<float>( script::LuaGetNumberFromTableItem( "animationSpeed", index ) );
								int frameCount = 0;
								int frameList[MAX_ANIM_FRAMES];
								std::memset( frameList, -1, sizeof(int)*MAX_ANIM_FRAMES );

								// grab table
								lua_pushstring( script::LuaScripting::GetState(), "tileIds" );
								lua_gettable( script::LuaScripting::GetState(), -2 ); // key

								// new table is top
								if( lua_istable( script::LuaScripting::GetState(), -1 ) )
								{
									int j=0;
									frameCount = static_cast<int>( luaL_len(script::LuaScripting::GetState(), -1) );

									// go through all the tables in this table
									for( j = 1; j <= frameCount; ++j )
									{
										lua_rawgeti( script::LuaScripting::GetState(), -1, j );
										int tileId = static_cast<int>( lua_tonumber( script::LuaScripting::GetState(), -1 ) );
										lua_pop( script::LuaScripting::GetState(), 1 );

										frameList[j-1] = tileId;
									}
								}
								// pop tileId
								lua_pop( script::LuaScripting::GetState(), 1 );

								// now add/load the animations
								TilesetHandler* pTileHandler = GetTileset( textureFile, tileSize, spacing, margin );
								if( pTileHandler != 0 )
								{
									TilesetHandler::AnimationData* pAnim = pTileHandler->CreateAnimation( animId, frameList, frameCount, animSpeed, false );

									m_TileAnimations.push_back(pAnim);
								}
							}
							lua_pop( script::LuaScripting::GetState(), 1 );
						}
					}
					// pop table
					lua_pop( script::LuaScripting::GetState(), 1 );
				}
				// pop function
				lua_pop( script::LuaScripting::GetState(), 1 );
			}
		}
	}
}

/////////////////////////////////////////////////////
/// Method: CreateB2DLine
/// Params: [in]startX, [in]startY, [in]endX, [in]endY
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateB2DLine( float startX, float startY, float endX, float endY )
{
	b2EdgeShape edgeDef;
	b2FixtureDef fd;

	fd.friction = 0.0f;
	fd.restitution = 1.0f;
	fd.density = 0.0f;

	fd.filter.categoryBits = 0xFFFF;
	fd.filter.maskBits = 0xFFFF;
	fd.filter.groupIndex = -1;

	b2Vec2 start( startX*m_Box2DScaleFactor, startY*m_Box2DScaleFactor );
	b2Vec2 end( endX*m_Box2DScaleFactor, endY*m_Box2DScaleFactor );

	edgeDef.Set( start, end );

	fd.shape = &edgeDef;
	m_pBody->CreateFixture( &fd );
}

/////////////////////////////////////////////////////
/// Method: CreateB2DBox
/// Params: [in]x, [in]y, [in]w, [in]y
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateB2DBox( float x, float y, float w, float h, float angle )
{
	b2PolygonShape boxDef;
	b2FixtureDef fd;

	fd.friction = 1.0f;
	fd.restitution = 1.0f;
	fd.density = 0.0f;

	fd.filter.categoryBits = 0xFFFF;
	fd.filter.maskBits = 0xFFFF;
	fd.filter.groupIndex = -1;

	b2Vec2 center( (x+(w*0.5f))*m_Box2DScaleFactor, (y+(h*0.5f))*m_Box2DScaleFactor );
	boxDef.SetAsBox((w*0.5f)*m_Box2DScaleFactor, (h*0.5f)*m_Box2DScaleFactor, center, 0.0f);
	
	fd.shape = &boxDef;
	m_pBody->CreateFixture( &fd );
}

/////////////////////////////////////////////////////
/// Method: CreateB2DCircle
/// Params: [in]x, [in]y, [in]r
///
/////////////////////////////////////////////////////
void TiledBaseLoader::CreateB2DCircle( float x, float y, float r )
{
	b2CircleShape circleDef;
	b2FixtureDef fd;

	fd.friction = 0.0f;
	fd.restitution = 1.0f;
	fd.density = 0.0f;

	fd.filter.categoryBits = 0xFFFF;
	fd.filter.maskBits = 0xFFFF;
	fd.filter.groupIndex = -1;

	b2Vec2 center( (x+r)*m_Box2DScaleFactor, (y+r)*m_Box2DScaleFactor );
	circleDef.m_p = center;
	circleDef.m_radius = r*m_Box2DScaleFactor;

	fd.shape = &circleDef;
	m_pBody->CreateFixture( &fd );

}

/////////////////////////////////////////////////////
/// Method: GetTileset
/// Params: [in]imageName, [in]animationId
///
/////////////////////////////////////////////////////
support::tiled::TilesetHandler* TiledBaseLoader::GetTileset(const char* imageName, const glm::ivec2& tileSize, int spacing, int margin)
{
	if( imageName == 0 )
		return 0;

	auto it = m_AnimationFiles.begin();
	while( it != m_AnimationFiles.end() )
	{
		if( strcmp( (*it)->animationFile, imageName ) == 0 )
		{
			return (*it)->pTilesetHandler;
		}

		// next
		++it;
	}

	AnimationFiles* pNewAnimFile = 0;

	pNewAnimFile = new AnimationFiles;
	DBG_ASSERT( pNewAnimFile != 0 );

	pNewAnimFile->animationFile = imageName;
	pNewAnimFile->pTilesetHandler = new TilesetHandler;
	DBG_ASSERT( pNewAnimFile->pTilesetHandler != 0 );

	pNewAnimFile->pTilesetHandler->LoadAndParseTileset( imageName, tileSize, spacing, margin );

	m_AnimationFiles.push_back(pNewAnimFile);

	return pNewAnimFile->pTilesetHandler;
}

/////////////////////////////////////////////////////
/// Method: GetTileAnimation
/// Params: [in]animationId
///
/////////////////////////////////////////////////////
support::tiled::TilesetHandler::AnimationData* TiledBaseLoader::GetTileAnimation(int animationId)
{
	auto it = m_TileAnimations.begin();
	while( it != m_TileAnimations.end() )
	{
		if( (*it)->animationId == animationId )
		{
			return (*it);
		}

		// next
		++it;
	}

	return 0;
}

#endif // BASE_SUPPORT_BOX2D
