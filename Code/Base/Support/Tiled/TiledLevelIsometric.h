
/*===================================================================
	File: TiledLevel.h
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifndef __TILEDLEVELISOMETRIC_H__
#define __TILEDLEVELISOMETRIC_H__
	
#ifdef BASE_SUPPORT_BOX2D

#include "Support/Tiled/TiledIsometricMap.h"
#include "Support/Tiled/TilesetHandler.h"

namespace support
{
	namespace tiled
	{
		class TiledLevelIsometric : public TiledIsometricMap
		{
		public:
			TiledLevelIsometric();
			virtual ~TiledLevelIsometric();

			virtual void DrawLayers(int startLayer = -1, int endLayer = -1);

		private:

		};
	}
}

#endif // BASE_SUPPORT_BOX2D
#endif // __TILEDLEVELISOMETRIC_H__

