
/*===================================================================
	File: TilesetHandler.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#ifdef BASE_SUPPORT_BOX2D

#include "CoreBase.h"
#include "RenderBase.h"

#include "Support/Tiled/TilesetHandler.h"

using support::tiled::TilesetHandler;

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
TilesetHandler::TilesetHandler()
{
	m_IsParsed = false;

	m_TextureId = renderer::INVALID_OBJECT;

	m_TotalNumTiles = 0;

	m_TotalTilesX = 0;
	m_TotalTilesY = 0;

	m_UVWidthHeight = glm::vec2(0.0f,0.0f);

	m_Spacing = 0;
	m_Margin = 0;

	m_TileSize.x = 0;
	m_TileSize.y = 0;

	m_ImageSize.x = 0;
	m_ImageSize.y = 0;

	m_Animations.clear(); 
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
TilesetHandler::~TilesetHandler()
{
	Release();
}

/////////////////////////////////////////////////////
/// Method: Release
/// Params: None
///
/////////////////////////////////////////////////////
void TilesetHandler::Release()
{
	int i=0;

	if( m_TextureId != renderer::INVALID_OBJECT )
	{
		renderer::RemoveTexture( m_TextureId );
		m_TextureId = renderer::INVALID_OBJECT;
	}

	for( i=0; i < static_cast<int>(m_Animations.size()); ++i )
	{
		if( m_Animations[i]->pTileFrames != 0 )
		{
			//(*it)->pTilesetHandler->Release();

			delete m_Animations[i]->pTileFrames;
			m_Animations[i]->pTileFrames = 0;
		}

		delete m_Animations[i];
	}

	m_Animations.clear();
}

/////////////////////////////////////////////////////
/// Method: LoadAndParseTileset
/// Params: [in]szFilename, [in]tileSize, [in]spacing, [in]margin
///
/////////////////////////////////////////////////////
int TilesetHandler::LoadAndParseTileset( const char* szFilename, const glm::ivec2& tileSize, int spacing, int margin )
{
	renderer::Texture texLoader;

	if( file::FileExists(szFilename) )
	{
		m_TextureId = renderer::TextureLoad( szFilename, texLoader, renderer::GetTextureFormat(szFilename), 0, false, GL_NEAREST, GL_NEAREST );

		int spacedTileWidth = tileSize.x + margin + spacing;
		int spacedTileHeight = tileSize.y + margin + spacing;

		m_TileSize = tileSize;
		m_Spacing = spacing;
		m_Margin = margin;

		m_TotalTilesX = (texLoader.nOriginalWidth / spacedTileWidth);
		m_TotalTilesY = (texLoader.nOriginalHeight / spacedTileHeight);

		m_TotalNumTiles = m_TotalTilesX*m_TotalTilesY;

		m_ImageSize.x = texLoader.nWidth;
		m_ImageSize.y = texLoader.nHeight;

		m_UVWidthHeight.x = static_cast<float>( tileSize.x ) / static_cast<float>( texLoader.nWidth );
		m_UVWidthHeight.y = static_cast<float>( tileSize.y ) / static_cast<float>( texLoader.nHeight );

		m_IsParsed = true;

		return 0;
	}

	return 1;
}

/////////////////////////////////////////////////////
/// Method: GetUVForTile
/// Params: [in]bottomLeft, [in]tileIndex
///
/////////////////////////////////////////////////////
void TilesetHandler::GetUVForTile( glm::vec2* bottomLeft, int tileIndex )
{
	DBG_ASSERT( bottomLeft != 0 );

	// get the id as an x/y index into the tile grid
	int offsetY = tileIndex / m_TotalTilesX;
	int offsetX = m_TotalTilesX - ((m_TotalTilesX * (offsetY+1)) - tileIndex);

	// get the pixel (from top left)
	int pixelStartX = (m_TileSize.x * offsetX) + (m_Spacing * (offsetX+1));
	int pixelStartY = (m_TileSize.y * offsetY) + (m_Spacing * (offsetY+1));

	bottomLeft->s = renderer::GetUVCoord( pixelStartX, m_ImageSize.x );
	bottomLeft->t = renderer::GetUVCoord( ( m_ImageSize.y - pixelStartY) - m_TileSize.y, m_ImageSize.y ); // bottom left for OGL
}

/////////////////////////////////////////////////////
/// Method: CreateAnimation
/// Params: [in]animationId, [in]tileIds, [in]numFrames, [in]animSpeed, [in]playOnce
///
/////////////////////////////////////////////////////
TilesetHandler::AnimationData* TilesetHandler::CreateAnimation( int animationId, int* tileIds, int numFrames, float animSpeed, bool playOnce )
{
	DBG_ASSERT( tileIds != 0 );

	int i=0;

	glm::vec2 scale( core::app::GetWidthScale(), core::app::GetHeightScale() );

	if( m_IsParsed &&
		m_TextureId != renderer::INVALID_OBJECT )
	{
		AnimationData* pNewAnim = AnimationAlreadyExists( animationId );

		// already exists
		if( pNewAnim != 0 )
			return pNewAnim;

		pNewAnim = new AnimationData;
		DBG_ASSERT( pNewAnim != 0 );

		pNewAnim->textureId = m_TextureId;

		pNewAnim->animationId = animationId;
		pNewAnim->numFrames = numFrames;
		pNewAnim->pTileFrames = 0;
		pNewAnim->pTileFrames = new AnimFrame[numFrames];

		pNewAnim->uvWidthHeight = m_UVWidthHeight;
		pNewAnim->isPlaying = false;

		DBG_ASSERT( pNewAnim->pTileFrames != nullptr );

		for( i=0; i < numFrames; ++i )
		{
			pNewAnim->pTileFrames[i].tileId = tileIds[i];
			pNewAnim->pTileFrames[i].tileShowTime = animSpeed;
			pNewAnim->pTileFrames[i].tileSize = m_TileSize;

			pNewAnim->pTileFrames[i].scaledTileSize.x = static_cast<float>(pNewAnim->pTileFrames[i].tileSize.x)*scale.x;
			pNewAnim->pTileFrames[i].scaledTileSize.y = static_cast<float>(pNewAnim->pTileFrames[i].tileSize.y)*scale.y;

			pNewAnim->pTileFrames[i].scaledHalfTileSize.x = pNewAnim->pTileFrames[i].scaledTileSize.x*0.5f;
			pNewAnim->pTileFrames[i].scaledHalfTileSize.y = pNewAnim->pTileFrames[i].scaledTileSize.y*0.5f;

			GetUVForTile( &pNewAnim->pTileFrames[i].uvCoord, tileIds[i] );
		}

		pNewAnim->playOnce = playOnce;

		pNewAnim->currentFrame = 0;
		pNewAnim->frameTime = pNewAnim->pTileFrames[pNewAnim->currentFrame].tileShowTime;
		pNewAnim->frameUV = pNewAnim->pTileFrames[pNewAnim->currentFrame].uvCoord;

		m_Animations.push_back(pNewAnim);

		return pNewAnim;
	}

	return 0;
}

/////////////////////////////////////////////////////
/// Method: AnimationExist
/// Params: [in]animationId, [in]tileIds, [in]numFrames, [in]animSpeed, [in]playOnce
///
/////////////////////////////////////////////////////
TilesetHandler::AnimationData* TilesetHandler::AnimationAlreadyExists( int animationId )
{
	auto it = m_Animations.begin();
	while( it != m_Animations.end() )
	{
		if( (*it)->animationId == animationId )
		{
			return *it;
		}

		// next
		++it;
	}

	return 0;
}

#endif // BASE_SUPPORT_BOX2D
