
/*===================================================================
	File: Camera.cpp
	Game: HTemplate

	(C)Hidden Games
=====================================================================*/

#include "CoreBase.h"

#include "Camera.h"

/////////////////////////////////////////////////////
/// Default Constructor
/// 
///
/////////////////////////////////////////////////////
Camera::Camera()
{
	PositionCamera( 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f );
	m_Target.z = m_Pos.z+1.0f;
}

/////////////////////////////////////////////////////
/// Default Destructor
/// 
///
/////////////////////////////////////////////////////
Camera::~Camera()
{

}

/////////////////////////////////////////////////////
/// Method: PositionCamera
/// 
///
/////////////////////////////////////////////////////
void Camera::PositionCamera( float posX, float posY, float posZ, float targetX, float targetY, float targetZ, float upX, float upY, float upZ )
{
	m_Pos		= glm::vec3(posX,  posY,  posZ);
	m_Target = glm::vec3(targetX, targetY, targetZ);
	m_Up = glm::vec3(upX, upY, upZ);
	
	m_Angle = 0.0f;
}

/////////////////////////////////////////////////////
/// Method: MoveCamera
/// 
///
/////////////////////////////////////////////////////
void Camera::MoveCamera(float cameraSpeed)
{
	glm::vec3 vVector = glm::vec3(0.0f, 0.0f, 0.0f);		// init a new view vector

	// Get our view vector (The direciton we are facing)
	vVector.x = m_Target.x - m_Pos.x;		// This gets the direction of the X	
	vVector.y = m_Target.y - m_Pos.y;		// This gets the direction of the Y
	vVector.z = m_Target.z - m_Pos.z;		// This gets the direction of the Z

	m_Pos.x += vVector.x * cameraSpeed;		// Add our acceleration to our position's X
	m_Pos.y += vVector.y * cameraSpeed;
	m_Pos.z += vVector.z * cameraSpeed;		// Add our acceleration to our position's Z
	m_Target.x += vVector.x * cameraSpeed;	// Add our acceleration to our view's X
	m_Target.y += vVector.y * cameraSpeed;
	m_Target.z += vVector.z * cameraSpeed;	// Add our acceleration to our view's Z
}

/////////////////////////////////////////////////////
/// Method: RotateView
/// 
///
/////////////////////////////////////////////////////
void Camera::RotateView(float x, float y, float z)
{
	glm::vec3 vVector = m_Target - m_Pos;
	float sinVal;
	float cosVal;

	if(x != 0.0f) 
	{
		sinVal = glm::sin(x);
		cosVal = glm::cos(x);

		m_Target.y = (float)(m_Pos.y + cosVal*vVector.y - sinVal*vVector.z);
		m_Target.z = (float)(m_Pos.z + sinVal*vVector.y + cosVal*vVector.z);
	}
	if(y != 0.0f) 
	{
		sinVal = glm::sin(y);
		cosVal = glm::cos(y);

		m_Target.x = (float)(m_Pos.x + cosVal*vVector.x + sinVal*vVector.z);
		m_Target.z = (float)(m_Pos.z - sinVal*vVector.x + cosVal*vVector.z);
	}
	if(z != 0.0f) 
	{
		sinVal = glm::sin(z);
		cosVal = glm::cos(z);

		m_Target.x = (float)(m_Pos.x + cosVal*vVector.x - sinVal*vVector.y);		
		m_Target.y = (float)(m_Pos.y + sinVal*vVector.x + cosVal*vVector.y);
	}
}

/////////////////////////////////////////////////////
/// Method: StrafeCamera
/// 
///
/////////////////////////////////////////////////////
void Camera::StrafeCamera(float cameraSpeed)
{
	// Initialize a variable for the cross product result
	glm::vec3 vCross = glm::vec3(0.0f, 0.0f, 0.0f);

	// Get the view vector of our camera and store it in a local variable
	glm::vec3 vViewVector = glm::vec3(m_Target.x - m_Pos.x, m_Target.y - m_Pos.y, m_Target.z - m_Pos.z);

	// Here we calculate the cross product of our up vector and view vector
	vCross = glm::cross( m_Up, vViewVector );

	// Now we want to just add this new vector to our position and view, as well as
	// multiply it by our speed factor.  If the speed is negative it will strafe the
	// opposite way.

	// Add the resultant vector to our position
	m_Pos.x += vCross.x * cameraSpeed;
	m_Pos.z += vCross.z * cameraSpeed;

	// Add the resultant vector to our view
	m_Target.x += vCross.x * cameraSpeed;
	m_Target.z += vCross.z * cameraSpeed;
}

